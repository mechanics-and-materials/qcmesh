// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Triangulate a set of points using CGAL.
 *
 * @author Prateek Gupta
 */

#pragma once
#include <array>
#include <vector>

namespace qcmesh::triangulation {

template <std::size_t Dimension>
using SimplexIndices = std::array<std::size_t, Dimension + 1>;

/**
 * @brief Returns a list of simplices given as a tuple of Dimension+1 indices
 * of elements of points, covering points.
 *
 * @param tolerance All simplices with volume < tolerance will be removed from
 * the mesh.
 */
std::vector<SimplexIndices<3>>
triangulate(const std::vector<std::array<double, 3>> &points);
std::vector<SimplexIndices<2>>
triangulate(const std::vector<std::array<double, 2>> &points);

/**
 * @brief Remove all zero volume cells at the boundary of the mesh, such that no
 * node is disconnected from the mesh.
 *
 * Warning: Might change the mesh topology.
 * Example: A 3 cell mesh consisting of 2 tetrahedra with no common face but
 * linked to one flat cell each. In this case the flat cell would be removed and
 * the 3 cell mesh would decompose into 2 disconnected components (single
 * cells).
 * However this does not happen if the original mesh has a box as a boundary.
 */
void clean_0_vol_cells_at_boundary(
    std::vector<SimplexIndices<3>> &cells,
    const std::vector<std::array<double, 3>> &nodes, const double tolerance);

/**
 * @brief Converts an array of indices indexing a vector of points into the
 * corresponding points.
 */
std::array<std::array<double, 2>, 3>
to_simplex(const std::vector<std::array<double, 2>> &points,
           const std::array<std::size_t, 3> &simplex_indices);
std::array<std::array<double, 3>, 4>
to_simplex(const std::vector<std::array<double, 3>> &points,
           const std::array<std::size_t, 4> &simplex_indices);

} // namespace qcmesh::triangulation
