// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author Prateek Gupta
 */

#include "qcmesh/triangulation/triangulation.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/geometry/triple_hash.hpp"
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <algorithm>

namespace qcmesh::triangulation {
namespace {

using CgalKernel = CGAL::Exact_predicates_inexact_constructions_kernel;

CgalKernel::Point_2 point_to_cgal(const std::array<double, 2> &mp) {
  return {mp[0], mp[1]};
}
CgalKernel::Point_3 point_to_cgal(const std::array<double, 3> &mp) {
  return {mp[0], mp[1], mp[2]};
}

template <std::size_t Dimension> struct CgalTypes;
template <> struct CgalTypes<2> {
  using PointType = CgalKernel::Point_2;
  using VertexBase =
      CGAL::Triangulation_vertex_base_with_info_2<int, CgalKernel>;
  using FaceBase = CGAL::Triangulation_face_base_with_info_2<int, CgalKernel>;
  using DataStructure =
      CGAL::Triangulation_data_structure_2<VertexBase, FaceBase>;
  using TriangulationType =
      CGAL::Delaunay_triangulation_2<CgalKernel, DataStructure>;
};
template <> struct CgalTypes<3> {
  using PointType = CgalKernel::Point_3;
  using VertexBase =
      CGAL::Triangulation_vertex_base_with_info_3<int, CgalKernel>;
  using CellBase = CGAL::Triangulation_cell_base_with_info_3<int, CgalKernel>;
  using DataStructure =
      CGAL::Triangulation_data_structure_3<VertexBase, CellBase>;
  using TriangulationType =
      CGAL::Delaunay_triangulation_3<CgalKernel, DataStructure>;
};

template <std::size_t Dimension>
auto points_to_cgal(const std::vector<std::array<double, Dimension>> &atoms) {
  using CgalPoint = typename CgalTypes<Dimension>::PointType;
  std::vector<CgalPoint> locations;
  locations.reserve(atoms.size());

  for (const auto &atom : atoms)
    locations.emplace_back(point_to_cgal(atom));
  return locations;
}

auto cells_begin(const CgalTypes<2>::TriangulationType &triangulation) {
  return triangulation.finite_faces_begin();
}
auto cells_begin(const CgalTypes<3>::TriangulationType &triangulation) {
  return triangulation.finite_cells_begin();
}
auto cells_end(const CgalTypes<2>::TriangulationType &triangulation) {
  return triangulation.finite_faces_end();
}
auto cells_end(const CgalTypes<3>::TriangulationType &triangulation) {
  return triangulation.finite_cells_end();
}
auto cells_size(const CgalTypes<2>::TriangulationType &triangulation) {
  return triangulation.number_of_faces();
}
auto cells_size(const CgalTypes<3>::TriangulationType &triangulation) {
  return triangulation.number_of_cells();
}

template <std::size_t Dimension>
std::vector<SimplexIndices<Dimension>> cgal_triangulation_to_simplices(
    const std::vector<std::array<double, Dimension>> &meshpoints,
    const typename CgalTypes<Dimension>::TriangulationType &triangulation) {

  auto simplices = std::vector<SimplexIndices<Dimension>>{};
  simplices.reserve(cells_size(triangulation));
  for (auto cgal_cell = cells_begin(triangulation);
       cgal_cell != cells_end(triangulation); ++cgal_cell) {
    auto simplex = SimplexIndices<Dimension>{};

    auto simplex_coordinates =
        std::array<std::array<double, Dimension>, Dimension + 1>{};
    for (std::size_t nd = 0; nd < Dimension + 1; nd++) {
      for (std::size_t d = 0; d < Dimension; d++)
        simplex_coordinates[nd][d] =
            meshpoints[cgal_cell->vertex(nd)->info()][d];
      simplex[nd] = cgal_cell->vertex(nd)->info();
    }
    const auto volume = geometry::simplex_volume(simplex_coordinates);

    if (volume < 0.0)
      std::swap(simplex[1], simplex[2]);
    simplices.emplace_back(simplex);
  }
  return simplices;
}

namespace implementation {
template <std::size_t Dimension>
std::array<std::array<double, Dimension>, Dimension + 1>
to_simplex(const std::vector<std::array<double, Dimension>> &points,
           const std::array<std::size_t, Dimension + 1> &simplex_indices) {
  auto simplex = std::array<std::array<double, Dimension>, Dimension + 1>{};
  for (std::size_t i = 0; i < Dimension + 1; i++)
    simplex[i] = points[simplex_indices[i]];
  return simplex;
}
} // namespace implementation

namespace implementation {

template <class PointType>
auto to_cgal_pairs(const std::vector<PointType> &coordinates) {
  std::vector<std::pair<PointType, std::size_t>> cgal_pairs{};
  cgal_pairs.reserve(coordinates.size());
  std::size_t idx = 0;
  for (const auto &p : coordinates)
    cgal_pairs.emplace_back(std::make_pair(p, idx++));
  return cgal_pairs;
}

template <std::size_t Dimension>
auto triangulate_cgal_pairs(
    const std::vector<std::pair<typename CgalTypes<Dimension>::PointType,
                                std::size_t>> &cgal_pairs) {
  auto triangulation = typename CgalTypes<Dimension>::TriangulationType{};
  triangulation.insert(cgal_pairs.begin(), cgal_pairs.end());
  return triangulation;
}

template <std::size_t Dimension>
std::vector<SimplexIndices<Dimension>>
triangulate(const std::vector<std::array<double, Dimension>> &points) {
  // generate CGAL triangulation over lattice points.
  const auto repatoms_locations = points_to_cgal<Dimension>(points);
  const auto triangulation =
      triangulate_cgal_pairs<Dimension>(to_cgal_pairs(repatoms_locations));

  auto simplex_indices = cgal_triangulation_to_simplices(points, triangulation);
  return simplex_indices;
}
} // namespace implementation
} // namespace

std::vector<SimplexIndices<3>>
triangulate(const std::vector<std::array<double, 3>> &points) {
  return implementation::triangulate(points);
}
std::vector<SimplexIndices<2>>
triangulate(const std::vector<std::array<double, 2>> &points) {
  return implementation::triangulate(points);
}

std::array<std::array<double, 2>, 3>
to_simplex(const std::vector<std::array<double, 2>> &points,
           const std::array<std::size_t, 3> &simplex_indices) {
  return implementation::to_simplex(points, simplex_indices);
}
std::array<std::array<double, 3>, 4>
to_simplex(const std::vector<std::array<double, 3>> &points,
           const std::array<std::size_t, 4> &simplex_indices) {
  return implementation::to_simplex(points, simplex_indices);
}

namespace {

template <class T, std::size_t N>
std::array<T, N> ordered_array(std::array<T, N> arr) {
  std::sort(arr.begin(), arr.end());
  return arr;
}

template <class T, std::size_t N>
std::array<T, N - 1> all_but(const std::array<T, N> &arr, std::size_t index) {
  auto out = std::array<T, N - 1>{};
  auto j = std::size_t{0};
  for (std::size_t i = 0; i < N; i++)
    if (i != index)
      out[j++] = arr[i];
  return out;
}

std::size_t n_boundary_faces(const std::array<std::size_t, 4> &face_counts) {
  auto n = std::size_t{0};
  // Potentially there are stacked flat cells.
  // This can result in faces which are shared by more than 2 cells.
  // A boundary face is therefore defined as beeing shared by an
  // odd number of cells.
  for (const auto count : face_counts)
    if (count % 2 == 1)
      ++n;
  return n;
}

std::vector<std::size_t>
zero_vol_cells_indices(std::vector<SimplexIndices<3>> &cells,
                       const std::vector<std::array<double, 3>> &nodes,
                       const double tolerance) {
  auto zero_vol_cells = std::vector<std::size_t>{};
  auto cell_idx = std::size_t{0};
  for (const auto &cell : cells) {
    if (std::abs(geometry::simplex_volume(to_simplex(nodes, cell))) < tolerance)
      zero_vol_cells.push_back(cell_idx);
    cell_idx++;
  }
  return zero_vol_cells;
}

bool disconnects_node(const SimplexIndices<3> &cell,
                      const std::vector<std::size_t> &node_counts) {
  for (const auto node : cell)
    if (node_counts[node] == 1)
      return true;
  return false;
}

std::unordered_map<std::array<std::size_t, 3>, std::size_t,
                   geometry::TripleHash>
face_share_counts(std::vector<SimplexIndices<3>> &cells) {
  auto face_counts = std::unordered_map<std::array<std::size_t, 3>, std::size_t,
                                        geometry::TripleHash>{};
  for (const auto &cell : cells) {
    const auto ordered_cell = ordered_array(cell);
    for (std::size_t apex = 0; apex < ordered_cell.size(); apex++)
      ++face_counts[all_but(ordered_cell, apex)];
  }
  return face_counts;
}

std::vector<std::size_t>
node_share_counts(std::vector<SimplexIndices<3>> &cells, std::size_t n_nodes) {
  auto node_counts = std::vector<std::size_t>(n_nodes);
  for (const auto &cell : cells) {
    for (const auto node : cell)
      ++node_counts[node];
  }
  return node_counts;
}

} // namespace

void clean_0_vol_cells_at_boundary(
    std::vector<SimplexIndices<3>> &cells,
    const std::vector<std::array<double, 3>> &nodes, const double tolerance) {
  // face share counts, to determine topological boundary:
  auto face_counts = face_share_counts(cells);
  // node share counts, to prevent isolation of nodes:
  auto node_counts = node_share_counts(cells, nodes.size());
  auto zero_vol_cells = zero_vol_cells_indices(cells, nodes, tolerance);
  auto remove_zero_vol_cell = std::vector<bool>(zero_vol_cells.size(), false);
  // Removal in multiple iterations.
  // If a cell is removed, recheck the remaining 0 vol cells if they now are at
  // the boundary:
  auto found_boundary_cell = true;
  while (found_boundary_cell) {
    found_boundary_cell = false;
    for (std::size_t i = 0; i < zero_vol_cells.size(); ++i) {
      if (remove_zero_vol_cell[i])
        continue;
      const auto ordered_cell = ordered_array(cells[zero_vol_cells[i]]);
      const auto cell_face_counts =
          std::array{std::ref(face_counts[all_but(ordered_cell, 0)]),
                     std::ref(face_counts[all_but(ordered_cell, 1)]),
                     std::ref(face_counts[all_but(ordered_cell, 2)]),
                     std::ref(face_counts[all_but(ordered_cell, 3)])};
      const auto is_at_boundary =
          n_boundary_faces({cell_face_counts[0], cell_face_counts[1],
                            cell_face_counts[2], cell_face_counts[3]}) > 0;
      if (is_at_boundary && !disconnects_node(ordered_cell, node_counts)) {
        for (const auto &count : cell_face_counts)
          --count;
        for (const auto node : ordered_cell)
          --node_counts[node];
        remove_zero_vol_cell[i] = true;
        found_boundary_cell = true;
      }
    }
  }
  for (std::size_t i = 0; i < zero_vol_cells.size(); i++)
    if (remove_zero_vol_cell[zero_vol_cells.size() - i - 1]) {
      const auto index = zero_vol_cells[zero_vol_cells.size() - i - 1];
      cells[index] = cells.back();
      cells.pop_back();
    }
}

} // namespace qcmesh::triangulation
