// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/mesh_repair/testing/boundary.hpp"
#include "qcmesh/mesh/mesh_repair/testing/intersection_detection.hpp"
#include "qcmesh/triangulation/triangulation.hpp"
#include <algorithm>
#include <cmath>
#include <gmock/gmock.h>
#include <set>

namespace qcmesh::triangulation {
namespace {

using testing::IsEmpty;
using testing::SizeIs;

TEST(test_triangulation,
     clean_0_vol_cells_at_boundary_does_not_remove_single_cell) {
  const auto nodes =
      std::vector{std::array{0., 0., 0.}, std::array{1., 0., 0.},
                  std::array{0., 1., 0.}, std::array{1., 1., 0.}};
  const auto original_cells = std::vector{SimplexIndices<3>{0, 1, 2, 3}};
  auto cells = original_cells;
  clean_0_vol_cells_at_boundary(cells, nodes, 0.125);
  EXPECT_THAT(cells, testing::ContainerEq(original_cells));
}
TEST(test_triangulation,
     clean_0_vol_cells_at_boundary_does_not_remove_2_flat_cells) {
  const auto nodes =
      std::vector{std::array{0., 0., 0.}, std::array{1., 0., 0.},
                  std::array{0., 1., 0.}, std::array{1., 1., 0.}};
  const auto original_cells =
      std::vector{SimplexIndices<3>{0, 1, 2, 3}, SimplexIndices<3>{0, 1, 2, 3}};
  auto cells = original_cells;
  clean_0_vol_cells_at_boundary(cells, nodes, 0.125);
  EXPECT_THAT(cells, testing::ContainerEq(original_cells));
}
TEST(test_triangulation,
     clean_0_vol_cells_at_boundary_does_not_remove_interior_cell) {
  const auto nodes = std::vector{
      std::array{0., 0., 0.}, std::array{1., 0., 0.}, std::array{0., 1., 0.},
      std::array{1., 1., 0.}, std::array{0., 0., 1.}, std::array{0., 0., -1.}};
  // 2 3
  // 0 1
  const auto original_cells =
      std::vector{SimplexIndices<3>{0, 1, 2, 3}, // <- flat
                                                 // top:
                  SimplexIndices<3>{0, 1, 2, 4}, SimplexIndices<3>{1, 3, 2, 4},
                  // bottom:
                  SimplexIndices<3>{0, 2, 3, 5}, SimplexIndices<3>{0, 3, 1, 5}};
  auto cells = original_cells;
  clean_0_vol_cells_at_boundary(cells, nodes, 0.125);
  EXPECT_THAT(cells, testing::ContainerEq(original_cells));
}
TEST(test_triangulation,
     clean_0_vol_cells_at_boundary_does_not_disconnect_node) {
  const auto nodes = std::vector{std::array{0., 0., 0.}, std::array{1., 0., 0.},
                                 std::array{0., 1., 0.}, std::array{1., 1., 0.},
                                 std::array{0., 0., 1.}};
  const auto original_cells =
      std::vector{SimplexIndices<3>{0, 1, 2, 3}, // <- flat
                  SimplexIndices<3>{0, 1, 2, 4}};
  auto cells = original_cells;
  clean_0_vol_cells_at_boundary(cells, nodes, 0.125);
  EXPECT_THAT(cells, testing::ContainerEq(original_cells));
}
TEST(test_triangulation,
     clean_0_vol_cells_at_boundary_removes_cell_at_boundary) {
  const auto nodes = std::vector{std::array{0., 0., 0.}, std::array{1., 0., 0.},
                                 std::array{0., 1., 0.}, std::array{1., 1., 0.},
                                 std::array{0., 0., 1.}};
  // 2 3
  // 0 1
  const auto original_cells = std::vector{
      SimplexIndices<3>{0, 1, 2, 3}, // <- flat
      SimplexIndices<3>{0, 1, 2, 4},
      SimplexIndices<3>{1, 3, 2, 4},
  };
  auto cells = original_cells;
  clean_0_vol_cells_at_boundary(cells, nodes, 0.125);
  const auto expected_cells = std::vector{
      SimplexIndices<3>{0, 1, 2, 4},
      SimplexIndices<3>{1, 3, 2, 4},
  };
  EXPECT_THAT(cells, testing::UnorderedElementsAreArray(expected_cells));
}

struct TestTriangulation3d
    : ::testing::TestWithParam<std::vector<std::array<double, 3>>> {};

TEST(test_triangulation_3d, yields_empty_mesh_for_0_points) {
  const auto simplices = triangulate(std::vector<std::array<double, 3>>{});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_3d, yields_empty_mesh_for_1_point) {
  const auto simplices =
      triangulate(std::vector<std::array<double, 3>>{{1., 0., 0.}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_3d, yields_empty_mesh_for_2_points) {
  const auto simplices = triangulate(
      std::vector<std::array<double, 3>>{{1., 0., 0.}, {0., 1., 0.}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_3d, yields_empty_mesh_for_3_points) {
  const auto simplices =
      triangulate({{1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_3d, yields_simplex_for_4_points) {
  const auto points = std::vector<std::array<double, 3>>{
      {0., 0., 0.}, {1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}};
  const auto simplices = triangulate(points);
  const auto expected_indices = std::array<std::size_t, 4>{0, 1, 2, 3};
  EXPECT_THAT(simplices, SizeIs(1));
  EXPECT_THAT(simplices[0],
              testing::UnorderedElementsAreArray(expected_indices));
}

template <std::size_t Dimension>
std::vector<std::array<std::array<double, Dimension>, Dimension + 1>>
to_simplices(const std::vector<std::array<double, Dimension>> &points,
             const std::vector<SimplexIndices<Dimension>> &simplices) {
  auto raw_simplices =
      std::vector<std::array<std::array<double, Dimension>, Dimension + 1>>{};
  for (const auto &simplex_indices : simplices)
    raw_simplices.emplace_back(
        triangulation::to_simplex(points, simplex_indices));
  return raw_simplices;
}

template <std::size_t Dimension>
double mesh_volume(
    const std::vector<std::array<std::array<double, Dimension>, Dimension + 1>>
        &simplices) {
  auto volume = double{0.};
  for (const auto &simplex : simplices)
    volume += geometry::simplex_volume(simplex);
  return volume;
}

/**
 *       z
 *
 *       18--21--24
 *       /   /   /|
 *     19--22--25 15
 *     /   /   /|/|
 *   20--23--26 16 6  y
 *    |   |   |/|/
 *   11--14--17 7
 *    |   |   |/
 *    2---5---8
 *  x
 */
std::vector<std::array<double, 3>> lattice_3x3x3() {
  auto points = std::vector<std::array<double, 3>>{};
  for (std::size_t i = 0; i < 3; i++)
    for (std::size_t j = 0; j < 3; j++)
      for (std::size_t k = 0; k < 3; k++)
        points.emplace_back(std::array<double, 3>{static_cast<double>(i),
                                                  static_cast<double>(j),
                                                  static_cast<double>(k)});
  return points;
}
/**
 * @brief In some situations, when boundary vertices are displaced, CGAL will
 * add some 0-volume simplices.
 */
std::vector<std::array<double, 3>> lattice_3x3x3_perturbed() {
  auto points = lattice_3x3x3();
  const auto eps = 2 * std::numeric_limits<double>::epsilon();
  points[26][0] += eps;
  points[26][1] += eps;
  return points;
}

/**
 * @brief All indices, except 13, which corresponds to (1,1,1).
 */
std::set<std::size_t> lattice_3x3x3_boundary_indices() {
  return {{0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12,
           14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26}};
}

TEST_P(TestTriangulation3d, yields_non_intersecting_and_covering_mesh) {
  const auto points = GetParam();
  const auto simplices = triangulate(points);
  const auto raw_simplices = to_simplices<3>(points, simplices);
  const auto volume = mesh_volume(raw_simplices);
  EXPECT_DOUBLE_EQ(volume, 8.);
  constexpr double EPSILON = 1.E-10;
  const auto n = raw_simplices.size();
  for (std::size_t i = 0; i < n - 1; i++) {
    for (std::size_t j = i + 1; j < n; j++) {
      EXPECT_TRUE(mesh::testing::simplices_dont_intersect(
          raw_simplices[i], raw_simplices[j], EPSILON));
    }
  }
}

template <std::size_t Dimension>
using Face = std::array<std::size_t, Dimension>;

std::array<Face<3>, 4> get_faces(const SimplexIndices<3> &t) {
  auto faces =
      std::array{std::array{t[0], t[1], t[2]}, std::array{t[0], t[1], t[3]},
                 std::array{t[0], t[2], t[3]}, std::array{t[1], t[2], t[3]}};
  for (auto &f : faces)
    std::sort(f.begin(), f.end());
  return faces;
}
std::array<std::size_t, 3> get_vertices(const Face<3> &face) {
  return std::array{face[0], face[1], face[2]};
}

struct TestTriangulation3dBoundary
    : ::testing::TestWithParam<std::tuple<std::vector<std::array<double, 3>>,
                                          std::set<std::size_t>>> {};
TEST_P(TestTriangulation3dBoundary, yields_correct_boundary) {
  const auto [points, expected_boundary_indices] = GetParam();
  auto simplices = triangulate(points);
  constexpr double TOLERANCE = 1.0e-5;
  clean_0_vol_cells_at_boundary(simplices, points, TOLERANCE);
  const auto boundary_indices =
      mesh::testing::boundary_vertices<std::size_t, Face<3>>(
          simplices, get_faces, get_vertices);
  EXPECT_EQ(boundary_indices, expected_boundary_indices);
}
TEST_P(TestTriangulation3d, does_not_generate_0_volume_elements) {
  const auto points = GetParam();
  auto simplices = triangulate(points);
  constexpr double TOLERANCE = 0.001;
  clean_0_vol_cells_at_boundary(simplices, points, TOLERANCE);
  auto zero_vol_simplices = std::vector<triangulation::SimplexIndices<3>>{};
  for (const auto &s : simplices)
    if (std::abs(geometry::simplex_volume(
            triangulation::to_simplex(points, s))) < TOLERANCE)
      zero_vol_simplices.push_back(s);
  EXPECT_THAT(zero_vol_simplices, ::testing::IsEmpty());
}

INSTANTIATE_TEST_CASE_P(
    Boundary, TestTriangulation3dBoundary,
    testing::Values(std::make_tuple(lattice_3x3x3(),
                                    lattice_3x3x3_boundary_indices()),
                    std::make_tuple(lattice_3x3x3_perturbed(),
                                    lattice_3x3x3_boundary_indices())));
INSTANTIATE_TEST_CASE_P(Boundary, TestTriangulation3d,
                        testing::Values(lattice_3x3x3(),
                                        lattice_3x3x3_perturbed()));

TEST(test_triangulation_2d, yields_empty_mesh_for_0_points) {
  const auto simplices = triangulate(std::vector<std::array<double, 2>>{{}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_2d, yields_empty_mesh_for_1_point) {
  const auto simplices =
      triangulate(std::vector<std::array<double, 2>>{{1., 0.}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_2d, yields_empty_mesh_for_2_points) {
  const auto simplices =
      triangulate(std::vector<std::array<double, 2>>{{1., 0.}, {0., 1.}});
  EXPECT_THAT(simplices, IsEmpty());
}
TEST(test_triangulation_2d, yields_simplex_for_3_points) {
  const auto points =
      std::vector<std::array<double, 2>>{{0., 0.}, {1., 0.}, {0., 1.}};
  const auto simplices = triangulate(points);
  const auto expected_indices = std::vector<std::size_t>{0, 1, 2};
  EXPECT_THAT(simplices, SizeIs(1));
  EXPECT_THAT(simplices[0],
              testing::UnorderedElementsAreArray(expected_indices));
}
TEST(test_triangulation_2d,
     yields_non_intersecting_and_covering_mesh_for_3x3_lattice) {
  auto points = std::vector<std::array<double, 2>>{};
  for (std::size_t i = 0; i < 3; i++)
    for (std::size_t j = 0; j < 3; j++)
      points.emplace_back(std::array<double, 2>{static_cast<double>(i),
                                                static_cast<double>(j)});
  const auto simplices = triangulate(points);
  const auto raw_simplices = to_simplices<2>(points, simplices);
  const auto volume = mesh_volume(raw_simplices);
  EXPECT_DOUBLE_EQ(volume, 4.);
  constexpr double EPSILON = 1.E-10;
  const auto n = raw_simplices.size();
  for (std::size_t i = 0; i < n - 1; i++) {
    for (std::size_t j = i + 1; j < n; j++) {
      EXPECT_TRUE(mesh::testing::simplices_dont_intersect(
          raw_simplices[i], raw_simplices[j], EPSILON));
    }
  }
}

} // namespace
} // namespace qcmesh::triangulation
