# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of qcmesh.
#
# qcmesh is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# qcmesh. If not, see <https://www.gnu.org/licenses/>.

include(CMakeFindDependencyMacro)

find_dependency(Boost 1.67 COMPONENTS mpi serialization REQUIRED)
find_dependency(Eigen3 3.4 CONFIG REQUIRED)

foreach(LIB array_ops geometry json_dump mpi mesh testing triangulation)
  include("${CMAKE_CURRENT_LIST_DIR}/qcmesh-${LIB}-export.cmake")
endforeach()
