# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of qcmesh.
#
# qcmesh is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# qcmesh. If not, see <https://www.gnu.org/licenses/>.

function(qcmesh_install_library LIB)
  include(GNUInstallDirs)
  install(DIRECTORY include/${PROJECT_NAME}
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
  )
  set_target_properties(${PROJECT_NAME}-${LIB}
    PROPERTIES EXPORT_NAME ${LIB}
  )
  install(TARGETS ${PROJECT_NAME}-${LIB}
    EXPORT ${PROJECT_NAME}-${LIB}-export
    ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
  )
  install(EXPORT ${PROJECT_NAME}-${LIB}-export
    DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}"
    NAMESPACE ${PROJECT_NAME}::
    FILE ${PROJECT_NAME}-${LIB}-export.cmake
  )
endfunction()
