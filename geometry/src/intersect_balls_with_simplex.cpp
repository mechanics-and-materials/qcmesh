// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/intersect_balls_with_simplex.hpp"
#include "qcmesh/geometry/ball.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/kd_tree.hpp"
#include "qcmesh/geometry/traits/bounding_box.hpp"
#include "qcmesh/geometry/traits/have_overlap.hpp"

namespace qcmesh::geometry {

namespace traits {

Box<3> BoundingBox<ExtrudedSimplex>::bounding_box(
    const ExtrudedSimplex &extruded_simplex) {
  auto box = qcmesh::geometry::bounding_box(
      extruded_simplex.simplex_distance_data.points);
  for (auto &val : box.lower)
    val -= extruded_simplex.padding;
  for (auto &val : box.upper)
    val += extruded_simplex.padding;
  return box;
}

bool HaveOverlap<ExtrudedSimplex, Ball<3>>::have_overlap(
    const ExtrudedSimplex &simplex, const Ball<3> &ball) {
  return simplex_signed_distance(simplex.simplex_distance_data, ball.center) <=
         ball.radius;
}

} // namespace traits

IntersectionIterator<Indexed<Ball<3>>, ExtrudedSimplex>
intersect_balls_with_simplex(
    const kd_tree::KDTree<Indexed<Ball<3>>> &balls,
    const std::array<std::array<double, 3>, 4> &simplex,
    const double max_radius) {
  auto extruded_simplex =
      ExtrudedSimplex{prepare_simplex_distance_data(simplex), max_radius};
  return kd_tree::intersect(balls, extruded_simplex);
}

double max_radius(const std::vector<Indexed<Ball<3>>> &balls) {
  auto max_radius = double{};
  for (const auto &ball : balls)
    if (ball.inner.radius > max_radius)
      max_radius = ball.inner.radius;
  return max_radius;
}

} // namespace qcmesh::geometry
