// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/box.hpp"

namespace qcmesh::geometry::traits {

/**
 * @brief Used to decide if two geometric shapes overlap.
 *
 * Used in @ref kd_tree::KDTree and @ref octree::Octree.
 */
template <class ShapeA, class ShapeB> struct HaveOverlap {
  static bool have_overlap(const ShapeA &a, const ShapeB &b);
};

/**
 * @brief Convenience wrapper for the @ref HaveOverlap trait.
 */
template <class ShapeA, class ShapeB>
bool have_overlap(const ShapeA &a, const ShapeB &b) {
  return HaveOverlap<ShapeA, ShapeB>::have_overlap(a, b);
}

/**
 * @brief Implementation of @ref HaveOverlap for @ref Box and
 * `std::array<double, Dimension>`.
 */
template <std::size_t Dimension>
struct HaveOverlap<Box<Dimension>, std::array<double, Dimension>> {
  static bool have_overlap(const Box<Dimension> &box,
                           const std::array<double, Dimension> &p) {
    return contains(box, p);
  }
};

} // namespace qcmesh::geometry::traits
