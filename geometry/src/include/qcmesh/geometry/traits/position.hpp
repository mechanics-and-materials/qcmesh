// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <array>

namespace qcmesh::geometry::traits {

/**
 * @brief Abstraction of objects having a position.
 */
template <class Shape> struct Position {
  constexpr static std::size_t DIMENSION = 0;
  static const std::array<double, DIMENSION> &position(const Shape &shape);
};

/**
 * @brief Abstraction of position for `std::array<double, K>`.
 *
 * This allows `std::array<double, K>` to be used with
 * @ref kd_tree::KDTree and @ref bounding_box.
 */
template <std::size_t Dimension>
struct Position<std::array<double, Dimension>> {
  constexpr static std::size_t DIMENSION = Dimension;
  static const std::array<double, DIMENSION> &
  position(const std::array<double, DIMENSION> &point) {
    return point;
  }
};

/**
 * @brief Convenience wrapper for the @ref Position trait.
 */
template <class Shape>
const std::array<double, Position<Shape>::DIMENSION> &
position(const Shape &shape) {
  return Position<Shape>::position(shape);
}

} // namespace qcmesh::geometry::traits
