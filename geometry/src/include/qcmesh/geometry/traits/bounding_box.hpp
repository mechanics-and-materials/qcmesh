// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/box.hpp"

namespace qcmesh::geometry::traits {

/**
 * @brief Used to determine the bounding box of geometric shapes.
 *
 * Used in @ref kd_tree::KDTree and @ref octree::Octree.
 */
template <class Shape> struct BoundingBox {
  constexpr static std::size_t DIMENSION = 0;

  /**
   * @brief Determine the bounding box of `shape`.
   */
  static Box<DIMENSION> bounding_box(const Shape &shape);
};

/**
 * @brief Convenience wrapper for the @ref BoundingBox trait.
 */
template <class Shape>
Box<BoundingBox<Shape>::DIMENSION> bounding_box(const Shape &shape) {
  return BoundingBox<Shape>::bounding_box(shape);
}

/**
 * @brief Implementation of @BoundingBox for @ref Box.
 */
template <std::size_t Dimension> struct BoundingBox<Box<Dimension>> {
  constexpr static std::size_t DIMENSION = Dimension;
  static Box<Dimension> bounding_box(const Box<DIMENSION> &box) { return box; }
};

} // namespace qcmesh::geometry::traits
