// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/traits/bounding_box.hpp"
#include "qcmesh/geometry/traits/position.hpp"
#include <array>

namespace qcmesh::geometry {

/**
 * Ball in `Dimension` dimensions defined by center and radius.
 */
template <std::size_t Dimension> struct Ball {
  std::array<double, Dimension> center{};
  double radius{};
};

namespace traits {

/**
 * @brief Implementation of @ref Position for @ref Ball.
 *
 * This allows @ref Ball to be used with @ref kd_tree::KDTree and @ref
 * bounding_box.
 */
template <std::size_t Dimension> struct Position<Ball<Dimension>> {
  constexpr static std::size_t DIMENSION = Dimension;
  static const std::array<double, DIMENSION> &
  position(const Ball<Dimension> &ball) {
    return ball.center;
  }
};

/**
 * @brief Implementation of @ref BoundingBox for @ref Ball.
 */
template <std::size_t Dimension> struct BoundingBox<Ball<Dimension>> {
  constexpr static std::size_t DIMENSION = Dimension;
  static Box<Dimension> bounding_box(const Ball<Dimension> &ball) {
    auto box = Box<Dimension>{ball.center, ball.center};
    for (auto &c : box.lower)
      c -= ball.radius;
    for (auto &c : box.upper)
      c += ball.radius;
    return box;
  }
};

} // namespace traits

} // namespace qcmesh::geometry
