// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Geometric properties of simplices.
 *
 * @author Prateek Gupta
 */

#pragma once

#include <array>
#include <cmath>

namespace qcmesh::geometry {

/**
 * @brief Computes the solid angle of each corner of a simplex.
 *
 * See: https://en.wikipedia.org/wiki/Solid_angle#Tetrahedron
 *
 * This function is only intended to be used with non-degenerate simplices.
 * The output value will always be in the interval [0, 2 pi], i.e.
 * there is no distinction between boundary orientations.
 */
std::array<double, 2 + 1>
simplex_solid_angles(const std::array<std::array<double, 2>, 3> &simplex);

std::array<double, 3 + 1>
simplex_solid_angles(const std::array<std::array<double, 3>, 4> &simplex);

/**
 * @brief Computes the shape measure according to
 * Int. J. Numer. Meth. Engng 2002; 53:1377–1391 (DOI: 10.1002/nme.341)
 *
 * The shape measure is defined as DIM/\kappa,
 * where \kappa is the condition number (section 2.2).
 */
double simplex_quality(const std::array<std::array<double, 2>, 2 + 1> &simplex);
double simplex_quality(const std::array<std::array<double, 3>, 3 + 1> &simplex);

double simplex_quality(const std::array<std::array<double, 2>, 2> &jac);
double simplex_quality(const std::array<std::array<double, 3>, 3> &jac);

/**
 * @brief Computes the volume of a simplex.
 */
double simplex_volume(const std::array<std::array<double, 2>, 2 + 1> &triangle);
double
simplex_volume(const std::array<std::array<double, 3>, 3 + 1> &tetrahedron);

/**
 * @brief Evaluates the shape functions of a simplex at a given point.
 *
 * See https://www.iue.tuwien.ac.at/phd/nentchev/node30.html
 */
std::array<double, 3 + 1> simplex_eval_shape_functions(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const std::array<double, 3> &point);
/**
 * @brief Evaluates the shape functions of a simplex at a given point.
 *
 * This variant returns 0 for all values, if the point lies outside of the
 * simplex.
 */
std::array<double, 3 + 1> simplex_eval_shape_functions_interior(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const std::array<double, 3> &point);

/**
 * @brief For given shape function values, return true if they represent a
 * point in the interior (including boundary) of the tetrahedron.
 */
bool shape_function_values_represent_contained_point(
    const std::array<double, 3 + 1> &shape_values,
    const double tolerance = 1e-8);

/**
 * @brief Computes the residual for given simplex quality.
 */
inline double quality_residual(const double quality) {
  return (1.0 - quality) * (1.0 / quality - 1.0);
}

/**
 * @brief Residual based on the tetrahedral shape measure.
 *
 * This deviates from the paper
 * "Int. J. Numer. Meth. Engng 2004; 60:597–639 (DOI: 10.1002/nme.977)", eq.
 * (40), and is our own convention.
 */
template <std::size_t Dimension>
double simplex_metric_residual(
    const std::array<std::array<double, Dimension>, Dimension + 1> &simplex) {
  return quality_residual(simplex_quality(simplex));
}

/**
 * @brief Residual based on the tetrahedral shape measure.
 */
template <std::size_t Dimension>
double simplex_metric_residual(
    const std::array<std::array<double, Dimension>, Dimension> &jac) {
  return quality_residual(simplex_quality(jac));
}

/**
 * @brief For each vertex of a simplex, compute the distance
 * to the opposite face.
 *
 * @param jac: Jacobian of the simplex
 */
std::array<double, 3 + 1> simplex_distances_to_opposite_facet(
    const std::array<std::array<double, 3>, 3> &jac);
std::array<double, 2 + 1> simplex_distances_to_opposite_facet(
    const std::array<std::array<double, 2>, 2> &jac);

/**
 * @brief For each vertex of a simplex, compute the minimum over the distances
 * to all other vertices.
 *
 * @param jac: Jacobian of the simplex
 */
std::array<double, 3 + 1> simplex_distances_to_other_vertices(
    const std::array<std::array<double, 3>, 3> &jac);
std::array<double, 2 + 1> simplex_distances_to_other_vertices(
    const std::array<std::array<double, 2>, 2> &jac);

/**
 * @brief Jacobian of a simplex: Edge vectors relative to first vertex.
 */
std::array<std::array<double, 3>, 3>
simplex_jacobian(const std::array<std::array<double, 3>, 3 + 1> &simplex);
std::array<std::array<double, 2>, 2>
simplex_jacobian(const std::array<std::array<double, 2>, 2 + 1> &simplex);
std::array<std::array<int, 3>, 3>
simplex_jacobian(const std::array<std::array<int, 3>, 3 + 1> &simplex);
std::array<std::array<int, 2>, 2>
simplex_jacobian(const std::array<std::array<int, 2>, 2 + 1> &simplex);

/**
 * @brief For each vertex of a simplex, compute the volume of the ball with
 * radius r, divided by the surface of the unit sphere, where for each vertex, r
 * is the minimum of:
 * - node_radius
 * - distances to middlepoints of connected edges
 * - distance to the opposite facet.
 */
std::array<double, 3 + 1> simplex_vertex_volumes_per_solid_angle(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const double node_radius);
std::array<double, 2 + 1> simplex_vertex_volumes_per_solid_angle(
    const std::array<std::array<double, 2>, 2 + 1> &simplex,
    const double node_radius);

/**
 * @brief Struct holding some metrics about a simplex.
 *
 * vertex_volumes: The product of simplex_vertex_volumes_per_solid_angle
 * and solid_angle.
 */
template <std::size_t Dimension> struct SimplexMetrics {
  std::array<double, Dimension + 1> vertex_volumes;
  std::array<double, Dimension + 1> solid_angles;
  double volume;
  double barycenter_volume;
};

/**
 * @brief Collect some metrics about a simplex.
 */
SimplexMetrics<3>
simplex_metrics(const std::array<std::array<double, 3>, 3 + 1> &simplex,
                const double max_vertex_radius);
SimplexMetrics<2>
simplex_metrics(const std::array<std::array<double, 2>, 2 + 1> &simplex,
                const double max_vertex_radius);

/**
 * @brief Plane in Euclidean space, defined by `<x, normal> = d`.
 */
template <std::size_t Dimension> struct Plane {
  std::array<double, Dimension> normal{};
  double d{};
};

/**
 * @brief Segment between two points `start` and
 * `start + tangent * (d_upper-d_lower)`.
 */
template <std::size_t Dimension> struct Segment {
  /// Point on segment between planes
  const std::array<double, Dimension> &start;
  /// Unit tangent
  std::array<double, Dimension> tangent{};
  /// `d_lower = <p, t>`
  double d_lower{};
  /// `d_upper - d_lower` is length of segment.
  double d_upper{};
};

/**
 * @brief For a given face, parametrized by
 * `{ p0 + s a + t b | 0 \leq s, t, s+t \leq 1 }`, encodes the transformation
 * which maps a vector `v = p0 + s a + t b + u n` to `(s, t)` for `n = a x b`.
 *
 * <n x a, v-p0> = t <n x a, b>,
 * <n x b, v-p0> = s <n x b, a> = s <a x n, b> = -s <n x a, b>,
 * => s = -<n x b, v-p0> / <n x a, b>, t = <n x a, v-p0> / <n x a, b>.
 */
struct FaceFrame {
  Plane<3> plane{};
  std::array<std::array<double, 3>, 2> p{};
  std::array<double, 2> p_p0{};
};

/**
 * @brief Cached data to compute the signed distance between a point and
 * a 3d simplex (tetrahedron).
 */
struct SimplexDistanceData3d {
  std::array<std::array<double, 3>, 4> points{};
  std::array<Segment<3>, 6> segments;
  std::array<FaceFrame, 4> face_frames{};
};

/**
 * @brief Compute cached data to be used by @ref simplex_signed_distance.
 *
 * `points` need to be oriented such that
 * `<(p[1]-p[0]) x (p[2]-p[0]), (p[3]-p[0])> > 0`.
 */
SimplexDistanceData3d prepare_simplex_distance_data(
    const std::array<std::array<double, 3>, 4> &points);

/**
 * @brief Signed distance between a point and a 3d simplex (tetrahedron).
 */
double
simplex_signed_distance(const SimplexDistanceData3d &simplex_distance_data,
                        const std::array<double, 3> &p);

} // namespace qcmesh::geometry
