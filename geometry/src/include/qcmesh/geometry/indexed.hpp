// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/traits/have_overlap.hpp"
#include "qcmesh/geometry/traits/position.hpp"

namespace qcmesh::geometry {

/**
 * @brief Wrapper type to attach an index to an object.
 *
 * Can be used to store indices alongside geometric objects in a
 * @ref octree::Octree which can be retrieved when using
 * @ref octree::Octree::find_or_insert or @ref octree::Octree::find.
 */
template <class T> struct Indexed {
  std::size_t index{};
  T inner{};
};

/**
 * @brief Implementation of @ref traits::Position for @ref Indexed<T>
 * if @ref traits::Position is implemented for `T`.
 */
template <class T> struct traits::Position<Indexed<T>> {
  constexpr static std::size_t DIMENSION = traits::Position<T>::DIMENSION;
  static const std::array<double, DIMENSION> &
  position(const Indexed<T> &indexed) {
    return traits::position(indexed.inner);
  }
};

/**
 * @brief Implementation of @ref traits::HaveOverlap for @ref Indexed<T>
 * if @ref traits::HaveOverlap is implemented for `Shape` and `T`.
 */
template <class Shape, class T> struct traits::HaveOverlap<Shape, Indexed<T>> {
  static bool have_overlap(const Shape &shape, const Indexed<T> &indexed) {
    return traits::have_overlap(shape, indexed.inner);
  }
};

} // namespace qcmesh::geometry
