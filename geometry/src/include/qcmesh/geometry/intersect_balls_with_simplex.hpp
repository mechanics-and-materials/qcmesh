// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/ball.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/indexed.hpp"
#include "qcmesh/geometry/kd_tree.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include <array>

namespace qcmesh::geometry {

/**
 * @brief Represents a simplex with extruded bounding box.
 *
 * Behaves like @ref SimplexDistanceData3d, but the @ref BoundingBox trait
 * returns a bounding box padded by `padding`.
 */
struct ExtrudedSimplex {
  const SimplexDistanceData3d simplex_distance_data;
  double padding;
};

namespace traits {

/**
 * @brief Implementation of @ref BoundingBox for @ref ExtrudedSimplex to add
 * a padding.
 */
template <> struct BoundingBox<ExtrudedSimplex> {
  constexpr static std::size_t DIMENSION = 3;
  static Box<DIMENSION> bounding_box(const ExtrudedSimplex &extruded_simplex);
};

/**
 * @brief Implementation of @ref HaveOverlap for @ref ExtrudedSimplex which
 * delegates to the implementation of @ref SimplexDistanceData3d.
 */
template <> struct HaveOverlap<ExtrudedSimplex, Ball<3>> {
  static bool have_overlap(const ExtrudedSimplex &simplex, const Ball<3> &ball);
};

} // namespace traits

/**
 * @brief Returns an iterable over all balls in `balls` which intersect with a
 * simplex `simplex`.
 *
 * For performance reasons, the maximal radius of the balls has to be
 * precomputed and passed as a parameter.
 */
IntersectionIterator<Indexed<Ball<3>>, ExtrudedSimplex>
intersect_balls_with_simplex(
    const kd_tree::KDTree<Indexed<Ball<3>>> &balls,
    const std::array<std::array<double, 3>, 4> &simplex,
    const double max_radius);

/**
 * @brief Compute the maximum radius of a vector of balls.
 *
 * The returned value can be passed to @ref intersect_balls_with_simplex.
 */
double max_radius(const std::vector<Indexed<Ball<3>>> &balls);

} // namespace qcmesh::geometry
