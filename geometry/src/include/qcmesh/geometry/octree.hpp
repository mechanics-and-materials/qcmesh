// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/ball.hpp"
#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/intersection_iterator.hpp"
#include "qcmesh/geometry/traits/bounding_box.hpp"
#include "qcmesh/geometry/traits/have_overlap.hpp"
#include "qcmesh/geometry/traits/position.hpp"
#include <array>
#include <limits>
#include <memory>
#include <optional>
#include <stdexcept>
#include <variant>
#include <vector>

namespace qcmesh::geometry::octree {

/**
 * @brief Leaf of a @ref Octree.
 */
template <class T> struct Leaf {
  std::vector<T> objects{};
  Box<3> domain{};
};

/**
 * @brief Branch of a @ref Octree.
 *
 * Consists of 8 octants with common corner `center`.
 */
template <class T> struct Branch {
  /// Octants
  std::array<std::unique_ptr<std::variant<Leaf<T>, Branch<T>>>, 8> octants{};
  /// Common corner of the octants
  std::array<double, 3> center{};
  Box<3> domain{};
};

/**
 * @brief Trait of the euclidean norm, needed by @ref
 * Octree::find_or_insert.
 */
struct EuclideanNorm {
  /**
   * @brief Returns `true` if |v| <= norm.
   */
  static bool norm_bounded_by(const std::array<double, 3> &v,
                              const double norm);
};
/**
 * @brief Trait of the max norm, needed by @ref Octree::find_or_insert.
 */
struct MaxNorm {
  /**
   * @brief Returns `true` if max_i |v[i]| <= norm.
   */
  static bool norm_bounded_by(const std::array<double, 3> &v,
                              const double norm);
};

/**
 * @brief A Ball where the radius is w.r.t. to a norm.
 */
template <class Norm> struct NormBall : Ball<3> {};

/**
 * @brief Implementation of a Octree (https://en.wikipedia.org/wiki/Octree).
 *
 * `T` must implement @ref traits::Position (like `std::array<double, 3>`
 * and @ref Ball). The `Domain` type must implement
 * @ref traits::BoundingBox and @ref traits::HaveOverlap<Domain,
 * std::array<double, 3>>. (like e.g. @ref Box).
 */
template <class T, class Domain = Box<3>> struct Octree {
  std::variant<Leaf<T>, Branch<T>> root;
  Domain domain;
  std::size_t leaf_size = 8;
  std::size_t n_objects = 0;

  /**
   * @brief Insert an object into the tree.
   */
  void insert(const T &object);

  /**
   * @brief Find objects close to `object`.
   *
   * The distance is w.r.t. the `Norm` template parameter.
   *
   * @return An iterable over all objects in the tree and in `tolerance` range.
   */
  template <class Norm = MaxNorm>
  IntersectionIterator<T, NormBall<Norm>>
  find(const std::array<double, 3> &position, const double tolerance);

  /**
   * @brief Insert an object into the tree if there is no other
   * object within distance `tolerance` in the tree and if the
   * object is in the domain.
   *
   * The distance is w.r.t. the `Norm` template parameter.
   *
   * @return An iterable over all objects in the tree and in `tolerance` range.
   *
   * Note that an empty iterable as the return value does not imply
   * that an insertion happened. Trying to insert an object outside
   * the domain will not insert the object but also yield an empty
   * intersection.
   */
  template <class Norm = MaxNorm>
  IntersectionIterator<T, NormBall<Norm>>
  find_or_insert(const T &object, const double tolerance);

  /**
   * @brief Insert an object into the tree if there is no other
   * object within distance `tolerance` in the tree and if the
   * object is in the domain.
   *
   * The distance is w.r.t. the `Norm` template parameter.
   *
   * @return `true` if an insertion happened, `false` otherwise.
   */
  template <class Norm = MaxNorm>
  bool insert_unique(const T &object, const double tolerance);

  /**
   * @brief Total size of objects stored in the tree.
   */
  [[nodiscard]] std::size_t size() const { return this->n_objects; }

  /**
   * @brief Construct an @ref Octree for given domain.
   *
   * @param leaf_size The upper limit of objects a leaf is allowed to hold.
   */
  explicit Octree(const Domain &domain, const std::size_t leaf_size = 8)
      : root{Leaf<T>{{}, traits::bounding_box(domain)}}, domain{domain},
        leaf_size{leaf_size} {}
  /**
   * @brief Construct an @ref Octree for given domain, filled with given
   * objects.
   *
   * @param leaf_size The upper limit of objects a leaf is allowed to hold.
   */
  Octree(const Domain &domain, const std::vector<T> &objects,
         const std::size_t leaf_size = 8)
      : Octree{domain, leaf_size} {
    for (const auto &obj : objects)
      this->insert(obj);
  }
  Octree() = delete;
};

namespace detail {

template <class T> using Node = std::variant<Leaf<T>, Branch<T>>;

/**
 * @brief Determine the octant around `center` containing `position`.
 */
inline std::size_t octant_index(const std::array<double, 3> &center,
                                const std::array<double, 3> &position) {
  return static_cast<std::size_t>(position[0] > center[0]) |
         (static_cast<std::size_t>(position[1] > center[1]) << 1U) |
         (static_cast<std::size_t>(position[2] > center[2]) << 2U);
}

inline Box<3> octant_domain(const Box<3> &domain,
                            const std::array<double, 3> &center,
                            const std::size_t octant_index) {
  const auto directions =
      std::array<bool, 3>{static_cast<bool>(octant_index & 1U),
                          static_cast<bool>(octant_index & 2U),
                          static_cast<bool>(octant_index & 4U)};
  auto box = Box<3>{};
  for (std::size_t dim = 0; dim < 3; dim++)
    if (directions[dim]) {
      box.lower[dim] = center[dim];
      box.upper[dim] = domain.upper[dim];
    } else {
      box.lower[dim] = domain.lower[dim];
      box.upper[dim] = center[dim];
    }
  return box;
}

template <class T>
std::optional<Node<T>> insert(Branch<T> &branch, const T &object,
                              const std::size_t leaf_size);

template <class T>
std::optional<Node<T>> insert(Leaf<T> &leaf, const T &object,
                              const std::size_t leaf_size) {
  const auto is_single_point = [](const auto &box) {
    for (std::size_t i = 0; i < box.lower.size(); i++)
      if (box.upper[i] - box.lower[i] > std::numeric_limits<double>::epsilon())
        return false;
    return true;
  };
  if (leaf.objects.size() >= leaf_size &&
      !is_single_point(bounding_box(leaf.objects))) {
    auto center = std::array<double, 3>{};
    for (std::size_t i = 0; i < center.size(); i++)
      center[i] = 0.5 * (leaf.domain.lower[i] + leaf.domain.upper[i]);

    auto branch = Branch<T>{{}, center, std::move(leaf.domain)};
    while (!leaf.objects.empty()) {
      insert(branch, std::move(leaf.objects.back()), leaf_size);
      leaf.objects.pop_back();
    }
    insert(branch, object, leaf_size);
    return branch;
  }
  leaf.objects.push_back(object);
  return std::nullopt;
}

template <class T>
void insert(Node<T> &node, const T &object, const std::size_t leaf_size) {
  auto octant_replacement =
      std::visit([&object, leaf_size](
                     auto &node) { return insert<T>(node, object, leaf_size); },
                 node);
  if (octant_replacement.has_value())
    std::swap(octant_replacement.value(), node);
}

template <class T>
std::optional<Node<T>> insert(Branch<T> &branch, const T &object,
                              const std::size_t leaf_size) {
  const auto position = traits::position(object);
  const auto oct_idx = octant_index(branch.center, position);
  if (!branch.octants[oct_idx])
    branch.octants[oct_idx] = std::make_unique<Node<T>>(
        Leaf<T>{std::vector<T>{object},
                octant_domain(branch.domain, branch.center, oct_idx)});
  else
    insert(*branch.octants[oct_idx].get(), object, leaf_size);
  return std::nullopt;
}

} // namespace detail

template <class T, class Domain>
void Octree<T, Domain>::insert(const T &object) {
  if (!traits::have_overlap(this->domain, traits::position(object)))
    throw std::runtime_error{"Cannot add object outside of Octree domain."};
  detail::insert(this->root, object, this->leaf_size);
  ++this->n_objects;
}

namespace detail {

inline bool boxes_overlap(const Box<3> &a, const Box<3> &b) {
  constexpr std::size_t D = 3;
  for (std::size_t i = 0; i < D; ++i)
    if (std::max(a.lower[i], b.lower[i]) > std::min(a.upper[i], b.upper[i]))
      return false;
  return true;
}

template <class T>
void register_node(const Leaf<T> &leaf,
                   std::vector<const std::vector<T> *> &leafs,
                   std::vector<const Branch<T> *> &) {
  if (!leaf.objects.empty())
    leafs.emplace_back(&leaf.objects);
};
template <class T>
void register_node(const Branch<T> &branch,
                   std::vector<const std::vector<T> *> &,
                   std::vector<const Branch<T> *> &branches) {
  branches.emplace_back(&branch);
};

template <class T> const Box<3> &domain(const Branch<T> &branch) {
  return branch.domain;
};
template <class T> const Box<3> &domain(const Leaf<T> &leaf) {
  return leaf.domain;
};

} // namespace detail

/**
 * @brief Return an interable iterating over all objects in the intersection
 * of a @ref Octree instance with a `Shape` instance.
 * The iterator only will yield objects for which
 * `traits::have_overlap(shape, object) == true`.
 * Suitable overloads of the following must exist:
 * - @ref traits::have_overlap for `Shape` and `T`,
 * - @ref traits::bounding_box for `Shape`.
 */
template <class T, class Shape, class Domain>
IntersectionIterator<T, Shape> intersect(const Octree<T, Domain> &tree,
                                         const Shape &shape) {
  auto leafs = std::vector<const std::vector<T> *>{};
  auto branches = std::vector<const Branch<T> *>{};
  auto box = traits::bounding_box(shape);
  const auto register_node = [&leafs, &branches](const auto &obj) {
    detail::register_node<T>(obj, leafs, branches);
  };
  if (detail::boxes_overlap(
          std::visit([](const auto &shape) { return detail::domain<T>(shape); },
                     tree.root),
          box))
    std::visit(register_node, tree.root);

  while (!branches.empty()) {
    const auto &branch = *branches.back();
    branches.pop_back();
    auto add_octant = std::array<bool, 8>{};
    for (const auto x : {box.lower[0], box.upper[0]})
      for (const auto y : {box.lower[1], box.upper[1]})
        for (const auto z : {box.lower[2], box.upper[2]})
          add_octant[detail::octant_index(branch.center, std::array{x, y, z})] =
              true;
    for (std::size_t i = 0; i < add_octant.size(); i++)
      if (add_octant[i] && branch.octants[i])
        std::visit(register_node, *branch.octants[i]);
  }
  return IntersectionIterator<T, Shape>(leafs, shape);
}

} // namespace qcmesh::geometry::octree

namespace qcmesh::geometry {

/**
 * @brief Implementation of @traits::BoundingBox for @ref octree::NormBall.
 */
template <class Norm> struct traits::BoundingBox<octree::NormBall<Norm>> {
  constexpr static std::size_t DIMENSION = 3;
  static Box<3> bounding_box(const octree::NormBall<Norm> &ball) {
    return traits::bounding_box<Ball<3>>(ball);
  }
};

/**
 * @brief Implementation of @traits::HaveOverlap for @ref octree::NormBall and
 * `std::array<double, 3>`.
 */
template <class Norm>
struct traits::HaveOverlap<octree::NormBall<Norm>, std::array<double, 3>> {
  static bool have_overlap(const octree::NormBall<Norm> &ball,
                           const std::array<double, 3> &p) {
    auto d = std::array{ball.center[0] - p[0], ball.center[1] - p[1],
                        ball.center[2] - p[2]};
    return Norm::norm_bounded_by(d, ball.radius);
  }
};

namespace octree {

template <class T, class Domain>
template <class Norm>
IntersectionIterator<T, NormBall<Norm>>
Octree<T, Domain>::find(const std::array<double, 3> &position,
                        const double tolerance) {
  auto norm_ball = NormBall<Norm>{position, tolerance};
  return intersect(*this, norm_ball);
}

template <class T, class Domain>
template <class Norm>
IntersectionIterator<T, NormBall<Norm>>
Octree<T, Domain>::find_or_insert(const T &object, const double tolerance) {
  const auto &position = traits::position(object);
  auto norm_ball = NormBall<Norm>{position, tolerance};
  if (!traits::have_overlap(this->domain, position))
    return IntersectionIterator<T, NormBall<Norm>>{{}, norm_ball};
  auto intersection = intersect(*this, norm_ball);
  if (intersection.empty()) {
    detail::insert(this->root, object, this->leaf_size);
    ++this->n_objects;
  }
  return intersection;
}

template <class T, class Domain>
template <class Norm>
bool Octree<T, Domain>::insert_unique(const T &object, const double tolerance) {
  const auto size_before = this->size();
  this->find_or_insert<Norm>(object, tolerance);
  return this->size() > size_before;
}

} // namespace octree

} // namespace qcmesh::geometry
