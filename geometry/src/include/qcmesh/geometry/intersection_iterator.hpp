// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/traits/have_overlap.hpp"
#include "qcmesh/geometry/traits/position.hpp"
#include <vector>

namespace qcmesh::geometry {

/**
 * @brief Iterator resulting from @ref kd_tree::intersect,
 * @ref octree::intersect, @ref octree::Octree::find, @ref
 * octree::find_or_insert.
 *
 * Can be used in range based for loops.
 */
template <class T, class Shape> class IntersectionIterator {
  std::vector<const std::vector<T> *> leafs;
  Shape shape;
  std::size_t leaf_idx;
  std::size_t idx;

  bool dereferences_to_exterior_point() {
    return this->leaf_idx < this->leafs.size() &&
           !traits::have_overlap(this->shape,
                                 (*this->leafs[this->leaf_idx])[this->idx]);
  }

public:
  /**
   * @brief Used as @ref IntersectionIterator::end().
   */
  struct Terminator {
    std::size_t leaf_size{};
  };

  // NOLINTBEGIN(readability-identifier-naming)
  using difference_type = std::size_t;
  using reference = T &;
  using value_type = T;
  using pointer = T *;
  using iterator_category = std::forward_iterator_tag;
  // NOLINTEND(readability-identifier-naming)

  IntersectionIterator() = delete;
  /**
   * @brief Construct from `leafs` and `shape`.
   */
  IntersectionIterator(std::vector<const std::vector<T> *> leafs, Shape shape)
      : leafs{std::move(leafs)}, shape{std::move(shape)}, leaf_idx{0}, idx{0} {
    if (this->dereferences_to_exterior_point())
      ++(*this);
  }
  /**
   * @brief Infix ++ operator to provide support for range based for loops.
   */
  IntersectionIterator &operator++() {
    if (this->leaf_idx >= this->leafs.size())
      return *this;
    do {
      ++this->idx;
      if (this->idx >= this->leafs[this->leaf_idx]->size()) {
        this->idx = 0;
        ++this->leaf_idx;
      }
    } while (this->dereferences_to_exterior_point());
    return *this;
  }
  /**
   * @brief Unequality operator to provide support for range based for loops.
   */
  bool operator!=(const Terminator &other) const {
    return this->leaf_idx < other.leaf_size;
  }
  /**
   * @brief Dereference operator to provide support for range based for loops.
   */
  const T &operator*() const {
    return (*this->leafs[this->leaf_idx])[this->idx];
  }
  /**
   * @brief Return `true` if this is the last iterator.
   */
  [[nodiscard]] bool empty() const {
    return this->leaf_idx >= this->leafs.size();
  }
  /**
   * @brief To provide support for range based for loops.
   */
  [[nodiscard]] IntersectionIterator<T, Shape> &begin() { return *this; }
  /**
   * @brief To provide support for range based for loops.
   */
  [[nodiscard]] Terminator end() const { return {this->leafs.size()}; }
};

} // namespace qcmesh::geometry
