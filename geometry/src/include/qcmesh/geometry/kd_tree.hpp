// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/intersection_iterator.hpp"
#include "qcmesh/geometry/traits/bounding_box.hpp"
#include "qcmesh/geometry/traits/position.hpp"
#include <algorithm>
#include <array>
#include <limits>
#include <memory>
#include <variant>
#include <vector>

namespace qcmesh::geometry::kd_tree {

/**
 * @brief Leaf of a @ref KDTree.
 */
template <class T> struct Leaf {
  std::vector<T> objects{};
};

/**
 * @brief Branch of a @ref KDTree.
 *
 * A branch splits a box into 2 smaller boxes `lower` and `upper` at the plane
 * `x[split_dimension] = split_value`.
 */
template <class T> struct Branch {
  /// Lower subtree
  std::unique_ptr<std::variant<Leaf<T>, Branch<T>>> lower{};
  /// Upper subtree
  std::unique_ptr<std::variant<Leaf<T>, Branch<T>>> upper{};
  /// Index of dimension splitting lower from upper
  std::size_t split_dimension{};
  /// Coordinate where lower is split from upper along `split_dimension`.
  double split_value{};
};

/**
 * @brief Implementation of a K-d tree (https://en.wikipedia.org/wiki/K-d_tree).
 *
 * There must be an overload of @ref traits::Position for the type `T` (this is
 * the case for `std::array<double, K>` and @ref Ball)
 */
template <class T> struct KDTree {
  std::variant<Leaf<T>, Branch<T>> root{};
  /**
   * @brief Create a @ref KDTree for given objects.
   *
   * There must be an overload of @ref traits::Position for the type `T` (this
   * is the case for `std::array<double, K>`).
   *
   * @param leaf_size The upper limit of objects a leaf is allowed to hold.
   */
  explicit KDTree(const std::vector<T> &objects,
                  const std::size_t leaf_size = 8);
};

namespace detail {

/**
 * @brief Recursively divide values into branches of a @ref KDTree.
 */
template <class T>
std::variant<Leaf<T>, Branch<T>> create_branch(
    std::array<std::vector<T>, traits::Position<T>::DIMENSION> &ordered_by_dim,
    const std::size_t leaf_size) {
  constexpr std::size_t K = traits::Position<T>::DIMENSION;
  auto interval_lengths = std::array<double, K>{};
  for (std::size_t i = 0; i < K; i++)
    interval_lengths[i] = traits::position(ordered_by_dim[i].back())[i] -
                          traits::position(ordered_by_dim[i].front())[i];
  // Index of the maximum interval length:
  const auto split_dim = static_cast<std::size_t>(
      std::max_element(interval_lengths.begin(), interval_lengths.end()) -
      interval_lengths.begin());

  if (interval_lengths[split_dim] <= std::numeric_limits<double>::epsilon())
    return Leaf<T>{ordered_by_dim[0]};

  const auto total_size = ordered_by_dim[0].size();
  auto median_index = total_size / 2;
  // If the median occurs multiple times, choose the first element:
  while (
      median_index > 0 &&
      traits::position(
          ordered_by_dim[split_dim][median_index - 1])[split_dim] ==
          traits::position(ordered_by_dim[split_dim][median_index])[split_dim])
    --median_index;
  // If all items up to the median index are equal:
  if (median_index == 0) {
    median_index = ordered_by_dim[0].size() / 2 + 1;
    while (median_index < total_size &&
           traits::position(
               ordered_by_dim[split_dim][median_index - 1])[split_dim] ==
               traits::position(
                   ordered_by_dim[split_dim][median_index])[split_dim])
      ++median_index;
  }
  const auto median =
      traits::position(ordered_by_dim[split_dim][median_index])[split_dim];
  const auto lower_size = median_index;
  const auto upper_size = total_size - median_index;
  auto ordered_by_dim_lower = std::array<std::vector<T>, K>{};
  auto ordered_by_dim_upper = std::array<std::vector<T>, K>{};
  // Partition split_dim:
  ordered_by_dim_upper[split_dim].reserve(upper_size);
  std::copy(ordered_by_dim[split_dim].begin() + median_index,
            ordered_by_dim[split_dim].end(),
            std::back_inserter(ordered_by_dim_upper[split_dim]));
  ordered_by_dim[split_dim].erase(ordered_by_dim[split_dim].begin() +
                                      median_index,
                                  ordered_by_dim[split_dim].end());
  ordered_by_dim_lower[split_dim] = std::move(ordered_by_dim[split_dim]);
  // Partition other dims:
  const auto lt_median = [median, split_dim](const auto &obj) {
    return traits::position(obj)[split_dim] < median;
  };
  const auto geq_median = [median, split_dim](const auto &obj) {
    return traits::position(obj)[split_dim] >= median;
  };
  if (lower_size > leaf_size) // only needed if not already a leaf
    for (std::size_t i = 0; i < K; ++i)
      if (i != split_dim) {
        ordered_by_dim_lower[i].reserve(lower_size);
        std::copy_if(ordered_by_dim[i].begin(), ordered_by_dim[i].end(),
                     std::back_inserter(ordered_by_dim_lower[i]), lt_median);
      }
  if (upper_size > leaf_size) // only needed if not already a leaf
    for (std::size_t i = 0; i < K; ++i)
      if (i != split_dim) {
        ordered_by_dim_upper[i].reserve(upper_size);
        std::copy_if(ordered_by_dim[i].begin(), ordered_by_dim[i].end(),
                     std::back_inserter(ordered_by_dim_upper[i]), geq_median);
      }
  using Node = std::variant<Leaf<T>, Branch<T>>;
  return Branch<T>{
      (lower_size <= leaf_size)
          ? std::make_unique<Node>(Leaf<T>{ordered_by_dim_lower[split_dim]})
          : std::make_unique<Node>(
                create_branch(ordered_by_dim_lower, leaf_size)),
      (upper_size <= leaf_size)
          ? std::make_unique<Node>(Leaf<T>{ordered_by_dim_upper[split_dim]})
          : std::make_unique<Node>(
                create_branch(ordered_by_dim_upper, leaf_size)),
      split_dim, median};
}

} // namespace detail

template <class T>
KDTree<T>::KDTree(const std::vector<T> &objects, const std::size_t leaf_size) {
  if (objects.empty()) {
    this->root = Leaf<T>{};
    return;
  }
  const auto sorted = [](const std::vector<T> &objects, const std::size_t dim) {
    auto out = objects;
    std::sort(out.begin(), out.end(), [dim](const auto &a, const auto &b) {
      return traits::position(a)[dim] < traits::position(b)[dim];
    });
    return out;
  };
  constexpr std::size_t K = traits::Position<T>::DIMENSION;
  // Sort w.r.t. each dimension once at the begining:
  auto ordered_by_dim = std::array<std::vector<T>, K>{};
  for (std::size_t i = 0; i < K; i++)
    ordered_by_dim[i] = sorted(objects, i);
  if (objects.size() < leaf_size)
    this->root = Leaf<T>{ordered_by_dim[0]};
  else
    this->root = detail::create_branch(ordered_by_dim, leaf_size);
}

namespace detail {
template <class T>
void register_node(const Leaf<T> &leaf,
                   std::vector<const std::vector<T> *> &leafs,
                   std::vector<const Branch<T> *> &) {
  if (!leaf.objects.empty())
    leafs.emplace_back(&leaf.objects);
};
template <class T>
void register_node(const Branch<T> &branch,
                   std::vector<const std::vector<T> *> &,
                   std::vector<const Branch<T> *> &branches) {
  branches.emplace_back(&branch);
};

} // namespace detail

/**
 * @brief Return an interable iterating over all objects in the intersection
 * of a @ref KDTree instance with a `Shape` instance.
 * The iterator only will yield objects for which
 * `traits::have_overlap(shape, object) == true`.
 * The following traits must be implemented:
 * - @ref traits::HaveOverlap for `Shape` and `T`,
 * - @ref traits::BoundingBox for `Shape`.
 */
template <class T, class Shape>
IntersectionIterator<T, Shape> intersect(const KDTree<T> &tree,
                                         const Shape &shape) {
  auto leafs = std::vector<const std::vector<T> *>{};
  auto branches = std::vector<const Branch<T> *>{};
  const auto register_node = [&leafs, &branches](const auto &obj) {
    detail::register_node<T>(obj, leafs, branches);
  };
  std::visit(register_node, tree.root);
  auto box = traits::bounding_box(shape);

  while (!branches.empty()) {
    const auto &branch = *branches.back();
    branches.pop_back();
    if (box.lower[branch.split_dimension] < branch.split_value)
      std::visit(register_node, *branch.lower);
    if (box.upper[branch.split_dimension] >= branch.split_value)
      std::visit(register_node, *branch.upper);
  }
  return IntersectionIterator<T, Shape>(leafs, shape);
}

} // namespace qcmesh::geometry::kd_tree
