// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <array>

namespace qcmesh::geometry {

/**
 * @brief Similar to `operator<<`, but wraps bits pushed beyond the left most
 * bit position to the right most bit position.
 */
std::size_t circ_shift_l(std::size_t n, const std::size_t shift);

/**
 * @brief Hash algorithm for a triple of std::size_t.
 */
std::size_t hash_triple(const std::size_t a, const std::size_t b,
                        const std::size_t c);

/**
 * @brief Hash class for `std::array<T, 3>`, which can be used in
 * `std::unordered_map` / `std::unordered_set`.
 */
struct TripleHash {
  template <class T>
  std::size_t operator()(const std::array<T, 3> &triple) const {
    return hash_triple(static_cast<std::size_t>(triple[0]),
                       static_cast<std::size_t>(triple[1]),
                       static_cast<std::size_t>(triple[2]));
  }
};

} // namespace qcmesh::geometry
