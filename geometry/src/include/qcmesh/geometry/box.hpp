// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/traits/position.hpp"
#include <array>

namespace qcmesh::geometry {

/**
 * @brief Hyperrectangle in `Dimension` dimensions, determined by
 * `lower[i] <= x[i] <= upper[i]`, `0 <= i < Dimension`.
 */
template <std::size_t Dimension> struct Box {
  std::array<double, Dimension> lower{};
  std::array<double, Dimension> upper{};
};

/**
 * @brief Returns `true` if `p` is in the box, i.e.
 * `lower[i] <= p[i] <= upper[i]`, `0 <= i < Dimension`.
 */
template <std::size_t Dimension>
bool contains(const Box<Dimension> &box,
              const std::array<double, Dimension> &p) {
  for (std::size_t d = 0; d < Dimension; ++d)
    if (p[d] < box.lower[d] || p[d] > box.upper[d])
      return false;
  return true;
}

/**
 * @brief Constructs the bounding box containing a given container of positions.
 *
 * `objects` should be a container of items which implement @ref
 * traits::Position. If the container is empty, return an empty bounding
 * box (containing no points at all).
 */
template <class Container>
Box<traits::Position<typename std::remove_reference<
    typename Container::value_type>::type>::DIMENSION>
bounding_box(Container &objects) {
  constexpr std::size_t DIMENSION =
      traits::Position<typename Container::value_type>::DIMENSION;
  auto iter = objects.begin();
  const auto end = objects.end();
  if (iter == end) {
    auto lower = std::array<double, DIMENSION>{};
    auto upper = std::array<double, DIMENSION>{};
    upper.fill(-1.);
    return Box<DIMENSION>{lower, upper};
  }
  const auto &first = *(iter++);
  auto result =
      Box<DIMENSION>{traits::position(first), traits::position(first)};
  for (; iter != end; ++iter) {
    const auto &current = traits::position(*iter);
    for (std::size_t i = 0; i < DIMENSION; ++i) {
      if (current[i] < result.lower[i])
        result.lower[i] = current[i];
      if (current[i] > result.upper[i])
        result.upper[i] = current[i];
    }
  }
  return result;
}

} // namespace qcmesh::geometry
