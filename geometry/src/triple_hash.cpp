// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/triple_hash.hpp"
#include <limits>

namespace qcmesh::geometry {

std::size_t circ_shift_l(std::size_t n, const std::size_t shift) {
  constexpr std::size_t SIZE_BITS = std::numeric_limits<std::size_t>::digits;
  constexpr std::size_t MASK = SIZE_BITS - 1;
  static_assert(0 == (SIZE_BITS & MASK),
                "rotate value bit length must be a power of two");
  return (n << shift) | (n >> ((-shift) & MASK));
}

std::size_t hash_triple(const std::size_t a, const std::size_t b,
                        const std::size_t c) {
  // number of bits / 2 rounded down to an even number:
  constexpr std::size_t N_ROT =
      (std::size_t{std::numeric_limits<std::size_t>::digits / 3}) &
      (std::size_t{std::size_t{0} - 2});
  return a ^ circ_shift_l(b, N_ROT) ^ circ_shift_l(c, 2 * N_ROT);
}

} // namespace qcmesh::geometry
