// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author Prateek Gupta
 */

#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include <Eigen/Dense>
#include <cmath>
#include <limits>

namespace qcmesh::geometry {
namespace {

/**
 * @brief Jacobian (differences of vertices 1, 2 to vertex 0) of the unit
 * triangle (edge length 1)
 */
const std::array w2 = std::array{std::array<double, 2>{1.0, 0.5},
                                 std::array<double, 2>{0.0, sqrt(3.0) / 2.0}};

/**
 * @brief Jacobian (differences of vertices 1, 2, 3 to vertex 0) of the unit
 * simplex (edge length 1):
 */
const std::array w3 =
    std::array{std::array<double, 3>{1.0, 0.5, 0.5},
               std::array<double, 3>{0.0, sqrt(3.0) / 2.0, sqrt(3.0) / 6.0},
               std::array<double, 3>{0.0, 0.0, sqrt(2.0 / 3.0)}};

template <std::size_t Dimension>
double
simplex_quality_eigen(const Eigen::Matrix<double, Dimension, Dimension> &edges,
                      const Eigen::Matrix<double, Dimension, Dimension> &w) {
  const auto s = edges * w.inverse();
  auto edges_inverse = Eigen::Matrix<double, Dimension, Dimension>{};
  auto invertible = bool{};
  // Using computeInverseWithCheck to silence warning about singular matrices:
  edges.computeInverseWithCheck(edges_inverse, invertible);
  if (!invertible)
    return 0.;
  const auto s_inverse = w * edges_inverse;

  return Dimension / (s.norm() * s_inverse.norm());
}

} // namespace

double
simplex_quality(const std::array<std::array<double, 2>, 2 + 1> &simplex) {
  auto jac = Eigen::Matrix<double, 2, 2>{};

  jac(0, 0) = simplex[1][0] - simplex[0][0];
  jac(0, 1) = simplex[2][0] - simplex[0][0];
  jac(1, 0) = simplex[1][1] - simplex[0][1];
  jac(1, 1) = simplex[2][1] - simplex[0][1];

  return simplex_quality_eigen<2>(jac, array_ops::as_eigen_matrix(w2));
}

double simplex_quality(const std::array<std::array<double, 2>, 2> &jac) {
  return simplex_quality_eigen<2>(array_ops::as_eigen_matrix(jac).transpose(),
                                  array_ops::as_eigen_matrix(w2));
}

double
simplex_quality(const std::array<std::array<double, 3>, 3 + 1> &simplex) {
  auto jac = Eigen::Matrix<double, 3, 3>{};

  for (Eigen::Index iv = 0; iv < 3; iv++)
    for (Eigen::Index d = 0; d < 3; d++)
      jac(d, iv) = simplex[iv + 1][d] - simplex[0][d];

  return simplex_quality_eigen<3>(jac, array_ops::as_eigen_matrix(w3));
}

double simplex_quality(const std::array<std::array<double, 3>, 3> &jac) {
  return simplex_quality_eigen<3>(array_ops::as_eigen_matrix(jac).transpose(),
                                  array_ops::as_eigen_matrix(w3));
}

std::array<double, 2 + 1>
simplex_solid_angles(const std::array<std::array<double, 2>, 3> &simplex) {
  const auto ab =
      array_ops::as_eigen(simplex[1]) - array_ops::as_eigen(simplex[0]);
  const auto ac =
      array_ops::as_eigen(simplex[2]) - array_ops::as_eigen(simplex[0]);
  const auto bc =
      array_ops::as_eigen(simplex[2]) - array_ops::as_eigen(simplex[1]);
  const auto r_ab = ab.norm();
  const auto r_ac = ac.norm();
  const auto r_bc = bc.norm();
  return std::array<double, 2 + 1>{acos(ab.dot(ac) / (r_ab * r_ac)),
                                   acos(-ab.dot(bc) / (r_ab * r_bc)),
                                   acos(ac.dot(bc) / (r_ac * r_bc))};
}

std::array<double, 3 + 1>
simplex_solid_angles(const std::array<std::array<double, 3>, 4> &simplex) {
  const auto e_01 =
      array_ops::as_eigen(simplex[1]) - array_ops::as_eigen(simplex[0]);
  const auto e_02 =
      array_ops::as_eigen(simplex[2]) - array_ops::as_eigen(simplex[0]);
  const auto e_03 =
      array_ops::as_eigen(simplex[3]) - array_ops::as_eigen(simplex[0]);
  const auto e_12 =
      array_ops::as_eigen(simplex[2]) - array_ops::as_eigen(simplex[1]);
  const auto e_13 =
      array_ops::as_eigen(simplex[3]) - array_ops::as_eigen(simplex[1]);
  const auto e_23 =
      array_ops::as_eigen(simplex[3]) - array_ops::as_eigen(simplex[2]);
  const auto volume = std::abs(e_01.dot(e_02.cross(e_03)));
  const auto omega = [](const double vol, const auto &v1, const auto &v2,
                        const auto &v3) {
    const auto quot =
        vol / (v1.norm() * v2.norm() * v3.norm() + v1.dot(v2) * v3.norm() +
               v3.dot(v2) * v1.norm() + v1.dot(v3) * v2.norm());
    if (std::isnan(quot))
      return M_PI;
    if (quot < 0.)
      return 2.0 * atan(quot) + 2. * M_PI;
    return 2.0 * atan(quot);
  };
  return std::array<double, 3 + 1>{
      omega(volume, e_01, e_02, e_03), omega(volume, -e_01, e_12, e_13),
      omega(volume, -e_02, -e_12, e_23), omega(volume, -e_03, -e_13, -e_23)};
}

double
simplex_volume(const std::array<std::array<double, 2>, 2 + 1> &triangle) {
  return (triangle[0][0] * (triangle[1][1] - triangle[2][1]) +
          triangle[1][0] * (triangle[2][1] - triangle[0][1]) +
          triangle[2][0] * (triangle[0][1] - triangle[1][1])) /
         2.0;
}

double
simplex_volume(const std::array<std::array<double, 3>, 3 + 1> &tetrahedron) {
  const std::array<double, 3> &a = tetrahedron[1];
  const std::array<double, 3> &b = tetrahedron[2];
  const std::array<double, 3> &c = tetrahedron[3];
  const std::array<double, 3> &d = tetrahedron[0];

  return ((a[0] - d[0]) *
              ((b[1] - d[1]) * (c[2] - d[2]) - (b[2] - d[2]) * (c[1] - d[1])) +
          (a[1] - d[1]) *
              ((b[2] - d[2]) * (c[0] - d[0]) - (b[0] - d[0]) * (c[2] - d[2])) +
          (a[2] - d[2]) *
              ((b[0] - d[0]) * (c[1] - d[1]) - (b[1] - d[1]) * (c[0] - d[0]))) /
         6.0;
}

std::array<double, 3 + 1> simplex_eval_shape_functions(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const std::array<double, 3> &point) {
  auto values = std::array<double, 3 + 1>{};

  for (int i = 0; i < 4; i++)
    if (array_ops::squared_distance(simplex[i], point) < 1.0e-6 * 1.0e-6) {
      values[i] = 1.0;
      return values;
    }

  const double volume = simplex_volume(simplex);
  auto subvertices = std::array<std::array<double, 3>, 3 + 1>{
      point, simplex[1], simplex[2], simplex[3]};
  values[0] = simplex_volume(subvertices) / volume;
  for (unsigned int i = 1; i < 4; i++) {
    subvertices[i - 1] = simplex[i - 1];
    subvertices[i] = point;
    values[i] = simplex_volume(subvertices) / volume;
  }

  return values;
}

std::array<double, 3 + 1> simplex_eval_shape_functions_interior(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const std::array<double, 3> &point) {
  auto values = simplex_eval_shape_functions(simplex, point);
  if (!shape_function_values_represent_contained_point(values))
    values.fill(0.0);
  return values;
}

bool shape_function_values_represent_contained_point(
    const std::array<double, 3 + 1> &shape_values, const double tolerance) {
  return !std::any_of(begin(shape_values), end(shape_values),
                      [tolerance](double i) { return i < -tolerance; });
}

namespace {
namespace detail {

template <class T, std::size_t Dimension>
std::array<std::array<T, Dimension>, Dimension> simplex_jacobian(
    const std::array<std::array<T, Dimension>, Dimension + 1> &simplex) {
  auto jac = std::array<std::array<T, Dimension>, Dimension>{};
  for (std::size_t i = 1; i < Dimension + 1; i++)
    array_ops::as_eigen(jac[i - 1]) =
        array_ops::as_eigen(simplex[i]) - array_ops::as_eigen(simplex[0]);
  return jac;
}

template <std::size_t Dimension> double vol_per_solid_angle(const double r);
template <> double vol_per_solid_angle<3>(const double r) {
  return r * r * r / 3.;
}
template <> double vol_per_solid_angle<2>(const double r) { return r * r / 2.; }

template <std::size_t Dimension>
std::array<double, Dimension + 1> simplex_vertex_volumes_per_solid_angle(
    const std::array<std::array<double, Dimension>, Dimension + 1> &simplex,
    const double node_radius) {
  const auto jac = simplex_jacobian(simplex);
  const auto r_facet = simplex_distances_to_opposite_facet(jac);
  const auto r_vertex = simplex_distances_to_other_vertices(jac);
  auto vertex_volumes = std::array<double, Dimension + 1>{};
  for (std::size_t i = 0; i < vertex_volumes.size(); i++) {
    const auto r = std::min({r_facet[i], 0.5 * r_vertex[i], node_radius});
    vertex_volumes[i] = vol_per_solid_angle<Dimension>(r);
  }
  return vertex_volumes;
}

template <std::size_t Dimension>
SimplexMetrics<Dimension> simplex_metrics(
    const std::array<std::array<double, Dimension>, Dimension + 1> &simplex,
    const double max_vertex_radius) {
  auto metrics = SimplexMetrics<Dimension>{};
  metrics.solid_angles = simplex_solid_angles(simplex);
  metrics.vertex_volumes = geometry::simplex_vertex_volumes_per_solid_angle(
      simplex, max_vertex_radius);
  array_ops::as_eigen(metrics.vertex_volumes).array() *=
      array_ops::as_eigen(metrics.solid_angles).array();
  metrics.volume = simplex_volume(simplex);
  metrics.barycenter_volume =
      metrics.volume -
      array_ops::as_eigen(metrics.vertex_volumes).array().sum();
  return metrics;
}

} // namespace detail

} // namespace

std::array<std::array<double, 3>, 3>
simplex_jacobian(const std::array<std::array<double, 3>, 3 + 1> &simplex) {
  return detail::simplex_jacobian<double, 3>(simplex);
}
std::array<std::array<double, 2>, 2>
simplex_jacobian(const std::array<std::array<double, 2>, 2 + 1> &simplex) {
  return detail::simplex_jacobian<double, 2>(simplex);
}
std::array<std::array<int, 3>, 3>
simplex_jacobian(const std::array<std::array<int, 3>, 3 + 1> &simplex) {
  return detail::simplex_jacobian<int, 3>(simplex);
}
std::array<std::array<int, 2>, 2>
simplex_jacobian(const std::array<std::array<int, 2>, 2 + 1> &simplex) {
  return detail::simplex_jacobian<int, 2>(simplex);
}

/**
 * Uses:
 * dist(b, face(a,c,d)) = <(c-a) x (d-a), b-a> / \| (c-a) x (d-a) \|
 *                      = det(jac) / \| jac[1] x jac[2] \|
 * and with jac = [b-a, c-a, d-a]:
 * (c-b)x(d-b) = (jac[1]-jac[0])x(jac[2]-jac[0])
 *             = jac[1] x jac[2] + jac[0] x jac[1] + jac[2] x jac[0]
 */
std::array<double, 3 + 1> simplex_distances_to_opposite_facet(
    const std::array<std::array<double, 3>, 3> &jac) {
  const auto dual_1 =
      array_ops::as_eigen(jac[1]).cross(array_ops::as_eigen(jac[2]));
  const auto dual_2 =
      array_ops::as_eigen(jac[2]).cross(array_ops::as_eigen(jac[0]));
  const auto dual_3 =
      array_ops::as_eigen(jac[0]).cross(array_ops::as_eigen(jac[1]));
  const auto vol_parallelepiped =
      std::fabs(dual_1.dot(array_ops::as_eigen(jac[0])));
  return std::array<double, 3 + 1>{
      vol_parallelepiped / (dual_1 + dual_2 + dual_3).norm(),
      vol_parallelepiped / dual_1.norm(), vol_parallelepiped / dual_2.norm(),
      vol_parallelepiped / dual_3.norm()};
}

std::array<double, 2 + 1> simplex_distances_to_opposite_facet(
    const std::array<std::array<double, 2>, 2> &jac) {
  const auto area_parallogram = array_ops::as_eigen_matrix(jac).determinant();
  return std::array<double, 2 + 1>{
      area_parallogram /
          (array_ops::as_eigen(jac[1]) - array_ops::as_eigen(jac[0])).norm(),
      area_parallogram / array_ops::as_eigen(jac[1]).norm(),
      area_parallogram / array_ops::as_eigen(jac[0]).norm()};
}

std::array<double, 3 + 1> simplex_distances_to_other_vertices(
    const std::array<std::array<double, 3>, 3> &jac) {
  const auto d_01 = array_ops::norm(jac[0]);
  const auto d_02 = array_ops::norm(jac[1]);
  const auto d_03 = array_ops::norm(jac[2]);
  const auto d_12 = array_ops::distance(jac[0], jac[1]);
  const auto d_13 = array_ops::distance(jac[0], jac[2]);
  const auto d_23 = array_ops::distance(jac[1], jac[2]);
  return std::array<double, 3 + 1>{
      std::min({d_01, d_02, d_03}), std::min({d_01, d_12, d_13}),
      std::min({d_02, d_12, d_23}), std::min({d_03, d_13, d_23})};
}

std::array<double, 2 + 1> simplex_distances_to_other_vertices(
    const std::array<std::array<double, 2>, 2> &jac) {
  const auto d_01 = array_ops::norm(jac[0]);
  const auto d_02 = array_ops::norm(jac[1]);
  const auto d_12 = array_ops::distance(jac[0], jac[1]);
  return std::array<double, 2 + 1>{
      std::min({d_01, d_02}), std::min({d_01, d_12}), std::min({d_02, d_12})};
}

std::array<double, 3 + 1> simplex_vertex_volumes_per_solid_angle(
    const std::array<std::array<double, 3>, 3 + 1> &simplex,
    const double node_radius) {
  return detail::simplex_vertex_volumes_per_solid_angle(simplex, node_radius);
}
std::array<double, 2 + 1> simplex_vertex_volumes_per_solid_angle(
    const std::array<std::array<double, 2>, 2 + 1> &simplex,
    const double node_radius) {
  return detail::simplex_vertex_volumes_per_solid_angle(simplex, node_radius);
}

SimplexMetrics<3>
simplex_metrics(const std::array<std::array<double, 3>, 3 + 1> &simplex,
                const double max_vertex_radius) {
  return detail::simplex_metrics(simplex, max_vertex_radius);
}
SimplexMetrics<2>
simplex_metrics(const std::array<std::array<double, 2>, 2 + 1> &simplex,
                const double max_vertex_radius) {
  return detail::simplex_metrics(simplex, max_vertex_radius);
}

SimplexDistanceData3d prepare_simplex_distance_data(
    const std::array<std::array<double, 3>, 4> &points) {
  const auto make_tangent = [](const auto &a, const auto &d) {
    const auto s = d.norm();
    auto t = std::array<double, 3>{};
    array_ops::as_eigen(t) = d / s;
    const auto d_lower = array_ops::dot(a, t);
    const auto d_upper = d_lower + s;
    return Segment<3>{a, t, d_lower, d_upper};
  };
  const auto e01 =
      array_ops::as_eigen(points[1]) - array_ops::as_eigen(points[0]);
  const auto e02 =
      array_ops::as_eigen(points[2]) - array_ops::as_eigen(points[0]);
  const auto e03 =
      array_ops::as_eigen(points[3]) - array_ops::as_eigen(points[0]);
  const auto e12 =
      array_ops::as_eigen(points[2]) - array_ops::as_eigen(points[1]);
  const auto e23 =
      array_ops::as_eigen(points[3]) - array_ops::as_eigen(points[2]);
  const auto e13 =
      array_ops::as_eigen(points[3]) - array_ops::as_eigen(points[1]);
  auto edge_terminating_planes = std::array{
      make_tangent(points[0], e01), make_tangent(points[0], e02),
      make_tangent(points[0], e03), make_tangent(points[1], e12),
      make_tangent(points[2], e23), make_tangent(points[1], e13),
  };

  const auto prepare_frame = [](const auto &a, const auto &b, const auto &p0) {
    auto frame = FaceFrame{};
    array_ops::as_eigen(frame.plane.normal) = a.cross(b).normalized();
    const auto n = array_ops::as_eigen(frame.plane.normal);
    frame.plane.d = array_ops::as_eigen(p0).dot(n);

    array_ops::as_eigen(frame.p[0]) = b.cross(n);
    array_ops::as_eigen(frame.p[1]) = n.cross(a);
    const auto denominator = array_ops::as_eigen(frame.p[1]).dot(b);
    array_ops::as_eigen(frame.p[0]) /= denominator;
    array_ops::as_eigen(frame.p[1]) /= denominator;
    array_ops::as_eigen(frame.p_p0) =
        array_ops::as_eigen_matrix(frame.p) * array_ops::as_eigen(p0).matrix();
    return frame;
  };
  auto face_frames = std::array<FaceFrame, 4>{
      // 0-1-2
      prepare_frame(e02, e01, points[0]),
      // 0-2-3
      prepare_frame(e03, e02, points[0]),
      // 0-3-1
      prepare_frame(e01, e03, points[0]),
      // 1-2-3
      prepare_frame(e12, e13, points[1]),
  };

  return {points, edge_terminating_planes, face_frames};
}

namespace {

bool is_above_face(const FaceFrame &frame, const std::array<double, 3> &p) {
  auto s = std::array<double, 2>{};
  array_ops::as_eigen(s) =
      array_ops::as_eigen_matrix(frame.p) * array_ops::as_eigen(p).matrix() -
      array_ops::as_eigen(frame.p_p0);
  return s[0] >= 0. && s[1] >= 0. && s[0] + s[1] <= 1.;
};

} // namespace

double
simplex_signed_distance(const SimplexDistanceData3d &simplex_distance_data,
                        const std::array<double, 3> &p) {
  auto d = std::array<double, 4>{};
  auto d_max = std::numeric_limits<double>::lowest();
  auto pos = std::size_t{0};
  for (const auto &face : simplex_distance_data.face_frames) {
    d[pos] = array_ops::dot(face.plane.normal, p) - face.plane.d;
    if (d[pos] > d_max)
      d_max = d[pos];
    ++pos;
  }
  // Check if p is an interior point:
  if (d_max <= 0.)
    return d_max;
  // Check faces:
  for (std::size_t i = 0; i < simplex_distance_data.face_frames.size(); i++)
    if (d[i] >= 0. && is_above_face(simplex_distance_data.face_frames[i], p))
      return d[i];
  // Check edges:
  const auto is_between_planes = [](const auto &segment, const auto &p) {
    const auto d = array_ops::dot(segment.tangent, p);
    return segment.d_lower <= d && d <= segment.d_upper;
  };
  const auto distance_to_edge = [](const auto &edge_tangent,
                                   const auto &edge_start, const auto &p) {
    return (array_ops::as_eigen(p) - array_ops::as_eigen(edge_start))
        .cross(array_ops::as_eigen(edge_tangent))
        .norm();
  };
  auto d_min = std::numeric_limits<double>::max();
  for (const auto &segment : simplex_distance_data.segments)
    if (is_between_planes(segment, p)) {
      auto d = distance_to_edge(segment.tangent, segment.start, p);
      if (d < d_min)
        d_min = d;
    }
  // Check vertices:
  for (const auto &q : simplex_distance_data.points) {
    const auto d = array_ops::distance(q, p);
    if (d < d_min)
      d_min = d;
  }
  return d_min;
}

} // namespace qcmesh::geometry
