// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/octree.hpp"

namespace qcmesh::geometry::octree {

bool EuclideanNorm::norm_bounded_by(const std::array<double, 3> &v,
                                    const double norm) {
  return v[0] * v[0] + v[1] * v[1] + v[2] * v[2] <= norm * norm;
}

bool MaxNorm::norm_bounded_by(const std::array<double, 3> &v,
                              const double norm) {
  return std::abs(v[0]) <= norm && std::abs(v[1]) <= norm &&
         std::abs(v[2]) <= norm;
}

} // namespace qcmesh::geometry::octree
