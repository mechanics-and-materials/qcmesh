// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/intersect_balls_with_simplex.hpp"
#include <gmock/gmock.h>

namespace qcmesh::geometry {

bool operator==(const Ball<3> &a, const Ball<3> &b) {
  for (std::size_t i = 0; i < a.center.size(); i++)
    if (a.center[i] != b.center[i])
      return false;
  return a.radius == b.radius;
}

bool operator==(const Indexed<Ball<3>> &a, const Indexed<Ball<3>> &b) {
  return a.index == b.index && a.inner == b.inner;
}

std::ostream &operator<<(std::ostream &os, const Ball<3> &ball) {
  return os << "Ball((" << ball.center[0] << ',' << ball.center[1] << ','
            << ball.center[2] << "); r=" << ball.radius << ')';
}

std::ostream &operator<<(std::ostream &os, const Indexed<Ball<3>> &ball) {
  return os << '[' << ball.index << "]: " << ball.inner;
}

namespace {

template <class T, class Shape>
std::vector<T> collect(IntersectionIterator<T, Shape> &&iter) {
  auto vec = std::vector<T>{};
  for (const auto &item : iter)
    vec.push_back(item);
  return vec;
}

template <class T> std::vector<Indexed<T>> indexed(const std::vector<T> &vec) {
  auto vec_indexed = std::vector<Indexed<T>>{};
  auto counter = std::size_t{0};
  for (const auto &item : vec)
    vec_indexed.push_back(Indexed<T>{counter++, item});
  return vec_indexed;
}

std::vector<Ball<3>> grid(const double radius) {
  auto balls = std::vector<Ball<3>>{};
  for (int k = -5; k <= 5; k++)
    for (int j = -5; j <= 5; j++)
      for (int i = -5; i <= 5; i++)
        balls.emplace_back(
            Ball<3>{std::array<double, 3>{static_cast<double>(i),
                                          static_cast<double>(j),
                                          static_cast<double>(k)},
                    radius});
  return balls;
}

TEST(test_intersect_balls_with_simplex,
     intersect_balls_with_simplex_finds_correct_balls) {
  const auto radius = 0.5;
  const auto balls = indexed(grid(radius));
  const auto simplex = std::array{
      std::array<double, 3>{0.0, 0.0, 0.0},
      std::array<double, 3>{2.75, 0.0, 0.0},
      std::array<double, 3>{0.0, 2.75, 0.0},
      std::array<double, 3>{0.0, 0.0, 2.75},
  };
  const auto ball_tree = kd_tree::KDTree(balls, 3);
  const auto intersecting_balls =
      collect(intersect_balls_with_simplex(ball_tree, simplex, radius));
  const auto ball = [radius](const std::array<double, 3> &position,
                             const std::size_t index) {
    return Indexed<Ball<3>>{index, {position, radius}};
  };
  const auto expected_intersecting_balls = std::vector{
      ball({0., 0., 0.}, 665), ball({1., 0., 0.}, 666),
      ball({2., 0., 0.}, 667), ball({3., 0., 0.}, 668),
      ball({0., 1., 0.}, 676), ball({1., 1., 0.}, 677),
      ball({2., 1., 0.}, 678), ball({0., 2., 0.}, 687),
      ball({1., 2., 0.}, 688), ball({0., 3., 0.}, 698),
      ball({0., 0., 1.}, 786), ball({1., 0., 1.}, 787),
      ball({2., 0., 1.}, 788), ball({0., 1., 1.}, 797),
      ball({1., 1., 1.}, 798), ball({0., 2., 1.}, 808),
      ball({0., 0., 2.}, 907), ball({1., 0., 2.}, 908),
      ball({0., 1., 2.}, 918), ball({0., 0., 3.}, 1028),
  };
  EXPECT_THAT(intersecting_balls,
              testing::UnorderedElementsAreArray(expected_intersecting_balls));
}

TEST(test_intersect_balls_with_simplex, max_radius_is_0_for_empty_vec) {
  const auto max_ball_radius = max_radius(std::vector<Indexed<Ball<3>>>{});
  EXPECT_EQ(max_ball_radius, 0);
}

TEST(test_intersect_balls_with_simplex, max_radius_is_correct_for_3_balls) {
  const auto balls =
      std::vector<Indexed<Ball<3>>>{Indexed<Ball<3>>{0, {{0., 0., 0.}, 0.125}},
                                    Indexed<Ball<3>>{1, {{0., 0., 0.}, 0.5}},
                                    Indexed<Ball<3>>{2, {{0., 0., 0.}, 0.125}}};
  const auto max_ball_radius = max_radius(balls);
  EXPECT_EQ(max_ball_radius, 0.5);
}

} // namespace
} // namespace qcmesh::geometry
