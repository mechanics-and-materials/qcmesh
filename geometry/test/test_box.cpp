// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/box.hpp"
#include <gmock/gmock.h>
#include <vector>

namespace qcmesh::geometry {
namespace {

constexpr double TOLERANCE = 0.0;

using Point = std::array<double, 3>;

TEST(test_box, bounding_box_works_for_empty_list) {
  const auto points = std::vector<Point>{};
  const auto box = bounding_box(points);
  EXPECT_FALSE(contains(box, box.lower));
  EXPECT_FALSE(contains(box, box.upper));
}
TEST(test_box, bounding_box_works_for_one_point) {
  const auto points = std::vector<Point>{Point{0.0, 1.0, 2.0}};
  const auto box = bounding_box(points);
  EXPECT_DOUBLE_EQ(box.lower[0], 0.0);
  EXPECT_DOUBLE_EQ(box.lower[1], 1.0);
  EXPECT_DOUBLE_EQ(box.lower[2], 2.0);
  EXPECT_DOUBLE_EQ(box.upper[0], 0.0);
  EXPECT_DOUBLE_EQ(box.upper[1], 1.0);
  EXPECT_DOUBLE_EQ(box.upper[2], 2.0);
}
TEST(test_box, bounding_box_works_for_multiple_points) {
  const auto points = std::vector<Point>{
      Point{0.0, 1.0, 2.0}, Point{1.0, -1.0, -2.0}, Point{5.0, 0.0, 0.0}};
  const auto box = bounding_box(points);
  EXPECT_DOUBLE_EQ(box.lower[0], 0.0);
  EXPECT_DOUBLE_EQ(box.lower[1], -1.0);
  EXPECT_DOUBLE_EQ(box.lower[2], -2.0);
  EXPECT_DOUBLE_EQ(box.upper[0], 5.0);
  EXPECT_DOUBLE_EQ(box.upper[1], 1.0);
  EXPECT_DOUBLE_EQ(box.upper[2], 2.0);
}

TEST(test_box, box_contains_interior_points) {
  const auto box = Box<3>{Point{0., 0., 0.}, Point{1., 1., 1.}};
  EXPECT_TRUE(contains(box, box.lower));
  EXPECT_TRUE(contains(box, box.upper));
  EXPECT_TRUE(contains(box, Point{0.5, 0.5, 0.5}));
}

TEST(test_box, box_does_not_contain_outer_point) {
  const auto box = Box<3>{Point{0., 0., 0.}, Point{1., 1., 1.}};
  EXPECT_FALSE(contains(box, Point{0.5, 0.5, -0.5}));
}

TEST(test_box, empty_box_does_not_contain_boundary) {
  const auto box = Box<3>{Point{0., 0., 0.}, Point{-1., 1., 1.}};
  EXPECT_FALSE(contains(box, box.lower));
  EXPECT_FALSE(contains(box, box.upper));
}

} // namespace
} // namespace qcmesh::geometry
