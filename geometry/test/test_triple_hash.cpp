// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/triple_hash.hpp"
#include <gmock/gmock.h>
#include <limits>

namespace qcmesh::geometry {
namespace {

std::size_t circ_shift_r(std::size_t n, std::size_t shift) {
  constexpr std::size_t SIZE_BITS = std::numeric_limits<std::size_t>::digits;
  constexpr std::size_t MASK = SIZE_BITS - 1;
  static_assert(0 == (SIZE_BITS & MASK),
                "rotate value bit length must be a power of two");
  return (n >> shift) | (n << ((-shift) & MASK));
}

TEST(test_triple_hash, circ_shift_routines_commute) {
  const auto n_rot = 10;
  EXPECT_EQ(circ_shift_r(circ_shift_l(1, n_rot), n_rot), 1);
  EXPECT_EQ(circ_shift_l(circ_shift_r(1, n_rot), n_rot), 1);
}

TEST(test_triple_hash,
     triple_hash_produces_different_hashes_for_first_100_integers) {
  auto hashes = std::set<std::size_t>{};
  for (std::size_t i = 0; i < 100; i += 10)
    for (std::size_t j = 0; j < 100; j += 10)
      for (std::size_t k = 0; k < 100; k += 10)
        hashes.emplace(TripleHash{}(std::array{i, j, k}));
  EXPECT_EQ(hashes.size(), 1000);
}

} // namespace
} // namespace qcmesh::geometry
