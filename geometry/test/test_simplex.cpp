// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <cmath>
#include <gmock/gmock.h>
#include <limits>
#include <string_view>

namespace qcmesh::geometry {
namespace {

using Simplex3D = std::array<std::array<double, 3>, 3 + 1>;
using Simplex2D = std::array<std::array<double, 2>, 2 + 1>;

const Simplex3D unit_simplex{
    std::array<double, 3>{0., 0., 0.}, std::array<double, 3>{1.0, 0.0, 0.0},
    std::array<double, 3>{0.5, sqrt(3.0) / 2.0, 0.0},
    std::array<double, 3>{0.5, sqrt(3.0) / 6.0, sqrt(2.0 / 3.0)}};

const Simplex3D coordinate_simplex{
    std::array<double, 3>{0., 0., 0.}, std::array<double, 3>{1., 0., 0.},
    std::array<double, 3>{0., 1., 0.}, std::array<double, 3>{0., 0., 1.}};
const Simplex2D coordinate_triangle =
    Simplex2D{std::array<double, 2>{0., 0.}, std::array<double, 2>{1., 0.},
              std::array<double, 2>{0., 1.}};

struct TestSimplex : ::testing::Test {
  Simplex3D simplex{coordinate_simplex};
};

TEST_F(TestSimplex,
       simplex_eval_shape_functions_computes_unit_vectors_for_points_on_edges) {
  for (unsigned int i = 0; i < simplex.size(); i++) {
    const auto shape_function_values =
        simplex_eval_shape_functions(simplex, simplex[i]);
    for (unsigned int j = 0; j < simplex.size(); j++) {
      EXPECT_DOUBLE_EQ(shape_function_values[j], ((i == j) ? 1. : 0.));
    }
  }
}

TEST_F(TestSimplex, simplex_eval_shape_functions_computes_correct_values) {
  const auto shape_function_values = simplex_eval_shape_functions(
      simplex, std::array<double, 3>{0.25, 0.25, 0.25});
  // Volume of simplex: 1/6
  // Volume of simplex after replacing (0,0,1) with (1,1,1)/4: 1/24
  // => 1/4 of the original volume. Due to symmetry: same volume for (1,0,0),
  // (0,1,0).
  // => The remaining part of the simplex also has to have volume 1/4.
  EXPECT_DOUBLE_EQ(shape_function_values[0], 0.25);
  EXPECT_DOUBLE_EQ(shape_function_values[1], 0.25);
  EXPECT_DOUBLE_EQ(shape_function_values[2], 0.25);
  EXPECT_DOUBLE_EQ(shape_function_values[3], 0.25);
}

TEST_F(TestSimplex,
       shape_function_values_represent_contained_point_detect_outer_point) {
  const auto shape_function_values = simplex_eval_shape_functions(
      simplex, std::array<double, 3>{0.5, 0.5, -0.1});
  const auto inside_simplex =
      shape_function_values_represent_contained_point(shape_function_values);
  EXPECT_FALSE(inside_simplex);
}

TEST_F(TestSimplex, simplex_eval_shape_functions_interior_detect_outer_point) {
  const auto shape_function_values = simplex_eval_shape_functions_interior(
      simplex, std::array<double, 3>{0.5, 0.5, -0.1});
  EXPECT_DOUBLE_EQ(shape_function_values[0], 0.);
  EXPECT_DOUBLE_EQ(shape_function_values[1], 0.);
  EXPECT_DOUBLE_EQ(shape_function_values[2], 0.);
  EXPECT_DOUBLE_EQ(shape_function_values[3], 0.);
}

TEST_F(
    TestSimplex,
    shape_function_values_represent_contained_point_respects_vertices_as_interior_points) {
  for (const auto &s : simplex) {
    const auto inside_simplex = shape_function_values_represent_contained_point(
        simplex_eval_shape_functions(simplex, s));
    EXPECT_TRUE(inside_simplex);
  }
}

std::array<std::array<double, 3>, 3> tail(const Simplex3D &simplex) {
  return std::array<std::array<double, 3>, 3>{simplex[1], simplex[2],
                                              simplex[3]};
}
std::array<std::array<double, 2>, 2> tail(const Simplex2D &simplex) {
  return std::array<std::array<double, 2>, 2>{simplex[1], simplex[2]};
}

TEST(test_simplex_quality_3d, is_1_for_unit_simplex) {
  EXPECT_DOUBLE_EQ(simplex_quality(unit_simplex), 1.);
  const auto jacobian = tail(unit_simplex);
  EXPECT_DOUBLE_EQ(simplex_quality(jacobian), 1.);
}
TEST(test_simplex_quality_3d, is_1_for_scaled_unit_simplex) {
  const auto scaled_unit_simplex = Simplex3D{
      std::array<double, 3>{0., 0., 0.}, std::array<double, 3>{0.1, 0.0, 0.0},
      std::array<double, 3>{0.1 * 0.5, 0.1 * sqrt(3.0) / 2.0, 0.0},
      std::array<double, 3>{0.1 * 0.5, 0.1 * sqrt(3.0) / 6.0,
                            0.1 * sqrt(2.0 / 3.0)}};
  EXPECT_DOUBLE_EQ(simplex_quality(scaled_unit_simplex), 1.);
  const auto jacobian = tail(scaled_unit_simplex);
  EXPECT_DOUBLE_EQ(simplex_quality(jacobian), 1.);
}
TEST(test_simplex_quality_3d, is_0_for_degenerate_simplex) {
  const auto flat_simplex = Simplex3D{
      std::array<double, 3>{0., 0., 0.}, std::array<double, 3>{1.0, 0.0, 0.0},
      std::array<double, 3>{0.5, sqrt(3.0) / 2.0, 0.0},
      std::array<double, 3>{0.5, sqrt(3.0) / 6.0, 0.0}};
  EXPECT_NEAR(simplex_quality(flat_simplex), 0.,
              std::numeric_limits<double>::epsilon());
  const auto jacobian = tail(flat_simplex);
  EXPECT_NEAR(simplex_quality(jacobian), 0.,
              std::numeric_limits<double>::epsilon());
}

TEST(test_simplex_quality_2d, is_1_for_unit_triangle) {
  const auto unit_triangle =
      Simplex2D{std::array<double, 2>{0., 0.}, std::array<double, 2>{1.0, 0.0},
                std::array<double, 2>{0.5, sqrt(3.0) / 2.0}};
  EXPECT_DOUBLE_EQ(simplex_quality(unit_triangle), 1.);
  const auto jacobian = tail(unit_triangle);
  EXPECT_DOUBLE_EQ(simplex_quality(jacobian), 1.);
}
TEST(test_simplex_quality_2d, is_1_for_scaled_unit_triangle) {
  const auto scaled_unit_triangle =
      Simplex2D{std::array<double, 2>{0., 0.}, std::array<double, 2>{0.1, 0.0},
                std::array<double, 2>{0.1 * 0.5, 0.1 * sqrt(3.0) / 2.0}};
  EXPECT_DOUBLE_EQ(simplex_quality(scaled_unit_triangle), 1.);
  const auto jacobian = tail(scaled_unit_triangle);
  EXPECT_DOUBLE_EQ(simplex_quality(jacobian), 1.);
}
TEST(test_simplex_quality_2d, is_0_for_degenerate_triangle) {
  const auto flat_triangle =
      Simplex2D{std::array<double, 2>{0., 0.}, std::array<double, 2>{1.0, 0.0},
                std::array<double, 2>{0.5, 0.0}};
  EXPECT_NEAR(simplex_quality(flat_triangle), 0.,
              std::numeric_limits<double>::epsilon());
  const auto jacobian = tail(flat_triangle);
  EXPECT_NEAR(simplex_quality(jacobian), 0.,
              std::numeric_limits<double>::epsilon());
}
TEST(test_quality_residual, is_inf_for_inf) {
  const auto result =
      geometry::quality_residual(std::numeric_limits<double>::infinity());
  EXPECT_DOUBLE_EQ(result, std::numeric_limits<double>::infinity());
}
TEST(test_quality_residual, is_inf_for_0) {
  const auto result = geometry::quality_residual(0.);
  EXPECT_DOUBLE_EQ(result, std::numeric_limits<double>::infinity());
}
TEST(test_quality_residual, is_0_for_1) {
  const auto result = geometry::quality_residual(1.);
  EXPECT_DOUBLE_EQ(result, 0.);
}

struct TestSimplexVolume3D
    : ::testing::TestWithParam<std::tuple<Simplex3D, double>> {};
TEST_P(TestSimplexVolume3D, computes_correct_volume) {
  const auto [simplex, expected_volume] = GetParam();
  const auto volume = simplex_volume(simplex);
  EXPECT_DOUBLE_EQ(volume, expected_volume);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexVolume3D,
    ::testing::Values(
        // Flat simplex:
        std::make_tuple(Simplex3D{std::array<double, 3>{0., 0., 0.},
                                  std::array<double, 3>{1., 0., 0.},
                                  std::array<double, 3>{0., 1., 0.},
                                  std::array<double, 3>{0.25, 0.25, 0.}},
                        0.),
        std::make_tuple(Simplex3D{std::array<double, 3>{0., 0., 0.},
                                  std::array<double, 3>{1., 0., 0.},
                                  std::array<double, 3>{0., 1., 0.},
                                  std::array<double, 3>{0., 0., 1.}},
                        1. / 6.),
        // unit simplex:
        std::make_tuple(unit_simplex, sqrt(2.) / 12.)));

struct TestSimplexVolume2D
    : ::testing::TestWithParam<std::tuple<Simplex2D, double>> {};
TEST_P(TestSimplexVolume2D, computes_correct_volume) {
  const auto [simplex, expected_volume] = GetParam();
  const auto volume = simplex_volume(simplex);
  EXPECT_DOUBLE_EQ(volume, expected_volume);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexVolume2D,
    ::testing::Values(
        // flat triangle:
        std::make_tuple(Simplex2D{std::array<double, 2>{0., 0.},
                                  std::array<double, 2>{1., 0.},
                                  std::array<double, 2>{0.5, 0.}},
                        0.),
        std::make_tuple(coordinate_triangle, 1. / 2.),
        // unit triangle:
        std::make_tuple(Simplex2D{std::array<double, 2>{0., 0.},
                                  std::array<double, 2>{1.0, 0.0},
                                  std::array<double, 2>{0.5, sqrt(3.0) / 2.0}},
                        sqrt(3.) / 4.)));

struct TestSimplexSolidAngle2D
    : ::testing::TestWithParam<std::tuple<Simplex2D, std::array<double, 3>>> {};
TEST_P(TestSimplexSolidAngle2D, computes_correct_angle) {
  const auto [simplex, expected_angles] = GetParam();
  const auto angles = simplex_solid_angles(simplex);
  EXPECT_DOUBLE_EQ(angles[0], expected_angles[0]);
  EXPECT_DOUBLE_EQ(angles[1], expected_angles[1]);
  EXPECT_DOUBLE_EQ(angles[2], expected_angles[2]);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexSolidAngle2D,
    ::testing::Values(
        // unit triangle:
        std::make_tuple(Simplex2D{std::array<double, 2>{0., 0.},
                                  std::array<double, 2>{1., 0.},
                                  std::array<double, 2>{0.5, sqrt(3.0) / 2.0}},
                        std::array<double, 3>{M_PI / 3., M_PI / 3., M_PI / 3.}),
        std::make_tuple(coordinate_triangle,
                        std::array<double, 3>{M_PI_2, M_PI / 4., M_PI / 4.})));

struct TestSimplexSolidAngle3D
    : ::testing::TestWithParam<std::tuple<Simplex3D, std::array<double, 4>>> {};
TEST_P(TestSimplexSolidAngle3D, computes_correct_angle) {
  const auto [simplex, expected_angles] = GetParam();
  const auto angles = simplex_solid_angles(simplex);
  EXPECT_NEAR(angles[0], expected_angles[0], 1.E-15);
  EXPECT_NEAR(angles[1], expected_angles[1], 1.E-15);
  EXPECT_NEAR(angles[2], expected_angles[2], 1.E-15);
  EXPECT_NEAR(angles[3], expected_angles[3], 1.E-15);
}

// See: https://en.wikipedia.org/wiki/Solid_angle#Tetrahedron
const double alpha = std::acos(23. / 27.);
const double alpha_2 = 2. * std::atan(3 - 2 * sqrt(2));

INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexSolidAngle3D,
    ::testing::Values(
        // unit simplex:
        std::make_tuple(unit_simplex,
                        std::array<double, 4>{alpha, alpha, alpha, alpha}),
        // simplex, spanned by 3 orthonormal vectors:
        std::make_tuple(coordinate_simplex,
                        std::array<double, 4>{M_PI_2, alpha_2, alpha_2,
                                              alpha_2})));
/**
 * @brief Constructs a simplex having one vertex at the origin
 * and the other 3 vertices symmetrically distributed on the intersection
 * circle of the unit sphere and the cone with the positive z-axis and
 * opening angle theta.
 */
Simplex3D axial_symmetric_simplex(const double cos_theta,
                                  const double sin_theta) {
  // phi = 120°:
  const auto cos_phi = -0.5;
  const auto sin_phi = 0.5 * sqrt(3.0);
  return {std::array<double, 3>{0., 0., 0.},
          std::array<double, 3>{sin_theta, 0., cos_theta},
          std::array<double, 3>{sin_theta * cos_phi, sin_theta * sin_phi,
                                cos_theta},
          std::array<double, 3>{sin_theta * cos_phi, -sin_theta * sin_phi,
                                cos_theta}};
}

/**
 * At sin(theta)^2 = 8/9, the denominator term vanishes
 */
TEST(test_simplex_solidangle_3d, computes_correct_critical_angle) {
  const auto critical_simplex =
      axial_symmetric_simplex(1. / 3., 2. * M_SQRT2 / 3.);
  const auto solid_angle = simplex_solid_angles(critical_simplex)[0];
  EXPECT_NEAR(solid_angle, M_PI, 1.E-15);
}
/**
 * For 8/9 < sin(theta)^2 < 1, the solid angle should be in the interval
 * (\pi, 2 \pi).
 * Note: 2\pi corresponds to the upper half sphere.
 */
TEST(test_simplex_solidangle_3d, computes_correct_over_critical_angle) {
  const auto sin_theta_2 = 15. / 16.;
  const auto over_critical_simplex =
      axial_symmetric_simplex(0.25, sqrt(sin_theta_2));
  const auto solid_angle = simplex_solid_angles(over_critical_simplex)[0];
  EXPECT_GT(solid_angle, M_PI);
  EXPECT_LT(solid_angle, 2. * M_PI);
}

TEST(test_simplex, simplex_jacobian_is_correct_for_3d_coordinate_simplex) {
  const auto jac = simplex_jacobian(coordinate_simplex);
  const auto expected_jac = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{0., 1., 0.},
      std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(jac, qcmesh::testing::DoubleMatrixEq(expected_jac));
}
TEST(test_simplex,
     simplex_jacobian_is_correct_for_translated_3d_coordinate_simplex) {
  const auto simplex = Simplex3D{
      std::array<double, 3>{1., 1., 1.}, std::array<double, 3>{2., 1., 1.},
      std::array<double, 3>{1., 2., 1.}, std::array<double, 3>{1., 1., 2.}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{0., 1., 0.},
      std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(jac, qcmesh::testing::DoubleMatrixEq(expected_jac));
}
TEST(test_simplex, simplex_jacobian_is_correct_for_2d_coordinate_simplex) {
  const auto simplex = std::array<std::array<double, 2>, 3>{
      std::array<double, 2>{0., 0.}, std::array<double, 2>{1., 0.},
      std::array<double, 2>{0., 1.}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac = std::array<std::array<double, 2>, 2>{
      std::array<double, 2>{1., 0.}, std::array<double, 2>{0., 1.}};
  EXPECT_THAT(jac, qcmesh::testing::DoubleMatrixEq(expected_jac));
}
TEST(test_simplex,
     simplex_jacobian_is_correct_for_translated_2d_coordinate_simplex) {
  const auto simplex = std::array<std::array<double, 2>, 3>{
      std::array<double, 2>{1., 1.}, std::array<double, 2>{2., 1.},
      std::array<double, 2>{1., 2.}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac = std::array<std::array<double, 2>, 2>{
      std::array<double, 2>{1., 0.}, std::array<double, 2>{0., 1.}};
  EXPECT_THAT(jac, qcmesh::testing::DoubleMatrixEq(expected_jac));
}
TEST(test_simplex, simplex_jacobian_is_correct_for_3d_int_coordinate_simplex) {
  const auto coordinate_simplex =
      std::array{std::array<int, 3>{0, 0, 0}, std::array<int, 3>{1, 0, 0},
                 std::array<int, 3>{0, 1, 0}, std::array<int, 3>{0, 0, 1}};
  const auto jac = simplex_jacobian(coordinate_simplex);
  const auto expected_jac =
      std::array{std::array<int, 3>{1, 0, 0}, std::array<int, 3>{0, 1, 0},
                 std::array<int, 3>{0, 0, 1}};
  EXPECT_THAT(jac, ::testing::Eq(expected_jac));
}
TEST(test_simplex,
     simplex_jacobian_is_correct_for_translated_3d_int_coordinate_simplex) {
  const auto simplex =
      std::array{std::array<int, 3>{1, 1, 1}, std::array<int, 3>{2, 1, 1},
                 std::array<int, 3>{1, 2, 1}, std::array<int, 3>{1, 1, 2}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac =
      std::array{std::array<int, 3>{1, 0, 0}, std::array<int, 3>{0, 1, 0},
                 std::array<int, 3>{0, 0, 1}};
  EXPECT_THAT(jac, ::testing::Eq(expected_jac));
}
TEST(test_simplex, simplex_jacobian_is_correct_for_2d_int_coordinate_simplex) {
  const auto simplex =
      std::array{std::array<int, 2>{0, 0}, std::array<int, 2>{1, 0},
                 std::array<int, 2>{0, 1}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac =
      std::array{std::array<int, 2>{1, 0}, std::array<int, 2>{0, 1}};
  EXPECT_THAT(jac, ::testing::Eq(expected_jac));
}
TEST(test_simplex,
     simplex_jacobian_is_correct_for_translated_2d_int_coordinate_simplex) {
  const auto simplex =
      std::array{std::array<int, 2>{1, 1}, std::array<int, 2>{2, 1},
                 std::array<int, 2>{1, 2}};
  const auto jac = simplex_jacobian(simplex);
  const auto expected_jac =
      std::array{std::array<int, 2>{1, 0}, std::array<int, 2>{0, 1}};
  EXPECT_THAT(jac, ::testing::Eq(expected_jac));
}

template <std::size_t Size, typename T>
std::array<T, Size> cycle_array(const std::array<T, Size> &arr,
                                const std::size_t shift) {
  auto cycled = std::array<T, Size>{};
  for (std::size_t i = 0; i < Size - shift; i++)
    cycled[i] = arr[i + shift];
  for (std::size_t i = Size - shift; i < Size; i++)
    cycled[i] = arr[i + shift - Size];
  return cycled;
}

TEST(test_simplex,
     simplex_distances_to_opposite_facet_is_correct_for_3d_coordinate_simplex) {
  for (std::size_t shift = 0; shift < coordinate_simplex.size(); shift++) {
    const auto jac = simplex_jacobian(cycle_array(coordinate_simplex, shift));
    const auto distances = simplex_distances_to_opposite_facet(jac);
    const auto expected_distances = cycle_array(
        std::array<double, 4>{1. / std::sqrt(3.), 1., 1., 1.}, shift);
    EXPECT_THAT(distances, qcmesh::testing::DoubleArrayEq(expected_distances));
  }
}
TEST(test_simplex,
     simplex_distances_to_opposite_facet_is_correct_for_2d_coordinate_simplex) {
  for (std::size_t shift = 0; shift < coordinate_triangle.size(); shift++) {
    const auto jac = simplex_jacobian(cycle_array(coordinate_triangle, shift));

    const auto distances = simplex_distances_to_opposite_facet(jac);
    const auto expected_distances =
        cycle_array(std::array<double, 3>{M_SQRT2 / 2., 1., 1.}, shift);
    EXPECT_THAT(distances, qcmesh::testing::DoubleArrayEq(expected_distances));
  }
}

TEST(test_simplex,
     simplex_distances_to_other_vertices_is_correct_for_3d_coordinate_simplex) {
  const auto simplex = Simplex3D{
      std::array<double, 3>{0., 0., 0.}, std::array<double, 3>{1., 0., 0.},
      std::array<double, 3>{0., 2., 0.}, std::array<double, 3>{0., 0., 3.}};
  for (std::size_t shift = 0; shift < simplex.size(); shift++) {
    const auto jac = simplex_jacobian(cycle_array(simplex, shift));
    const auto distances = simplex_distances_to_other_vertices(jac);
    const auto expected_distances =
        cycle_array(std::array<double, 4>{1., 1., 2., 3.}, shift);
    EXPECT_THAT(distances, qcmesh::testing::DoubleArrayEq(expected_distances));
  }
}
TEST(test_simplex,
     simplex_distances_to_other_vertices_is_correct_for_2d_coordinate_simplex) {
  const auto simplex =
      Simplex2D{std::array<double, 2>{0., 0.}, std::array<double, 2>{1., 0.},
                std::array<double, 2>{0., 2.}};
  for (std::size_t shift = 0; shift < simplex.size(); shift++) {
    const auto jac = simplex_jacobian(cycle_array(simplex, shift));
    const auto distances = simplex_distances_to_other_vertices(jac);
    const auto expected_distances =
        cycle_array(std::array<double, 3>{1., 1., 2.}, shift);
    EXPECT_THAT(distances, qcmesh::testing::DoubleArrayEq(expected_distances));
  }
}

template <std::size_t Size>
std::array<double, Size> repeat(const double value) {
  auto arr = std::array<double, Size>{};
  for (auto &item : arr)
    item = value;
  return arr;
}

struct TestSimplexVolumePerSolidAngle3D
    : ::testing::TestWithParam<
          std::tuple<Simplex3D, double, std::array<double, 4>>> {};

TEST_P(
    TestSimplexVolumePerSolidAngle3D,
    simplex_vertex_volumes_per_solid_angle_is_correct_for_3d_coordinate_simplex) {
  const auto [simplex, node_radius, expected_volume] = GetParam();
  const auto volumes =
      simplex_vertex_volumes_per_solid_angle(simplex, node_radius);
  EXPECT_THAT(volumes, qcmesh::testing::DoubleArrayEq(expected_volume));
}
const double r3 = std::sqrt(5.) / 4.;
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexVolumePerSolidAngle3D,
    ::testing::Values(
        std::make_tuple(coordinate_simplex, 1.,
                        repeat<4>(0.5 * 0.5 * 0.5 / 3.)),
        std::make_tuple(coordinate_simplex, 0.25,
                        repeat<4>(0.25 * 0.25 * 0.25 / 3.)),
        // Side lenght of triangle in xy plane: sqrt(3)
        // 1 vertex is closest to opposite face,
        // other vertices are closest to middle points of edges.
        std::make_tuple(
            Simplex3D{std::array<double, 3>{0., 0., 0.5},
                      std::array<double, 3>{1., 0., 0.},
                      std::array<double, 3>{-0.5, -std::sqrt(3.) / 2., 0.},
                      std::array<double, 3>{-0.5, std::sqrt(3.) / 2., 0.}},
            1.0,
            std::array<double, 4>{0.5 * 0.5 * 0.5 / 3., r3 *r3 *r3 / 3.,
                                  r3 *r3 *r3 / 3., r3 *r3 *r3 / 3.})));

struct TestSimplexVolumePerSolidAngle2D
    : ::testing::TestWithParam<
          std::tuple<Simplex2D, double, std::array<double, 3>>> {};

TEST_P(
    TestSimplexVolumePerSolidAngle2D,
    simplex_vertex_volumes_per_solid_angle_is_correct_for_2d_coordinate_simplex) {
  const auto [simplex, node_radius, expected_volume] = GetParam();
  const auto volumes =
      simplex_vertex_volumes_per_solid_angle(simplex, node_radius);
  EXPECT_THAT(volumes, qcmesh::testing::DoubleArrayEq(expected_volume));
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexVolumePerSolidAngle2D,
    ::testing::Values(
        std::make_tuple(coordinate_triangle, 1., repeat<3>(0.5 * 0.5 / 2.)),
        std::make_tuple(coordinate_triangle, 0.25, repeat<3>(0.25 * 0.25 / 2.)),
        // First point is closest to face, points 2, 3 are closest to middle
        // points of sides:
        std::make_tuple(Simplex2D{std::array<double, 2>{0., 0.5},
                                  std::array<double, 2>{-1., 0.},
                                  std::array<double, 2>{1.0, 0.}},
                        1.0, std::array<double, 3>{0.125, 0.15625, 0.15625})));

struct TestSimplexMetrics3D
    : ::testing::TestWithParam<std::tuple<Simplex3D, double, double>> {};
TEST_P(TestSimplexMetrics3D, simplex_metrics_barycenter_volume_is_correct) {
  const auto [simplex, node_radius, expected_barycenter_volume] = GetParam();
  const auto metrics = simplex_metrics(simplex, node_radius);
  EXPECT_NEAR(metrics.barycenter_volume, expected_barycenter_volume, 1.E-8);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexMetrics3D,
    ::testing::Values(
        std::make_tuple(coordinate_simplex, 1., 0.0587372),
        std::make_tuple(coordinate_simplex, 0.25, 0.15317548),
        std::make_tuple(
            Simplex3D{std::array<double, 3>{0., 0., 0.5},
                      std::array<double, 3>{1., 0., 0.},
                      std::array<double, 3>{-0.5, -std::sqrt(3.) / 2., 0.},
                      std::array<double, 3>{-0.5, std::sqrt(3.) / 2., 0.}},
            1.0, 0.075330048)));

struct TestSimplexMetrics2D
    : ::testing::TestWithParam<std::tuple<Simplex2D, double, double>> {};
TEST_P(TestSimplexMetrics2D, simplex_metrics_barycenter_volume_is_correct) {
  const auto [simplex, node_radius, expected_barycenter_volume] = GetParam();
  const auto metrics = simplex_metrics(simplex, node_radius);
  EXPECT_NEAR(metrics.barycenter_volume, expected_barycenter_volume, 1.E-8);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexMetrics2D,
    ::testing::Values(std::make_tuple(coordinate_triangle, 1., 0.107300918),
                      std::make_tuple(coordinate_triangle, 0.25, 0.4018252296),
                      std::make_tuple(Simplex2D{std::array<double, 2>{0., 0.5},
                                                std::array<double, 2>{-1., 0.},
                                                std::array<double, 2>{1.0, 0.}},
                                      1.0, 0.07832294)));

TEST(test_simplex, simplex_signed_distance_works_for_non_unit_simplex) {
  const auto p = std::array<double, 3>{4., 3., 4.};
  const auto simplex = std::array{
      std::array<double, 3>{1.0, 0.0, 0.0},
      std::array<double, 3>{3.75, 0.0, 0.0},
      std::array<double, 3>{1.0, 2.75, 0.0},
      std::array<double, 3>{1.0, 0.0, 2.75},
  };
  const auto simplex_distance_data = prepare_simplex_distance_data(simplex);
  const auto distance = simplex_signed_distance(simplex_distance_data, p);
  const auto expected_distance = 7.25 / std::sqrt(3.);
  EXPECT_DOUBLE_EQ(distance, expected_distance);
}

struct TestSimplexSignedDistance
    : ::testing::TestWithParam<
          std::tuple<std::string_view, std::array<double, 3>, double>> {};
TEST_P(TestSimplexSignedDistance, simplex_signed_distance_works) {
  const auto [_, p, expected_distance] = GetParam();
  const auto simplex = std::array{
      std::array<double, 3>{0.0, 0.0, 0.0},
      std::array<double, 3>{1.0, 0.0, 0.0},
      std::array<double, 3>{0.0, 1.0, 0.0},
      std::array<double, 3>{0.0, 0.0, 1.0},
  };
  const auto simplex_distance_data = prepare_simplex_distance_data(simplex);
  const auto distance = simplex_signed_distance(simplex_distance_data, p);
  EXPECT_DOUBLE_EQ(distance, expected_distance);
}
INSTANTIATE_TEST_CASE_P(
    Simplex, TestSimplexSignedDistance,
    ::testing::Values(
        std::make_tuple(std::string_view{"interior"},
                        std::array<double, 3>{0.25, 0.25, 0.25},
                        std::sqrt(3.) * (0.25 - 1. / 3.)),
        std::make_tuple(std::string_view{"0_1_2"},
                        std::array<double, 3>{0.25, 0.25, -0.5}, 0.5),
        std::make_tuple(std::string_view{"0_1_3"},
                        std::array<double, 3>{0.25, -0.5, 0.25}, 0.5),
        std::make_tuple(std::string_view{"0_2_3"},
                        std::array<double, 3>{-0.5, 0.25, 0.25}, 0.5),
        std::make_tuple(std::string_view{"1_2_3"},
                        std::array<double, 3>{1.0, 1.0, 1.0},
                        std::sqrt(3.) * 2. / 3.),
        std::make_tuple(std::string_view{"0_1"},
                        std::array<double, 3>{0.5, -1.0, -1.0}, std::sqrt(2.)),
        std::make_tuple(std::string_view{"0_2"},
                        std::array<double, 3>{-1.0, 0.5, -1.0}, std::sqrt(2.)),
        std::make_tuple(std::string_view{"0_3"},
                        std::array<double, 3>{-1.0, -1.0, 0.5}, std::sqrt(2.)),
        std::make_tuple(std::string_view{"1_2"},
                        std::array<double, 3>{1.0, 1.0, 0.0},
                        std::sqrt(2.0) / 2.0),
        std::make_tuple(std::string_view{"2_3"},
                        std::array<double, 3>{1.0, 0.0, 1.0},
                        std::sqrt(2.0) / 2.0),
        std::make_tuple(std::string_view{"3_1"},
                        std::array<double, 3>{0.0, 1.0, 1.0},
                        std::sqrt(2.0) / 2.0),
        std::make_tuple(std::string_view{"0"},
                        std::array<double, 3>{-1.0, -1.0, -1.0},
                        std::sqrt(3.0)),
        std::make_tuple(std::string_view{"1"},
                        std::array<double, 3>{2.0, -1.0, 0.0}, std::sqrt(2.0)),
        std::make_tuple(std::string_view{"2"},
                        std::array<double, 3>{-1.0, 2.0, 0.0}, std::sqrt(2.0)),
        std::make_tuple(std::string_view{"3"},
                        std::array<double, 3>{0.0, -1.0, 2.0}, std::sqrt(2.0))),
    [](const ::testing::TestParamInfo<TestSimplexSignedDistance::ParamType>
           &info) { return std::string{std::get<0>(info.param)}; });

} // namespace
} // namespace qcmesh::geometry
