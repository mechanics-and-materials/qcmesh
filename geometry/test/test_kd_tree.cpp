// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/kd_tree.hpp"
#include <gmock/gmock.h>

namespace qcmesh::geometry::kd_tree {
namespace {

TEST(test_kd_tree, kd_tree_works_for_empty_point_set) {
  const auto points = std::vector<std::array<double, 3>>{};
  const auto tree = KDTree(points, 10);
  constexpr double INF = std::numeric_limits<double>::infinity();
  const auto intersection =
      intersect(tree, Box<3>{std::array<double, 3>{-INF, -INF, -INF},
                             std::array<double, 3>{INF, INF, INF}});
  EXPECT_TRUE(intersection.empty());
}

template <class T, class Shape>
std::vector<T> collect(IntersectionIterator<T, Shape> &&iter) {
  auto vec = std::vector<T>{};
  for (const auto &item : iter)
    vec.push_back(item);
  return vec;
}

TEST(test_kd_tree, kd_tree_works_for_one_point) {
  const auto points = std::vector{std::array<double, 3>{1., 1., 1.}};
  const auto tree = KDTree(points, 1);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{std::array<double, 3>{0.5, 0.5, 0.5},
                                     std::array<double, 3>{1.5, 1.5, 1.5}}));
  EXPECT_THAT(intersection_points, testing::ContainerEq(points));
}

TEST(test_kd_tree, kd_tree_works_for_many_points) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{1.5, 1.5, 1.5}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_kd_tree, kd_tree_works_for_3_equal_points) {
  using P = std::array<double, 3>;
  const auto points = std::vector{P{0., 0., 0.}, P{0., 0., 0.}, P{0., 0., 0.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{0.5, 0.5, 0.5}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_kd_tree, kd_tree_works_for_many_equal_points_at_lower_bound) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{0., 0., 0.}, P{0., 0., 0.}, P{1., 0., 0.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-1.0, -1.0, -1.0}, P{2.0, 2.0, 2.0}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_kd_tree, kd_tree_works_for_many_equal_points_at_upper_bound) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{1., 0., 0.}, P{1., 0., 0.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-1.0, -1.0, -1.0}, P{2.0, 2.0, 2.0}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_kd_tree, kd_tree_works_for_many_points_and_empty_intersection) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points = collect(
      intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{-0.25, -0.25, -0.25}}));
  EXPECT_THAT(intersection_points, testing::IsEmpty());
}

TEST(test_kd_tree, kd_tree_works_for_many_points_and_partial_intersection) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = KDTree(points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{0.5, -0.5, 0.5}, P{1.5, 1.5, 1.5}}));
  const auto expected_intersection_points =
      std::vector{P{1., 0., 1.}, P{1., 1., 1.}};
  EXPECT_THAT(intersection_points,
              testing::UnorderedElementsAreArray(expected_intersection_points));
}

} // namespace
} // namespace qcmesh::geometry::kd_tree
