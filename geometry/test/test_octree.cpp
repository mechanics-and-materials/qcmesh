// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/indexed.hpp"
#include "qcmesh/geometry/octree.hpp"
#include <gmock/gmock.h>

namespace qcmesh::geometry {
template <class T> bool operator==(const Indexed<T> &a, const Indexed<T> &b) {
  return a.index == b.index && a.inner == b.inner;
}
} // namespace qcmesh::geometry

namespace qcmesh::geometry::octree {
namespace {

TEST(test_octree, octree_works_for_empty_point_set) {
  const auto points = std::vector<std::array<double, 3>>{};
  const auto tree = Octree(Box<3>{}, points);
  constexpr double INF = std::numeric_limits<double>::infinity();
  const auto intersection =
      intersect(tree, Box<3>{std::array<double, 3>{-INF, -INF, -INF},
                             std::array<double, 3>{INF, INF, INF}});
  EXPECT_TRUE(intersection.empty());
}

template <class T, class Shape>
std::vector<T> collect(IntersectionIterator<T, Shape> &&iter) {
  auto vec = std::vector<T>{};
  for (const auto &item : iter)
    vec.push_back(item);
  return vec;
}

template <class T, class Shape>
std::vector<std::size_t>
collect_indices(IntersectionIterator<T, Shape> &&iter) {
  auto indices = std::vector<std::size_t>{};
  for (const auto &obj : iter)
    indices.push_back(obj.index);
  return indices;
};

TEST(test_octree, octree_works_for_one_point) {
  const auto points = std::vector{std::array<double, 3>{1., 1., 1.}};
  const auto tree = Octree(bounding_box(points), points, 1);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{std::array<double, 3>{0.5, 0.5, 0.5},
                                     std::array<double, 3>{1.5, 1.5, 1.5}}));
  EXPECT_THAT(intersection_points, testing::ContainerEq(points));
}

TEST(test_octree, octree_works_for_many_points) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{1.5, 1.5, 1.5}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_octree, octree_works_for_3_equal_points) {
  using P = std::array<double, 3>;
  const auto points = std::vector{P{0., 0., 0.}, P{0., 0., 0.}, P{0., 0., 0.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{0.5, 0.5, 0.5}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_octree, octree_works_for_many_equal_points_at_lower_bound) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{0., 0., 0.}, P{0., 0., 0.}, P{1., 0., 0.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-1.0, -1.0, -1.0}, P{2.0, 2.0, 2.0}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_octree, octree_works_for_many_equal_points_at_upper_bound) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{1., 0., 0.}, P{1., 0., 0.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{-1.0, -1.0, -1.0}, P{2.0, 2.0, 2.0}}));
  EXPECT_THAT(intersection_points, testing::UnorderedElementsAreArray(points));
}

TEST(test_octree, octree_works_for_many_points_and_empty_intersection) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points = collect(
      intersect(tree, Box<3>{P{-0.5, -0.5, -0.5}, P{-0.25, -0.25, -0.25}}));
  EXPECT_THAT(intersection_points, testing::IsEmpty());
}

TEST(test_octree, octree_works_for_many_points_and_partial_intersection) {
  using P = std::array<double, 3>;
  const auto points =
      std::vector{P{0., 0., 0.}, P{1., 0., 0.}, P{0., 1., 0.}, P{1., 1., 0.},
                  P{0., 0., 1.}, P{1., 0., 1.}, P{0., 1., 1.}, P{1., 1., 1.}};
  const auto tree = Octree(bounding_box(points), points, 2);
  const auto intersection_points =
      collect(intersect(tree, Box<3>{P{0.5, -0.5, 0.5}, P{1.5, 1.5, 1.5}}));
  const auto expected_intersection_points =
      std::vector{P{1., 0., 1.}, P{1., 1., 1.}};
  EXPECT_THAT(intersection_points,
              testing::UnorderedElementsAreArray(expected_intersection_points));
}

TEST(test_octree,
     octree_throws_exception_when_trying_to_insert_outside_domain) {
  auto tree = Octree<std::array<double, 3>>(Box<3>{{0., 0., 0.}, {1., 1., 1.}});
  EXPECT_ANY_THROW(tree.insert(std::array<double, 3>{2., 2., 2.}));
}

TEST(test_octree, octree_insert_unique_works) {
  using P = std::array<double, 3>;
  auto tree = Octree<P>(Box<3>{{0., 0., 0.}, {1., 1., 1.}});
  EXPECT_TRUE(tree.insert_unique({0.5, 0.5, 0.5}, 1.));
  EXPECT_EQ(tree.size(), 1);
  EXPECT_TRUE(tree.insert_unique({0.75, 0.75, 0.75}, 0.125));
  EXPECT_EQ(tree.size(), 2);
  EXPECT_FALSE(tree.insert_unique({0.25, 0.25, 0.25}, 0.25));
  EXPECT_FALSE(tree.insert_unique({0.25, 0.25, 0.25}, 0.5));
  EXPECT_FALSE(tree.insert_unique({10.0, 10.0, 10.0}, 0.5));
  EXPECT_EQ(tree.size(), 2);
  const auto intersection_points = collect(intersect(tree, tree.domain));
  const auto expected_intersection_points =
      std::vector{P{0.5, 0.5, 0.5}, P{0.75, 0.75, 0.75}};
  EXPECT_THAT(intersection_points,
              testing::UnorderedElementsAreArray(expected_intersection_points));
}

TEST(test_octree, octree_find_or_insert_works_with_indexed) {
  using P = std::array<double, 3>;
  auto tree = Octree<Indexed<P>>(Box<3>{{0., 0., 0.}, {1., 1., 1.}});
  EXPECT_TRUE(tree.find_or_insert(Indexed<P>{0, P{0.5, 0.5, 0.5}}, 1.).empty());
  EXPECT_EQ(tree.size(), 1);
  EXPECT_TRUE(
      tree.find_or_insert(Indexed<P>{1, P{0.75, 0.75, 0.75}}, 0.125).empty());
  EXPECT_EQ(tree.size(), 2);

  {
    const auto indices = collect_indices(
        tree.find_or_insert(Indexed<P>{2, P{0.275, 0.275, 0.275}}, 0.25));
    EXPECT_THAT(indices, testing::ContainerEq(std::vector<std::size_t>{0}));
  }
  {
    const auto indices = collect_indices(
        tree.find_or_insert(Indexed<P>{3, P{0.8, 0.8, 0.8}}, 0.125));
    EXPECT_THAT(indices, testing::ContainerEq(std::vector<std::size_t>{1}));
  }
  EXPECT_TRUE(
      tree.find_or_insert(Indexed<P>{4, P{10., 10., 10.}}, 0.125).empty());

  EXPECT_EQ(tree.size(), 2);
  const auto intersection_points = collect(intersect(tree, tree.domain));
  const auto expected_intersection_points = std::vector{
      Indexed<P>{0, P{0.5, 0.5, 0.5}}, Indexed<P>{1, P{0.75, 0.75, 0.75}}};
  EXPECT_THAT(intersection_points,
              testing::UnorderedElementsAreArray(expected_intersection_points));
}

TEST(test_octree, octree_find_finds_point_from_outside_the_domain) {
  using P = std::array<double, 3>;
  const auto p = P{0.875, 0.875, 0.875};
  auto tree = Octree<P>(Box<3>{{0., 0., 0.}, {1., 1., 1.}}, std::vector{p});
  const auto intersection_points =
      collect(tree.find(P{0.875, 0.875, 1.125}, 0.5));
  const auto expected_intersection_points = std::vector{p};
  EXPECT_THAT(intersection_points,
              testing::UnorderedElementsAreArray(expected_intersection_points));
}

} // namespace
} // namespace qcmesh::geometry::octree
