// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <array>
#include <gmock/gmock.h>

namespace qcmesh::testing {
namespace detail {
/**
 * @brief A factory for compare functions.
 *
 * For Given epsilon return a function comparing 2 doubles with tolerance
 * epsilon.
 */
inline auto make_cmp_with_tolerance(const double epsilon) {
  return [epsilon](const double value, const double reference,
                   ::testing::MatchResultListener *const stream) noexcept {
    return ::testing::ExplainMatchResult(
        ::testing::DoubleNear(reference, epsilon), value, stream);
  };
}
/**
 * @brief Returns true if and only if value is approximately equal to the
 * reference.
 */
inline bool
cmp_almost_eq(const double value, const double reference,
              ::testing::MatchResultListener *const stream) noexcept {
  return ::testing::ExplainMatchResult(::testing::DoubleEq(reference), value,
                                       stream);
}
/**
 * @brief Returns true if and only if value is approximately equal to the
 * reference, with respect to the compare function cmp.
 */
template <std::size_t Dimension, class F>
inline bool
double_array_near(const std::array<double, Dimension> &value,
                  const std::array<double, Dimension> &reference, const F &cmp,
                  ::testing::MatchResultListener *const stream) noexcept {
  for (std::size_t i = 0; i < Dimension; i++)
    if (!cmp(value[i], reference[i], stream))
      return false;
  return true;
}

/**
 * @brief Returns true if and only if value is approximately equal to the
 * reference, with respect to the compare function cmp.
 */
template <std::size_t Dimension, class F>
inline bool double_matrix_near(
    const std::array<std::array<double, Dimension>, Dimension> &value,
    const std::array<std::array<double, Dimension>, Dimension> &reference,
    const F &cmp, ::testing::MatchResultListener *const stream) noexcept {
  for (std::size_t i = 0; i < Dimension; i++)
    if (!double_array_near(value[i], reference[i], cmp, stream))
      return false;
  return true;
}

/**
 * @brief Returns true if and only if value is approximately equal to the
 * reference, with respect to the compare function cmp.
 */
template <std::size_t Dimension, class F>
inline bool double_array_vec_near(
    const std::vector<std::array<double, Dimension>> &value,
    const std::vector<std::array<double, Dimension>> &reference, const F &cmp,
    ::testing::MatchResultListener *const stream) noexcept {
  if (!::testing::ExplainMatchResult(::testing::Eq(reference.size()),
                                     value.size(), stream))
    return false;
  for (std::size_t i = 0; i < value.size(); i++)
    if (!double_array_near(value[i], reference[i], cmp, stream))
      return false;
  return true;
}

} // namespace detail

/**
 * @brief Check that the value has almost the same values as reference.
 */
MATCHER_P(DoubleArrayEq, reference,
          "value " + std::string(negation ? "not " : "") + "equal to " +
              ::testing::PrintToString(reference)) {
  return detail::double_array_near(arg, reference, detail::cmp_almost_eq,
                                   result_listener);
}

/**
 * @brief Check that the value has almost the same values as reference.
 */
MATCHER_P(DoubleMatrixEq, reference,
          "value " + std::string(negation ? "not " : "") + "equal to " +
              ::testing::PrintToString(reference)) {
  return detail::double_matrix_near(arg, reference, detail::cmp_almost_eq,
                                    result_listener);
}

/**
 * @brief Check that the value has almost the same values as reference.
 */
MATCHER_P(DoubleVecArrayEq, reference,
          "value " + std::string(negation ? "not " : "") + "equal to " +
              ::testing::PrintToString(reference)) {
  return detail::double_array_vec_near(arg, reference, detail::cmp_almost_eq,
                                       result_listener);
}

/**
 * @brief Check that the value has almost the same values as reference,
 * within a tolerance of epsilon.
 */
MATCHER_P2(DoubleArrayNear, reference, epsilon,
           "value " + std::string(negation ? "not " : "") + "equal to " +
               ::testing::PrintToString(reference)) {
  return detail::double_array_near(arg, reference,
                                   detail::make_cmp_with_tolerance(epsilon),
                                   result_listener);
}

/**
 * @brief Check that the value has almost the same values as reference,
 * within a tolerance of epsilon.
 */
MATCHER_P2(DoubleMatrixNear, reference, epsilon,
           "value " + std::string(negation ? "not " : "") + "equal to " +
               ::testing::PrintToString(reference)) {
  return detail::double_matrix_near(arg, reference,
                                    detail::make_cmp_with_tolerance(epsilon),
                                    result_listener);
}

/**
 * @brief Check that the value has almost the same values as reference,
 * within a tolerance of epsilon.
 */
MATCHER_P2(DoubleVecArrayNear, reference, epsilon,
           "value " + std::string(negation ? "not " : "") + "equal to " +
               ::testing::PrintToString(reference)) {
  return detail::double_array_vec_near(arg, reference,
                                       detail::make_cmp_with_tolerance(epsilon),
                                       result_listener);
}

} // namespace qcmesh::testing
