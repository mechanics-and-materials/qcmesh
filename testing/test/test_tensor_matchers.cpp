// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::testing {

namespace {

TEST(test_tensor_matchers, double_array_eq_works) {
  const auto a = std::array<double, 3>{1., 2., 3.};
  const auto b = std::array<double, 3>{2., 2., 3.};
  EXPECT_THAT(a, DoubleArrayEq(a));
  EXPECT_THAT(a, ::testing::Not(DoubleArrayEq(b)));
}

TEST(test_tensor_matchers, double_array_near_works) {
  const auto a = std::array<double, 3>{1., 2., 3.};
  const auto b = std::array<double, 3>{1.25, 2., 3.};
  EXPECT_THAT(a, DoubleArrayNear(a, 0.125));
  EXPECT_THAT(a, ::testing::Not(DoubleArrayNear(b, 0.125)));
  EXPECT_THAT(a, DoubleArrayNear(b, 0.375));
}

TEST(test_tensor_matchers, double_matrix_eq_works) {
  const auto a = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto b = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{0., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  EXPECT_THAT(a, DoubleMatrixEq(a));
  EXPECT_THAT(a, ::testing::Not(DoubleMatrixEq(b)));
}

TEST(test_tensor_matchers, double_matrix_near_works) {
  const auto a = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto b = std::array<std::array<double, 3>, 3>{
      std::array<double, 3>{1.25, 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  EXPECT_THAT(a, DoubleMatrixNear(a, 0.125));
  EXPECT_THAT(a, ::testing::Not(DoubleMatrixNear(b, 0.125)));
  EXPECT_THAT(a, DoubleMatrixNear(b, 0.375));
}

TEST(test_tensor_matchers, double_array_vec_eq_works) {
  const auto a = std::vector<std::array<double, 3>>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto b = std::vector<std::array<double, 3>>{
      std::array<double, 3>{0., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto c = std::vector<std::array<double, 3>>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.}};
  EXPECT_THAT(a, DoubleVecArrayEq(a));
  EXPECT_THAT(a, ::testing::Not(DoubleVecArrayEq(b)));
  EXPECT_THAT(a, ::testing::Not(DoubleVecArrayEq(c)));
}

TEST(test_tensor_matchers, double_array_vec_near_works) {
  const auto a = std::vector<std::array<double, 3>>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto b = std::vector<std::array<double, 3>>{
      std::array<double, 3>{1.25, 2., 3.}, std::array<double, 3>{4., 5., 6.},
      std::array<double, 3>{7., 8., 9.}};
  const auto c = std::vector<std::array<double, 3>>{
      std::array<double, 3>{1., 2., 3.}, std::array<double, 3>{4., 5., 6.}};
  EXPECT_THAT(a, DoubleVecArrayNear(a, 0.125));
  EXPECT_THAT(a, ::testing::Not(DoubleVecArrayNear(b, 0.125)));
  EXPECT_THAT(a, DoubleVecArrayNear(b, 0.375));
  EXPECT_THAT(a, ::testing::Not(DoubleVecArrayNear(c, 0.125)));
  EXPECT_THAT(a, ::testing::Not(DoubleVecArrayNear(c, 0.375)));
}

} // namespace
} // namespace qcmesh::testing
