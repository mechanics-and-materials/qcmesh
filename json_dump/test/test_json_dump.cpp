// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/json_dump/json_dump.hpp"
#include <gmock/gmock.h>
#include <sstream>

namespace qcmesh::json_dump {

namespace {

TEST(test_json_dump, json_dump_works_for_int) {
  auto stream = std::stringstream{};
  stream << as_json(int{1});
  EXPECT_EQ(stream.str(), "1");
}

TEST(test_json_dump, json_dump_works_for_bool) {
  auto stream = std::stringstream{};
  stream << as_json(true) << "!=" << as_json(false);
  EXPECT_EQ(stream.str(), "true!=false");
}

TEST(test_json_dump, json_dump_works_for_strings) {
  auto stream = std::stringstream{};
  stream << as_json("...") << as_json(std::string_view{"___"})
         << as_json(std::string{"---"});
  EXPECT_EQ(stream.str(), "\"...\"\"___\"\"---\"");
}

TEST(test_json_dump, json_dump_works_for_double) {
  auto stream = std::stringstream{};
  stream << as_json(1.0);
  EXPECT_EQ(stream.str(), "1.0");
}

TEST(test_json_dump, json_dump_works_for_variant) {
  auto stream = std::stringstream{};
  stream << as_json(std::variant<int, bool>{1});
  EXPECT_EQ(stream.str(), "1");
}

TEST(test_json_dump, json_dump_works_for_optional) {
  auto stream = std::stringstream{};
  stream << as_json(std::optional<bool>{false})
         << "!=" << as_json(std::optional<bool>{});
  EXPECT_EQ(stream.str(), "false!=null");
}

TEST(test_json_dump, json_dump_works_for_vector) {
  auto stream = std::stringstream{};
  stream << as_json(std::vector{1, 2, 3});
  EXPECT_EQ(stream.str(), "[1,2,3]");
}

TEST(test_json_dump, json_dump_works_for_array) {
  auto stream = std::stringstream{};
  stream << as_json(std::array{1, 2, 3});
  EXPECT_EQ(stream.str(), "[1,2,3]");
}

TEST(test_json_dump, json_dump_works_for_objects) {
  auto stream = std::stringstream{};
  stream << json_obj().attr("a", 1).attr("b", 2).attr("c", 3);
  EXPECT_EQ(stream.str(), "{\"a\":1,\"b\":2,\"c\":3}");
}

} // namespace
} // namespace qcmesh::json_dump
