// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief json serialisation.
 */

#pragma once

#include <array>
#include <optional>
#include <ostream>
#include <sstream>
#include <tuple>
#include <variant>
#include <vector>

namespace qcmesh::json_dump {

/**
 * @brief Wrapper struct which provides operator<< to serialize as json.
 */
template <class T> struct AsJson {
  const T &value;
};

/**
 * @brief Wrap an object in a @ref AsJson so operator<< can be used to serialize
 * as json.
 */
template <class T> AsJson<T> as_json(const T &value) {
  return AsJson<T>{value};
}

/**
 * @brief Representation of a json object `{"key": "val", ...}`.
 *
 * Objects can be built by using @ref json_obj() and @ref JsonObject::attr.
 * Example:
 * ```c++
 * json_obj().attr("key", "val").attr("other_key", "other_value");
 * ```
 */
template <class... T> struct JsonObject {
  std::tuple<std::tuple<std::string_view, T>...> value;

  template <class U>
  JsonObject<T..., U> attr(const std::string_view &key, const U &val) {
    return JsonObject<T..., U>{std::tuple_cat(
        this->value, std::make_tuple(std::make_tuple(key, val)))};
  }
};

/**
 * @brief Create an empty json object `{}` which can be extended by @ref
 * JsonObject::attr.
 */
inline JsonObject<> json_obj() { return JsonObject<>{}; }

template <class T>
std::ostream &operator<<(std::ostream &os, const AsJson<T> &json) {
  return os << json.value;
}

inline std::ostream &operator<<(std::ostream &os, const AsJson<bool> &json) {
  if (json.value)
    return os << "true";
  return os << "false";
}

inline std::ostream &operator<<(std::ostream &os, const AsJson<double> &json) {
  auto stream = std::stringstream{};
  stream << json.value;
  os << stream.str();
  if (stream.str().find('.') == std::string::npos)
    os << ".0";
  return os;
}

template <class T>
std::ostream &operator<<(std::ostream &os,
                         const AsJson<std::optional<T>> &json) {
  if (json.value.has_value())
    return os << as_json(json.value.value());
  return os << "null";
}

template <class... T>
std::ostream &operator<<(std::ostream &os,
                         const AsJson<std::variant<T...>> &json) {
  std::visit([&os](const auto &value) { os << as_json(value); }, json.value);
  return os;
}

namespace detail {

template <class C>
std::ostream &print_list(std::ostream &os, const C &iterable) {
  os << '[';
  for (std::size_t i = 0; i < iterable.size(); i++) {
    os << as_json(iterable[i]);
    if (i + 1 < iterable.size())
      os << ',';
  }
  os << ']';
  return os;
}

} // namespace detail

inline std::ostream &operator<<(std::ostream &os,
                                const AsJson<std::string_view> &json) {
  return os << '"' << json.value << '"';
}

inline std::ostream &operator<<(std::ostream &os,
                                const AsJson<std::string> &json) {
  return os << as_json(std::string_view(json.value));
}

template <std::size_t N>
// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays)
std::ostream &operator<<(std::ostream &os, const AsJson<char[N]> &json) {
  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-array-to-pointer-decay,hicpp-no-array-decay)
  return os << as_json(std::string_view(json.value));
}

template <class T>
std::ostream &operator<<(std::ostream &os, const AsJson<std::vector<T>> &json) {
  return detail::print_list(os, json.value);
}

template <class T, std::size_t N>
std::ostream &operator<<(std::ostream &os,
                         const AsJson<std::array<T, N>> &json) {
  return detail::print_list(os, json.value);
}

namespace detail {

template <std::size_t I = 0, class... T>
void print_tuple(std::ostream &os, const JsonObject<T...> &json) {
  if constexpr (I > 0)
    os << ',';
  const auto &[key, val] = std::get<I>(json.value);
  os << as_json(key) << ':' << as_json(val);
  if constexpr (I + 1 < std::tuple_size<decltype(json.value)>{})
    print_tuple<I + 1, T...>(os, json);
}
} // namespace detail

template <std::size_t I = 0, class... T>
std::ostream &operator<<(std::ostream &os, const JsonObject<T...> &json) {
  os << '{';
  detail::print_tuple(os, json);
  os << '}';
  return os;
}

} // namespace qcmesh::json_dump
