// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/array_ops/array_ops.hpp"
#include <cmath>
#include <gmock/gmock.h>
#include <memory>
#include <sstream>

namespace qcmesh::array_ops {
namespace {

struct TestArrayOps : testing::Test {};

TEST_F(TestArrayOps, arithmetics_work) {
  const auto x = std::array<double, 3>{{1., 0., 0.}};
  const auto y = std::array<double, 3>{{0., 1., 0.}};
  auto z = std::array<double, 3>{{0., 0., 1.}};

  array_ops::as_eigen(z) = array_ops::as_eigen(x) + array_ops::as_eigen(y);
  EXPECT_DOUBLE_EQ(z[0], 1.);
  EXPECT_DOUBLE_EQ(z[1], 1.);
  EXPECT_DOUBLE_EQ(z[2], 0.);
}

TEST_F(TestArrayOps, as_const_array_does_not_create_dangling_reference) {
  const auto *temp_data = (const double *)nullptr;
  auto temp_array = std::make_unique<std::array<double, 1>>(std::array{1.});
  const auto create_temp_array = [&]() {
    const std::array<double, 1> &temp = *temp_array;
    temp_data = temp.data();
    return temp;
  };
  const auto copy = array_ops::as_eigen(create_temp_array());
  temp_array.reset(nullptr);
  EXPECT_DOUBLE_EQ(copy[0], 1.);
  EXPECT_NE(copy.data(), temp_data);
}

TEST_F(TestArrayOps, distance_works) {
  const auto p1 = std::array<double, 3>{3., 3., 3.};
  const auto p2 = std::array<double, 3>{3., 0., -1.};
  const auto distance = array_ops::distance(p1, p2);
  EXPECT_DOUBLE_EQ(distance, 5.);
}

TEST_F(TestArrayOps, squared_distance_works) {
  const auto p1 = std::array<double, 3>{1., 1., 1.};
  const auto p2 = std::array<double, 3>{1., 0., -1.};
  const auto squared_distance = array_ops::squared_distance(p1, p2);
  EXPECT_DOUBLE_EQ(squared_distance, 5.);
}

TEST_F(TestArrayOps, dot_works) {
  const auto p1 = std::array<double, 3>{1., 1., 1.};
  const auto p2 = std::array<double, 3>{2., 0., -1.};
  const auto inner_product = array_ops::dot(p1, p2);
  EXPECT_DOUBLE_EQ(inner_product, 1.);
}

TEST_F(TestArrayOps, norm_works) {
  const auto p = std::array<double, 3>{0., 3., 4.};
  const auto norm = array_ops::norm(p);
  EXPECT_DOUBLE_EQ(norm, 5.);
}

TEST_F(TestArrayOps, normalize_works_for_nonzero_vector) {
  auto p = std::array<double, 3>{3., 0., 0.};
  array_ops::normalize(p);
  EXPECT_DOUBLE_EQ(p[0], 1.);
  EXPECT_DOUBLE_EQ(p[1], 0.);
  EXPECT_DOUBLE_EQ(p[2], 0.);
}

TEST_F(TestArrayOps, normalize_sets_nan_for_zero_vector) {
  auto p = std::array<double, 3>{0., 0., 0.};
  array_ops::normalize(p);
  EXPECT_TRUE(std::isnan(p[0]));
  EXPECT_TRUE(std::isnan(p[1]));
  EXPECT_TRUE(std::isnan(p[2]));
}

TEST_F(TestArrayOps, as_streamable_streams_vector) {
  auto p = std::array<double, 3>{1., 2., 3.};
  auto buffer = std::stringstream{};
  buffer << array_ops::as_streamable(p);
  const auto content = buffer.str();
  EXPECT_EQ(content, "1 2 3");
}

TEST_F(TestArrayOps, as_eigen_matrix_maps_all_entries) {
  const auto matrix = std::array{std::array<double, 3>{1., 2., 3.},
                                 std::array<double, 3>{4., 5., 6.}};

  const auto eigen_matrix = array_ops::as_eigen_matrix(matrix);
  EXPECT_DOUBLE_EQ(eigen_matrix(0, 0), 1.);
  EXPECT_DOUBLE_EQ(eigen_matrix(0, 1), 2.);
  EXPECT_DOUBLE_EQ(eigen_matrix(0, 2), 3.);
  EXPECT_DOUBLE_EQ(eigen_matrix(1, 0), 4.);
  EXPECT_DOUBLE_EQ(eigen_matrix(1, 1), 5.);
  EXPECT_DOUBLE_EQ(eigen_matrix(1, 2), 6.);
}
TEST_F(TestArrayOps, as_eigen_matrix_can_write_to_nested_arrays) {
  const auto src = std::array{std::array<double, 3>{1., 2., 3.},
                              std::array<double, 3>{4., 5., 6.}};
  auto dest = std::array{std::array<double, 3>{0., 0., 0.},
                         std::array<double, 3>{0., 0., 0.}};

  array_ops::as_eigen_matrix(dest) = array_ops::as_eigen_matrix(src);

  EXPECT_DOUBLE_EQ(dest[0][0], 1.);
  EXPECT_DOUBLE_EQ(dest[0][1], 2.);
  EXPECT_DOUBLE_EQ(dest[0][2], 3.);
  EXPECT_DOUBLE_EQ(dest[1][0], 4.);
  EXPECT_DOUBLE_EQ(dest[1][1], 5.);
  EXPECT_DOUBLE_EQ(dest[1][2], 6.);
}

} // namespace
} // namespace qcmesh::array_ops
