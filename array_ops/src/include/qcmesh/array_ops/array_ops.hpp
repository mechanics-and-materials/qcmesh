// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.
#pragma once

#include <Eigen/Core>
#include <array>
#include <iterator>
#include <limits>

namespace qcmesh::array_ops {
/**
 * @brief Wraps a std::array into a Eigen::Map.
 *
 * @param arr Data source.
 *
 * @return An Eigen::Map wrapping arr.
 */
template <class T, std::size_t N> auto as_eigen(std::array<T, N> &arr) {
  return Eigen::Map<Eigen::Matrix<T, N, 1>>(arr.data());
}
/**
 * @brief Overload for R-value references.
 */
template <class T, std::size_t N> auto as_eigen(std::array<T, N> &&arr) {
  Eigen::Matrix<T, N, 1> copy = as_eigen(arr);
  return copy;
}
/**
 * @brief Overload for const references.
 */
template <class T, std::size_t N> auto as_eigen(const std::array<T, N> &arr) {
  return Eigen::Map<const Eigen::Matrix<T, N, 1>>(arr.data());
}
/**
 * @brief Deleted overload for const R-value references.
 */
template <class T, std::size_t N>
auto as_eigen(const std::array<T, N> &&arr) = delete;

/**
 * @brief Wraps a nested const std::array into a const Eigen::Map.
 * The memory order is row major.
 *
 * @param arr Data source.
 *
 * @return An Eigen::Map wrapping arr.
 */
template <class T, std::size_t M, std::size_t N>
auto as_eigen_matrix(std::array<std::array<T, N>, M> &arr) {
  return Eigen::Map<Eigen::Matrix<T, M, N, Eigen::RowMajor>>(arr[0].data());
}
template <class T, std::size_t M, std::size_t N>
auto as_eigen_matrix(const std::array<std::array<T, N>, M> &arr) {
  return Eigen::Map<const Eigen::Matrix<T, M, N, Eigen::RowMajor>>(
      arr[0].data());
}
template <class T, std::size_t M, std::size_t N>
auto as_eigen_matrix(const std::array<std::array<T, N>, M> &&arr) = delete;

/**
 * @brief Return the Euclidean distance between two points.
 */
template <typename T, std::size_t N>
inline T distance(const std::array<T, N> &x, const std::array<T, N> &y) {
  return (array_ops::as_eigen(x) - array_ops::as_eigen(y)).norm();
}

/**
 * @brief Return the square of the Euclidean distance between two points.
 */
template <typename T, std::size_t N>
inline T squared_distance(const std::array<T, N> &x,
                          const std::array<T, N> &y) {
  const auto d = (array_ops::as_eigen(x) - array_ops::as_eigen(y));
  return d.dot(d);
}

/**
 * @brief Return the Euclidean inner product of two vectors.
 */
template <class T, std::size_t N>
double dot(const std::array<T, N> &x, const std::array<T, N> &y) {
  return array_ops::as_eigen(x).dot(array_ops::as_eigen(y));
}

/**
 * @brief Return the Euclidean norm of an array.
 */
template <class T, std::size_t N> double norm(const std::array<T, N> &x) {
  return array_ops::as_eigen(x).norm();
}

/**
 * @brief Normalize the vector to unit norm.
 */
template <typename T, std::size_t N>
inline void normalize(std::array<T, N> &x) {
  const auto x_norm = norm(x);
  array_ops::as_eigen(x) /= x_norm;
}

/**
 * @brief Wrapper struct for std::array to make it streamable.
 */
template <class T, std::size_t N> struct StreamableArray {
  const std::array<T, N> &inner;
};
template <class T, std::size_t N>
std::ostream &operator<<(std::ostream &out, const StreamableArray<T, N> &x) {
  return out << as_eigen(x.inner).transpose();
}

/**
 * @brief Wraps a std::array into a StreamableArray, which can be used
 * as the right hand side of the `<<` operator.
 */
template <class T, std::size_t N>
StreamableArray<T, N> as_streamable(const std::array<T, N> &arr) {
  return StreamableArray<T, N>{arr};
}

} // namespace qcmesh::array_ops
