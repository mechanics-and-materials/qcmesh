// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/repair_mesh.hpp"
#include "qcmesh/mesh/empty_data.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/greedy_driver.hpp"
#include "qcmesh/mesh/serialization/empty_data.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include <iostream>

namespace mesh = qcmesh::mesh;

namespace {
/**
 * @brief Cell data type which has a quality field.
 *
 * qcmesh does not ship a ready to use cell data type, as
 * this is assumed to be very different from project to project.
 * The quality field together with an implementation of the
 * @ref qcmesh::mesh::traits::Quality trait is required for
 * @ref qcmesh::mesh::repair_mesh.
 */
struct CellData {
  double quality{};
};
} // namespace

/**
 * @brief Implement @ref qcmesh::mesh::traits::Quality for @ref CellData.
 */
template <> struct mesh::traits::Quality<CellData> {
  static double &quality(CellData &data) { return data.quality; }
  static double quality(const CellData &data) { return data.quality; }
};

/**
 * @brief Implement `boost::serialization::serialize` for @ref CellData.
 *
 * This is needed when distributing the mesh over multiple mpi processes.
 */
namespace boost::serialization {
template <class Archive>
void serialize(Archive &ar, CellData &data, const unsigned int) {
  ar &data.quality;
}
} // namespace boost::serialization

/**
 * Implement other mpi serialization optimizations.
 * See
 * https://www.boost.org/doc/libs/release/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
BOOST_CLASS_IMPLEMENTATION(CellData, object_serializable)
BOOST_CLASS_TRACKING(CellData, track_never)
BOOST_IS_MPI_DATATYPE(CellData)

/**
 * @brief Edge removal example: 3 tetrahedra around edge (0,0,-1) -- (0,0,1)
 * The "ring" of tetrahedra around the edge is broken up at a face.
 *
 *       (2)
 *      / |
 *   (3)  |
 *    | \ |
 *    | (0,1)
 *    | / |
 *   (4)  |
 *      \ |
 *       (5)
 */
int main() {
  // First initialize MPI.
  // Even though, we dont need more than one mpi process here and we also
  // only create a process local mesh, we still need to perform this step.
  const auto env = boost::mpi::environment{};
  using Cell = std::array<std::size_t, 4>;
  using Vertex = std::array<double, 3>;

  // Place vertices as depicted above:
  const auto d = double{0.025};
  const auto dy = double{0.1};
  const auto vertices = std::vector{Vertex{0., 0., -1.}, Vertex{0., 0., 1.},
                                    Vertex{0., dy, 0.},  Vertex{-1., d, 0.},
                                    Vertex{-1., -d, 0.}, Vertex{0., -1., 0.}};
  // Define cells by vertex indices:
  const auto cells =
      std::vector{Cell{0, 1, 2, 3}, Cell{0, 1, 3, 4}, Cell{0, 1, 4, 5}};
  // Create the mesh (The first template parameter mesh::EmptyData declares,
  // that vertices do not carry extra data):
  auto mesh = mesh::create_local_simplex_mesh<mesh::EmptyData, CellData>(
      vertices, cells);
  // Repair the mesh. The cell 0-1-3-4 has a quality below threshold and should
  // be replaced by edge removal:
  auto driver = mesh::GreedyDriver{};
  try {
    mesh::repair_mesh(mesh, driver);
  } catch (const std::exception &e) {
    std::cerr << e.what();
    boost::mpi::environment::abort(1);
  }
  // Output the new cells:
  std::cout << "Cells after repair:" << std::endl;
  for (const auto &[cell_id, cell] : mesh.cells)
    std::cout << static_cast<std::size_t>(cell_id) << ": ("
              << static_cast<std::size_t>(cell.nodes[0]) << ','
              << static_cast<std::size_t>(cell.nodes[1]) << ','
              << static_cast<std::size_t>(cell.nodes[2]) << ','
              << static_cast<std::size_t>(cell.nodes[3]) << ')' << std::endl;

  return 0;
}
