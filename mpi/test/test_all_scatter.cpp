// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

// NOLINTBEGIN(clang-analyzer-optin.mpi.MPI-Checker)
#include "qcmesh/mpi/all_scatter.hpp"
#include <array>
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>
#include <gmock/gmock.h>
#include <vector>

namespace {

/**
 * @brief i-th basis vector in dim dimensional Euclidean space.
 */
auto basis_vec(const std::size_t i, const std::size_t dim) {
  auto vec = std::vector<std::size_t>(dim);
  vec[i] = 1;
  return vec;
}

TEST(test_all_scatter, all_scatter_works_for_vectors_of_same_length) {
  const auto world = boost::mpi::communicator{};
  const std::size_t mpi_rank = world.rank();
  const std::size_t mpi_size = world.size();

  // Send e_i to rank i (e_i ith basis vector in dim=mpi_size)
  auto vec = std::vector<std::vector<std::size_t>>(mpi_size);
  for (std::size_t i = 0; i < vec.size(); i++)
    vec[i] = basis_vec(i, mpi_size);
  auto scattered = std::vector<std::vector<std::size_t>>(mpi_size);
  // Should receive mpi_size-1 copies of e_{mpi_rank}
  auto expected_scattered = std::vector<std::vector<std::size_t>>(
      mpi_size, basis_vec(mpi_rank, mpi_size));
  // Current process does not send to itself:
  expected_scattered[mpi_rank] = std::vector<std::size_t>{};
  qcmesh::mpi::all_scatter(vec, scattered);
  EXPECT_THAT(scattered, testing::ContainerEq(expected_scattered));
}

TEST(test_all_scatter, all_scatter_works_for_vectors_of_different_length) {
  const auto world = boost::mpi::communicator{};
  const std::size_t mpi_rank = world.rank();
  const std::size_t mpi_size = world.size();

  // Send vec of ones with lenght dest_rank to each dest process:
  auto vec = std::vector<std::vector<std::size_t>>(mpi_size);
  for (std::size_t i = 0; i < vec.size(); i++)
    vec[i] = std::vector<std::size_t>(i, 1);
  auto scattered = std::vector<std::vector<std::size_t>>(mpi_size);
  // Should receive mpi_size-1 copies of vec of ones of size mpi_rank
  auto expected_scattered = std::vector<std::vector<std::size_t>>(
      mpi_size, std::vector<std::size_t>(mpi_rank, 1));
  // Current process does not send to itself:
  expected_scattered[mpi_rank] = std::vector<std::size_t>{};
  qcmesh::mpi::all_scatter(vec, scattered);
  EXPECT_THAT(scattered, testing::ContainerEq(expected_scattered));
}

} // namespace

namespace {
/**
 * @brief Serializable data type for testing
 */
struct Data {
  constexpr static std::size_t SIZE = 5;
  std::array<std::size_t, SIZE> x{};
};

bool operator==(const Data &lhs, const Data &rhs) noexcept {
  return lhs.x == rhs.x;
}
} // namespace

namespace boost::serialization {
template <class Archive>
void serialize(Archive &ar, Data &data, const unsigned int) {
  ar &data.x;
}
} // namespace boost::serialization

namespace {

template <std::size_t N> std::array<std::size_t, N> ones() {
  auto arr = std::array<std::size_t, N>{};
  std::fill(arr.begin(), arr.end(), 1);
  return arr;
}

TEST(
    test_all_scatter,
    all_scatter_works_for_vectors_of_different_length_with_serializable_data_type) {
  const auto world = boost::mpi::communicator{};
  const std::size_t mpi_rank = world.rank();
  const std::size_t mpi_size = world.size();

  if (mpi_size > 4)
    return;
  // Define vecs of all processes in all processes (up to 4):
  const auto d = Data{ones<Data::SIZE>()};
  const auto vecs = std::array<std::vector<std::vector<Data>>, 4>{
      std::vector{std::vector<Data>{}, std::vector<Data>(1833, d),
                  std::vector<Data>(1942, d), std::vector<Data>(1897, d)},
      std::vector{std::vector<Data>(1214, d), std::vector<Data>{},
                  std::vector<Data>(1897, d), std::vector<Data>(632, d)},
      std::vector{std::vector<Data>(87, d), std::vector<Data>(2065, d),
                  std::vector<Data>{}, std::vector<Data>(950, d)},
      std::vector{std::vector<Data>(475, d), std::vector<Data>(606, d),
                  std::vector<Data>(106, d), std::vector<Data>{}}};

  auto expected_scattered =
      std::vector<std::vector<Data>>(mpi_size, std::vector<Data>{});
  auto input = std::vector<std::vector<Data>>(mpi_size, std::vector<Data>{});
  for (std::size_t i = 0; i < mpi_size; i++) {
    for (const auto &val : vecs[mpi_rank][i])
      input[i].push_back(val);
    expected_scattered[i] = vecs[i][mpi_rank];
  }
  auto scattered = qcmesh::mpi::all_scatter(input);
  EXPECT_THAT(scattered, testing::ContainerEq(expected_scattered));
}

} // namespace
// NOLINTEND(clang-analyzer-optin.mpi.MPI-Checker)
