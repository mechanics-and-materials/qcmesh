// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/all_union_set.hpp"
#include <gmock/gmock.h>

namespace {

TEST(test_all_union_set, all_union_set_works_for_overlapping_sets) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = static_cast<std::size_t>(world.rank());
  const auto mpi_size = static_cast<std::size_t>(world.size());

  const auto set =
      std::set<int>{static_cast<int>(mpi_rank), static_cast<int>(mpi_rank + 1)};

  const auto merged = qcmesh::mpi::all_union_set(set);

  const auto expected_merged = [mpi_size]() {
    auto out = std::set<int>{};
    for (std::size_t i = 0; i < mpi_size + 1; i++)
      out.emplace(static_cast<int>(i));
    return out;
  }();
  EXPECT_THAT(merged, testing::ContainerEq(expected_merged));
}

} // namespace
