// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/all_concat_vec.hpp"
#include <array>
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>
#include <gmock/gmock.h>
#include <vector>

namespace {

std::vector<unsigned int> range(const unsigned int from,
                                const unsigned int to) {
  const auto size = to - from;
  auto vec = std::vector<unsigned int>(size);
  for (std::size_t i = 0; i < size; i++)
    vec[i] = from + i;
  return vec;
}

TEST(test_all_concat_vec,
     all_concat_vec_works_for_vectors_of_different_lengths) {
  const auto world = boost::mpi::communicator{};
  const std::size_t mpi_rank = world.rank();
  const std::size_t mpi_size = world.size();

  const auto vec =
      range(mpi_rank * (mpi_rank - 1) / 2, mpi_rank * (mpi_rank + 1) / 2);

  const auto concatenated = qcmesh::mpi::all_concat_vec(vec);

  const auto expected_concatenated = range(0, mpi_size * (mpi_size - 1) / 2);
  EXPECT_THAT(concatenated, testing::ContainerEq(expected_concatenated));
}

} // namespace
