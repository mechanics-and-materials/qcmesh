// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/all_reduce_sum.hpp"
#include <array>
#include <gmock/gmock.h>

namespace {

TEST(test_all_reduce_sum, all_reduce_sum_works_for_double) {
  const auto mpi_size =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());

  const auto value = 1.;

  const auto sum = qcmesh::mpi::all_reduce_sum(value);

  const auto expected_sum = static_cast<double>(mpi_size);
  EXPECT_EQ(sum, expected_sum);
}

} // namespace
