// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/all_union_unordered_map.hpp"
#include <gmock/gmock.h>

namespace {

TEST(test_all_union_unordered_map,
     all_union_unordered_map_works_for_overlapping_maps) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = static_cast<std::size_t>(world.rank());
  const auto mpi_size = static_cast<std::size_t>(world.size());

  const auto map = std::unordered_map<std::size_t, int>{
      {mpi_rank, static_cast<int>(mpi_rank)},
      {mpi_rank + 1, static_cast<int>(mpi_rank)}};

  const auto merged = qcmesh::mpi::all_union_unordered_map(map);

  const auto expected_merged = [mpi_size]() {
    auto out = std::unordered_map<std::size_t, int>{};
    for (std::size_t i = 0; i < mpi_size; i++)
      out.emplace(i + 1, static_cast<int>(i));
    out.emplace(0, 0);
    return out;
  }();
  EXPECT_THAT(merged, testing::ContainerEq(expected_merged));
}

} // namespace
