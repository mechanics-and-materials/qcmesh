// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/serialization/tuple.hpp"
#include "serialize_deserialize.hpp"
#include <gmock/gmock.h>

namespace {

TEST(test_serialization_tuple,
     serialization_and_deserialization_works_for_empty_tuple) {
  const auto t = std::tuple<>{};
  const auto deserialized = serialize_deserialize(t);
  EXPECT_EQ(deserialized, t);
}

TEST(test_serialization_tuple,
     serialization_and_deserialization_works_for_1_tuple) {
  const auto t = std::tuple<int>{1};
  const auto deserialized = serialize_deserialize(t);
  EXPECT_EQ(deserialized, t);
}

TEST(test_serialization_tuple,
     serialization_and_deserialization_works_for_2_tuple) {
  const auto t = std::tuple<int, bool>{2, true};
  const auto deserialized = serialize_deserialize(t);
  EXPECT_EQ(deserialized, t);
}

} // namespace
