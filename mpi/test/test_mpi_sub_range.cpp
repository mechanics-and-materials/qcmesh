// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/mpi_sub_range.hpp"
#include <gmock/gmock.h>

namespace {

TEST(test_mpi_sub_range, mpi_sub_range_produces_covering_ranges) {
  const auto world = boost::mpi::communicator{};
  const std::size_t mpi_size = world.size();

  const auto total_size = 17;
  const auto [range_start, range_size] = qcmesh::mpi::mpi_sub_range(total_size);

  auto range_starts = std::vector<std::size_t>{};
  auto range_sizes = std::vector<std::size_t>{};
  boost::mpi::all_gather(world, range_start, range_starts);
  boost::mpi::all_gather(world, range_size, range_sizes);

  EXPECT_EQ(range_starts[0], 0);
  EXPECT_EQ(range_starts.back() + range_sizes.back(), total_size);

  for (std::size_t i = 0; i < mpi_size - 1; i++) {
    EXPECT_EQ(range_starts[i] + range_sizes[i], range_starts[i + 1]);
  }
}

} // namespace
