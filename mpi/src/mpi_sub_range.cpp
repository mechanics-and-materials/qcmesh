// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mpi/mpi_sub_range.hpp"

namespace qcmesh::mpi {

std::tuple<std::size_t, std::size_t> sub_range(const std::size_t total_size,
                                               const std::size_t n_sub_ranges,
                                               const std::size_t range_index) {
  const auto chunk_size = total_size / n_sub_ranges;
  const auto chunk_reminder = total_size % n_sub_ranges;
  const auto start_idx =
      range_index * chunk_size + std::min(chunk_reminder, range_index);
  const auto local_size = chunk_size + (range_index < chunk_reminder ? 1 : 0);
  return std::make_tuple(start_idx, local_size);
}

std::tuple<std::size_t, std::size_t>
mpi_sub_range(const std::size_t total_size) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = static_cast<std::size_t>(world.size());
  const auto mpi_rank = static_cast<std::size_t>(world.rank());
  return sub_range(total_size, mpi_size, mpi_rank);
}

} // namespace qcmesh::mpi
