// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <boost/mpi.hpp>
#include <tuple>

namespace qcmesh::mpi {

/**
 * @brief Divide the range `0..total_size` into `mpi_size` sub ranges such that
 * the lengths of the different sub ranges differ by 1 at max.
 *
 * @returns The start position and size of the range corresponding to
 * `mpi_rank`.
 */
std::tuple<std::size_t, std::size_t>
mpi_sub_range(const std::size_t total_size);

/**
 * @brief Divide the range `0..total_size` into `n_sub_ranges` sub ranges such
 * that the lengths of the different sub ranges differ by 1 at max.
 *
 * @returns The start position and size of the range corresponding to
 * `range_index`.
 */
std::tuple<std::size_t, std::size_t> sub_range(const std::size_t total_size,
                                               const std::size_t n_sub_ranges,
                                               const std::size_t range_index);

} // namespace qcmesh::mpi
