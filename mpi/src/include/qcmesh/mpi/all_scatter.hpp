// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 *
 * @author Prateek Gupta, Shashank Saxena
 */

#pragma once
// NOLINTBEGIN(clang-analyzer-optin.mpi.MPI-Checker)
#include <boost/mpi.hpp>
#include <boost/mpi/nonblocking.hpp>
#include <boost/mpi/request.hpp>
#include <boost/mpi/status.hpp>
#include <stdexcept>
#include <vector>

namespace qcmesh::mpi {

/**
 * @brief Exception class thrown when an input vector has less than `mpi_size`
 * items.
 */
struct MpiSizeException : std::runtime_error {
  using std::runtime_error::runtime_error;
};

/**
 * @brief Similar to boost::mpi::scatter, but scatters from / to all processes.
 *
 * @param in_values A vector that will contain the values to send to each
 * process, indexed by process rank (the value at the index of the current rank
 * will be ignored).
 * @param out_values The values received by each process indexed by process rank
 * (the value at the index of the current rank will left unchanged).
 */
template <typename T>
void all_scatter(const std::vector<T> &in_values, std::vector<T> &out_values) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_rank = (std::size_t)world.rank();
  const auto mpi_size = (std::size_t)world.size();
  const auto n_requests = 2 * (mpi_size - 1);
  auto mpi_requests = std::vector<boost::mpi::request>{};
  mpi_requests.reserve(n_requests);

  if (in_values.size() < mpi_size)
    throw MpiSizeException{
        "in_values does not have size equal to the number of procs"};

  if (out_values.size() < mpi_size)
    throw MpiSizeException{
        "out_values does not have size equal to the number of procs"};

  constexpr int TAG = 0;
  for (std::size_t i_proc = 0; i_proc < mpi_size; i_proc++)
    if (i_proc != mpi_rank)
      mpi_requests.emplace_back(world.isend(i_proc, TAG, in_values[i_proc]));

  for (std::size_t i_proc = 0; i_proc < mpi_size; i_proc++)
    if (i_proc != mpi_rank)
      mpi_requests.emplace_back(
          world.irecv(i_proc, boost::mpi::any_tag, out_values[i_proc]));
  boost::mpi::wait_all(mpi_requests.begin(), mpi_requests.end());
}

/**
 * @brief Similar to boost::mpi::scatter, but scatters from / to all processes.
 *
 * @param in_values A vector that will contain the values to send to each
 * process, indexed by process rank (the value at the index of the current rank
 * will be ignored).
 * @returns The values received by each process indexed by process rank
 * (the value at the index of the current rank will left unchanged).
 */
template <typename T>
std::vector<T> all_scatter(const std::vector<T> &in_values) {
  auto out_values = std::vector<T>(in_values.size());
  all_scatter(in_values, out_values);
  return out_values;
}

} // namespace qcmesh::mpi

// NOLINTEND(clang-analyzer-optin.mpi.MPI-Checker)
