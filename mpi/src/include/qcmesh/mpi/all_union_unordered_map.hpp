// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <boost/mpi.hpp>
#include <boost/serialization/unordered_map.hpp>

namespace qcmesh::mpi {

/**
 * @brief Collect and merges maps from all MPI processes.
 */
template <class K, class V>
std::unordered_map<K, V>
all_union_unordered_map(const std::unordered_map<K, V> &map) {
  const auto world = boost::mpi::communicator{};
  return boost::mpi::all_reduce(
      world, map, [](auto &map, const auto &other_map) {
        map.insert(other_map.begin(), other_map.end());
        return map;
      });
}

} // namespace qcmesh::mpi
