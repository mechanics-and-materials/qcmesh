// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Provides non-intrusive serialization for std::optional.
 */

#pragma once

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/split_free.hpp>
#include <optional>

namespace boost::serialization {

template <class Archive, class T>
void save(Archive &ar, const std::optional<T> &t, const unsigned int /*version*/
) {
  const bool tflag = t.has_value();
  ar << boost::serialization::make_nvp("initialized", tflag);
  if (tflag)
    ar << boost::serialization::make_nvp("value", *t);
}

template <class Archive, class T>
void load(Archive &ar, std::optional<T> &t, const unsigned int /*version*/
) {
  bool tflag{};
  ar >> boost::serialization::make_nvp("initialized", tflag);
  if (tflag) {
    auto value = T{};
    ar >> boost::serialization::make_nvp("value", value);
    t.emplace(value);
  } else {
    t.reset();
  }
}

template <class Archive, class T>
void serialize(Archive &ar, std::optional<T> &t, const unsigned int version) {
  boost::serialization::split_free(ar, t, version);
}

} // namespace boost::serialization
