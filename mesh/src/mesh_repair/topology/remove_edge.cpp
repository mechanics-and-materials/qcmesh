// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @author Prateek Gupta
 */

#include "qcmesh/mesh/mesh_repair/topology/remove_edge.hpp"

namespace qcmesh::mesh {

std::vector<KlincsekTreeNode>
create_klincsek_tree(const std::vector<std::size_t> &klincsek_index,
                     const std::size_t ring_size) {
  std::vector<KlincsekTreeNode> nodes{};
  const auto n_triangles = ring_size - 2;
  nodes.reserve(n_triangles);
  nodes.emplace_back(KlincsekTreeNode{
      .triangle = {0, klincsek_index[ring_size - 1], ring_size - 1},
      .parent = 0,
      .parent_apex = 1});
  for (std::size_t parent_idx = 0;
       parent_idx < nodes.size() && nodes.size() < n_triangles; parent_idx++) {
    const auto [i, k, j] = nodes[parent_idx].triangle;
    // i < k < j
    if (i + 1 < k) // apex is j (index=2)
      nodes.emplace_back(KlincsekTreeNode{
          .triangle = {i, klincsek_index[ring_size * i + k], k},
          .parent = parent_idx,
          .parent_apex = 2});
    if (k + 1 < j && nodes.size() < n_triangles) // apex is i (index=0)
      nodes.emplace_back(KlincsekTreeNode{
          .triangle = {k, klincsek_index[ring_size * k + j], j},
          .parent = parent_idx,
          .parent_apex = 0});
  }
  return nodes;
}

} // namespace qcmesh::mesh
