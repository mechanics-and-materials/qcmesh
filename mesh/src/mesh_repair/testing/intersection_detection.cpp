// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/intersection_detection.hpp"
#include <cmath>

namespace qcmesh::mesh::testing {

namespace {
template <std::size_t Dimension> struct HyperPlane {
  std::array<double, Dimension> n{};
  /**
   * @brief d = <x, n> for some x on the plane (i.e. it is a "scaled" distance).
   */
  double d{};
};

template <std::size_t Dimension> using Point = std::array<double, Dimension>;

/**
 * @brief Plane containing p1 and p2, which is perpendicular to (p2-p1) x
 * (q2-q1).
 */
HyperPlane<3> plane_by_two_segments(const Point<3> &p1, const Point<3> &p2,
                                    const Point<3> &q1, const Point<3> &q2) {
  const auto dp =
      std::array<double, 3>{p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
  const auto dq =
      std::array<double, 3>{q2[0] - q1[0], q2[1] - q1[1], q2[2] - q1[2]};
  const auto n = std::array<double, 3>{dp[1] * dq[2] - dp[2] * dq[1],
                                       dp[2] * dq[0] - dp[0] * dq[2],
                                       dp[0] * dq[1] - dp[1] * dq[0]};
  return HyperPlane<3>{n, n[0] * p1[0] + n[1] * p1[1] + n[2] * p1[2]};
}

/**
 * @brief Plane containing p1, p2, p3.
 */
HyperPlane<3> plane_through_3_points(const Point<3> &p1, const Point<3> &p2,
                                     const Point<3> &p3) {
  return plane_by_two_segments(p1, p2, p3, p1);
}

/**
 * @brief Line containing p1, p2.
 */
auto line_through_2_points(const Point<2> &p1, const Point<2> &p2) {
  const auto dp = std::array<double, 2>{p2[0] - p1[0], p2[1] - p1[1]};
  const auto n = std::array<double, 2>{dp[1], -dp[0]};
  return HyperPlane<2>{n, n[0] * p1[0] + n[1] * p1[1]};
}

/**
 * @brief Non-normalized distance of plane g to point p (distance unit is 1/||
 * g.n ||).
 */
double nonnormalized_distance(const HyperPlane<3> &g, const Point<3> &p) {
  return g.n[0] * p[0] + g.n[1] * p[1] + g.n[2] * p[2] - g.d;
}

double nonnormalized_distance(const HyperPlane<2> &g, const Point<2> &p) {
  return g.n[0] * p[0] + g.n[1] * p[1] - g.d;
}

struct TetrahedronRef {
  const Point<3> &p1, &p2, &p3, &p4;
};

/**
 * @brief Checks if the plane containing t1.p1, t1.p2, t1.p3 separates t1.p4
 from the tetrahedron t2.
 *
 * Points are considered to be on a plane if their non-normalized distance to
 the plane is <= epsilon.

 * @pre Tetrahedron t1 should have non-zero volume.
 * Otherwise an orientation depending on the order of the vertices will be
 chosen and the
 * function has to be called another time with a different order of vertices.
 */
bool face_separates_tetrahedra(const TetrahedronRef &t1,
                               const TetrahedronRef &t2,
                               const double epsilon = 0.) {
  const auto g = plane_through_3_points(t1.p1, t1.p2, t1.p3);
  const bool sgn = nonnormalized_distance(g, t1.p4) >
                   0.; // distance(g, p4) == 0 not covered (0-vol tetrahedron)
  for (const auto &p : {t2.p1, t2.p2, t2.p3, t2.p4}) {
    const auto d = nonnormalized_distance(g, p);
    if (std::fabs(d) > epsilon && (d > 0.) == sgn)
      return false;
  }
  return true;
}

struct TriangleRef {
  const std::array<double, 2> &p1, &p2, &p3;
};

/**
 * @brief Checks if the line containing t1.p1, t1.p2 separates t1.p3 from the
 triangle t2.
 *
 * Points are considered to be on a line if their non-normalized distance to the
 line is <= epsilon.

 * @pre Triangle t1 should have non-zero area.
 * Otherwise an orientation depending on the order of the vertices will be
 chosen and the
 * function has to be called another time with a different order of vertices.
 */
bool line_separates_triangles(const TriangleRef &t1, const TriangleRef &t2,
                              const double epsilon = 0.) {
  const auto l = line_through_2_points(t1.p1, t1.p2);
  const auto sgn = nonnormalized_distance(l, t1.p3) > 0.;
  for (const auto &p : {t2.p1, t2.p2, t2.p3}) {
    const auto d = nonnormalized_distance(l, p);
    if (std::fabs(d) > epsilon && (d > 0.) == sgn)
      return false;
  }
  return true;
}

/**
 * @brief Checks if the plane containing t1.p1, t1.p2, parallel to t2.p1 - t2.p2
 * separates t1 from t2.
 *
 * Points are considered to be on a plane if their non-normalized distance to
 * the plane is <= epsilon.
 */
bool edge_separates_tetrahedra(const TetrahedronRef &t1,
                               const TetrahedronRef &t2,
                               const double epsilon = 0.) {
  const auto g = plane_by_two_segments(t1.p1, t1.p2, t2.p1, t2.p2);
  if (g.n[0] * g.n[0] + g.n[1] * g.n[1] + g.n[2] * g.n[2] <= epsilon * epsilon)
    return false; // zero normal => skip this pair of edges
  const auto d3 = nonnormalized_distance(g, t1.p3);
  const auto d4 = nonnormalized_distance(g, t1.p4);
  const auto p_on_both_sides = (d3 > 0.) != (d4 > 0.);
  if (std::fabs(d3) > epsilon && std::fabs(d4) > epsilon && p_on_both_sides)
    return false;
  const bool sgn = (std::fabs(d3) > std::fabs(d4)) ? (d3 > 0.) : (d4 > 0.);
  for (const auto &p :
       {t2.p2, t2.p3, t2.p4}) { // Dont need to check p1, as distance(g, p1) ==
                                // distance(g, p2)
    const auto d = nonnormalized_distance(g, p);
    if (std::fabs(d) > epsilon && (d > 0.) == sgn)
      return false;
  }
  return true;
}
} // namespace

/**
 * @brief Checks if the two tetrahedra can be separated by a plane.
 * @param epsilon If points have distance <= epsilon to a plane, they are
 * considered to be on that plane.
 */
bool simplices_dont_intersect(const Simplex<3> &t1, const Simplex<3> &t2,
                              const double epsilon) {
  // First 3 indices determine a face (4 faces):
  constexpr auto FACES = std::array<std::array<int, 4>, 4>{
      {{0, 1, 2, 3}, {0, 1, 3, 2}, {0, 2, 3, 1}, {1, 2, 3, 0}}};
  // For each face, check if it separates:
  for (const auto &s : FACES)
    if (face_separates_tetrahedra(
            TetrahedronRef{t1[s[0]], t1[s[1]], t1[s[2]], t1[s[3]]},
            TetrahedronRef{t2[0], t2[1], t2[2], t2[3]}, epsilon) ||
        face_separates_tetrahedra(
            TetrahedronRef{t2[s[0]], t2[s[1]], t2[s[2]], t2[s[3]]},
            TetrahedronRef{t1[0], t1[1], t1[2], t1[3]}, epsilon))
      return true;

  // First 2 indices determine an edge (6 edges):
  constexpr auto EDGES = std::array<std::array<int, 4>, 6>{{{0, 1, 2, 3},
                                                            {0, 2, 1, 3},
                                                            {0, 3, 1, 2},
                                                            {1, 2, 0, 3},
                                                            {1, 3, 0, 2},
                                                            {2, 3, 0, 1}}};
  // For each combination of edge pairs, check if the corresponding plane
  // separates:
  for (const auto &s1 : EDGES)
    for (const auto &s2 : EDGES)
      if (edge_separates_tetrahedra(
              TetrahedronRef{t1[s1[0]], t1[s1[1]], t1[s1[2]], t1[s1[3]]},
              TetrahedronRef{t2[s2[0]], t2[s2[1]], t2[s2[2]], t2[s2[3]]},
              epsilon))
        return true;
  return false;
}

/**
 * @brief Checks if the two triangles can be separated by a line.
 * @param epsilon If points have distance <= epsilon to a line, they are
 * considered to be on that line.
 */
bool simplices_dont_intersect(const Simplex<2> &t1, const Simplex<2> &t2,
                              const double epsilon) {
  // First 2 indices determine a line (3 lines):
  constexpr auto LINES =
      std::array<std::array<int, 3>, 3>{{{0, 1, 2}, {0, 2, 1}, {1, 2, 0}}};
  // For each face, check if it separates:
  for (const auto &s : LINES)
    if (line_separates_triangles(TriangleRef{t1[s[0]], t1[s[1]], t1[s[2]]},
                                 TriangleRef{t2[0], t2[1], t2[2]}, epsilon) ||
        line_separates_triangles(TriangleRef{t2[s[0]], t2[s[1]], t2[s[2]]},
                                 TriangleRef{t1[0], t1[1], t1[2]}, epsilon)) {
      return true;
    }

  return false;
}

} // namespace qcmesh::mesh::testing
