// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/create_lattice_mesh.hpp"
#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/simplex.hpp"

namespace qcmesh::mesh::testing {

std::vector<LatticeVertex> lattice_vertices_from_lattice_points(
    const std::vector<std::array<int, 3>> &lattice_points,
    const std::array<std::array<double, 3>, 3> &basis) {
  auto out = std::vector<LatticeVertex>{};
  out.reserve(lattice_points.size());
  for (const auto &p : lattice_points) {
    auto position = std::array<double, 3>{};
    array_ops::as_eigen(position) =
        array_ops::as_eigen_matrix(basis).transpose() *
        array_ops::as_eigen(p).cast<double>();
    out.emplace_back(LatticeVertex{position, p});
  }
  return out;
}

BareMesh<LatticeVertex> create_lattice_mesh(
    const std::size_t n_x, const std::size_t n_y, const std::size_t n_z,
    const std::array<std::array<double, 3>, 3> &basis,
    const std::vector<std::array<std::size_t, 4>> &unit_connectivity) {
  auto lattice_points = std::vector<std::array<int, 3>>{};
  lattice_points.reserve(n_x * n_y * n_z);
  for (std::size_t i = 0; i < n_x; i++)
    for (std::size_t j = 0; j < n_y; j++)
      for (std::size_t k = 0; k < n_z; k++)
        lattice_points.emplace_back(std::array<int, 3>{
            static_cast<int>(i), static_cast<int>(j), static_cast<int>(k)});

  const auto coordinates_to_index = [&](const std::array<int, 3> &coordinates) {
    const auto [i_x, i_y, i_z] = coordinates;
    return i_x * n_y * n_z + i_y * n_z + i_z;
  };
  const auto unit_cell_index_to_coordinates = [&](const std::size_t idx) {
    return std::array{static_cast<int>((idx & std::size_t{0b100}) >> 2U),
                      static_cast<int>((idx & std::size_t{0b010}) >> 1U),
                      static_cast<int>(idx & std::size_t{0b001})};
  };
  const auto shift_coordinates = [](const std::array<int, 3> &coordinate,
                                    const std::array<int, 3> &shift) {
    return std::array{coordinate[0] + shift[0], coordinate[1] + shift[1],
                      coordinate[2] + shift[2]};
  };
  const auto shift_index = [&](const std::size_t index,
                               const std::array<int, 3> &shift) {
    const auto coordinates = unit_cell_index_to_coordinates(index);
    const auto shifted_coordinates = shift_coordinates(coordinates, shift);
    return coordinates_to_index(shifted_coordinates);
  };
  auto connectivity = std::vector<std::array<std::size_t, 4>>{};
  connectivity.reserve((n_x - 1) * (n_y - 1) * (n_z - 1) * connectivity.size());
  for (std::size_t i = 0; i < n_x - 1; i++)
    for (std::size_t j = 0; j < n_y - 1; j++)
      for (std::size_t k = 0; k < n_z - 1; k++)
        for (const auto &[a, b, c, d] : unit_connectivity) {
          const auto shift = std::array{(int)i, (int)j, (int)k};
          connectivity.emplace_back(
              std::array{shift_index(a, shift), shift_index(b, shift),
                         shift_index(c, shift), shift_index(d, shift)});
        }
  return BareMesh<LatticeVertex>{
      lattice_vertices_from_lattice_points(lattice_points, basis),
      connectivity};
}

} // namespace qcmesh::mesh::testing
