// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/face_connection.hpp"

namespace qcmesh::mesh::testing {

bool operator==(const FaceConnection &c1, const FaceConnection &c2) {
  return c1.cell_id == c2.cell_id && c1.apex_idx == c2.apex_idx &&
         c1.neighbor_id == c2.neighbor_id;
}
bool operator<(const FaceConnection &c1, const FaceConnection &c2) {
  if (c1.cell_id != c2.cell_id)
    return c1.cell_id < c2.cell_id;
  if (c1.apex_idx != c2.apex_idx)
    return c1.apex_idx < c2.apex_idx;
  return c1.neighbor_id < c2.neighbor_id;
}

std::ostream &operator<<(std::ostream &os, const FaceConnection &connection) {
  os << static_cast<std::size_t>(connection.cell_id) << '['
     << connection.apex_idx << "]->"
     << static_cast<std::size_t>(connection.neighbor_id);
  return os;
}

} // namespace qcmesh::mesh::testing
