// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"

namespace qcmesh::mesh::testing {

std::array<NodeId, 3> ordered_oriented_array(const std::array<NodeId, 3> &arr) {
  if (arr[1] < arr[0] && arr[1] <= arr[2])
    return std::array<NodeId, 3>{arr[1], arr[2], arr[0]};
  if (arr[2] < arr[0] && arr[2] <= arr[1])
    return std::array<NodeId, 3>{arr[2], arr[0], arr[1]};
  return arr;
}
std::array<NodeId, 4> ordered_oriented_array(const std::array<NodeId, 4> &arr) {
  constexpr std::array PERMUTATIONS = std::array{
      std::array<std::size_t, 3>{1, 2, 3}, std::array<std::size_t, 3>{0, 3, 2},
      std::array<std::size_t, 3>{0, 1, 3}, std::array<std::size_t, 3>{2, 1, 0}};
  for (std::size_t i = 0; i < 4; ++i) {
    if ((i == 0 || arr[i] <= arr[0]) && (i == 1 || arr[i] <= arr[1]) &&
        (i == 2 || arr[i] <= arr[2]) && (i == 3 || arr[i] <= arr[3])) {
      const auto ordered_tail = ordered_oriented_array(
          std::array{arr[PERMUTATIONS[i][0]], arr[PERMUTATIONS[i][1]],
                     arr[PERMUTATIONS[i][2]]});
      return std::array{arr[i], ordered_tail[0], ordered_tail[1],
                        ordered_tail[2]};
    }
  }
  return arr;
}

bool operator==(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2) {
  return ordered_oriented_array(t1.node_ids) ==
         ordered_oriented_array(t2.node_ids);
}

bool operator!=(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2) {
  return !(t1 == t2);
}

bool operator<(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2) {
  const auto ordered_t1 = ordered_oriented_array(t1.node_ids);
  const auto ordered_t2 = ordered_oriented_array(t2.node_ids);
  for (std::size_t i = 0; i < ordered_t2.size(); i++)
    if (ordered_t1[i] != ordered_t2[i])
      return ordered_t1[i] < ordered_t2[i];
  return false;
}

std::ostream &operator<<(std::ostream &os, const TopologicalCell<4> &t) {
  const auto ordered_t = ordered_oriented_array(t.node_ids);
  return os << '(' << static_cast<std::size_t>(ordered_t[0]) << ','
            << static_cast<std::size_t>(ordered_t[1]) << ','
            << static_cast<std::size_t>(ordered_t[2]) << ','
            << static_cast<std::size_t>(ordered_t[3]) << ')';
}

TopologicalCell<4> to_topological_cell(const std::array<std::size_t, 4> &cell) {
  return TopologicalCell<4>{NodeId{cell[0]}, NodeId{cell[1]}, NodeId{cell[2]},
                            NodeId{cell[3]}};
}

std::set<TopologicalCell<4>>
to_topological_cells(const std::vector<std::array<std::size_t, 4>> &cells) {
  auto topological_cells = std::set<TopologicalCell<4>>{};
  for (const auto &cell : cells)
    topological_cells.emplace(to_topological_cell(cell));
  return topological_cells;
}

} // namespace qcmesh::mesh::testing
