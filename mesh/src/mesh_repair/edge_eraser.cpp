// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/edge_eraser.hpp"

namespace qcmesh::mesh {
namespace detail {

std::tuple<std::size_t, std::size_t>
opposite_edge_indices(const std::size_t a_idx, const std::size_t b_idx) {
  auto apex_forward = std::size_t{0};
  while (apex_forward == a_idx || apex_forward == b_idx)
    ++apex_forward;
  auto apex_back = apex_forward + 1;
  while (apex_back == a_idx || apex_back == b_idx)
    ++apex_back;
  // Under the assumption that {0,1,2,3} index a positively oriented cell,
  // make sure that {a_idx, b_idx, apex_backward, apex_forward}
  // also indexes a positively oriented cell:
  if ((a_idx > b_idx || b_idx - a_idx == 2) && !(a_idx - b_idx == 2))
    std::swap(apex_forward, apex_back);
  return std::make_tuple(apex_forward, apex_back);
}

} // namespace detail

bool double_pyramid_has_positive_orientation(
    const std::array<double, 3> &t1, const std::array<double, 3> &t2,
    const std::array<double, 3> &t3, const std::array<double, 3> &apex_a,
    const std::array<double, 3> &apex_b) {
  const auto n = (array_ops::as_eigen(t2) - array_ops::as_eigen(t1))
                     .cross(array_ops::as_eigen(t3) - array_ops::as_eigen(t1));
  return (n.dot(array_ops::as_eigen(apex_a) - array_ops::as_eigen(t1)) < 0.) &&
         (n.dot(array_ops::as_eigen(apex_b) - array_ops::as_eigen(t1)) > 0.);
}

} // namespace qcmesh::mesh
