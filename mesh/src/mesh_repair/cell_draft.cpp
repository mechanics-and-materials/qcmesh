// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include <Eigen/Dense>

namespace qcmesh::mesh {

namespace {
template <class T>
Eigen::Matrix<T, 3, 3> from_cols(const Eigen::Matrix<T, 3, 1> &a,
                                 const Eigen::Matrix<T, 3, 1> &b,
                                 const Eigen::Matrix<T, 3, 1> &c) {
  auto temp_matrix = Eigen::Matrix<T, 3, 3>{};
  temp_matrix.col(0) = a;
  temp_matrix.col(1) = b;
  temp_matrix.col(2) = c;
  return temp_matrix;
}
} // namespace

std::tuple<int, Eigen::Matrix<double, 3, 3>>
lattice_vectors_with_vol(const EdgeWithLatticeCoordinates &a,
                         const EdgeWithLatticeCoordinates &b,
                         const EdgeWithLatticeCoordinates &c) {
  // Lattice coordinates:
  const auto &n = from_cols(a.lattice, b.lattice, c.lattice);
  const auto n_inv = n.cast<double>().inverse();
  // Euclidean coordinates:
  const auto &jac = from_cols(a.position, b.position, c.position);
  return std::make_tuple(n.determinant(), jac * n_inv);
}
} // namespace qcmesh::mesh
