// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/orient_elements.hpp"

namespace qcmesh::mesh {

template std::unordered_set<SimplexCellId>
orient_elements(SimplexMesh<3, 3, EmptyData, EmptyData> &mesh);

namespace detail {

std::tuple<std::array<NodeId, 3>, bool>
ordered_face_with_orientation(const std::array<NodeId, 3> &face) {
  auto sign = false;
  auto ordered_face = face;
  for (const auto [i, j] :
       {std::array{0, 1}, std::array{1, 2}, std::array{0, 1}})
    if (ordered_face[i] > ordered_face[j]) {
      sign = !sign;
      std::swap(ordered_face[i], ordered_face[j]);
    }
  return {ordered_face, sign};
}

} // namespace detail

} // namespace qcmesh::mesh
