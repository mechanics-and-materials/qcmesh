// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/face.hpp"

namespace qcmesh::mesh {

template bool cell_contains_vertex(const std::array<NodeId, 4> &nodes,
                                   const NodeId face);

template bool cell_contains_face(const std::array<NodeId, 4> &nodes,
                                 const std::array<NodeId, 3> &face);

template std::optional<std::size_t>
face_apex_index(const std::array<NodeId, 4> &nodes,
                const std::array<NodeId, 3> &face);

template std::array<NodeId, 3>
face_vertex_ids_by_idx(const std::array<NodeId, 4> &cell,
                       const std::size_t apex_index);
template std::array<NodeId, 3>
face_vertex_ids(const std::array<NodeId, 4> &cell, const NodeId apex_id);

} // namespace qcmesh::mesh
