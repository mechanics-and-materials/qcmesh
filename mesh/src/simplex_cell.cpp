// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_cell.hpp"

namespace qcmesh::mesh {

template struct SimplexCell<3, EmptyData>;

template std::optional<std::size_t>
index_of_node(const SimplexCell<3, EmptyData> &cell, const NodeId node_id);
template bool simplex_cell_has_node(const SimplexCell<3, EmptyData> &cell,
                                    const NodeId node_id);
template std::optional<std::size_t>
index_of_neighbor(const SimplexCell<3, EmptyData> &cell,
                  const SimplexCellId neighbor_id);
template void flip_orientation(SimplexCell<3, EmptyData> &cell);

} // namespace qcmesh::mesh
