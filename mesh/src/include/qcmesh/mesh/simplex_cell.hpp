// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/empty_data.hpp"
#include "qcmesh/mesh/node.hpp"
#include <array>
#include <limits>
#include <optional>

namespace qcmesh::mesh {

enum class SimplexCellId : std::size_t {};

/**
 * @brief Cell of a simplex mesh.
 */
template <std::size_t Dimension, class CellData> struct SimplexCell {
  SimplexCellId id{};
  int process_rank{};

  /** @brief ids of nodes making the cell */
  std::array<NodeId, Dimension + 1> nodes{};
  /**
   * @brief face neighoburs of the cell
   *
   * All other neighbors (edge and node) are extracted from these, if needed.
   */
  std::array<std::optional<SimplexCellId>, Dimension + 1> neighbors{};
  CellData data{};
};
extern template struct SimplexCell<3, EmptyData>;

/**
 * @brief Find the position of the node within nodes with matching node_id,
 * if it exists.
 */
template <std::size_t Dimension, class CellData>
std::optional<std::size_t>
index_of_node(const SimplexCell<Dimension, CellData> &cell,
              const NodeId node_id) {
  for (std::size_t d = 0; d < Dimension + 1; d++)
    if (cell.nodes[d] == node_id)
      return d;
  return std::nullopt;
}
extern template std::optional<std::size_t>
index_of_node(const SimplexCell<3, EmptyData> &cell, const NodeId node_id);

/**
 * @brief Return true if the cell contains a node with id node_id.
 */
template <std::size_t Dimension, class CellData>
bool simplex_cell_has_node(const SimplexCell<Dimension, CellData> &cell,
                           const NodeId node_id) {
  return index_of_node(cell, node_id).has_value();
}
extern template bool
simplex_cell_has_node(const SimplexCell<3, EmptyData> &cell,
                      const NodeId node_id);

/**
 * @brief Find the position of the cell within neighbors with matching
 * neighbor_id, if it exists.
 */
template <std::size_t Dimension, class CellData>
std::optional<std::size_t>
index_of_neighbor(const SimplexCell<Dimension, CellData> &cell,
                  const SimplexCellId neighbor_id) {
  for (std::size_t d = 0; d < Dimension + 1; d++) {
    const auto neighbor = cell.neighbors[d];
    if (neighbor.has_value() && neighbor.value() == neighbor_id)
      return d;
  }
  return std::nullopt;
}
extern template std::optional<std::size_t>
index_of_neighbor(const SimplexCell<3, EmptyData> &cell,
                  const SimplexCellId neighbor_id);

/**
 * @brief Flip the orientation of the cell by exchanging 2 vertices and the
 * corresponding neighbors.
 */
template <std::size_t Dimension, class CellData>
void flip_orientation(SimplexCell<Dimension, CellData> &cell) {
  std::swap(cell.nodes[0], cell.nodes[1]);
  std::swap(cell.neighbors[0], cell.neighbors[1]);
}
extern template void flip_orientation(SimplexCell<3, EmptyData> &cell);

} // namespace qcmesh::mesh
