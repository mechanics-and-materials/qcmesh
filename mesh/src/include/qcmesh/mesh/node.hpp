// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/empty_data.hpp"
#include <array>
#include <utility>

namespace qcmesh::mesh {

enum class NodeId : std::size_t {};

template <std::size_t Dimension, typename NodalData> struct Node {
  NodeId id{};
  int process_rank{};
  std::array<double, Dimension> position{};
  NodalData data{};
};

extern template struct Node<3, EmptyData>;

} // namespace qcmesh::mesh
