// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/node.hpp"
#include <array>
#include <optional>
#include <stdexcept>

namespace qcmesh::mesh {

/**
 * @brief Exception class for face routines.
 */
struct FaceException : std::runtime_error {
  using std::runtime_error::runtime_error;
};

/**
 * @brief Return true, if the cell contains the vertex.
 */
template <std::size_t N>
bool cell_contains_vertex(const std::array<NodeId, N> &cell,
                          const NodeId vertex_id) {
  for (const auto &vertex : cell)
    if (vertex == vertex_id)
      return true;
  return false;
}
extern template bool cell_contains_vertex(const std::array<NodeId, 4> &cell,
                                          const NodeId vertex_id);

/**
 * @brief Return true, if the cell contains the face.
 */
template <std::size_t Dimension>
bool cell_contains_face(const std::array<NodeId, Dimension + 1> &cell,
                        const std::array<NodeId, Dimension> &face) {
  for (const auto &vertex : face)
    if (!cell_contains_vertex(cell, vertex))
      return false;
  return true;
}
extern template bool cell_contains_face(const std::array<NodeId, 4> &nodes,
                                        const std::array<NodeId, 3> &face);

/**
 * @brief Returns the position of the apex vertex to face within cell if cell
 * contains the face, std::nullopt otherwise.
 */
template <std::size_t Dimension>
std::optional<std::size_t>
face_apex_index(const std::array<NodeId, Dimension + 1> &cell,
                const std::array<NodeId, Dimension> &face) {
  auto index = std::optional<std::size_t>{};
  for (std::size_t i = 0; i < cell.size(); i++)
    if (!cell_contains_vertex(face, cell[i])) {
      if (index.has_value())
        return std::nullopt;
      index = i;
    }

  return index;
}
extern template std::optional<std::size_t>
face_apex_index(const std::array<NodeId, 4> &nodes,
                const std::array<NodeId, 3> &face);

/**
 * @brief Extract the vertex ids belonging to the face within cell,
 * determined by its apex by apex_id.
 */
template <std::size_t N>
std::array<NodeId, N - 1>
face_vertex_ids_by_idx(const std::array<NodeId, N> &cell,
                       const std::size_t apex_index) {
  if (apex_index >= N)
    throw FaceException{"apex_index out of bounds"};
  auto vertices = std::array<NodeId, N - 1>{};
  auto n = std::size_t{0};
  for (std::size_t i = 0; i < N; i++)
    if (i != apex_index)
      vertices[n++] = cell[i];
  if (apex_index % 2 == 1)
    std::swap(vertices[0], vertices[1]);
  return vertices;
}
extern template std::array<NodeId, 3>
face_vertex_ids_by_idx(const std::array<NodeId, 4> &cell,
                       const std::size_t apex_index);

/**
 * @brief Extract the vertex ids belonging to the face within cell,
 * determined by its apex by apex_id.
 */
template <std::size_t N>
std::array<NodeId, N - 1> face_vertex_ids(const std::array<NodeId, N> &cell,
                                          const NodeId apex_id) {
  auto vertices = std::array<NodeId, N - 1>{};
  auto n = std::size_t{0};
  auto apex_index = std::size_t{N};
  for (const auto &vertex_id : cell)
    if (vertex_id == apex_id)
      apex_index = n;
    else
      vertices[n++] = vertex_id;
  if (apex_index == N)
    throw FaceException{"Cell does not contain apex_id"};
  if (apex_index % 2 == 1)
    std::swap(vertices[0], vertices[1]);
  return vertices;
}
extern template std::array<NodeId, 3>
face_vertex_ids(const std::array<NodeId, 4> &cell, const NodeId apex_id);

} // namespace qcmesh::mesh
