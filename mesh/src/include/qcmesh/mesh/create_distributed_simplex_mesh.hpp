// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/connect_global_cell_faces.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include "qcmesh/mesh/traits/node.hpp"
#include "qcmesh/mpi/mpi_sub_range.hpp"
#include <boost/mpi.hpp>

namespace qcmesh::mesh {

namespace detail {
inline std::tuple<std::size_t, std::size_t>
mpi_sub_range(const std::size_t total_size) {
  const auto [start_idx, range_size] = qcmesh::mpi::mpi_sub_range(total_size);
  return std::make_tuple(start_idx, start_idx + range_size);
}
} // namespace detail

/**
 * @brief Create a SimplexMesh instance for given nodal data, cell data and mpi
 * rank. In contrast to @ref create_distributed_simplex_mesh, this function does
 * not touch the field `neighbors` of `mesh.cells`.
 */
template <class NodalData, class CellData, std::size_t K, class NodeType>
SimplexMesh<traits::Position<NodeType>::DIMENSION, K - 1, NodalData, CellData>
create_distributed_unconnected_simplex_mesh(
    const std::vector<NodeType> &nodal_data,
    const std::vector<std::array<std::size_t, K>> &cells,
    const bool throw_on_degeneracy = true) {
  static_assert(traits::IS_IMPLEMENTED<traits::Position<NodeType>>);
  constexpr std::size_t D = traits::Position<NodeType>::DIMENSION;
  using Cell = SimplexCell<K - 1, CellData>;

  const auto mpi_rank = boost::mpi::communicator{}.rank();
  const auto [start_idx, end_idx] = detail::mpi_sub_range(cells.size());

  auto indexed_cells = std::unordered_map<SimplexCellId, Cell>{};
  auto indexed_nodes = std::unordered_map<NodeId, Node<D, NodalData>>{};
  for (std::size_t idx = start_idx; idx < end_idx; idx++) {
    const auto cell =
        build_cell<CellData>(nodal_data, cells[idx], SimplexCellId{idx},
                             mpi_rank, throw_on_degeneracy);
    indexed_cells.emplace(cell.id, cell);
    for (const auto node_idx : cells[idx]) {
      const auto node_id = NodeId{node_idx};
      if (indexed_nodes.find(node_id) == indexed_nodes.end())
        indexed_nodes.emplace(
            node_id,
            build_node<NodalData>(nodal_data[node_idx], node_id, mpi_rank));
    }
  }

  auto mesh = SimplexMesh<D, K - 1, NodalData, CellData>{
      .nodes = indexed_nodes, .cells = indexed_cells};
  mesh.cell_id_counter = cells.size();

  return mesh;
}

/**
 * @brief This whole detail block is only about adding a new iterator
 * cell_range() -> EnumeratorRange.
 */
namespace detail {

template <class IteratorType> struct EnumeratorRange {
  std::size_t start_pos, end_pos;
  IteratorType iter_start, iter_end;
  EnumeratorRange(const std::size_t start_pos, const std::size_t end_pos,
                  const IteratorType iter_start, const IteratorType iter_end)
      : start_pos{start_pos}, end_pos{end_pos},
        iter_start{iter_start}, iter_end{iter_end} {}
};
template <class IteratorType> struct CellEnumerator {
  std::size_t pos;
  IteratorType iter;
  CellEnumerator(const std::size_t pos, const IteratorType iter)
      : pos{pos}, iter{iter} {};
};

template <class IteratorType>
CellEnumerator<IteratorType> begin(const EnumeratorRange<IteratorType> &range) {
  return CellEnumerator{range.start_pos, range.iter_start};
}
template <class IteratorType>
CellEnumerator<IteratorType> end(const EnumeratorRange<IteratorType> &range) {
  return CellEnumerator{range.end_pos, range.iter_end};
}

template <class IteratorType>
bool operator!=(const CellEnumerator<IteratorType> &enumerator,
                const CellEnumerator<IteratorType> &other) {
  return enumerator.iter != other.iter;
}
template <class IteratorType>
std::tuple<
    std::size_t,
    std::array<NodeId, std::tuple_size<typename IteratorType::value_type>{}>>
operator*(CellEnumerator<IteratorType> &enumerator) {
  return std::make_tuple(enumerator.pos,
                         detail::index_cell_indices(*enumerator.iter));
}
template <class IteratorType>
CellEnumerator<IteratorType> &
operator++(CellEnumerator<IteratorType> &enumerator) {
  ++enumerator.pos;
  ++enumerator.iter;
  return enumerator;
}

template <std::size_t K>
EnumeratorRange<
    typename std::vector<std::array<std::size_t, K>>::const_iterator>
cell_range(const std::vector<std::array<std::size_t, K>> &cells,
           const std::size_t start, const std::size_t stop) {
  return EnumeratorRange{start, stop, cells.cbegin() + start,
                         cells.cbegin() + stop};
}
} // namespace detail

/**
 * @brief Create a distributed SimplexMesh instance for given nodal data, cell
 * data and mpi rank.
 */
template <class NodalData, class CellData, std::size_t K, class NodeType>
SimplexMesh<traits::Position<NodeType>::DIMENSION, K - 1, NodalData, CellData>
create_distributed_simplex_mesh(
    const std::vector<NodeType> &nodal_data,
    const std::vector<std::array<std::size_t, K>> &cells,
    const bool throw_on_degeneracy = true) {
  auto mesh = create_distributed_unconnected_simplex_mesh<NodalData, CellData>(
      nodal_data, cells, throw_on_degeneracy);
  const auto face_hashes = connect_local_cell_faces(mesh.cells);
  const auto [start_idx, end_idx] = detail::mpi_sub_range(cells.size());

  detail::connect_external_cells(
      mesh.cells, detail::cell_range(cells, 0, start_idx), face_hashes);
  detail::connect_external_cells(
      mesh.cells, detail::cell_range(cells, end_idx, cells.size()),
      face_hashes);
  return mesh;
}

} // namespace qcmesh::mesh
