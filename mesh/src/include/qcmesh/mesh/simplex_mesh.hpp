// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Flexible mesh type.
 *
 * @author Prateek Gupta
 */

#pragma once

#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/connect_local_cell_faces.hpp"
#include "qcmesh/mesh/node.hpp"
#include "qcmesh/mesh/primitive_node.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include "qcmesh/mesh/traits/node.hpp"
#include "qcmesh/mesh/traits/transfer_data.hpp"
#include <Eigen/Dense>
#include <stdexcept>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh {

/**
 * @brief Mesh type consisting of cells and nodes only.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
struct SimplexMesh {
  std::unordered_map<NodeId, Node<D, NodalData>> nodes{};
  std::unordered_map<SimplexCellId, SimplexCell<K, CellData>> cells{};
  std::unordered_map<NodeId, Node<D, NodalData>> halo_nodes{};
  std::unordered_map<SimplexCellId, SimplexCell<K, CellData>> halo_cells{};
  std::size_t cell_id_counter = 0;
};
extern template struct SimplexMesh<3, 3, EmptyData, EmptyData>;

/**
 * @brief Build a Node for given nodal data node id and mpi_rank.
 */
template <class NodalData, class NodeType>
Node<traits::Position<NodeType>::DIMENSION, NodalData>
build_node(const NodeType &raw_node, const NodeId node_id, const int mpi_rank) {
  static_assert(traits::IS_IMPLEMENTED<traits::Position<NodeType>>);
  constexpr std::size_t D = traits::Position<NodeType>::DIMENSION;
  auto node = Node<D, NodalData>{};
  if constexpr (traits::IS_IMPLEMENTED<
                    traits::TransferData<NodeType, NodalData>>)
    traits::transfer_data(raw_node, node.data);
  node.id = node_id;
  node.process_rank = mpi_rank;
  node.position = traits::position(raw_node);
  return node;
}

/**
 * @brief Build an std::unordered_map of Node for given nodal data and mpi_rank.
 */
template <class NodalData, class NodeType>
std::unordered_map<NodeId,
                   Node<traits::Position<NodeType>::DIMENSION, NodalData>>
index_nodes(const std::vector<NodeType> &node_drafts, const int mpi_rank) {
  static_assert(traits::IS_IMPLEMENTED<traits::Position<NodeType>>);
  constexpr std::size_t D = traits::Position<NodeType>::DIMENSION;
  auto indexed_nodes = std::unordered_map<NodeId, Node<D, NodalData>>{};
  indexed_nodes.reserve(node_drafts.size());
  auto counter = std::size_t{0};
  for (const auto &draft : node_drafts) {
    const auto node = build_node<NodalData>(draft, NodeId{counter++}, mpi_rank);
    indexed_nodes.emplace(node.id, node);
  }
  return indexed_nodes;
}

namespace detail {
template <std::size_t K>
std::array<NodeId, K>
index_cell_indices(const std::array<std::size_t, K> &cell) {
  auto node_ids = std::array<NodeId, K>{};
  for (std::size_t i = 0; i < K; i++)
    node_ids[i] = NodeId{cell[i]};
  return node_ids;
}

} // namespace detail

/**
 * @brief For given vertices and a cell defined by a set of indices within the
 * vector of vertices, return the lattice coordinates of the cell nodes.
 */
template <std::size_t K, class V>
std::array<std::array<int, traits::LatticeCoordinate<V>::DIMENSION>, K>
cell_lattice_coordinates(const std::vector<V> &vertices,
                         const std::array<std::size_t, K> &cell) {
  static_assert(traits::IS_IMPLEMENTED<traits::LatticeCoordinate<V>>);
  auto out =
      std::array<std::array<int, traits::LatticeCoordinate<V>::DIMENSION>, K>{};
  for (std::size_t i = 0; i < K; i++)
    out[i] = traits::lattice_coordinate(vertices[cell[i]]);
  return out;
};

/**
 * @brief Mesh exception
 */
struct Exception : std::runtime_error {
  using std::runtime_error::runtime_error;
};

namespace detail {
template <class T, std::size_t N, std::size_t M>
std::string to_string(const std::array<std::array<T, N>, M> &matrix) {
  auto repr = std::string{"["};
  for (const auto &row : matrix) {
    repr += '[';
    for (std::size_t i = 0; i < row.size(); i++) {
      repr += std::to_string(row[i]);
      if (i + 1 != row.size())
        repr += ", ";
    }
    repr += ']';
  }
  repr += ']';
  return repr;
}

template <std::size_t N>
std::string to_string(const std::array<std::size_t, N> &arr) {
  auto repr = std::string{"("};
  for (std::size_t i = 0; i < arr.size(); i++) {
    repr += std::to_string(arr[i]);
    if (i + 1 != arr.size())
      repr += ", ";
  }
  repr += ')';
  return repr;
}

template <std::size_t N, class NodeType>
std::array<std::array<double, traits::Position<NodeType>::DIMENSION>, N>
cell_node_positions(const std::vector<NodeType> &vertices,
                    const std::array<std::size_t, N> &cell) {
  static_assert(traits::IS_IMPLEMENTED<traits::Position<NodeType>>);
  constexpr std::size_t DIMENSION = traits::Position<NodeType>::DIMENSION;
  auto out = std::array<std::array<double, DIMENSION>, N>{};
  for (std::size_t i = 0; i < cell.size(); i++)
    out[i] = traits::position(vertices[cell[i]]);
  return out;
};

} // namespace detail

/**
 * @brief For given vertices and a cell defined by a set of indices within the
 * vector of vertices, construct the lattice basis of the cell.
 */
template <std::size_t Dimension>
std::array<std::array<double, Dimension>, Dimension>
cell_basis(const std::array<std::array<int, Dimension>, Dimension> &n,
           const std::array<std::array<double, Dimension>, Dimension> &jac,
           const std::array<std::size_t, Dimension + 1> &cell,
           const bool throw_on_degeneracy = true) {
  if (throw_on_degeneracy && array_ops::as_eigen_matrix(n).determinant() == 0)
    throw Exception{std::string{"cell "} + detail::to_string(cell) +
                    ": det(N) = 0 for N = " + detail::to_string(n)};
  constexpr double TOLERANCE = 1.E-10;
  if (throw_on_degeneracy &&
      std::abs(array_ops::as_eigen_matrix(jac).determinant()) < TOLERANCE)
    throw Exception{"det(jac) below tolerance"};
  auto out = std::array<std::array<double, Dimension>, Dimension>{};
  array_ops::as_eigen_matrix(out) =
      array_ops::as_eigen_matrix(jac).transpose() *
      array_ops::as_eigen_matrix(n)
          .transpose()
          .template cast<double>()
          .inverse();
  return out;
};

/**
 * @brief Set volume and quality of a cell if it carries volume / quality.
 */
template <class CellType, std::size_t D, std::size_t N>
void set_cell_data(CellType &cell,
                   const std::array<std::array<double, D>, N> &node_positions) {
  if constexpr (traits::IS_IMPLEMENTED<traits::Volume<CellType>>)
    traits::volume(cell) = std::abs(geometry::simplex_volume(node_positions));
  if constexpr (traits::IS_IMPLEMENTED<traits::Quality<CellType>>)
    traits::quality(cell) =
        geometry::simplex_metric_residual<D>(node_positions);
}

/**
 * @brief Build a Cell<K> for given cell data, cell id and mpi_rank.
 */
template <class CellData, std::size_t K, class NodeType>
SimplexCell<K - 1, CellData>
build_cell(const std::vector<NodeType> &node_drafts,
           const std::array<std::size_t, K> &cell_draft,
           const SimplexCellId cell_id, const int mpi_rank,
           const bool throw_on_degeneracy = true) {
  using Cell = SimplexCell<K - 1, CellData>;
  auto cell = SimplexCell<K - 1, CellData>{
      .id = cell_id,
      .process_rank = mpi_rank,
      .nodes = detail::index_cell_indices(cell_draft)};
  if constexpr (traits::IS_IMPLEMENTED<traits::Volume<Cell>> ||
                traits::IS_IMPLEMENTED<traits::Quality<Cell>> ||
                traits::IS_IMPLEMENTED<traits::LatticeBasis<Cell>> ||
                traits::IS_IMPLEMENTED<traits::IsAtomistic<Cell>>) {
    const auto simplex = detail::cell_node_positions(node_drafts, cell_draft);
    if constexpr (traits::IS_IMPLEMENTED<traits::LatticeBasis<Cell>> ||
                  traits::IS_IMPLEMENTED<traits::IsAtomistic<Cell>>) {
      if constexpr (traits::IS_IMPLEMENTED<
                        traits::LatticeCoordinate<NodeType>>) {
        static_assert(traits::LatticeCoordinate<NodeType>::DIMENSION == K - 1);
        const std::array<std::array<int, 3>, 3> n = geometry::simplex_jacobian(
            cell_lattice_coordinates(node_drafts, cell_draft));
        if constexpr (traits::IS_IMPLEMENTED<traits::LatticeBasis<Cell>>) {
          const auto jac = geometry::simplex_jacobian(simplex);
          traits::LatticeBasis<Cell>::lattice_basis(cell) =
              cell_basis(n, jac, cell_draft, throw_on_degeneracy);
        }
        if constexpr (traits::IS_IMPLEMENTED<traits::IsAtomistic<Cell>>)
          traits::is_atomistic(cell) =
              array_ops::as_eigen_matrix(n).determinant() == 1;
      } else { // use canonical lattice coordinates with N = Id
        if constexpr (traits::IS_IMPLEMENTED<traits::LatticeBasis<Cell>>)
          traits::LatticeBasis<Cell>::lattice_basis(cell) =
              geometry::simplex_jacobian(simplex);
        if constexpr (traits::IS_IMPLEMENTED<traits::IsAtomistic<Cell>>)
          traits::is_atomistic(cell) = true;
      }
    }
    set_cell_data(cell, simplex);
  }
  return cell;
}

/**
 * @brief Build an std::unordered_map of Cell<K> for given cell data and
 * mpi_rank.
 */
template <class CellData, std::size_t K, class NodeType>
std::unordered_map<SimplexCellId, SimplexCell<K - 1, CellData>>
build_cells(const std::vector<NodeType> &node_drafts,
            const std::vector<std::array<std::size_t, K>> &cells,
            const int mpi_rank, const bool throw_on_degeneracy = true) {
  using Cell = SimplexCell<K - 1, CellData>;
  auto indexed_cells = std::unordered_map<SimplexCellId, Cell>{};
  indexed_cells.reserve(cells.size());
  auto counter = std::size_t{0};
  for (const auto &cell_draft : cells) {
    const auto cell =
        build_cell<CellData>(node_drafts, cell_draft, SimplexCellId{counter++},
                             mpi_rank, throw_on_degeneracy);
    indexed_cells.emplace(cell.id, cell);
  }
  return indexed_cells;
}

/**
 * @brief Create a SimplexMesh instance for given nodal data, cell data and mpi
 * rank. In contrast to `create_local_simplex_mesh`, this function does not
 * touch the field `neighbors` of `mesh.cells`.
 * Data stored in the `NodeType` instances of the argument `nodes`
 * is transferred to the created mesh via the node traits:
 * - @ref traits::Position
 * More over, cell data in the created mesh will be initialized depending
 * on th following cell traits implemented by `SimplexCell<K, CellData>`:
 * - @ref traits::LatticeBasis (e.g. `cell.data.lattice_vector`)
 * - @ref traits::Volume (e.g. `cell.data.volume`)
 * - @ref traits::Quality (e.g. `cell.data.quality`)
 * - @ref traits::IsAtomistic (`cell.data.is_atomistic`)
 *
 * Furthermore, to support custom nodal data, the trait
 * @ref qcmesh::mesh::traits::TransferData can be used to inject nodal data.
 * Here, the `FromType` template argument must be the node type used in @ref
 * BareMesh::nodes and the `ToType` must be @ref QCNodalData.
 */
template <class NodalData, class CellData, std::size_t K, class NodeType>
SimplexMesh<traits::Position<NodeType>::DIMENSION, K - 1, NodalData, CellData>
create_local_unconnected_simplex_mesh(
    const std::vector<NodeType> &nodal_data,
    const std::vector<std::array<std::size_t, K>> &cells,
    const int mpi_rank = 0, const bool throw_on_degeneracy = true) {
  static_assert(traits::IS_IMPLEMENTED<traits::Position<NodeType>>);
  constexpr std::size_t D = traits::Position<NodeType>::DIMENSION;
  auto mesh = SimplexMesh<D, K - 1, NodalData, CellData>{
      .nodes = index_nodes<NodalData>(nodal_data, mpi_rank),
      .cells = build_cells<CellData>(nodal_data, cells, mpi_rank,
                                     throw_on_degeneracy)};
  mesh.cell_id_counter = mesh.cells.size();

  return mesh;
}

/**
 * @brief Create a SimplexMesh instance for given nodal data, cell data and mpi
 * rank.
 */
template <class NodalData, class CellData, std::size_t K, class NodeType>
SimplexMesh<traits::Position<NodeType>::DIMENSION, K - 1, NodalData, CellData>
create_local_simplex_mesh(const std::vector<NodeType> &nodal_data,
                          const std::vector<std::array<std::size_t, K>> &cells,
                          const int mpi_rank = 0,
                          const bool throw_on_degeneracy = true) {
  auto mesh = create_local_unconnected_simplex_mesh<NodalData, CellData>(
      nodal_data, cells, mpi_rank, throw_on_degeneracy);
  connect_local_cell_faces(mesh.cells);
  return mesh;
}

/**
 * @brief Get a (halo) vertex by handle.
 */
template <std::size_t N, std::size_t K, class NodalData, class CellData>
inline const Node<N, NodalData> &
deref_node_id(const SimplexMesh<N, K, NodalData, CellData> &mesh,
              const NodeId node_id) {
  if (mesh.nodes.find(node_id) != mesh.nodes.end())
    return mesh.nodes.at(node_id);
  return mesh.halo_nodes.at(node_id);
}

/**
 * @brief Get a const (halo) cell by id.
 */
template <std::size_t N, std::size_t K, class NodalData, class CellData>
inline const SimplexCell<N, CellData> &
deref_cell_id(const SimplexMesh<N, K, NodalData, CellData> &mesh,
              const SimplexCellId id) {
  if (mesh.cells.find(id) != mesh.cells.end())
    return mesh.cells.at(id);
  return mesh.halo_cells.at(id);
}

/**
 * @brief Get a (halo) cell by id.
 */
template <std::size_t N, std::size_t K, class NodalData, class CellData>
inline SimplexCell<N, CellData> &
deref_cell_id(SimplexMesh<N, K, NodalData, CellData> &mesh,
              const SimplexCellId id) {
  if (mesh.cells.find(id) != mesh.cells.end())
    return mesh.cells.at(id);
  return mesh.halo_cells.at(id);
}

/**
 * @brief For given cell and containing mesh, return the positions of the cell
 * nodes.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::array<std::array<double, D>, K + 1>
simplex_cell_points(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                    const std::array<NodeId, K + 1> &cell) {
  auto out = std::array<std::array<double, D>, K + 1>{};
  for (std::size_t i = 0; i < cell.size(); i++)
    out[i] = deref_node_id(mesh, cell[i]).position;
  return out;
};
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::array<std::array<double, D>, K + 1>
simplex_cell_points(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                    const SimplexCell<K, CellData> &cell) {
  auto out = std::array<std::array<double, D>, K + 1>{};
  for (std::size_t i = 0; i < cell.nodes.size(); i++)
    out[i] = deref_node_id(mesh, cell.nodes[i]).position;
  return out;
};
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::array<std::array<double, D>, K + 1>
simplex_cell_points(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                    const SimplexCellId cell_id) {
  return simplex_cell_points(mesh, deref_cell_id(mesh, cell_id));
};

template <std::size_t D, std::size_t K, class NodalData, class CellData>
bool mesh_has_node(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                   const NodeId node_id, const int process_rank) {
  if (mesh.nodes.find(node_id) == mesh.nodes.end())
    return false;
  return mesh.nodes.at(node_id).process_rank == process_rank;
}

template <std::size_t D, std::size_t K, class NodalData, class CellData>
bool mesh_has_cell(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                   const SimplexCellId cell_id, const int process_rank) {
  if (mesh.cells.find(cell_id) == mesh.cells.end())
    return false;
  return mesh.cells.at(cell_id).process_rank == process_rank;
}

/**
 * @brief Remove the cell from all neighbors' `neighbors` field.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void disconnect_cell_from_mesh(SimplexMesh<D, K, NodalData, CellData> &mesh,
                               const SimplexCellId cell_id) {
  const auto &cell = deref_cell_id(mesh, cell_id);
  for (auto &neighbor_handle : cell.neighbors)
    if (neighbor_handle.has_value()) {
      auto &neighbor_cell = deref_cell_id(mesh, neighbor_handle.value());
      for (auto &neighbor_neighbor : neighbor_cell.neighbors)
        if (neighbor_neighbor.has_value() &&
            neighbor_neighbor.value() == cell_id)
          neighbor_neighbor = std::nullopt;
    }
}

/**
 * @brief Increment the cell_id_counter of a mesh such that no id collisions
 * occur between multiple processes.
 *
 * This is implemented by enforcing that process id `rank` only can take values
 * in `{rank + n_processes * i | i = 0, 1, ...}`.
 */
std::size_t increment_counter(const std::size_t current_value,
                              const std::size_t process_rank,
                              const std::size_t n_processes);

/**
 * @brief A counter using @ref increment_counter to yield values.
 */
class IdCounter {
  std::size_t current_value{};
  std::size_t process_rank{};
  std::size_t n_processes{};

public:
  IdCounter() = delete;
  IdCounter(std::size_t current_value, std::size_t process_rank,
            std::size_t n_processes);
  /**
   * @brief Yield the next value.
   */
  std::size_t next();
};

} // namespace qcmesh::mesh
