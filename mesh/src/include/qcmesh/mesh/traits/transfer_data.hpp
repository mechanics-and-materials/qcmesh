// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/node.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"

namespace qcmesh::mesh::traits {

/**
 * @brief Trait used to transfer data from objects of `FromType` to
 * objects of `ToType`.
 *
 * An implementation must provide:
 * - `static void transfer_data(const FromType&, ToType &)`
 */
template <class FromType, class ToType, class Enable = void>
struct TransferData;

/**
 * @brief Create an instance of `ToType` initializing with data from `FromType`
 * via the @ref TransferData trait.
 */
template <class ToType, class FromType> ToType convert(const FromType &from) {
  auto to = ToType{};
  TransferData<FromType, ToType>::transfer_data(from, to);
  return to;
}

/**
 * @brief Transfer data from objects of `FromType` to
 * objects of `ToType` via the @ref TransferData trait
 */
template <class FromType, class ToType>
void transfer_data(const FromType &from, ToType &to) {
  TransferData<FromType, ToType>::transfer_data(from, to);
}

/**
 * @brief Implementation of @ref TransferData for `Node<_, NodalData> ->
 * NodalData`.
 */
template <std::size_t D, class NodalData>
struct TransferData<Node<D, NodalData>, NodalData> {
  static void transfer_data(const Node<D, NodalData> &node, NodalData &data) {
    data = node.data;
  }
};

/**
 * @brief Implementation of @ref TransferData for `Node<_, TypeFrom> -> Node<_,
 * TypeTo>`, if @ref TransferData is implemented for `TypeFrom` -> `TypeTo`.
 */
template <std::size_t D, class TypeFrom, class TypeTo>
struct TransferData<
    Node<D, TypeFrom>, Node<D, TypeTo>,
    std::enable_if_t<IS_IMPLEMENTED<TransferData<TypeFrom, TypeTo>>>> {
  static void transfer_data(const Node<D, TypeFrom> &from,
                            Node<D, TypeTo> &to) {
    to.id = from.id;
    to.process_rank = from.process_rank;
    to.position = from.position;
    to.type = from.type;
    traits::transfer_data(from.data, to.data);
  }
};

/**
 * @brief Implementation of @ref TransferData for `SimplexCell<_, CellData> ->
 * CellData`.
 */
template <std::size_t K, class CellData>
struct TransferData<SimplexCell<K, CellData>, CellData> {
  static void transfer_data(const SimplexCell<K, CellData> &cell,
                            CellData &data) {
    data = cell.data;
  }
};

/**
 * @brief Implementation of @ref TransferData for `SimplexCell<_, TypeA> ->
 * SimplexCell<_, TypeB>`, if @ref TransferData is implemented for `TypeA` ->
 * `TypeB`.
 */
template <std::size_t K, class TypeFrom, class TypeTo>
struct TransferData<
    SimplexCell<K, TypeFrom>, SimplexCell<K, TypeTo>,
    std::enable_if_t<IS_IMPLEMENTED<TransferData<TypeFrom, TypeTo>>>> {
  static void transfer_data(const SimplexCell<K, TypeFrom> &from,
                            SimplexCell<K, TypeTo> &to) {
    to.id = from.id;
    to.process_rank = from.process_rank;
    to.nodes = from.nodes;
    to.neighbors = from.neighbors;
    traits::transfer_data(from.data, to.data);
  }
};

} // namespace qcmesh::mesh::traits
