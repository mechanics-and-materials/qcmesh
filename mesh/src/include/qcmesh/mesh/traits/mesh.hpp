// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include <array>

namespace qcmesh::mesh::traits {

/**
 * @brief Used to access the undeformed lattice basis of mesh types.
 *
 * An implementation must provide:
 * - `static const std::array<std::array<double, DIMENSION>, DIMENSION>&
 * undeformed_lattice_basis(const T&, const SimplexCell<DIMENSION, CellData>&)`
 * - `constexpr static std::size_t DIMENSION`
 * - `using CellType = ...`
 */
template <class T, class Enable = void> struct UndeformedLatticeBasis;

/**
 * @brief Access the undeformed lattice basis of mesh types via
 * @ref UndeformedLatticeBasis.
 */
template <class T>
const std::array<std::array<double, UndeformedLatticeBasis<T>::DIMENSION>,
                 UndeformedLatticeBasis<T>::DIMENSION> &
undeformed_lattice_basis(
    const T &mesh, const typename UndeformedLatticeBasis<T>::CellType &cell) {
  return UndeformedLatticeBasis<T>::undeformed_lattice_basis(mesh, cell);
}

} // namespace qcmesh::mesh::traits
