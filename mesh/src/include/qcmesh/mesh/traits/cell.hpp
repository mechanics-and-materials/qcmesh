// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <array>
#include <type_traits>

namespace qcmesh::mesh::traits {

/**
 * @brief Used to access the lattice basis of cells.
 *
 * If implemented for a cell type, it will be used by @ref create_simplex_mesh
 * to populate the `lattice_vectors` field. An implementation must provide:
 * - `constexpr static std::size_t DIMENSION`,
 * - `static std::array<std::array<double, DIMENSION>, DIMENSION>&
 *   lattice_basis(T&)`,
 * - `static const std::array<std::array<double, DIMENSION>, DIMENSION>&
 *   lattice_basis(const T&)`.
 */
template <class T, class Enable = void> struct LatticeBasis;

/**
 * @brief Access the lattice basis of a cell.
 */
template <class T>
std::array<std::array<double, LatticeBasis<T>::DIMENSION>,
           LatticeBasis<T>::DIMENSION> &
lattice_basis(T &cell) {
  return LatticeBasis<T>::lattice_basis(cell);
}
/**
 * @brief Access the lattice basis of a cell.
 */
template <class T>
const std::array<std::array<double, LatticeBasis<T>::DIMENSION>,
                 LatticeBasis<T>::DIMENSION> &
lattice_basis(const T &cell) {
  return LatticeBasis<T>::lattice_basis(cell);
}

/**
 * @brief Implementation of @ref LatticeBasis for @ref SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct LatticeBasis<SimplexCell<K, DataType>,
                    std::enable_if_t<IS_IMPLEMENTED<LatticeBasis<DataType>>>> {
  constexpr static std::size_t DIMENSION = LatticeBasis<DataType>::DIMENSION;
  static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(SimplexCell<K, DataType> &cell) {
    return LatticeBasis<DataType>::lattice_basis(cell.data);
  }
  static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(const SimplexCell<K, DataType> &cell) {
    return LatticeBasis<DataType>::lattice_basis(cell.data);
  }
};

/**
 * @brief Used to access the volume of a cell.
 *
 * An implementation must provide:
 * - `static double& volume(T&)`.
 * - `static double volume(const T&)`.
 */
template <class T, class Enable = void> struct Volume;

/**
 * @brief Access the volume of a cell.
 */
template <class T> double &volume(T &cell) { return Volume<T>::volume(cell); }
/**
 * @brief Access the volume of a cell.
 */
template <class T> double volume(const T &cell) {
  return Volume<T>::volume(cell);
}

/**
 * @brief Implementation of @ref Volume for @ref SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct Volume<SimplexCell<K, DataType>,
              std::enable_if_t<IS_IMPLEMENTED<Volume<DataType>>>> {
  static double &volume(SimplexCell<K, DataType> &cell) {
    return Volume<DataType>::volume(cell.data);
  }
  static double volume(const SimplexCell<K, DataType> &cell) {
    return Volume<DataType>::volume(cell.data);
  }
};

/**
 * @brief Used to access the quality of a cell.
 *
 * An implementation must provide:
 * - `static double& quality(T&)`,
 * - `static const double& quality(const T&)`.
 */
template <class T, class Enable = void> struct Quality;

/**
 * @brief Access the quality of a cell.
 */
template <class T> double &quality(T &cell) {
  return Quality<T>::quality(cell);
}
/**
 * @brief Access the quality of a cell.
 */
template <class T> double quality(const T &cell) {
  return Quality<T>::quality(cell);
}

/**
 * @brief Implementation of @ref Quality for @ref SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct Quality<SimplexCell<K, DataType>,
               std::enable_if_t<IS_IMPLEMENTED<Quality<DataType>>>> {
  static double &quality(SimplexCell<K, DataType> &cell) {
    return Quality<DataType>::quality(cell.data);
  }
  static double quality(const SimplexCell<K, DataType> &cell) {
    return Quality<DataType>::quality(cell.data);
  }
};

/**
 * @brief Used to access the jacobian of a cell.
 *
 * An implementation must provide:
 * - `static double& jacobian(T&)`,
 * - `static const double& jacobian(const T&)`.
 */
template <class T, class Enable = void> struct Jacobian;

/**
 * @brief Access the jacobian of a cell.
 */
template <class T> double &jacobian(T &cell) {
  return Jacobian<T>::jacobian(cell);
}
/**
 * @brief Access the jacobian of a cell.
 */
template <class T> double jacobian(const T &cell) {
  return Jacobian<T>::jacobian(cell);
}

/**
 * @brief Implementation of @ref Jacobian for @ref SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct Jacobian<SimplexCell<K, DataType>,
                std::enable_if_t<IS_IMPLEMENTED<Jacobian<DataType>>>> {
  static double &jacobian(SimplexCell<K, DataType> &cell) {
    return Jacobian<DataType>::jacobian(cell.data);
  }
  static double jacobian(const SimplexCell<K, DataType> &cell) {
    return Jacobian<DataType>::jacobian(cell.data);
  }
};

/**
 * @brief Used to access the property of a cell if it is atomistic.
 *
 * An implementation must provide:
 * - `static bool& is_atomistic(T&)`,
 * - `static bool is_atomistic(const T&)`.
 */
template <class T, class Enable = void> struct IsAtomistic;

/**
 * @brief Access the property of a cell if it is atomistic.
 */
template <class T> bool &is_atomistic(T &cell) {
  return IsAtomistic<T>::is_atomistic(cell);
}
/**
 * @brief Access the property of a cell if it is atomistic.
 */
template <class T> bool is_atomistic(const T &cell) {
  return IsAtomistic<T>::is_atomistic(cell);
}

/**
 * @brief Implementation of @ref Is_Atomistic for @ref SimplexCell<K, DataType>
 * if `DataType` implements the trait.
 */
template <std::size_t K, class DataType>
struct IsAtomistic<SimplexCell<K, DataType>,
                   std::enable_if_t<IS_IMPLEMENTED<IsAtomistic<DataType>>>> {
  static bool &is_atomistic(SimplexCell<K, DataType> &cell) {
    return IsAtomistic<DataType>::is_atomistic(cell.data);
  }
  static bool is_atomistic(const SimplexCell<K, DataType> &cell) {
    return IsAtomistic<DataType>::is_atomistic(cell.data);
  }
};

} // namespace qcmesh::mesh::traits
