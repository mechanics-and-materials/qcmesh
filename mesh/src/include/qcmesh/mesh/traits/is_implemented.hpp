// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <type_traits>

namespace qcmesh::mesh::traits {

namespace detail {
/**
 * @brief Trigger a SFINAE failure if `T` is not instanceable.
 *
 * This will deselect the "true" specialization of @ref IsImplemented.
 */
template <class T> using AssertIsInstanceable = std::void_t<decltype(T{})>;
} // namespace detail

/**
 * @brief Check if a trait is implemented.
 *
 * Provides the member constant value equal to `true` if the trait is
 * implemented, i.e. if the corresponding struct is instanceable, equal to
 * `false` otherwise.
 */
template <class T, class = void> struct IsImplemented : std::false_type {};
template <class T>
struct IsImplemented<T, detail::AssertIsInstanceable<T>> : std::true_type {};

/**
 * @brief Check if a trait is implemented.
 *
 * `true` if the trait is implemented, i.e. if the corresponding struct is
 * instanceable, `false` otherwise.
 */
template <class T>
inline constexpr bool IS_IMPLEMENTED = IsImplemented<T>::value;

} // namespace qcmesh::mesh::traits
