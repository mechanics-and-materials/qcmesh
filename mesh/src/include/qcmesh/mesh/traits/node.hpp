// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/node.hpp"
#include <array>
#include <cstddef>

namespace qcmesh::mesh::traits {

/**
 * @brief Used to access the position of node types.
 *
 * An implementor can be used in @ref create_simplex_mesh to build the nodes of
 * the mesh. An implementation must provide
 * - `constexpr static std::size_t DIMENSION`,
 * - `static const std::array<double, DIMENSION>& position(const T&)`.
 */
template <class T, class Enable = void> struct Position;

/**
 * @brief Access the position of a node via the @ref Position trait.
 */
template <class T>
const std::array<double, Position<T>::DIMENSION> &position(const T &node) {
  return Position<T>::position(node);
}

/**
 * @brief Implementation of @ref Position for `std::array<double, D>`.
 */
template <std::size_t D> struct Position<std::array<double, D>> {
  constexpr static std::size_t DIMENSION = D;

  static const std::array<double, DIMENSION> &
  position(const std::array<double, D> &arr) {
    return arr;
  }
};
/**
 * @brief Implementation of @ref Position for @ref Node.
 */
template <std::size_t D, class NodalData> struct Position<Node<D, NodalData>> {
  constexpr static std::size_t DIMENSION = D;

  static const std::array<double, DIMENSION> &
  position(const Node<D, NodalData> &node) {
    return node.position;
  }
};

/**
 * @brief Used to access the lattice coordinates of node types.
 *
 * If implemented for a node type, it will be used by @ref create_simplex_mesh
 * to populate the `lattice_vectors` field. An implementation must provide
 * - `constexpr static std::size_t DIMENSION`,
 * - `static const std::array<int, DIMENSION>& lattice_coordinate(const T&)`.
 */
template <class T, class Enable = void> struct LatticeCoordinate;

/**
 * @brief Access the lattice coordinates of a node via the
 * @ref LatticeCoordinate trait.
 */
template <class T>
const std::array<int, LatticeCoordinate<T>::DIMENSION> &
lattice_coordinate(const T &node) {
  return LatticeCoordinate<T>::lattice_coordinate(node);
}

} // namespace qcmesh::mesh::traits
