// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mpi/serialization/optional.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

/**
 * @brief Provides serialization of qcmesh::mesh::SimplexCell.
 */
template <typename Archive, std::size_t Dimension, class CellData>
void serialize(Archive &ar,
               qcmesh::mesh::SimplexCell<Dimension, CellData> &cell,
               const unsigned int) {
  ar &cell.id;
  ar &cell.process_rank;
  ar &cell.nodes;
  ar &cell.neighbors;
  ar &cell.data;
}

/**
 * See
 * https://www.boost.org/doc/libs/release/doc/html/mpi/tutorial.html#mpi.tutorial.performance_optimizations
 */
template <std::size_t Dimension, class CellData>
struct implementation_level<qcmesh::mesh::SimplexCell<Dimension, CellData>> {
  // BOOST_CLASS_IMPLEMENTATION for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<object_serializable>;
  BOOST_STATIC_CONSTANT(int, value = implementation_level::type::value);
};

template <std::size_t Dimension, class CellData>
struct tracking_level<qcmesh::mesh::SimplexCell<Dimension, CellData>> {
  // BOOST_CLASS_TRACKING for template class
  // NOLINTNEXTLINE(readability-identifier-naming)
  using tag = mpl::integral_c_tag;
  // NOLINTNEXTLINE(readability-identifier-naming)
  using type = mpl::int_<boost::serialization::track_never>;
  BOOST_STATIC_CONSTANT(int, value = tracking_level::type::value);
  BOOST_STATIC_ASSERT(
      (mpl::greater<
          implementation_level<qcmesh::mesh::SimplexCell<Dimension, CellData>>,
          mpl::int_<primitive_type>>::value));
};

} // namespace boost::serialization
