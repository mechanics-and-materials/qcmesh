// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/json_dump/cell.hpp"
#include "qcmesh/mesh/json_dump/node.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"

namespace qcmesh::json_dump {

namespace detail {
template <class K, class T>
std::vector<T> values(const std::unordered_map<K, T> &map) {
  auto v = std::vector<T>{};
  v.reserve(map.size());
  for (const auto &[_, val] : map) {
    v.push_back(val);
  }
  return v;
}
} // namespace detail

template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::ostream &operator<<(
    std::ostream &os,
    const AsJson<qcmesh::mesh::SimplexMesh<D, K, NodalData, CellData>> &json) {
  const auto &mesh = json.value;
  return os << json_obj()
                   .attr("nodes", detail::values(mesh.nodes))
                   .attr("cells", detail::values(mesh.cells));
}

} // namespace qcmesh::json_dump
