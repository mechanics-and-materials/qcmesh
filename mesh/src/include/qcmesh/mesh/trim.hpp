// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Implementation of tetrahedral mesh trim
 */

#pragma once

#include "qcmesh/geometry/box.hpp"
#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/mesh/orient_elements.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"

namespace qcmesh::mesh {

/**
 * @brief Trims a mesh based on a box.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void trim(SimplexMesh<D, K, NodalData, CellData> &mesh,
          const geometry::Box<D> &domain, const double tolerance) {
  std::vector<SimplexCellId> elements_to_remove;

  for (const auto &[cell_id, cell] : mesh.cells) {
    const auto simplex = simplex_cell_points(mesh, cell);

    if (geometry::simplex_volume(simplex) < tolerance) {
      std::size_t surface_nodes = 0;

      for (std::size_t nd = 0; nd < D + 1; nd++)
        for (std::size_t d = 0; d < D; d++)
          if (abs(simplex[nd][d] - domain.lower[d]) < 1.0e-4 ||
              abs(simplex[nd][d] - domain.upper[d]) < 1.0e-4) {
            surface_nodes++;
            break;
          }
      if (surface_nodes > K)
        elements_to_remove.push_back(cell_id);
    }
  }

  // remove these degenerate elements on the surface
  for (const auto cell_id : elements_to_remove)
    mesh.cells.erase(cell_id);
}
extern template void trim(SimplexMesh<3, 3, EmptyData, EmptyData> &mesh,
                          const geometry::Box<3> &domain,
                          const double tolerance);

} // namespace qcmesh::mesh
