// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/simplex.hpp"
#include "qcmesh/geometry/triple_hash.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "unordered_set"

namespace qcmesh::mesh {

/**
 * @brief orient simplices so the volume comes positive.
 *
 * @return Ids of non orientable 0 volume cells.
 *
 * Reported 0 volume cells can be separately treated by using
 * @ref orient_local_0_vol_cells or
 * @ref orient_local_0_vol_cells_using_neighbors.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::unordered_set<SimplexCellId>
orient_elements(SimplexMesh<D, K, NodalData, CellData> &mesh) {
  constexpr double ZERO_THRESHOLD = 1.0e-8;
  auto zero_vol_cells = std::unordered_set<SimplexCellId>{};
  for (auto &[cell_id, cell] : mesh.cells) {
    const auto simplex = simplex_cell_points(mesh, cell);
    const auto volume = geometry::simplex_volume(simplex);
    if (std::abs(volume) < ZERO_THRESHOLD)
      zero_vol_cells.emplace(cell_id);
    else if (volume < 0.0)
      flip_orientation(cell);
  }
  return zero_vol_cells;
}

extern template std::unordered_set<SimplexCellId>
orient_elements(SimplexMesh<3, 3, EmptyData, EmptyData> &mesh);

namespace detail {
constexpr static std::array FACE_INDICES =
    std::array{std::array{0, 1, 2}, std::array{0, 3, 1}, std::array{0, 2, 3},
               std::array{3, 2, 1}};

std::tuple<std::array<NodeId, 3>, bool>
ordered_face_with_orientation(const std::array<NodeId, 3> &face);

template <std::size_t D, class NodalData, class CellData>
std::unordered_set<std::array<NodeId, 3>, geometry::TripleHash>
collect_zero_vol_faces(
    const SimplexMesh<D, 3, NodalData, CellData> &mesh,
    const std::unordered_set<SimplexCellId> &zero_vol_cells) {
  auto zero_vol_faces =
      std::unordered_set<std::array<NodeId, 3>, geometry::TripleHash>{};
  for (const auto cell_id : zero_vol_cells) {
    const auto &cell = mesh.cells.at(cell_id);
    for (const auto [i, j, k] : detail::FACE_INDICES) {
      const auto [ordered_face, _] = ordered_face_with_orientation(
          std::array{cell.nodes[i], cell.nodes[j], cell.nodes[k]});
      zero_vol_faces.emplace(ordered_face);
    }
  }
  return zero_vol_faces;
}

template <std::size_t D, class NodalData, class CellData>
std::unordered_map<std::array<NodeId, 3>, bool, geometry::TripleHash>
find_oriented_faces(const SimplexMesh<D, 3, NodalData, CellData> &mesh,
                    const std::unordered_set<std::array<NodeId, 3>,
                                             geometry::TripleHash> &faces,
                    const std::unordered_set<SimplexCellId> &zero_vol_cells) {
  auto oriented_faces =
      std::unordered_map<std::array<NodeId, 3>, bool, geometry::TripleHash>{};
  for (const auto &[cell_id, cell] : mesh.cells) {
    if (zero_vol_cells.find(cell_id) != zero_vol_cells.end())
      continue;
    for (const auto [i, j, k] : detail::FACE_INDICES) {
      const auto [ordered_face, orientation] = ordered_face_with_orientation(
          std::array{cell.nodes[i], cell.nodes[j], cell.nodes[k]});
      if (faces.find(ordered_face) != faces.end())
        oriented_faces.emplace(ordered_face, orientation);
    }
  }
  return oriented_faces;
}

template <class T, class U, std::size_t N>
std::size_t index_of(const std::array<T, N> &arr, const U val) {
  for (std::size_t i = 0; i < arr.size(); i++)
    if (arr[i] == val)
      return i;
  return arr.size();
}

/**
 * @brief Orientation of a face determined by its apex,
 * induced by the orientation of the cell.
 *
 * The orientation of the face is positive (true) if the
 * number of swaps needed to sort the face node ids
 * plus the apex position is even.
 * Examples (a denoting the apex node):
 * (a, 1, 2, 3) -> +
 * (1, a, 2, 3) -> -
 * (a, 2, 1, 3) -> -
 */
template <class T, std::size_t N>
bool face_orientation(const std::array<T, N> &cell, const std::size_t apex) {
  auto orientation = apex % 2 == 0;
  for (std::size_t i = 1; i < cell.size() - 1; i++)
    for (std::size_t j = i + 1; j < cell.size(); j++)
      if (cell[(apex + i) % cell.size()] > cell[(apex + j) % cell.size()])
        orientation = !orientation;
  return orientation;
}

} // namespace detail

/**
 * @brief Orient 0 volume cells towards there non zero volume neighbors.
 *
 * Does not require cells to have their `neighbors` fields populated.
 * Only considers thread-local neighbors of the cells.
 * Removes orientable 0 volume cells from `zero_vol_cells`.
 *
 * Idea: Given a 0 volume cell A, sharing a face with a non-zero
 * volume cell B.
 * If the orientation of the face as contained by B is defined as
 * "outward", then the same face with inverse orientation must be
 * "outward" for A.
 * The orientation of this face induces an orientation to the cell A.
 */
template <std::size_t D, class NodalData, class CellData>
void orient_local_0_vol_cells(
    SimplexMesh<D, 3, NodalData, CellData> &mesh,
    std::unordered_set<SimplexCellId> &zero_vol_cells) {
  const auto zero_vol_faces =
      detail::collect_zero_vol_faces(mesh, zero_vol_cells);
  auto oriented_faces =
      detail::find_oriented_faces(mesh, zero_vol_faces, zero_vol_cells);
  while (!zero_vol_cells.empty()) {
    std::vector<SimplexCellId> oriented_0_vol_cells{};
    for (const auto cell_id : zero_vol_cells) {
      auto &cell = mesh.cells.at(cell_id);
      for (const auto [i, j, k] : detail::FACE_INDICES) {
        const auto [ordered_face, current_orientation] =
            detail::ordered_face_with_orientation(
                std::array{cell.nodes[i], cell.nodes[j], cell.nodes[k]});
        const auto orientation = oriented_faces.find(ordered_face);
        if (orientation != oriented_faces.end()) {
          if (current_orientation == std::get<1>(*orientation))
            flip_orientation(cell);
          oriented_0_vol_cells.emplace_back(cell_id);
          // Register new oriented faces for potential 0-vol neighbors
          for (const auto [i, j, k] : detail::FACE_INDICES) {
            const auto [ordered_face, new_orientation] =
                detail::ordered_face_with_orientation(
                    std::array{cell.nodes[i], cell.nodes[j], cell.nodes[k]});
            oriented_faces.emplace(ordered_face, new_orientation);
          }
          break;
        }
      }
    }
    if (oriented_0_vol_cells.empty())
      break;
    for (const auto cell_id : oriented_0_vol_cells)
      zero_vol_cells.erase(cell_id);
  }
}

/**
 * @brief Orient 0 volume cells towards there non zero volume neighbors.
 *
 * Requires cells to have their `neighbors` fields populated.
 * Only considers thread-local neighbors of the cells.
 * Removes orientable 0 volume cells from `zero_vol_cells`.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void orient_local_0_vol_cells_using_neighbors(
    SimplexMesh<D, K, NodalData, CellData> &mesh,
    std::unordered_set<SimplexCellId> &zero_vol_cells) {

  while (!zero_vol_cells.empty()) {
    std::vector<SimplexCellId> oriented_0_vol_cells{};
    for (const auto cell_id : zero_vol_cells) {
      auto &cell = mesh.cells.at(cell_id);
      for (std::size_t apex_idx = 0; apex_idx < cell.neighbors.size();
           apex_idx++) {
        const auto &neighbor_id = cell.neighbors[apex_idx];
        if (neighbor_id.has_value() &&
            zero_vol_cells.find(neighbor_id.value()) == zero_vol_cells.end() &&
            mesh.cells.find(neighbor_id.value()) != mesh.cells.end()) {
          const auto &neighbor = mesh.cells.at(neighbor_id.value());
          const auto remote_apex_idx =
              detail::index_of(neighbor.neighbors, cell_id);
          const auto orientation =
              detail::face_orientation(cell.nodes, apex_idx);
          const auto remote_orientation =
              detail::face_orientation(neighbor.nodes, remote_apex_idx);
          if (remote_orientation == orientation)
            flip_orientation(cell);
          oriented_0_vol_cells.emplace_back(cell_id);
          break;
        }
      }
    }
    if (oriented_0_vol_cells.empty())
      break;
    for (const auto cell_id : oriented_0_vol_cells)
      zero_vol_cells.erase(cell_id);
  }
}

} // namespace qcmesh::mesh
