// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/testing/lattice_cell_data.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/array.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

template <class Archive>
void serialize(Archive &ar, qcmesh::mesh::testing::LatticeCellData &data,
               const unsigned int) {
  ar &data.lattice_vectors;
  ar &data.is_atomistic;
  ar &data.quality;
}
} // namespace boost::serialization

BOOST_CLASS_IMPLEMENTATION(qcmesh::mesh::testing::LatticeCellData,
                           object_serializable)
BOOST_CLASS_TRACKING(qcmesh::mesh::testing::LatticeCellData, track_never)
BOOST_IS_MPI_DATATYPE(qcmesh::mesh::testing::LatticeCellData)
