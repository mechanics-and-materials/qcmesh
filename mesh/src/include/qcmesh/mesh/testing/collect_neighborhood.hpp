// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Collect nearest (node) neighbors of a cell.
 */

#pragma once

#include "qcmesh/mesh/simplex_mesh.hpp"
#include <unordered_set>

namespace qcmesh::mesh::testing {

/** @file
 * @brief Add all nearest (node) neighbors of the cells in neighborhood
 * (inplace).
 */
template <std::size_t Dimension, class CellData>
void extend_neighborhood(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    std::unordered_set<SimplexCellId> &neighborhood) {
  auto node_ids = std::unordered_set<NodeId>{};
  for (const auto cell_id : neighborhood) {
    const auto &cell = cells.at(cell_id);
    for (const auto node_id : cell.nodes)
      node_ids.emplace(node_id);
  }
  for (const auto &[_, cell] : cells)
    for (const auto node_id : cell.nodes)
      if (node_ids.find(node_id) != node_ids.end())
        neighborhood.emplace(cell.id);
}

/** @file
 * @brief Collect a neighborhood of a cell consisting of the cell and all its
 * level-nearest (node) neighbors.
 */
template <std::size_t Dimension, class CellData>
std::unordered_set<SimplexCellId> collect_neighborhood(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::unordered_set<SimplexCellId> &region,
    const std::size_t level = 1) {
  auto neighbors = region;
  for (std::size_t i = 0; i < level; ++i)
    extend_neighborhood(cells, neighbors);
  return neighbors;
}

/** @file
 * @brief Copy all cells specified by `cell_ids` with the nodes they contain to
 * a new submesh.
 *
 * If level > 0 also add level-nearest (node) neighbors of the cells.
 */
template <std::size_t D, std::size_t K, class NodalDataType, class CellDataType>
SimplexMesh<D, K, NodalDataType, CellDataType>
extract_sub_mesh(const SimplexMesh<D, K, NodalDataType, CellDataType> &mesh,
                 const std::unordered_set<SimplexCellId> &cell_ids,
                 const std::size_t level = 0) {
  auto submesh = SimplexMesh<D, K, NodalDataType, CellDataType>{};
  const auto neighborhood = collect_neighborhood(mesh.cells, cell_ids, level);
  for (const auto cell_id : neighborhood) {
    const auto &cell = mesh.cells.at(cell_id);
    for (const auto node_id : cell.nodes)
      submesh.nodes.insert({node_id, mesh.nodes.at(node_id)});
    submesh.cells.insert({cell_id, cell});
  }
  submesh.cell_id_counter = mesh.cell_id_counter;
  return submesh;
}

} // namespace qcmesh::mesh::testing
