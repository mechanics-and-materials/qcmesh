// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/traits/node.hpp"
#include <array>

namespace qcmesh::mesh {
namespace testing {

/**
 * @brief Vertex with euclidean position and lattice coordinates.
 */
struct LatticeVertex {
  std::array<double, 3> position = {};
  std::array<int, 3> lattice_coordinates = {};
};

} // namespace testing

namespace traits {

/**
 * @brief Implementation of @ref Position.
 */
template <> struct Position<testing::LatticeVertex> {
  constexpr static std::size_t DIMENSION = 3;

  inline static const std::array<double, DIMENSION> &
  position(const testing::LatticeVertex &node) {
    return node.position;
  }
};

/**
 * @brief Implementation of LatticeCoordinate.
 */
template <> struct LatticeCoordinate<testing::LatticeVertex> {
  constexpr static std::size_t DIMENSION = 3;
  inline static const std::array<int, 3> &
  lattice_coordinate(const testing::LatticeVertex &vertex) {
    return vertex.lattice_coordinates;
  }
};

} // namespace traits

} // namespace qcmesh::mesh
