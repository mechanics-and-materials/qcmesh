// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/node.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include <algorithm>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh::testing {

/**
 * @brief Order and reindex nodes and cells defining a mesh, such that:
 * - `nodes` are ordered by position (lexicographically),
 * - `nodes[i].id = i`,
 * - for each cell, the node ids `SimplexCell::nodes` are ordered
 *   preserving orientation (ids ordered ascending, the last 2 ids
     flipped if the ordering is representable by an uneven number of id
     flipps),
 * - `cells` are ordered by `SimplexCell::nodes` (lexicographically),
 * - `cells[i].id = i`.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::tuple<std::vector<Node<D, NodalData>>,
           std::vector<SimplexCell<K, CellData>>>
canonically_order_mesh(const std::vector<Node<D, NodalData>> &nodes,
                       const std::vector<SimplexCell<K, CellData>> &cells) {
  auto ordered_nodes = nodes;
  auto ordered_cells = cells;
  std::sort(ordered_nodes.begin(), ordered_nodes.end(),
            [](const auto &node_a, const auto &node_b) {
              return node_a.position < node_b.position;
            });
  auto renumber_table =
      std::unordered_map<qcmesh::mesh::NodeId, qcmesh::mesh::NodeId>{};
  auto index = std::size_t{0};
  for (auto &node : ordered_nodes) {
    const auto new_id = qcmesh::mesh::NodeId{index++};
    renumber_table.emplace(node.id, new_id);
    node.id = new_id;
  }
  for (auto &cell : ordered_cells) {
    for (auto &id : cell.nodes)
      id = renumber_table.at(id);
    const auto original_neighbors = cell.neighbors;
    const auto original_nodes = cell.nodes;
    cell.nodes = ordered_oriented_array(cell.nodes);
    for (std::size_t i = 0; i < original_neighbors.size(); i++)
      for (std::size_t j = 0; j < original_nodes.size(); j++)
        if (original_nodes[i] == cell.nodes[j]) {
          cell.neighbors[i] = original_neighbors[j];
          break;
        }
  }
  const auto cast_array = [](const auto &ids) {
    auto out = std::array<std::size_t, K + 1>{};
    for (std::size_t i = 0; i < out.size(); i++)
      out[i] = static_cast<std::size_t>(ids[i]);
    return out;
  };
  std::sort(ordered_cells.begin(), ordered_cells.end(),
            [&cast_array](const auto &cell_a, const auto &cell_b) {
              return cast_array(cell_a.nodes) < cast_array(cell_b.nodes);
            });
  auto cell_renumber_table = std::unordered_map<qcmesh::mesh::SimplexCellId,
                                                qcmesh::mesh::SimplexCellId>{};
  for (std::size_t i = 0; i < ordered_cells.size(); i++) {
    cell_renumber_table.emplace(ordered_cells[i].id,
                                qcmesh::mesh::SimplexCellId{i});
    ordered_cells[i].id = qcmesh::mesh::SimplexCellId{i};
  }
  for (auto &cell : ordered_cells)
    for (auto &neighbor : cell.neighbors)
      if (neighbor.has_value())
        neighbor = cell_renumber_table.at(neighbor.value());

  return std::make_tuple(ordered_nodes, ordered_cells);
}

} // namespace qcmesh::mesh::testing
