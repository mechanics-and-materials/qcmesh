// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include <array>

namespace qcmesh::mesh {
namespace testing {

/**
 * @brief Cell data type for tests about lattice properties of a mesh.
 */
struct LatticeCellData {
  std::array<std::array<double, 3>, 3> lattice_vectors{};
  bool is_atomistic{};
  double quality{};
};

} // namespace testing

template <> struct traits::LatticeBasis<testing::LatticeCellData> {
  constexpr static std::size_t DIMENSION = 3;
  inline static std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(testing::LatticeCellData &data) {
    return data.lattice_vectors;
  }
  inline static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  lattice_basis(const testing::LatticeCellData &data) {
    return data.lattice_vectors;
  }
};

template <> struct traits::IsAtomistic<testing::LatticeCellData> {
  inline static bool &is_atomistic(testing::LatticeCellData &data) {
    return data.is_atomistic;
  }
  inline static bool is_atomistic(const testing::LatticeCellData &data) {
    return data.is_atomistic;
  }
};

template <> struct traits::Quality<testing::LatticeCellData> {
  inline static double &quality(testing::LatticeCellData &data) {
    return data.quality;
  }
  inline static double quality(const testing::LatticeCellData &data) {
    return data.quality;
  }
};

} // namespace qcmesh::mesh
