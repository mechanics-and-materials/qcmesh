// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Provides operator<< and literals for @ref qcmesh::mesh::NodeId.
 * Example: 0_vx, 1_vx.
 */

#pragma once

#include "qcmesh/mesh/node.hpp"
#include <ostream>

constexpr qcmesh::mesh::NodeId operator""_vx(const unsigned long long id) {
  return qcmesh::mesh::NodeId{id};
}

namespace qcmesh::mesh {

inline std::ostream &operator<<(std::ostream &os, const NodeId &node_id) {
  os << static_cast<std::size_t>(node_id) << "_vx";
  return os;
}

} // namespace qcmesh::mesh
