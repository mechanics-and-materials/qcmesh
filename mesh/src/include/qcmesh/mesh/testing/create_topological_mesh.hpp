// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"

namespace qcmesh::mesh::testing {

/**
 * @brief Create a mesh without geometry (no node positions / 0 dimensional
 * position).
 */
template <std::size_t NodesPerCell>
SimplexMesh<0, NodesPerCell - 1, EmptyData, EmptyData> create_topological_mesh(
    const std::vector<std::tuple<SimplexCellId, TopologicalCell<NodesPerCell>>>
        &topological_cells) {
  auto mesh = SimplexMesh<0, 3, EmptyData, EmptyData>{};
  for (const auto &[cell_id, cell] : topological_cells) {
    mesh.cells.emplace(cell_id, SimplexCell<3, EmptyData>{
                                    .id = cell_id, .nodes = cell.node_ids});
    for (const auto node_id : cell.node_ids)
      mesh.nodes.emplace(node_id, Node<0, EmptyData>{.id = node_id});
  }
  connect_local_cell_faces(mesh.cells);
  return mesh;
}

} // namespace qcmesh::mesh::testing
