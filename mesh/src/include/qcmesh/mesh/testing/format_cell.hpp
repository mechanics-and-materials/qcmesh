// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Provides operator<< @ref qcmesh::mesh::SimplexCell.
 */

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include <experimental/type_traits>
#include <ostream>

namespace qcmesh::mesh {

namespace detail {

template <class T>
using Streamable = decltype(std::declval<std::ostream>() << std::declval<T>());

} // namespace detail

template <std::size_t K, class CellData>
std::ostream &operator<<(std::ostream &os,
                         const SimplexCell<K, CellData> &cell) {
  os << '(' << static_cast<std::size_t>(cell.id) << ',' << cell.process_rank
     << ")<";
  for (std::size_t i = 0; i < cell.nodes.size(); i++) {
    os << static_cast<std::size_t>(cell.nodes[i]);
    if (i + 1 < cell.nodes.size())
      os << ',';
  }
  if constexpr (std::experimental::is_detected_v<detail::Streamable,
                                                 CellData>) {
    os << "|" << cell.data;
  }
  os << '>';
  return os;
}

} // namespace qcmesh::mesh
