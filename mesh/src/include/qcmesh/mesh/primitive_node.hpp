// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/traits/node.hpp"
#include <array>

namespace qcmesh::mesh {

/**
 * @brief Minimal nodal data.
 */
template <std::size_t D> struct PrimitiveNode {
  /// Position in Euclidean space
  std::array<double, D> position{};
};
extern template struct PrimitiveNode<3>;

/**
 * @brief Implementation of @ref traits::Position for @ref PrimitiveNode.
 */
template <std::size_t D> struct traits::Position<PrimitiveNode<D>> {
  constexpr static std::size_t DIMENSION = D;

  inline static const std::array<double, DIMENSION> &
  position(const PrimitiveNode<D> &node) {
    return node.position;
  }
};

} // namespace qcmesh::mesh
