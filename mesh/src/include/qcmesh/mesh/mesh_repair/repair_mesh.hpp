// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Mesh repairing.
 *
 * @author Prateek Gupta
 */
#pragma once

#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/optimize_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/serialization/cell_address.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/serialization/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/edge_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/exchange_halos.hpp"
#include "qcmesh/mesh/mesh_repair/face_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/serialization/cell_draft.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include "qcmesh/mesh/traits/mesh.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include <boost/mpi.hpp>

namespace qcmesh::mesh {
/**
 * @brief Apply a change set (cell ids to remove, cell drafts to insert) to a
 * mesh.
 */
template <class MeshType, class CellData>
void apply_change_set(
    MeshType &mesh,
    const ChangeSet<CellDraft<CellData>, SimplexCellId> &change_set) {
  if (change_set.out.size() == 1)
    for (const auto &id : change_set.out)
      disconnect_cell_from_mesh(mesh, id);
  // Info: remove_cells can be overloaded for specific mesh classes
  remove_cells(mesh, change_set.out);
  for (const auto &cell_draft : change_set.in)
    insert_cell(mesh, cell_draft);
}

/**
 * @brief If the change_set corresponds to removing a single cell without
 * adding new cells, we also need to notify other processes to disconnect
 * its neighbors.
 */
template <class CellData> struct ChangeSetExt {
  ChangeSet<CellDraft<CellData>, SimplexCellId> change_set;
  std::vector<SimplexCellId> disconnect_neighbors;
};

} // namespace qcmesh::mesh

namespace boost::serialization {

/**
 * @brief boost::mpi serializing support for meshing::ChangeSetExt.
 */
template <class Archive, class CellData>
void serialize(Archive &ar, qcmesh::mesh::ChangeSetExt<CellData> &change_set,
               const unsigned int) {
  ar &change_set.change_set;
  ar &change_set.disconnect_neighbors;
}

} // namespace boost::serialization

namespace qcmesh::mesh {

/**
 * @brief Collect all neighbors of a cell referenced by cell_handle
 * (allowed to reference a halo cell).
 */
template <const std::size_t D, const std::size_t K, class NodalData,
          class CellData>
std::vector<SimplexCellId>
collect_cell_neighbors(SimplexMesh<D, K, NodalData, CellData> &mesh,
                       const SimplexCellId cell_handle) {
  const auto collect_neighbors = [](const auto &cell) {
    auto neighbors = std::vector<SimplexCellId>{};
    for (std::size_t i = 0; i < cell.neighbors.size(); i++)
      if (cell.neighbors[i].has_value())
        neighbors.push_back(cell.neighbors[i].value());
    return neighbors;
  };
  if (mesh.cells.find(cell_handle) != mesh.cells.end())
    return collect_neighbors(mesh.cells.at(cell_handle));
  if (mesh.halo_cells.find(cell_handle) != mesh.halo_cells.end())
    return collect_neighbors(mesh.halo_cells.at(cell_handle));
  return {};
}

/**
 * @brief From a cell referenced by cell_handle (allowed to reference a holo
 * cell), remove all neighbor references to all cells in neighbors.
 */
template <const std::size_t N, const std::size_t K, class NodalData,
          class CellData>
void remove_cell_neighbors(SimplexMesh<N, K, NodalData, CellData> &mesh,
                           const SimplexCellId cell_handle,
                           const std::vector<SimplexCellId> &neighbors) {
  const auto remove_neighbors =
      [](auto &cell, const std::vector<SimplexCellId> &neighbors) {
        for (std::size_t i = 0; i < cell.neighbors.size(); i++)
          for (const auto &neighbor : neighbors)
            if (cell.neighbors[i].has_value() &&
                (cell.neighbors[i].value() == neighbor))
              cell.neighbors[i] = std::nullopt;
      };
  if (mesh.cells.find(cell_handle) != mesh.cells.end())
    remove_neighbors(mesh.cells.at(cell_handle), neighbors);
  if (mesh.halo_cells.find(cell_handle) != mesh.halo_cells.end())
    remove_neighbors(mesh.halo_cells.at(cell_handle), neighbors);
}

/**
 * @brief Same as insert_cell, but insert to halo cells instead.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
SimplexCell<D, CellData> &
insert_halo_cell(SimplexMesh<D, K, NodalData, CellData> &mesh,
                 const CellDraft<CellData> &cell_draft) {
  const auto cell_id = cell_draft.cell.id;

  if (static_cast<std::size_t>(cell_id) >= mesh.cell_id_counter)
    mesh.cell_id_counter = static_cast<std::size_t>(cell_id) + 1;

  connect_mesh_to_cell_draft(mesh, cell_draft);

  auto [inserted_cell_item, _already_existed] =
      mesh.halo_cells.emplace(cell_id, cell_draft.cell);
  auto &[_key, inserted_cell] = *inserted_cell_item;
  if constexpr (traits::IS_IMPLEMENTED<
                    traits::LatticeBasis<SimplexCell<K, CellData>>>)
    array_ops::as_eigen_matrix(traits::lattice_basis(inserted_cell)) =
        cell_draft.cell_basis;
  return inserted_cell;
}

/**
 * @brief Returns a vector of nodal data for all vertices occuring in
 * `cell_drafts`.
 *
 * This will be called in @ref apply_change_set_distributed_leaving_nodes and
 * can be overloaded for derived mesh types.
 */
template <const std::size_t D, class NodalData, class CellData>
auto collect_nodes(const SimplexMesh<D, 3, NodalData, CellData> &mesh,
                   const std::vector<CellDraft<CellData>> &cell_drafts) {
  auto nodes = std::vector<Node<D, NodalData>>{};
  auto collected_node_ids = std::unordered_set<NodeId>{};
  for (const auto &cell_draft : cell_drafts)
    for (const auto &node_id : cell_draft.cell.nodes)
      if (!collected_node_ids.emplace(node_id).second)
        nodes.push_back(deref_node_id(mesh, node_id));
  return nodes;
}

/**
 * @brief Collect all local node ids of cells specified by cell ids.
 */
template <const std::size_t D, const std::size_t K, class NodalData,
          class CellData>
auto collect_node_ids(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                      const std::vector<SimplexCellId> &cell_ids) {
  auto collected_node_ids = std::unordered_set<NodeId>{};
  for (const auto &cell_id : cell_ids) {
    if (mesh.cells.find(cell_id) == mesh.cells.end())
      continue;
    const auto &cell = mesh.cells.at(cell_id);
    for (const auto &node_id : cell.nodes)
      collected_node_ids.emplace(node_id);
  }
  return collected_node_ids;
}

/**
 * @brief Insert a halo node into a mesh.
 *
 * This will be called in @ref apply_change_set_distributed_leaving_nodes and
 * can be overloaded for derived mesh types.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void insert_halo_node(SimplexMesh<D, K, NodalData, CellData> &mesh,
                      const Node<D, NodalData> &node) {
  mesh.halo_nodes.insert({node.id, node});
}

/**
 * @brief Remove a cell from ordinary cells / halo cells of the mesh.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void remove_cell(SimplexMesh<D, K, NodalData, CellData> &mesh,
                 const SimplexCellId cell_id) {
  if (mesh.cells.find(cell_id) != mesh.cells.end())
    mesh.cells.erase(cell_id);
  else if (mesh.halo_cells.find(cell_id) != mesh.halo_cells.end())
    mesh.halo_cells.erase(cell_id);
}

/**
 * @brief Move a node from the halo to ordinary cell nodes.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void insert_node_from_halo(SimplexMesh<D, K, NodalData, CellData> &mesh,
                           const NodeId node_id) {
  if (mesh.nodes.find(node_id) == mesh.nodes.end())
    mesh.nodes.insert({node_id, mesh.halo_nodes.at(node_id)});
}

/**
 * @brief Remove multiple cells from ordinary cells / halo cells of the mesh.
 *
 * Will be called in @ref apply_change_set and @ref
 * apply_change_set_distributed_leaving_nodes and can be overloaded for derived
 * mesh types needing more work to wipe the cells.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void remove_cells(SimplexMesh<D, K, NodalData, CellData> &mesh,
                  const std::vector<SimplexCellId> &cell_ids) {
  for (const auto cell_id : cell_ids)
    remove_cell(mesh, cell_id);
}

/**
 * @brief Distributed version of @ref apply_change_set.
 *
 * This version does not remove unreferenced nodes after applying the change
 * set.
 */
template <class MeshType, class CellData>
bool apply_change_set_distributed_leaving_nodes(
    MeshType &mesh, ChangeSet<CellDraft<CellData>, SimplexCellId> &change_set,
    const CellAddress<int, SimplexCellId> &bad_cell) {
  const auto mpi_root = bad_cell.process_id;
  const auto world = boost::mpi::communicator{};
  auto payload = ChangeSetExt<CellData>{change_set, {}};
  if (world.rank() == mpi_root && change_set.out.size() == 1)
    // Need to notify other processes, so they disconnect the single cell:
    payload.disconnect_neighbors =
        collect_cell_neighbors(mesh, change_set.out.back());
  boost::mpi::broadcast(world, payload, mpi_root);
  change_set = payload.change_set;
  if (payload.change_set.out.empty() && payload.change_set.in.empty())
    return false;
  // sync cell id counter:
  boost::mpi::broadcast(world, mesh.cell_id_counter, mpi_root);

  for (const auto &neighbor : payload.disconnect_neighbors)
    remove_cell_neighbors(mesh, neighbor, payload.change_set.out);
  remove_cells(mesh, payload.change_set.out);

  // Insert new cells:
  auto halo_nodes = decltype(collect_nodes(mesh, payload.change_set.in)){};
  if (world.rank() == mpi_root) {
    for (const auto &cell_draft : payload.change_set.in)
      insert_cell(mesh, cell_draft);
    halo_nodes = collect_nodes(mesh, payload.change_set.in);
  }

  // distribute nodes:
  boost::mpi::broadcast(world, halo_nodes, mpi_root);
  if (world.rank() != mpi_root) {
    for (const auto &cell_draft : payload.change_set.in)
      insert_halo_cell(mesh, cell_draft);
    for (const auto &node : halo_nodes)
      if (mesh.nodes.find(node.id) == mesh.nodes.end() &&
          mesh.halo_nodes.find(node.id) == mesh.halo_nodes.end())
        insert_halo_node(mesh, node);
  }
  return true;
}

/**
 * @brief Distributed version of @ref apply_change_set.
 */
template <class MeshType, class CellData>
bool apply_change_set_distributed(
    MeshType &mesh, ChangeSet<CellDraft<CellData>, SimplexCellId> &change_set,
    const CellAddress<int, SimplexCellId> &bad_cell) {
  apply_change_set_distributed_leaving_nodes(mesh, change_set, bad_cell);
  // Remove nodes not owned by any cell:
  auto unneeded_nodes = collect_node_ids(mesh, change_set.out);
  for (const auto &[_, cell] : mesh.cells)
    for (const auto node_id : cell.nodes)
      unneeded_nodes.erase(node_id);
  for (const auto node_id : unneeded_nodes)
    mesh.nodes.erase(node_id);
  return true;
}

/**
 * @brief Perform the actual connection, noted by connect_cells.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
inline void
connect_mesh_to_cell_draft(SimplexMesh<D, K, NodalData, CellData> &mesh,
                           const CellDraft<CellData> &cell_draft) {
  for (const auto &connection : cell_draft.boundary_connections) {
    auto neighbor_cell = mesh.cells.find(connection.neighbor_handle);
    if (neighbor_cell != mesh.cells.end())
      neighbor_cell->second.neighbors[connection.neighbor_apex_idx] =
          cell_draft.cell.id;
    auto halo_neighbor_cell = mesh.halo_cells.find(connection.neighbor_handle);
    if (halo_neighbor_cell != mesh.halo_cells.end())
      halo_neighbor_cell->second.neighbors[connection.neighbor_apex_idx] =
          cell_draft.cell.id;
  }
}

/**
 * @brief Insert a QCCellDraft into a mesh.
 *
 * This includes:
 * - connecting the cell to the mesh
 * - Set cell data via cell traits, except the cell basis
 *
 * This function can be overloaded for mesh cells needing more work to
 * wipe the cells.
 */
template <class NodalData, class CellData>
SimplexCell<3, CellData> &
insert_cell_without_basis(SimplexMesh<3, 3, NodalData, CellData> &mesh,
                          const CellDraft<CellData> &cell_draft) {
  const auto cell_id = cell_draft.cell.id;
  using CellType = SimplexCell<3, CellData>;
  if (static_cast<std::size_t>(cell_id) >= mesh.cell_id_counter)
    mesh.cell_id_counter = static_cast<std::size_t>(cell_id) + 1;

  connect_mesh_to_cell_draft(mesh, cell_draft);

  auto &&[cell_iter, _insertion_happend] =
      mesh.cells.insert(std::make_pair(cell_id, cell_draft.cell));
  auto &&[_new_cell_key, new_cell] = *cell_iter;

  if constexpr (traits::IS_IMPLEMENTED<traits::Volume<CellType>> ||
                traits::IS_IMPLEMENTED<traits::Quality<CellType>>)
    set_cell_data(new_cell, simplex_cell_points(mesh, new_cell));
  return new_cell;
}

/**
 * @brief Same as insert_cell_without_basis but also set cell basis.
 */
template <class CellData, class MeshType>
SimplexCell<3, CellData> &insert_cell(MeshType &mesh,
                                      const CellDraft<CellData> &cell_draft) {
  // check if all nodes exist locally. If not, fetch them from the halo.
  for (const auto node_id : cell_draft.cell.nodes)
    insert_node_from_halo(mesh, node_id);
  // Info: here a possibly overloaded version is called:
  auto &new_cell = insert_cell_without_basis(mesh, cell_draft);
  using CellType = SimplexCell<3, CellData>;
  if constexpr (traits::IS_IMPLEMENTED<traits::LatticeBasis<CellType>>) {
    static_assert(
        traits::IS_IMPLEMENTED<traits::UndeformedLatticeBasis<MeshType>>);
    const auto undeformed_basis =
        Eigen::Matrix<double, 3, 3>{array_ops::as_eigen_matrix(
            traits::undeformed_lattice_basis(mesh, new_cell))};
    array_ops::as_eigen_matrix(traits::lattice_basis(new_cell)) =
        cell_draft.cell_basis;
    if constexpr (traits::IS_IMPLEMENTED<traits::Jacobian<CellType>>) {
      const auto deformation =
          cell_draft.cell_basis * undeformed_basis.inverse();
      traits::jacobian(new_cell) = std::abs(deformation.determinant());
    }
    if constexpr (traits::IS_IMPLEMENTED<traits::IsAtomistic<CellType>>)
      traits::is_atomistic(new_cell) = cell_draft.n_volume == 1;
  }
  return new_cell;
}

/**
 * @brief Returns an iterable of bad elements in the local part of the mesh.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::vector<SimplexCellId>
gather_local_bad_elements(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                          const double remesh_tolerance) {
  static_assert(
      traits::IS_IMPLEMENTED<traits::Quality<SimplexCell<K, CellData>>>);
  auto bad_elements = std::vector<SimplexCellId>{};
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  for (const auto &[_, cell] : mesh.cells)
    if (mpi_rank == cell.process_rank &&
        traits::quality(cell) >= remesh_tolerance)
      bad_elements.push_back(cell.id);
  return bad_elements;
}

/**
 * @brief Find bad elements in the interior of the process-local part of the
 * mesh.
 *
 * @returns An iterable of ids of cells which dont share faces / edges with
 * cells managed by other processes.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::vector<SimplexCellId> gather_local_independent_bad_elements(
    const SimplexMesh<D, K, NodalData, CellData> &mesh,
    const double remesh_tolerance) {
  const auto all_bad_elements =
      gather_local_bad_elements(mesh, remesh_tolerance);
  const auto local_boundary_nodes =
      collect_boundary_nodes(mesh.cells, all_bad_elements);
  const auto is_independent = [&](const auto cell_id) {
    auto n_boundary_vertices = std::size_t{0};
    const auto &cell = mesh.cells.at(cell_id);
    for (const auto &node_id : cell.nodes)
      if (local_boundary_nodes.find(node_id) != local_boundary_nodes.end())
        n_boundary_vertices++;
    return n_boundary_vertices <= 1;
  };
  auto independent_bad_elements = std::vector<SimplexCellId>{};
  std::copy_if(all_bad_elements.begin(), all_bad_elements.end(),
               std::back_inserter(independent_bad_elements), is_independent);
  return independent_bad_elements;
}

/**
 * @brief Returns an iterable of bad cells throughout the global mesh.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
std::vector<CellAddress<int, SimplexCellId>>
gather_bad_elements(const SimplexMesh<D, K, NodalData, CellData> &mesh,
                    const double remesh_tolerance) {
  const auto local_ids = gather_local_bad_elements(mesh, remesh_tolerance);
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  auto local_addresses = std::vector<CellAddress<int, SimplexCellId>>{};
  local_addresses.reserve(local_ids.size());
  for (const auto id : local_ids)
    local_addresses.emplace_back(CellAddress<int, SimplexCellId>{mpi_rank, id});
  return qcmesh::mpi::all_concat_vec(local_addresses);
}

/**
 * @brief Repair a mesh by applying specified modifiers.
 */
template <class... Modifiers, class NodalData, class CellData, class Driver>
void repair_mesh_with_modifiers(SimplexMesh<3, 3, NodalData, CellData> &mesh,
                                Driver &driver,
                                const double remesh_tolerance = 1.0e1) {
  using Mesh = SimplexMesh<3, 3, NodalData, CellData>;

  /**
   * @brief Called before applying a modifier to the provided element.
   * Gathers elements from other mpi ranks
   */
  const auto exchange_cell_halos =
      [](Mesh &mesh,
         const std::vector<CellAddress<int, SimplexCellId>> &cell_handles) {
        const auto mpi_rank = boost::mpi::communicator{}.rank();
        // assume correct node cells adjacencies exist before this function is
        // called.
        auto cell_ids = std::vector<SimplexCellId>{};
        cell_ids.reserve(cell_handles.size());
        for (const auto cell_address : cell_handles)
          if (cell_address.process_id == mpi_rank)
            cell_ids.push_back(cell_address.cell_handle);
        exchange_halos(mesh, cell_ids);
      };

  auto modifiers = std::tuple<Modifiers...>{};

  const auto compute_residual = [&](const SimplexCellId cell_handle) {
    const auto cell = deref_cell_id(mesh, cell_handle);
    return geometry::simplex_metric_residual(simplex_cell_points(mesh, cell));
  };

  const auto gather_local_bad_elements = [remesh_tolerance](const auto &mesh) {
    return gather_local_independent_bad_elements(mesh, remesh_tolerance);
  };

  optimize_local_mesh<CellDraft<CellData>, SimplexCellId>(
      mesh, gather_local_bad_elements, driver, compute_residual,
      apply_change_set<Mesh, CellData>, modifiers);

  const auto mpi_rank = boost::mpi::communicator{}.rank();
  optimize_global_mesh<CellDraft<CellData>, SimplexCellId>(
      mesh, mpi_rank,
      [remesh_tolerance](const auto &mesh) {
        return gather_bad_elements(mesh, remesh_tolerance);
      },
      exchange_cell_halos, driver, compute_residual,
      apply_change_set_distributed<Mesh, CellData>, modifiers);
}

/**
 * @brief Repair a mesh by applying edge removal / face removal operations.
 */
template <class NodalData, class CellData, class Driver>
void repair_mesh(SimplexMesh<3, 3, NodalData, CellData> &mesh, Driver &driver,
                 const double remesh_tolerance = 1.0e1) {
  using ModifierEdgeEraser = EdgeEraser<NodalData, CellData>;
  using ModifierFaceEraser = FaceEraser<NodalData, CellData>;
  repair_mesh_with_modifiers<ModifierEdgeEraser, ModifierFaceEraser>(
      mesh, driver, remesh_tolerance);
}

} // namespace qcmesh::mesh
