// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Exchange halos for distributed mesh repair.
 *
 * @author Prateek Gupta
 */

#pragma once

#include "qcmesh/mesh/serialization/node.hpp"
#include "qcmesh/mesh/serialization/simplex_cell.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mpi/all_scatter.hpp"
#include <boost/mpi/collectives.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <unordered_set>

namespace qcmesh::mesh {

/**
 * @brief true if there is a face neighbor which is not part of the local mesh.
 */
template <std::size_t Dimension, class CellData>
bool is_proc_boundary_face(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::optional<SimplexCellId> &face_neighbor) {
  return face_neighbor.has_value() &&
         cells.find(face_neighbor.value()) == cells.end();
}

/**
 * @brief For each node, collect a list of cells which the node is part of.
 */
template <std::size_t Dimension, class CellData>
std::unordered_map<NodeId, std::vector<SimplexCellId>> collect_node_neighbors(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::unordered_set<NodeId> &node_ids) {
  auto node_neighbors =
      std::unordered_map<NodeId, std::vector<SimplexCellId>>{};
  for (const auto &[_, cell] : cells)
    for (const auto node_id : cell.nodes)
      if (node_ids.find(node_id) != node_ids.end())
        node_neighbors[node_id].emplace_back(cell.id);
  return node_neighbors;
}

/**
 * @brief Collect all node ids of nodes part of the cells specified by
 * `cell_ids` and subset of `node_ids`.
 */
template <std::size_t Dimension, class CellData>
std::unordered_set<NodeId> collect_node_ids(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::vector<SimplexCellId> &cell_ids,
    const std::unordered_set<NodeId> &node_ids) {
  auto out = std::unordered_set<NodeId>{};
  for (const auto cell_id : cell_ids)
    if (cells.find(cell_id) != cells.end())
      for (const auto node_id : cells.at(cell_id).nodes)
        if (node_ids.find(node_id) != node_ids.end())
          out.emplace(node_id);
  return out;
}
/**
 * @brief Collect all node ids of nodes part of the cells specified by
 * `cell_ids`.
 */
template <std::size_t Dimension, class CellData>
std::unordered_set<NodeId> collect_node_ids(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::vector<SimplexCellId> &cell_ids) {
  auto out = std::unordered_set<NodeId>{};
  for (const auto cell_id : cell_ids)
    if (cells.find(cell_id) != cells.end())
      for (const auto node_id : cells.at(cell_id).nodes)
        out.emplace(node_id);
  return out;
}

/**
 * @brief Collect all nodes of boundary faces (face shared
 * by a local cell and a non-local cell).
 *
 * @returns A `std::unordered_set` of the ids of the collected nodes.
 */
template <std::size_t Dimension, class CellData>
std::unordered_set<NodeId> collect_boundary_nodes(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells) {
  auto boundary_nodes = std::unordered_set<NodeId>{};
  for (const auto &[_, cell] : cells)
    for (std::size_t idx_apex = 0; idx_apex < cell.nodes.size(); idx_apex++)
      if (is_proc_boundary_face(cells, cell.neighbors[idx_apex]))
        for (std::size_t i = 0; i < cell.nodes.size(); i++)
          if (i != idx_apex)
            boundary_nodes.emplace(cell.nodes[i]);

  return boundary_nodes;
}

/**
 * @brief Variant of @ref collect_boundary_nodes which only returns boundary
 * nodes which are contained by a cell specified by `cell_ids`.
 */
template <std::size_t Dimension, class CellData>
std::unordered_set<NodeId> collect_boundary_nodes(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells,
    const std::vector<SimplexCellId> &cell_ids) {
  // All local nodes which are part of a local cell specified by cell_ids:
  auto node_ids = collect_node_ids(cells, cell_ids);
  auto boundary_nodes = std::unordered_set<NodeId>{};
  for (const auto &[_, cell] : cells)
    for (std::size_t idx_apex = 0; idx_apex < cell.nodes.size(); idx_apex++)
      if (is_proc_boundary_face(cells, cell.neighbors[idx_apex]))
        for (std::size_t i = 0; i < cell.nodes.size(); i++)
          if (i != idx_apex) {
            const auto node_id = cell.nodes[i];
            if (node_ids.find(node_id) != node_ids.end())
              boundary_nodes.emplace(node_id);
          }
  return boundary_nodes;
}

// NOLINTBEGIN(readability-function-cognitive-complexity)

/**
 * @brief For a given mesh, update `mesh.halo_cells` and `mesh.halo_nodes` so
 * the variables contain cell neighbors to (process) boundary nodes and their
 * nodes from other processes.
 */
template <std::size_t D, std::size_t K, class NodalData, class CellData>
void exchange_halos(SimplexMesh<D, K, NodalData, CellData> &mesh,
                    const std::vector<SimplexCellId> &cell_ids) {
  const auto all_local_boundary_nodes = collect_boundary_nodes(mesh.cells);
  const auto node_neighbors =
      collect_node_neighbors(mesh.cells, all_local_boundary_nodes);
  const auto local_boundary_nodes =
      collect_node_ids(mesh.cells, cell_ids, all_local_boundary_nodes);
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = static_cast<std::size_t>(world.size());
  const auto mpi_rank = static_cast<std::size_t>(world.rank());
  auto boundary_nodes_by_rank = std::vector<std::unordered_set<NodeId>>{};
  boost::mpi::all_gather(world, local_boundary_nodes, boundary_nodes_by_rank);
  boundary_nodes_by_rank[mpi_rank].clear(); // Dont want to process local nodes

  // Cells containing a boundary node from another process.
  const auto cells_send = [&]() {
    auto out = std::vector<std::vector<SimplexCell<K, CellData>>>(mpi_size);
    for (std::size_t i = 0; i < mpi_size; i++) {
      const auto cell_ids_send = [&]() {
        auto out = std::unordered_set<SimplexCellId>{};
        for (const auto node_id : boundary_nodes_by_rank[i])
          if (node_neighbors.find(node_id) != node_neighbors.end())
            for (const auto cell_id : node_neighbors.at(node_id))
              out.emplace(cell_id);
        return out;
      }();
      for (const auto cell_id : cell_ids_send)
        if (mesh.cells.find(cell_id) != mesh.cells.end())
          out[i].push_back(mesh.cells.at(cell_id));
    }
    return out;
  }();
  // All local nodes from cells to send if not in boundary nodes of other
  // process.
  const auto nodes_send = [&]() {
    auto out = std::vector<std::vector<Node<D, NodalData>>>(mpi_size);
    for (std::size_t i = 0; i < mpi_size; i++)
      for (const auto &cell : cells_send[i])
        for (const auto node_id : cell.nodes)
          if (mesh.nodes.find(node_id) != mesh.nodes.end() &&
              boundary_nodes_by_rank[i].find(node_id) ==
                  boundary_nodes_by_rank[i].end())
            out[i].push_back(mesh.nodes.at(node_id));
    return out;
  }();
  auto cells_recv =
      std::vector<std::vector<SimplexCell<K, CellData>>>(mpi_size);
  qcmesh::mpi::all_scatter(cells_send, cells_recv);

  mesh.halo_cells.clear();
  for (auto &cells : cells_recv)
    while (!cells.empty()) {
      mesh.halo_cells.emplace(cells.back().id, std::move(cells.back()));
      cells.pop_back();
    }

  auto nodes_recv = std::vector<std::vector<Node<D, NodalData>>>(mpi_size);
  qcmesh::mpi::all_scatter(nodes_send, nodes_recv);
  mesh.halo_nodes.clear();
  for (auto &nodes : nodes_recv)
    while (!nodes.empty()) {
      if (mesh.nodes.find(nodes.back().id) == mesh.nodes.end())
        mesh.halo_nodes.emplace(nodes.back().id, std::move(nodes.back()));
      nodes.pop_back();
    }
}

// NOLINTEND(readability-function-cognitive-complexity)

} // namespace qcmesh::mesh
