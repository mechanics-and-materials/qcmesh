// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Provides operator<< @ref qcmesh::mesh::ChangeSet.
 */

#pragma once

#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/node.hpp"
#include <ostream>

namespace qcmesh::mesh {

template <class CellDraft, class CellHandle, class Mesh> struct ChangeSetFmt {
  ChangeSet<CellDraft, CellHandle> &change_set;
  Mesh &mesh;
};

template <class CellDraft, class CellHandle, class Mesh>
std::ostream &
operator<<(std::ostream &os,
           const ChangeSetFmt<CellDraft, CellHandle, Mesh> &change_set) {
  const auto print_cell_nodes = [](const auto &nodes, std::ostream &os) {
    os << '(' << static_cast<std::size_t>(nodes[0]) << ")-("
       << static_cast<std::size_t>(nodes[1]) << ")-("
       << static_cast<std::size_t>(nodes[2]) << ")-("
       << static_cast<std::size_t>(nodes[3]) << ')';
  };
  for (const auto &cell_handle : change_set.change_set.out) {
    os << "- ";
    print_nodes(deref_cell_id(change_set.mesh, cell_handle).nodes);
    os << std::endl;
  }
  for (const auto &cell : change_set.change_set.in) {
    os << "+ ";
    print_nodes(cell.cell.nodes);
    os << std::endl;
  }

  return os;
}

} // namespace qcmesh::mesh
