// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_vertex.hpp"
#include "qcmesh/mesh/traits/cell.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>

namespace qcmesh::mesh::testing {

/**
 * @brief Compute the simplex lattice jacobian of a given cell
 * via its edges and lattice basis.
 */
template <class NodalData, class CellData>
std::array<std::array<int, 3>, 3> intrinsic_simplex_lattice_jacobian(
    const SimplexMesh<3, 3, NodalData, CellData> &mesh,
    const SimplexCell<3, CellData> &cell) {
  static_assert(traits::IS_IMPLEMENTED<traits::LatticeBasis<CellData>>);
  static_assert(
      traits::IS_IMPLEMENTED<traits::LatticeBasis<SimplexCell<3, CellData>>>);
  const auto lattice_basis =
      array_ops::as_eigen_matrix(traits::lattice_basis(cell));
  const auto &vertices = cell.nodes;
  const auto jac = geometry::simplex_jacobian(
      std::array{deref_node_id(mesh, vertices.front()).position,
                 deref_node_id(mesh, vertices[1]).position,
                 deref_node_id(mesh, vertices[2]).position,
                 deref_node_id(mesh, vertices[3]).position});
  auto lattice_jac = std::array<std::array<int, 3>, 3>{};
  array_ops::as_eigen_matrix(lattice_jac) =
      (lattice_basis.inverse() * array_ops::as_eigen_matrix(jac).transpose())
          .transpose()
          .array()
          .round()
          .matrix()
          .template cast<int>();
  return lattice_jac;
}

/**
 * @brief For each cell, compute the simplex lattice jacobian
 * via its edges and lattice basis.
 */
template <class NodalData, class CellData>
std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>
intrinsic_simplex_lattice_jacobians(
    const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  auto lattice_jacobians =
      std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>{};
  for (const auto &[id, cell] : mesh.cells)
    lattice_jacobians.emplace(id,
                              intrinsic_simplex_lattice_jacobian(mesh, cell));
  return lattice_jacobians;
}

namespace detail {
/**
 * @brief For given lattice coordinates of all vertices determine
 * the simplex lattice jacobian of a cell specified by 4 vertex indices.
 */
inline std::array<std::array<int, 3>, 3> simplex_lattice_jacobian_from_vertices(
    const std::unordered_map<NodeId, LatticeVertex> &lattice_vertices,
    const std::array<NodeId, 4> &vertex_ids) {
  return geometry::simplex_jacobian(std::array{
      lattice_vertices.at(vertex_ids[0]).lattice_coordinates,
      lattice_vertices.at(vertex_ids[1]).lattice_coordinates,
      lattice_vertices.at(vertex_ids[2]).lattice_coordinates,
      lattice_vertices.at(vertex_ids[3]).lattice_coordinates,
  });
}
} // namespace detail

/**
 * @brief For each cell, compute the simplex lattice jacobian
 * via external absolute lattice coordinates for each vertex.
 */
template <class CellData>
std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>
extrinsic_simplex_lattice_jacobians(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells,
    const std::unordered_map<NodeId, LatticeVertex> &lattice_vertices) {
  auto lattice_jacobians =
      std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>{};
  for (const auto &[id, cell] : cells)
    lattice_jacobians.emplace(
        id, detail::simplex_lattice_jacobian_from_vertices(lattice_vertices,
                                                           cell.nodes));
  return lattice_jacobians;
}

/**
 * @brief For each cell, compute the simplex lattice volume
 * (determinant of the lattice jacobian)
 * via external absolute lattice coordinates for each vertex.
 */
template <class CellData>
std::unordered_map<SimplexCellId, int> extrinsic_simplex_lattice_volumes(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells,
    const std::unordered_map<NodeId, LatticeVertex> &lattice_vertices) {
  auto lattice_volumes = std::unordered_map<SimplexCellId, int>{};
  for (const auto &[id, cell] : cells) {
    const auto n = detail::simplex_lattice_jacobian_from_vertices(
        lattice_vertices, cell.nodes);
    lattice_volumes.emplace(id, array_ops::as_eigen_matrix(n).determinant());
  }
  return lattice_volumes;
}

/**
 * @brief For each cell, determine if it is "atomistic", i.e. its lattice volume
 * is 1. The extrinsic version recomputes the volume.
 */
template <class CellData>
std::unordered_map<SimplexCellId, bool> extrinsic_simplex_atomistic_flags(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells,
    const std::unordered_map<NodeId, LatticeVertex> &lattice_vertices) {
  const auto lattice_volumes =
      extrinsic_simplex_lattice_volumes(cells, lattice_vertices);
  auto atomistic_flags = std::unordered_map<SimplexCellId, bool>{};
  for (const auto [id, vol] : lattice_volumes)
    atomistic_flags.emplace(id, vol == 1);
  return atomistic_flags;
}

/**
 * @brief For each cell, determine if it is "atomistic", i.e. its lattice volume
 * is 1. The intrinsic version uses the precomputed property of the cell.
 */
template <class CellData>
std::unordered_map<SimplexCellId, bool> intrinsic_simplex_atomistic_flags(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  static_assert(
      traits::IS_IMPLEMENTED<traits::IsAtomistic<SimplexCell<3, CellData>>>);
  auto atomistic_flags = std::unordered_map<SimplexCellId, bool>{};
  for (const auto &[cell_id, cell] : cells)
    atomistic_flags.emplace(cell_id, traits::is_atomistic(cell));
  return atomistic_flags;
}

} // namespace qcmesh::mesh::testing
