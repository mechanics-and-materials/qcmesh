// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/connect_local_cell_faces.hpp"
#include "qcmesh/mesh/mesh_repair/testing/face_connection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/face_connection.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include "qcmesh/mpi/serialization/tuple.hpp"
#include <boost/serialization/array.hpp>
#include <set>
#include <stdexcept>

namespace qcmesh::mesh::testing {

/**
 * @brief Collects face connections between cells in the map `cells`
 * by searching for shared faces (triples of nodes).
 */
template <class CellType>
std::set<FaceConnection> collect_local_extrinsic_face_connections(
    const std::unordered_map<SimplexCellId, CellType> &cells) {
  auto connections = std::set<FaceConnection>{};

  const auto connect_cells =
      [&](const SimplexCellId cell_id, const std::size_t apex_idx,
          const SimplexCellId other_cell_id, const std::size_t other_apex_idx) {
        connections.emplace(FaceConnection{.cell_id = cell_id,
                                           .apex_idx = apex_idx,
                                           .neighbor_id = other_cell_id});
        connections.emplace(FaceConnection{.cell_id = other_cell_id,
                                           .apex_idx = other_apex_idx,
                                           .neighbor_id = cell_id});
      };
  mesh::detail::connect_local_cell_faces<
      const std::unordered_map<SimplexCellId, CellType> &>(cells,
                                                           connect_cells);
  return connections;
}

/**
 * @brief Collects face connections between cells in the map `cells`
 * by inspection of the field @ref SimplexCell::neighbors.
 */
template <class CellData>
std::set<FaceConnection> collect_local_intrinsic_face_connections(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  auto connections = std::set<FaceConnection>{};

  for (const auto &[cell_id, cell] : cells)
    for (std::size_t apex = 0; apex < 4; apex++) {
      const auto neighbor = cell.neighbors[apex];
      if (neighbor.has_value())
        connections.emplace(FaceConnection{.cell_id = cell_id,
                                           .apex_idx = apex,
                                           .neighbor_id = neighbor.value()});
    }
  return connections;
}

namespace detail {

template <class CellData>
std::vector<std::tuple<SimplexCellId, std::array<NodeId, 4>>>
pack_local_cell_nodes(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  auto local_cell_nodes =
      std::vector<std::tuple<SimplexCellId, std::array<NodeId, 4>>>{};
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  for (const auto &[id, cell] : cells) {
    if (cell.process_rank != mpi_rank)
      continue;
    local_cell_nodes.emplace_back(std::make_tuple(id, cell.nodes));
  }

  return local_cell_nodes;
}

} // namespace detail

/**
 * @brief Exception thrown due to cell collisions.
 */
struct IdCollisionError : std::runtime_error {
  using std::runtime_error::runtime_error;
};

/**
 * @brief Gather a map of @ref SimplexCell from all processes to a map of
 * `std::array<NodeId, 4>`, throwing an exception if an id collision is
 * encountered.
 */
template <class CellData>
std::unordered_map<SimplexCellId, std::array<NodeId, 4>> gather_cell_nodes(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  const auto local_cell_nodes = detail::pack_local_cell_nodes(cells);
  const auto global_cell_nodes_vec =
      qcmesh::mpi::all_concat_vec(local_cell_nodes);
  auto global_cell_nodes =
      std::unordered_map<SimplexCellId, std::array<NodeId, 4>>{};
  for (const auto &[id, cell] : global_cell_nodes_vec) {
    const auto [_, no_collision] = global_cell_nodes.emplace(id, cell);
    if (!no_collision)
      throw IdCollisionError{std::string{"Cell id collision: id="} +
                             std::to_string(static_cast<std::size_t>(id))};
  }
  return global_cell_nodes;
}

/**
 * @brief Distributed version of @ref collect_local_intrinsic_face_connections.
 */
template <class CellData>
std::set<FaceConnection> collect_intrinsic_face_connections(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  const auto connections = collect_local_intrinsic_face_connections(cells);
  return qcmesh::mpi::all_union_set(connections);
}
/**
 * @brief Distributed version of @ref collect_local_extrinsic_face_connections.
 */
template <class CellData>
std::set<FaceConnection> collect_extrinsic_face_connections(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  return collect_local_extrinsic_face_connections(gather_cell_nodes(cells));
}

} // namespace qcmesh::mesh::testing
