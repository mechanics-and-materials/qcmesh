// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include <algorithm>
#include <array>
#include <functional>
#include <iterator>
#include <set>
#include <stdexcept>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh::testing {

/**
 * @brief Collects all faces of a mesh which belong to only one tetrahedron.
 * @param get_faces I faces(const T&), where I is an iterable with elements of
 * type F
 */
template <class F, class Mesh, class GetFaces>
std::set<F> boundary_faces(const Mesh &mesh, GetFaces get_faces) {
  using TetrahedronType = typename Mesh::value_type;
  static_assert(std::is_constructible<
                    std::function<std::array<F, 4>(const TetrahedronType &)>,
                    GetFaces>::value,
                "Invalid type of get_faces.");

  auto out_boundary = std::set<F>{};
  auto shared_faces = std::set<F>{};
  for (const auto &cell : mesh)
    for (const F &face : get_faces(cell)) {
      if (out_boundary.erase(face) > 0)
        shared_faces.insert(face);
      else {
        shared_faces.erase(face);
        out_boundary.insert(face);
      }
    }

  return out_boundary;
}

/**
 * @brief Converts a cells of a mesh into a set of unoriented cells
 * (sorted 4-arrays of node ids).
 */
template <class CellData>
std::set<std::array<NodeId, 4>> unoriented_mesh(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  const auto ordered_array = [](const auto &arr) {
    auto t = decltype(arr){arr};
    std::sort(t.begin(), t.end());
    return t;
  };

  auto unoriented_cells = std::set<std::array<NodeId, 4>>{};
  for (const auto &[_, cell] : cells)
    unoriented_cells.emplace(ordered_array(cell.nodes));
  return qcmesh::mpi::all_union_set(unoriented_cells);
}

/**
 * @brief Returns a set of vertices appearing in the boundary determined by
 * boundary_faces. Abstract overload.
 *
 * @pre the mesh does not contain faces shared by more than 2 tetrahedra
 * (invalid mesh)
 * @param get_faces I faces(const T&), where I is an iterable with elements of
 * type F
 */
template <class V, class F, class Mesh, class GetFaces, class GetVertices>
std::set<V> boundary_vertices(const Mesh &mesh, GetFaces get_faces,
                              GetVertices get_vertices) {
  static_assert(
      std::is_constructible<std::function<std::array<V, 3>(const F &)>,
                            GetVertices>::value,
      "Invalid type of get_vertices.");
  const auto faces = boundary_faces<F, Mesh, GetFaces>(mesh, get_faces);
  auto vertices = std::set<V>{};
  for (const auto &face : faces)
    for (const auto &vx : get_vertices(face))
      vertices.insert(vx);
  return vertices;
}

namespace detail {
inline std::array<std::array<NodeId, 3>, 4>
cell_faces(const std::array<NodeId, 4> &cell) {
  auto ordered_cell = cell;
  std::sort(ordered_cell.begin(), ordered_cell.end());
  return std::array{
      std::array{ordered_cell[0], ordered_cell[1], ordered_cell[2]},
      std::array{ordered_cell[0], ordered_cell[1], ordered_cell[3]},
      std::array{ordered_cell[0], ordered_cell[2], ordered_cell[3]},
      std::array{ordered_cell[1], ordered_cell[2], ordered_cell[3]}};
}

} // namespace detail

template <class CellData>
std::set<NodeId> boundary_vertices(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  const auto unordered_cells = unoriented_mesh(cells);
  const auto get_vertices = [](const auto &face) { return face; };
  using Face = std::array<NodeId, 3>;
  return boundary_vertices<NodeId, Face>(unordered_cells, detail::cell_faces,
                                         get_vertices);
}

} // namespace qcmesh::mesh::testing
