// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/create_distributed_simplex_mesh.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_cell_data.hpp"
#include "qcmesh/mesh/traits/mesh.hpp"
#include <array>

namespace qcmesh::mesh {
namespace testing {

/**
 * @brief Contains vertices with lattice coordinates and
 * cells defined by 4 vertex indices each.
 */
template <class T> struct BareMesh {
  std::vector<T> vertices = {};
  std::vector<std::array<std::size_t, 4>> cells = {};
};

template <class CellData>
using LatticeMesh = SimplexMesh<3, 3, EmptyData, CellData>;

/**
 * @brief Wrapper around @ref create_local_simplex_mesh which directly creates
 * a @ref SimplexMesh instance from a @ref BareMesh instance.
 */
template <class CellData = LatticeCellData, class VertexType>
LatticeMesh<CellData>
create_local_lattice_mesh(const BareMesh<VertexType> &bare_mesh,
                          const bool throw_on_degeneracy = true) {
  return create_local_simplex_mesh<EmptyData, CellData>(
      bare_mesh.vertices, bare_mesh.cells, 0, throw_on_degeneracy);
}

/**
 * @brief Wrapper around @ref create_distributed_simplex_mesh which directly
 * creates a @ref SimplexMesh instance from a @ref BareMesh instance.
 */
template <class CellData = LatticeCellData, class VertexType>
LatticeMesh<CellData>
create_distributed_lattice_mesh(const BareMesh<VertexType> &bare_mesh,
                                const bool throw_on_degeneracy = true) {
  return create_distributed_simplex_mesh<EmptyData, CellData>(
      bare_mesh.vertices, bare_mesh.cells, throw_on_degeneracy);
}

namespace detail {
template <class Id, class T>
std::unordered_map<Id, T> index_vec(const std::vector<T> &vec) {
  auto out = std::unordered_map<Id, T>{};
  std::size_t counter = 0;
  for (const auto &item : vec)
    out.insert({Id{counter++}, item});
  return out;
}
} // namespace detail

/**
 * @brief Create a mesh together with the original lattice coordinates used to
 * compute the cells lattice vectors.
 */
template <class CellData = LatticeCellData, class VertexType>
std::tuple<LatticeMesh<CellData>, std::unordered_map<NodeId, VertexType>>
create_local_lattice_mesh_with_lattice_coordinates(
    const BareMesh<VertexType> &bare_mesh) {
  return std::make_tuple(create_local_simplex_mesh<EmptyData, CellData>(
                             bare_mesh.vertices, bare_mesh.cells),
                         detail::index_vec<NodeId>(bare_mesh.vertices));
}

} // namespace testing

template <class CellData>
struct traits::UndeformedLatticeBasis<testing::LatticeMesh<CellData>> {
  constexpr static std::size_t DIMENSION = 3;
  static constexpr std::array LATTICE_BASIS =
      std::array{std::array<double, DIMENSION>{1., 0., 0.},
                 std::array<double, DIMENSION>{0., 1., 0.},
                 std::array<double, DIMENSION>{0., 0., 1.}};
  using CellType = SimplexCell<DIMENSION, CellData>;
  static const std::array<std::array<double, DIMENSION>, DIMENSION> &
  undeformed_lattice_basis(const testing::LatticeMesh<CellData> &,
                           const CellType &) {
    return LATTICE_BASIS;
  }
};

} // namespace qcmesh::mesh
