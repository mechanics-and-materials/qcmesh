// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_mesh.hpp"
#include <ostream>

namespace qcmesh::mesh::testing {

/**
 * @brief Keeps track of a connection of a cell `cell` with `cell.id = cell_id`
 * to a cell with `neighbor_id` via the face defined by an apex node
 * `cell.nodes[apex_id]`.
 */
struct FaceConnection {
  SimplexCellId cell_id;
  std::size_t apex_idx;
  SimplexCellId neighbor_id;
};

bool operator==(const FaceConnection &c1, const FaceConnection &c2);
bool operator<(const FaceConnection &c1, const FaceConnection &c2);
std::ostream &operator<<(std::ostream &os, const FaceConnection &connection);

} // namespace qcmesh::mesh::testing
