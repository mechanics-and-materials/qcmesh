// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/node.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include <array>
#include <ostream>
#include <set>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh::testing {

/**
 * @brief Describes a tetrahedron where 2 sets of 4 vertices each are considered
 * equal if they differ by an even permutation.
 */
template <std::size_t NodesPerCell> struct TopologicalCell {
  std::array<NodeId, NodesPerCell> node_ids;
};

/**
 * @brief Equivalence relation as described above.
 */
bool operator==(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2);
bool operator!=(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2);
bool operator<(const TopologicalCell<4> &t1, const TopologicalCell<4> &t2);
std::ostream &operator<<(std::ostream &os, const TopologicalCell<4> &t);

/**
 * @brief Converts cells of a mesh into a set of oriented tetrahedra (swapping
 * vertices inverts the orientation).
 */
template <std::size_t Dimension, class CellData>
std::set<TopologicalCell<Dimension + 1>> collect_local_topological_cells(
    const std::unordered_map<SimplexCellId, SimplexCell<Dimension, CellData>>
        &cells) {
  auto out = std::set<TopologicalCell<Dimension + 1>>{};
  for (const auto &[_, cell] : cells)
    out.emplace(TopologicalCell<Dimension + 1>{cell.nodes});
  return out;
}

/**
 * @brief Converts an array of std::size_t as used in testing::BareMesh to a
 * TopologicalCell.
 */
TopologicalCell<4> to_topological_cell(const std::array<std::size_t, 4> &cell);

/**
 * @brief Converts a vector of arrays of std::size_t as used in
 * testing::BareMesh to a set of TopologicalCell.
 */
std::set<TopologicalCell<4>>
to_topological_cells(const std::vector<std::array<std::size_t, 4>> &cells);

/**
 * @brief Canonically reorder the node ids (3 node variang).
 */
std::array<NodeId, 3> ordered_oriented_array(const std::array<NodeId, 3> &arr);
/**
 * @brief Canonically reorder the node ids (4 node variang).
 */
std::array<NodeId, 4> ordered_oriented_array(const std::array<NodeId, 4> &arr);

} // namespace qcmesh::mesh::testing
