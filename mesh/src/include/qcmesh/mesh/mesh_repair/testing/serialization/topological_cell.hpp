// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include <boost/mpi/datatype.hpp>
#include <boost/serialization/level.hpp>
#include <boost/serialization/tracking.hpp>

namespace boost::serialization {

template <class Archive, std::size_t NodesPerCell>
void serialize(Archive &ar,
               qcmesh::mesh::testing::TopologicalCell<NodesPerCell> &cell,
               const unsigned int) {
  ar &cell.node_ids;
}
} // namespace boost::serialization

BOOST_CLASS_IMPLEMENTATION(qcmesh::mesh::testing::TopologicalCell<4>,
                           object_serializable)
BOOST_CLASS_TRACKING(qcmesh::mesh::testing::TopologicalCell<4>, track_never)
BOOST_IS_MPI_DATATYPE(qcmesh::mesh::testing::TopologicalCell<4>)
