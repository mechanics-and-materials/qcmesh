// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_vertex.hpp"

namespace qcmesh::mesh::testing {
/**
 * @brief Create lattice vertices from lattice coordinates
 * and a basis (lattice vectors).
 */
std::vector<LatticeVertex> lattice_vertices_from_lattice_points(
    const std::vector<std::array<int, 3>> &lattice_points,
    const std::array<std::array<double, 3>, 3> &basis);

/**
 * @brief Creates a mesh of n_x x n_y x n_z lattice points with corresponding
 * vertices determined by basis and connects every lattice unit box according to
 * unit_connectivity.
 *
 * Entries of unit_connectivity are given by 4 int's each, where
 * the binary representation of each int encodes the coordinates of the unit
 * box, e.g. 0b011 encodes lattice site (0,1,1).
 */
BareMesh<LatticeVertex> create_lattice_mesh(
    const std::size_t n_x, const std::size_t n_y, const std::size_t n_z,
    const std::array<std::array<double, 3>, 3> &basis,
    const std::vector<std::array<std::size_t, 4>> &unit_connectivity);

} // namespace qcmesh::mesh::testing
