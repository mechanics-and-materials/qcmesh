// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Some example meshes used throughout the mesh tests.
 */

#pragma once

#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/create_lattice_mesh.hpp"
#include "qcmesh/mesh/primitive_node.hpp"
#include "qcmesh/mesh/testing/lattice_vertex.hpp"

namespace qcmesh::mesh::testing {

/**
 * @brief Edge removal example: 3 tetrahedra around edge (0,0,-1) -- (0,0,1)
 *
 * (n): Vertex
 * (n,m): 2 vertices on top of each other (edge)
 *
 *  (3)-._
 *   | \  `--._
 *   |  (0,1)--(2)
 *   | /   _.-՛
 *  (4)_.-՛
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_1() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      .vertices = {{{0., 0., -1.}, {0, 0, -1}},
                   {{0., 0., 1.}, {0, 0, 1}},
                   {{1., 0., 0.}, {1, 0, 0}},
                   {{-1., d, 0.}, {-1, 1, 0}},
                   {{-1., -d, 0.}, {-1, -1, 0}}},
      .cells = {{0, 1, 2, 3}, {0, 1, 3, 4}, {0, 1, 4, 2}}};
}
/**
 * @brief Edge removal example: Same as mesh_1 but with more cells at the
 * boundary.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_1b() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{0., 0., 1.}, {0, 0, 1}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
       testing::LatticeVertex{{-1., d, 0.}, {-1, 1, 0}},
       testing::LatticeVertex{{-1., -d, 0.}, {-1, -1, 0}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}},
       testing::LatticeVertex{{0., -1, 0.}, {0, -1, 0}},
       testing::LatticeVertex{{-2., 0., 0.}, {-2, 0, 0}}},
      {{0, 1, 2, 3},
       {0, 1, 3, 4},
       {0, 1, 4, 2},
       {0, 2, 5, 3},
       {1, 2, 3, 5},
       {0, 2, 4, 6},
       {1, 2, 6, 4},
       {0, 3, 7, 4},
       {1, 3, 4, 7}}};
}
/**
 * @brief Edge removal example: 3 tetrahedra around edge (0,0,-1) -- (0,0,1)
 * This time the "ring" of tetrahedra around the edge is broken up at a face.
 *
 *       (2)
 *      / |
 *   (3)  |
 *    | \ |
 *    | (0,1)
 *    | / |
 *   (4)  |
 *      \ |
 *       (5)
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_2() {
  const auto d = double{.03};
  const auto dy = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{0., 0., 1.}, {0, 0, 1}},
       testing::LatticeVertex{{0., dy, 0.}, {0, 1, 0}},
       testing::LatticeVertex{{-1., d, 0.}, {-1, 1, 0}},
       testing::LatticeVertex{{-1., -d, 0.}, {-1, -1, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}}},
      {{0, 1, 2, 3}, {0, 1, 3, 4}, {0, 1, 4, 5}}};
}
/**
 * @brief Same as mesh_2 but with more surrounding cells.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_2b() {
  const auto d = double{.03};
  const auto dy = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{0., 0., 1.}, {0, 0, 1}},
       testing::LatticeVertex{{0., dy, 0.}, {0, 1, 0}},
       testing::LatticeVertex{{-1., d, 0.}, {-1, 1, 0}},
       testing::LatticeVertex{{-1., -d, 0.}, {-1, -1, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}},
       testing::LatticeVertex{{0., 0., -2.}, {0, 0, -2}},
       testing::LatticeVertex{{0., 0., 2.}, {0, 0, 2}}},
      {{0, 1, 2, 3},
       {0, 1, 3, 4},
       {0, 1, 4, 5},
       {6, 0, 2, 3},
       {7, 1, 3, 2},
       {6, 0, 3, 4},
       {7, 1, 4, 3},
       {6, 0, 4, 5},
       {7, 1, 5, 4}}};
}
/**
 * @brief Face removal example: 2 tetrahedra sharing a face
 *
 * (n): Vertex
 * (n,m): 2 vertices on top of each other (edge)
 *
 *    (0)
 *   /   \
 * (2)--(3,4)
 *   \   /
 *    (1)
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_3() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{1., 1., 0.}, {1, 1, 0}},
       testing::LatticeVertex{{1., -1., 0.}, {1, -1, 0}}},
      {{0, 2, 3, 4}, {1, 4, 3, 2}}};
}
/**
 * @brief Like mesh_3, but with more cells surrounding the sandwich.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_3b() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{1., 1., 0.}, {1, 1, 0}},
       testing::LatticeVertex{{1., -1., 0.}, {1, -1, 0}},
       testing::LatticeVertex{{0., 0., 2.}, {0, 0, 2}},
       testing::LatticeVertex{{0., 0., -2.}, {0, 0, -2}}},
      {{0, 4, 2, 3},
       {2, 3, 4, 1},
       {0, 4, 3, 5},
       {1, 3, 4, 6},
       {0, 3, 2, 5},
       {1, 2, 3, 6},
       {0, 2, 4, 5},
       {1, 4, 2, 6}}};
}
/**
 * @brief Like mesh_3, but with displaced vertices, so edge removal does not
 * improve the residual.
 *
 *    (3)
 *   /   \
 * (0)--(1,2)
 *   \   /
 *    (4)
 *
 * Edge removal of
 * - 0-1 -> {0,2,4,3}, {1,2,3,4}
 * - 0-2 -> {0,2,4,3}, {0,1,3,4}
 * - 1-2 -> {1,2,3,4}, {0,1,3,4}
 * Face removal of
 * - {0,1,2} -> {0,2,4,3}, {0,1,3,4}, {1,2,3,4}
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_3c() {
  const auto d = 0.05;
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., 0.}, {0, 0, 0}},
       testing::LatticeVertex{{1. - d, 1., 0.}, {1, 2, 0}},
       testing::LatticeVertex{{1., 1. - d, 0.}, {2, 1, 0}},
       testing::LatticeVertex{{1. - d, 1. - d, d}, {1, 1, 1}},
       testing::LatticeVertex{{0.5, 0.5, -1.0}, {1, 1, -1}}},
      {{0, 1, 2, 3}, {0, 1, 2, 4}}};
}
/**
 * @brief Face removal example: 2 adjacent faces
 * sandwiched between 0 and 1
 *
 * Top view of sandwiched faces. All faces are connected to vertices 0 (top)
 * and 1 (bottom).
 * Edge 0-1 marked by x.
 *
 *    (3)
 *   / | \
 * (2) x (5)
 *   \ | /
 *    (4)
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_4() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}}},
      {{0, 2, 3, 4}, {1, 4, 3, 2}, {0, 3, 5, 4}, {1, 5, 3, 4}}};
}
/**
 * @brief Like mesh_4, but with more cells surrounding the sandwich.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_4b() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
       testing::LatticeVertex{{0., 0., 2.}, {0, 0, 2}},
       testing::LatticeVertex{{0., 0., -2.}, {0, 0, -2}}},
      {{0, 3, 4, 2},
       {2, 3, 4, 1},
       {5, 4, 0, 3},
       {3, 4, 1, 5},
       {6, 2, 3, 0},
       {6, 3, 5, 0},
       {6, 5, 4, 0},
       {6, 4, 2, 0},
       {7, 1, 3, 2},
       {7, 1, 5, 3},
       {7, 1, 4, 5},
       {7, 1, 2, 4}}};
}
/**
 * @brief Face removal example: 3 pairwise adjacent faces
 * sandwiched between 0 and 1
 *
 * Top view of sandwiched faces. All faces are connected to vertices 0 (top)
 * and 1 (bottom).
 * Edge 0-1 marked by x.
 *
 *    (3)---(4)
 *     \\x  //
 *      \(2)/
 *       \|/
 *       (5)
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_5() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{-0.5, 0.5, d}, {-1, 1, 1}},
       testing::LatticeVertex{{-0.5, 0.5, -1.}, {-1, 1, -1}},
       testing::LatticeVertex{{0., 0., 0.}, {0, 0, 0}},
       testing::LatticeVertex{{-1., 1., 0.}, {-2, 2, 0}},
       testing::LatticeVertex{{1., 1., 0.}, {2, 2, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -2, 0}}},
      {{0, 2, 3, 4},
       {1, 4, 3, 2},
       {0, 5, 3, 2},
       {1, 2, 3, 5},
       {0, 2, 4, 5},
       {1, 5, 4, 2}}};
}
/**
 * @brief Like mesh_5, but with more cells surrounding the sandwich.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_5b() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{-0.5, 0.5, d}, {-1, 1, 1}},
       testing::LatticeVertex{{-0.5, 0.5, -1.}, {-1, 1, -1}},
       testing::LatticeVertex{{0., 0., 0.}, {0, 0, 0}},
       testing::LatticeVertex{{-1., 1., 0.}, {-2, 2, 0}},
       testing::LatticeVertex{{1., 1., 0.}, {2, 2, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -2, 0}},
       testing::LatticeVertex{{0., 0., 3.}, {0, 0, 3}},
       testing::LatticeVertex{{0., 0., -3.}, {0, 0, -3}}},
      {{0, 3, 4, 2},
       {2, 3, 4, 1},
       {2, 3, 5, 0},
       {5, 3, 2, 1},
       {5, 4, 2, 0},
       {2, 4, 5, 1},
       {6, 0, 5, 3},
       {6, 0, 3, 4},
       {6, 0, 4, 5},
       {7, 1, 3, 5},
       {7, 1, 4, 3},
       {7, 1, 5, 4}}};
}
/**
 * @brief Face removal example: 3 faces
 * sandwiched between 0 and 1
 *
 * Top view of sandwiched faces. All faces are connected to vertices 0 (top)
 * and 1 (bottom).
 *
 *    (2)
 *   / | \
 * (3) | (5)
 *   \ | / \
 *    (4)--(6)
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_6() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
       testing::LatticeVertex{{2., -1., 0.}, {2, -1, 0}}},
      {{0, 4, 3, 2},
       {1, 2, 3, 4},
       {0, 5, 4, 2},
       {1, 2, 4, 5},
       {0, 4, 5, 6},
       {1, 6, 5, 4}}};
}
/**
 * @brief Like mesh_6, but with more cells surrounding the sandwich.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_6b() {
  const auto d = double{0.1};
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., d}, {0, 0, 1}},
       testing::LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}},
       testing::LatticeVertex{{-1., 0., 0.}, {-1, 0, 0}},
       testing::LatticeVertex{{0., -1., 0.}, {0, -1, 0}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
       testing::LatticeVertex{{2., -1., 0.}, {2, -1, 0}},
       testing::LatticeVertex{{0., 0., 2.}, {0, 0, 2}},
       testing::LatticeVertex{{0., 0., -2.}, {0, 0, -2}}},
      {{0, 4, 3, 2},
       {1, 2, 3, 4},
       {0, 5, 4, 2},
       {1, 2, 4, 5},
       {0, 4, 5, 6},
       {1, 6, 5, 4},
       {7, 0, 3, 2},
       {7, 0, 2, 5},
       {7, 0, 5, 6},
       {7, 0, 6, 4},
       {7, 0, 4, 3},
       {8, 1, 2, 3},
       {8, 1, 5, 2},
       {8, 1, 6, 5},
       {8, 1, 4, 6},
       {8, 1, 3, 4}}};
}

/**
 * @brief Edge removal example: flat tetrahedron connected to a regular
 * neighbor.
 *
 *      flat:         regular:
 *
 *        z             z
 *
 *        2---          2---
 *       /|  /|        /|  /|
 *      3---  |        ---  |
 *      | 0-|-  y     | 0-|-4 y
 *      |/  |/        |/  |/
 *      1---          1---
 *     x             x
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_flat_cell() {
  return testing::BareMesh<testing::LatticeVertex>{
      {testing::LatticeVertex{{0., 0., 0.}, {0, 0, 0}},
       testing::LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
       testing::LatticeVertex{{0., 0., 1.}, {0, 0, 1}},
       testing::LatticeVertex{{1., 0., 1.}, {1, 0, 1}},
       testing::LatticeVertex{{0., 1., 0.}, {0, 1, 0}}},
      {{0, 1, 2, 3}, {0, 1, 4, 2}}};
}

/**
 * @brief Example of a mesh with a flat (0 volume) cell.
 *
 * It has:
 * 6-ring around 0-3: 1-4-6-7-5-2,
 * open 5-ring around 0-2: 8-4-1-3-5
 * 3-ring around 0-1: 3-4-2
 *
 * E.g. Edge removal of 0-1:
 *       -> new cells: {0,3,4,2}, {1,3,4,2}, remove cells: 0,1,3
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_lattice() {
  constexpr double G = 1.;
  const auto basis = std::array{std::array<double, 3>{G, G, 0.},
                                std::array<double, 3>{0., G, G},
                                std::array<double, 3>{G, 0., G}};
  return testing::BareMesh<testing::LatticeVertex>{
      testing::lattice_vertices_from_lattice_points(
          std::vector<std::array<int, 3>>{{2, 3, -6}, // 0: (-4, 5, -3)
                                          {3, 2, -7}, // 1: (-4, 5, -5)
                                          {3, 3, -7}, // 2: (-4, 6, -4)
                                          {2, 2, -6}, // 3: (-4, 4, -4)
                                          {2, 3, -7},
                                          {3, 2, -6},
                                          {1, 3, -6},
                                          {2, 2, -5},
                                          {2, 4, -7}},
          basis),
      {
          {0, 1, 2, 3}, // flat cell
          {4, 1, 0, 3},
          {2, 0, 3, 5},
          {1, 4, 0, 2},
          {4, 0, 6, 3},
          {7, 6, 0, 3},
          {0, 7, 3, 5},
          {8, 0, 4, 2},
      }};
}
/**
 * @brief Example of a 4x4x4 lattice mesh with a flat (0 volume) cell.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_lattice_4x4x4() {
  constexpr double G = 1.;
  const auto basis = std::array{std::array<double, 3>{G, G, 0.},
                                std::array<double, 3>{0., G, G},
                                std::array<double, 3>{G, 0., G}};
  /* 7 tetrahedra, edges are marked by `*`:
                     z             z             z
                      *---o         *---*         o---*
                     /|  /|        /|  /|        /|  /|
                    o---o |       o---o |       o---o |
       z            | *-|-* y     | o-|-* y     | o-|-* y
        *---*       |/  |/        |/  |/        |/  |/
       /|  /|       *---o         *---o         *---*
      o---o |      x             x             x
      | o-|-o y
      |/  |/         z             z             z
      *---*           *---o         *---*         o---*
     x               /|  /|        /|  /|        /|  /|
                    *---o |       *---o |       *---* |
                    | o-|-o y     | o-|-o y     | o-|-o y
                    |/  |/        |/  |/        |/  |/
                    *---*         o---*         o---*
                   x             x             x
  */
  const auto unit_connectivity = std::vector<std::array<std::size_t, 4>>{
      {0b100, 0b110, 0b011, 0b001}, // flat cell
      {0b000, 0b100, 0b010, 0b001}, {0b100, 0b010, 0b001, 0b011},
      {0b100, 0b010, 0b011, 0b110}, {0b100, 0b110, 0b001, 0b101},
      {0b110, 0b001, 0b101, 0b011}, {0b110, 0b101, 0b111, 0b011},
  };
  return testing::create_lattice_mesh(4, 4, 4, basis, unit_connectivity);
}
/**
 * @brief Example of a 4x4x4 lattice mesh with a flat (0 volume) cell on the
 * boundary.
 */
inline testing::BareMesh<testing::LatticeVertex> mesh_lattice_4x4x4_boundary() {
  constexpr double G = 1.;
  const auto basis = std::array{std::array<double, 3>{G, G, 0.},
                                std::array<double, 3>{0., G, G},
                                std::array<double, 3>{G, 0., G}};
  /* 6 tetrahedra, edges are marked by `*`:
                     z                           z
                      *---o                       *---*
                     /|  /|                      /|  /|
                    o---o |                     o---* |
       z            | *-|-* y      z            | o-|-* y
        *---o       |/  |/          *---o       |/  |/
       /|  /|       *---o          /|  /|       o---o
      *---o |      x              o---* |      x
      | *-|-o y                   | o-|-* y
      |/  |/         z            |/  |/         z
      *---o           *---o       *---o           o---o
     x               /|  /|      x               /|  /|
                    *---* |                     o---* |
                    | o-|-o y                   | o-|-* y
                    |/  |/                      |/  |/
                    *---o                       *---*
                   x                           x
  */
  const auto unit_connectivity = std::vector<std::array<std::size_t, 4>>{
      {0b000, 0b100, 0b001, 0b101}, // flat cell
      {0b000, 0b100, 0b010, 0b001}, {0b011, 0b111, 0b001, 0b010},
      {0b111, 0b100, 0b001, 0b010}, // center cell
      {0b101, 0b001, 0b111, 0b100}, {0b110, 0b100, 0b111, 0b010},
  };
  return testing::create_lattice_mesh(4, 4, 4, basis, unit_connectivity);
}

/**
 * @brief A ring of `n_cells` cells around a common edge 0-1
 */
inline BareMesh<PrimitiveNode<3>> mesh_cell_ring(const std::size_t n_cells) {
  const auto apex_a = PrimitiveNode<3>{.position = std::array{0., 0., 1.}};
  const auto apex_b = PrimitiveNode<3>{.position = std::array{0., 0., -1.}};
  const auto node_ring_size = std::max(std::size_t{2}, n_cells);
  auto nodes = std::vector<PrimitiveNode<3>>{apex_a, apex_b};
  for (std::size_t i = 0; i < node_ring_size; i++)
    nodes.emplace_back(PrimitiveNode<3>{
        .position = std::array{
            cos(2. * M_PI / static_cast<double>(node_ring_size * i)),
            sin(2. * M_PI / static_cast<double>(node_ring_size * i)), 0.}});
  auto cells = std::vector<std::array<std::size_t, 4>>{};
  for (std::size_t i = 0; i < n_cells; i++) {
    const auto apex_a_idx = std::size_t{0};
    const auto apex_b_idx = std::size_t{1};
    const auto ring_a_idx = std::size_t{2 + i};
    const auto ring_b_idx = std::size_t{2 + (i + 1) % node_ring_size};
    cells.emplace_back(
        std::array{apex_a_idx, apex_b_idx, ring_a_idx, ring_b_idx});
  }
  return testing::BareMesh<PrimitiveNode<3>>{nodes, cells};
}

} // namespace qcmesh::mesh::testing
