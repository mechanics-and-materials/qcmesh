// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include <array>
#include <boost/mpi.hpp>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh::testing {

template <std::size_t Dimension>
using Simplex = std::array<std::array<double, Dimension>, Dimension + 1>;

/**
 * @brief Checks if the two simplices can be separated by a hyperplane.
 * @param epsilon If points have (non-normalized) distance <= epsilon to a
 * hyperplane, they are considered to be on that hyperplane.
 */
bool simplices_dont_intersect(const Simplex<3> &t1, const Simplex<3> &t2,
                              const double epsilon);
bool simplices_dont_intersect(const Simplex<2> &t1, const Simplex<2> &t2,
                              const double epsilon);

struct SimplexGeometry {
  SimplexCellId id;
  std::array<std::array<double, 3>, 4> vertices;
};

inline bool operator==(const SimplexGeometry &a, const SimplexGeometry &b) {
  return a.id == b.id && a.vertices == b.vertices;
}

inline std::ostream &operator<<(std::ostream &os, const SimplexGeometry &cell) {
  const auto output_pt = [](std::ostream &os,
                            const std::array<double, 3> &pt) -> std::ostream & {
    return os << '(' << pt[0] << ',' << pt[1] << ',' << pt[2] << ')';
  };

  os << "{id=" << static_cast<std::size_t>(cell.id) << ", cells=[";
  for (const auto &vx : cell.vertices)
    output_pt(os, vx) << ',';
  return os << "]}";
}

namespace detail {

/**
 * @brief Helper function to turn a SimplexCell<3> into an std::array of 4
 * points.
 */
template <class NodalData, class CellData>
SimplexGeometry to_simplex_geometry(
    const SimplexCell<3, CellData> &simplex_cell,
    const std::unordered_map<NodeId, Node<3, NodalData>> &vertices) {
  const auto &vertex_ids = simplex_cell.nodes;
  return SimplexGeometry{simplex_cell.id,
                         {vertices.at(vertex_ids[0]).position,
                          vertices.at(vertex_ids[1]).position,
                          vertices.at(vertex_ids[2]).position,
                          vertices.at(vertex_ids[3]).position}};
}

/**
 * @brief Collect all tetrahedra of a local mesh.
 */
template <class NodalData, class CellData>
std::vector<SimplexGeometry>
collect_local_tetrahedra(const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  auto tetrahedra = std::vector<SimplexGeometry>{};
  const auto n = mesh.cells.size();
  tetrahedra.reserve(n);
  for (const auto &[_id, cell] : mesh.cells)
    if (cell.process_rank == mpi_rank)
      tetrahedra.push_back(to_simplex_geometry(cell, mesh.nodes));
  return tetrahedra;
}
/**
 * @brief Collect all tetrahedra of a mesh.
 */
template <class NodalData, class CellData>
std::vector<SimplexGeometry>
collect_global_tetrahedra(const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  const auto local_tetrahedra = collect_local_tetrahedra(mesh);
  return qcmesh::mpi::all_concat_vec(local_tetrahedra);
}

} // namespace detail

template <class NodalData, class CellData>
std::vector<std::tuple<SimplexGeometry, SimplexGeometry>>
cell_overlaps(const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  constexpr double EPSILON = 2.E-10;
  const auto tetrahedra = detail::collect_global_tetrahedra(mesh);
  auto intersections =
      std::vector<std::tuple<SimplexGeometry, SimplexGeometry>>{};
  for (std::size_t i = 0; i + 1 < tetrahedra.size(); i++)
    for (std::size_t j = i + 1; j < tetrahedra.size(); j++)
      if (!simplices_dont_intersect(tetrahedra[i].vertices,
                                    tetrahedra[j].vertices, EPSILON))
        intersections.push_back({tetrahedra[i], tetrahedra[j]});
  return intersections;
}

} // namespace qcmesh::mesh::testing

namespace boost::serialization {

template <class Archive>
void serialize(Archive &ar, qcmesh::mesh::testing::SimplexGeometry &obj,
               const unsigned int) {
  ar &obj.id;
  ar &obj.vertices;
}

} // namespace boost::serialization

BOOST_CLASS_IMPLEMENTATION(qcmesh::mesh::testing::SimplexGeometry,
                           object_serializable)
BOOST_CLASS_TRACKING(qcmesh::mesh::testing::SimplexGeometry, track_never)
BOOST_IS_MPI_DATATYPE(qcmesh::mesh::testing::SimplexGeometry)
