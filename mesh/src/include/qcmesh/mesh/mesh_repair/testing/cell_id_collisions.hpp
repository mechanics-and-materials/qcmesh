// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mpi/all_concat_vec.hpp"
#include <algorithm>
#include <set>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh::testing {

namespace detail {
template <class T, class CellData, class F>
std::set<T> cell_collisions(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells,
    const F f) {
  auto local_ids = std::vector<T>{};
  for (const auto &[_, cell] : cells)
    local_ids.push_back(f(cell));
  const auto global_ids = qcmesh::mpi::all_concat_vec(local_ids);

  auto unique_ids = std::set<T>{};
  auto collisions = std::set<T>{};
  for (const auto id : global_ids) {
    const auto [_, no_collision] = unique_ids.emplace(id);
    if (!no_collision)
      collisions.emplace(id);
  }
  return collisions;
}
} // namespace detail

/**
 * @brief Find cell id collisions in a distributed mesh.
 */
template <class CellData>
std::set<SimplexCellId> cell_id_collisions(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  return detail::cell_collisions<SimplexCellId>(
      cells, [](const auto &cell) { return cell.id; });
}

/**
 * @brief Find collisions of cells determined by their nodes.
 */
template <class CellData>
std::set<std::array<NodeId, 4>> topological_cell_collisions(
    const std::unordered_map<SimplexCellId, SimplexCell<3, CellData>> &cells) {
  return detail::cell_collisions<std::array<NodeId, 4>>(
      cells, [](const auto &cell) {
        auto nodes = cell.nodes;
        std::sort(nodes.begin(), nodes.end());
        return nodes;
      });
}

} // namespace qcmesh::mesh::testing
