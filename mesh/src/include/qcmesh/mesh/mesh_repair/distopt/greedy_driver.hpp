// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cstdlib>
#include <limits>

namespace qcmesh::mesh {

/**
 * @brief This class implements a mesh repair schedule based on always choosing
 * maximum improvement for a bad element removal.
 */
class GreedyDriver {

  std::size_t n_iter = 0;
  std::size_t max_iters = 100;

public:
  inline explicit GreedyDriver(const std::size_t max_iters = 100)
      : max_iters{max_iters} {};

  inline bool run_queue() { return this->n_iter++ < this->max_iters; }

  /**
   * @brief dummy function here, used in other drivers
   */
  inline void notify() const {}

  /**
   * @brief Function to decide wether we accept changeset or not using greedy
   * process, i.e. only accept best change possible. So only take change if
   * current changeset's residual is better than residuals of previous
   * changesets resulting from different primitives of bad element.
   *
   * @param new_residual residual of proposed new set of elements (changeset)
   *
   * @param old_residual residual of current set of elements containing bad
   * element
   */
  [[nodiscard]] inline static bool is_improvement(const double new_residual,
                                                  const double old_residual,
                                                  const double best_residual) {
    return new_residual < old_residual && new_residual < best_residual;
  }
};

} // namespace qcmesh::mesh
