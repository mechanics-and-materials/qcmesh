// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief High level mesh optimization algorithm.
 *
 * @author Prateek Gupta, Manuel Weberndorfer
 */

#pragma once

#include "qcmesh/mesh/mesh_repair/distopt/change_preview.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/max_in_range.hpp"
#include <cassert>
#include <functional>
#include <optional>
#include <tuple>
#include <unordered_set>

namespace qcmesh::mesh {

/**
 * @param mesh A pointer to a valid instance of a mesh.
 *
 * @param gather_bad_elements
 *   Callback with signature std::vector<CellHandle> gather_bad_elements(const
 * Mesh &): Returns an iterable of bad elements in the local part of the mesh,
 *   which do not share faces / edges with other parts of the mesh.
 *
 * @param run_queue Returns true if a queue shall be built and optimized. It can
 *   be used to stop at a fixed number of iterations or to increase the global
 *   temperature.
 *
 * @param compute_residual Computes the residuals of cells given in the "region"
 * part of previews.
 * @param apply_change_set Modifies the mesh, so that the "out" cells of the
 * changeset are removed and the "in" cells are inserted into the mesh.
 *
 * @param modifiers A tuple of modifiers (topological
 *        transformations as e.g. edge removal / multi face removal).
 *
 * Each modifier must provide the following typedefs:
 *
 * - PrimitiveHandle:
 *   A handle to a geometric primitive.
 *
 * - CellDraft:
 *   Modifier specific information on how to create the cell replacements.
 *
 * Each modifier must provide the following methods:
 *
 * - (...) primitives_for_element(const Mesh&, const CellHandle) const:
 *   Returns an iterable of PrimitiveHandles.
 *
 * - ChangePreview<CellHandle, CellDraft> preview(const Mesh& mesh, const
 * PrimitiveHandle&) const: Computes a preview of the operation, the modifier
 * suggests.
 *
 * - void preview_to_changeset(const ChangePreview<CellHandle, CellDraft>&&,
 * const Mesh&): Given a preview for a change, assemble the elements to be
 * removed and the cell drafts for elements to be inserted.
 */
template <class CellDraft, class CellHandle, class Mesh,
          class GatherBadElements, class Driver, class ComputeResidual,
          class ApplyChangeSet, class... Modifiers>
void optimize_local_mesh(Mesh &mesh, GatherBadElements gather_bad_elements,
                         Driver &driver, ComputeResidual compute_residual,
                         ApplyChangeSet apply_change_set,
                         const std::tuple<Modifiers...> &modifiers);

/**
 * @brief Info how to find the cell in the global mesh: process_id and
 * cell_handle within process.
 */
template <class ProcessId, class CellHandle> struct CellAddress {
  ProcessId process_id;
  CellHandle cell_handle;
};

/**
 * @param mesh A pointer to a valid instance of a mesh.
 *
 * @param local_process_id Id / rank of the local process.
 *
 * @param gather_bad_elements
 *   Callback with signature std::vector<CellAddress<ProcessId, CellHandle>>
 * gather_bad_elements(const Mesh &): Returns an iterables of globally existing
 * bad elements.
 *
 * @param exchange_halos
 *   Callback with signature void exchange_halos(Mesh &, const
 * CellAddress<ProcessId, CellHandle>): Called before applying a modifier to
 * the provided element. Exchange halos with other MPI processes.
 *
 * @param run_queue Returns true if a queue shall be built and optimized. It can
 * be used to stop at a fixed number of iterations or to increase the global
 * temperature.
 *
 * @param compute_residual Computes the residuals of cells given in the "region"
 * part of previews.
 *
 * @param apply_change_set_distributed Modifies the mesh, so that the "out"
 * cells of the changeset are removed and the "in" cells are inserted into the
 * mesh. In contrast to the "local" variant of this function,
 * `apply_change_set_distributed` has to apply the changes in a distributed
 * manner.
 *
 * @param modifiers A tuple of modifiers (topological
 *        transformations as e.g. edge removal / multi face removal).
 *
 * Each modifier must provide the following typedefs:
 *
 * - PrimitiveHandle:
 *   A handle to a geometric primitive.
 *
 * - CellDraft:
 *   Modifier specific information on how to create the cell replacements.
 *
 * Each modifier must provide the following methods:
 *
 * - (...) primitives_for_element(const Mesh&, const CellHandle) const:
 *   Returns an iterable of PrimitiveHandles.
 *
 * - ChangePreview<CellHandle, CellDraft> preview(const Mesh& mesh, const
 * PrimitiveHandle&) const: Computes a preview of the operation, the modifier
 * suggests.
 *
 * - void preview_to_changeset(const ChangePreview<CellHandle, CellDraft>&&,
 * const Mesh&): Given a preview for a change, assemble the elements to be
 * removed and the cell drafts for elements to be inserted.
 */
template <class CellDraft, class CellHandle, class ProcessId, class Mesh,
          class GatherBadElements, class ExchangeHalos, class Driver,
          class ComputeResidual, class ApplyChangeSetDistributed,
          class... Modifiers>
void optimize_global_mesh(
    Mesh &mesh, ProcessId local_process_id,
    GatherBadElements gather_bad_elements, ExchangeHalos exchange_halos,
    Driver &driver, ComputeResidual compute_residual,
    ApplyChangeSetDistributed apply_change_set_distributed,
    const std::tuple<Modifiers...> &modifiers);
} // namespace qcmesh::mesh

namespace qcmesh::mesh {

namespace detail {

/**
 * @brief Apply a single modifier to a cell of a mesh.
 */
template <class CellDraft, class CellHandle, class Mesh, class ComputeResidual,
          class Modifier, class Driver>
std::optional<ChangeSet<CellDraft, CellHandle>>
apply_modifier(const CellHandle element, Mesh &mesh,
               ComputeResidual compute_residual, const Modifier &modifier,
               Driver &driver) {

  using PreviewType = typename Modifier::Preview;
  auto chosen_preview = PreviewType{};
  auto best_residual = std::numeric_limits<double>::infinity();

  for (auto &&primitive : modifier.primitives_for_element(mesh, element)) {
    // compute a candidate
    const auto preview = modifier.preview(mesh, primitive);
    if (preview.region.empty())
      continue;

    // measure candidate
    const auto in_residual = preview.replacement_residual;
    const auto out_residual =
        max_in_range(preview.region.begin(), preview.region.end(),
                     std::numeric_limits<double>::lowest(), compute_residual);

    // is_improvement makes the decision of accepting preview based on new
    // proposed residual, old (existing) residual and best_residual among
    // previous previews of current element.
    if (driver.is_improvement(in_residual, out_residual, best_residual))
      chosen_preview = preview;

    // Update the best_residual now, not before is_improvement, otherwise we can
    // overwrite it and no longer reflect best among previews of past primitives
    if (in_residual < best_residual)
      best_residual = in_residual;
  }

  // can also use best_preview.region.empty() as above
  if (!chosen_preview.region.empty())
    return modifier.preview_to_changeset(std::move(chosen_preview), mesh);
  return std::nullopt;
};

/**
 * @brief "Static for loop" over the items of a tuple which calls apply_modifier
 * with each modifier in the tuple.
 */
template <class CellDraft, std::size_t I = 0, class CellHandle, class Mesh,
          class ComputeResidual, class Driver, class... Modifiers>
std::optional<ChangeSet<CellDraft, CellHandle>>
apply_modifiers(const CellHandle element, Mesh &mesh,
                ComputeResidual compute_residual,
                const std::tuple<Modifiers...> &modifiers, Driver &driver) {
  if constexpr (I < sizeof...(Modifiers)) {
    auto maybe_change_set = apply_modifier<CellDraft>(
        element, mesh, compute_residual, std::get<I>(modifiers), driver);
    if (maybe_change_set.has_value())
      return maybe_change_set;
    return apply_modifiers<CellDraft, I + 1>(element, mesh, compute_residual,
                                             modifiers, driver);
  } else
    return std::nullopt;
}
} // namespace detail

template <class CellDraft, class CellHandle, class Mesh,
          class GatherBadElements, class Driver, class ComputeResidual,
          class ApplyChangeSet, class... Modifiers>
void optimize_local_mesh(Mesh &mesh, GatherBadElements gather_bad_elements,
                         Driver &driver, ComputeResidual compute_residual,
                         ApplyChangeSet apply_change_set,
                         const std::tuple<Modifiers...> &modifiers) {

  static_assert(std::is_constructible<std::function<double(CellHandle)>,
                                      ComputeResidual>::value,
                "Invalid type of compute_residual.");
  static_assert(std::is_constructible<
                    std::function<std::vector<CellHandle>(const Mesh &)>,
                    GatherBadElements>::value,
                "Invalid type of gather_bad_elements.");
  static_assert(
      std::is_constructible<
          std::function<void(Mesh &, const ChangeSet<CellDraft, CellHandle> &)>,
          ApplyChangeSet>::value,
      "Invalid type of apply_change_set.");

  while (driver.run_queue()) {
    auto mesh_changed = false;
    // get sorted list of bad elements
    const auto bad_elements = gather_bad_elements(mesh);

    if (bad_elements.empty())
      break;

    auto removed_elements = std::unordered_set<CellHandle>{};
    for (auto &&bad_element : bad_elements) {
      // If cell already has been removed, it is not owned by this rank. Skip
      // it.
      if (removed_elements.find(bad_element) != removed_elements.end())
        continue;
      if (auto change_set = detail::apply_modifiers<CellDraft>(
              bad_element, mesh, compute_residual, modifiers, driver)) {

        apply_change_set(mesh, change_set.value());
        mesh_changed = true;
        removed_elements.insert(change_set->out.begin(), change_set->out.end());

        driver.notify();
      }
    }
    if (!mesh_changed)
      break;
  }
}

template <class CellDraft, class CellHandle, class ProcessId, class Mesh,
          class GatherBadElements, class ExchangeHalos, class Driver,
          class ComputeResidual, class ApplyChangeSetDistributed,
          class... Modifiers>
void optimize_global_mesh(
    Mesh &mesh, ProcessId local_process_id,
    GatherBadElements gather_bad_elements, ExchangeHalos exchange_halos,
    Driver &driver, ComputeResidual compute_residual,
    ApplyChangeSetDistributed apply_change_set_distributed,
    const std::tuple<Modifiers...> &modifiers) {

  static_assert(std::is_constructible<std::function<double(CellHandle)>,
                                      ComputeResidual>::value,
                "Invalid type of compute_residual.");
  static_assert(
      std::is_constructible<
          std::function<std::vector<CellAddress<ProcessId, CellHandle>>(
              const Mesh &)>,
          GatherBadElements>::value,
      "Invalid type of gather_bad_elements.");
  static_assert(
      std::is_constructible<
          std::function<void(
              Mesh &, const std::vector<CellAddress<ProcessId, CellHandle>> &)>,
          ExchangeHalos>::value,
      "Invalid type of exchange_halos.");
  static_assert(
      std::is_constructible<
          std::function<bool(Mesh &, ChangeSet<CellDraft, CellHandle> &,
                             const CellAddress<ProcessId, CellHandle> &)>,
          ApplyChangeSetDistributed>::value,
      "Invalid type of apply_change_set_distributed.");

  while (driver.run_queue()) {
    auto mesh_changed = false;
    // get sorted list of bad elements
    const auto bad_elements = gather_bad_elements(mesh);

    if (bad_elements.empty())
      break;

    // prepare local data
    exchange_halos(mesh, bad_elements);
    auto removed_elements = std::unordered_set<CellHandle>{};
    for (const auto &bad_element : bad_elements) {
      if (removed_elements.find(bad_element.cell_handle) !=
          removed_elements.end())
        continue;
      auto change_set = ChangeSet<CellDraft, CellHandle>{};
      if (bad_element.process_id == local_process_id)
        if (auto maybe_change_set = detail::apply_modifiers<CellDraft>(
                bad_element.cell_handle, mesh, compute_residual, modifiers,
                driver))
          change_set = maybe_change_set.value();
      if (apply_change_set_distributed(mesh, change_set, bad_element)) {
        mesh_changed = true;
        removed_elements.insert(change_set.out.begin(), change_set.out.end());
        driver.notify();
      }
    }
    if (!mesh_changed)
      break;
  }
}

} // namespace qcmesh::mesh
