// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include <tuple>
#include <vector>

namespace qcmesh::mesh {

/**
 * @brief Holds information about of how to replace a part of a mesh
 * with new cells.
 */
template <class CellHandle, class CellDraft> struct ChangePreview {
  std::vector<CellHandle> region{};
  CellDraft replacement_draft{};
  double replacement_residual{};
  std::vector<EdgeWithLatticeCoordinates> upper_edge_pyramid{};
  std::vector<EdgeWithLatticeCoordinates> lower_edge_pyramid{};
};

template <class CellHandle, class CellDraft>
bool operator==(const ChangePreview<CellHandle, CellDraft> &lhs,
                const ChangePreview<CellHandle, CellDraft> &rhs) noexcept {
  return std::tie(lhs.replacement_residual, lhs.region,
                  lhs.replacement_draft) ==
         std::tie(rhs.replacement_residual, rhs.region, rhs.replacement_draft);
}

template <class CellHandle, class CellDraft>
bool operator!=(const ChangePreview<CellHandle, CellDraft> &lhs,
                const ChangePreview<CellHandle, CellDraft> &rhs) noexcept {
  return !(lhs == rhs);
}

} // namespace qcmesh::mesh
