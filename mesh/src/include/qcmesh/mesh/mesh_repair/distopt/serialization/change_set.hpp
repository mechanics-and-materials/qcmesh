// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include <boost/serialization/vector.hpp>

namespace boost::serialization {

template <class Archive, class CellDraft, class CellHandle>
void serialize(Archive &ar, qcmesh::mesh::ChangeSet<CellDraft, CellHandle> &obj,
               const unsigned int) {
  ar &obj.in;
  ar &obj.out;
}

} // namespace boost::serialization
