// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"

namespace qcmesh::mesh {
/**
 * @brief Register cells as face neighbors. Faces are specified by apex id.
 */
template <std::size_t Dimension, class CellData>
void connect_cells(SimplexCell<Dimension, CellData> &a,
                   const std::size_t apex_a,
                   SimplexCell<Dimension, CellData> &b,
                   const std::size_t apex_b) {
  a.neighbors[apex_a] = b.id;
  b.neighbors[apex_b] = a.id;
}

/**
 * @brief Register cells as face neighbors. Faces are specified by apex id.
 * This variant is intended to make a note of a connection of a cell draft A
 * with an already existing cell B, without modifying B.
 */
template <class CellData>
void connect_cells(CellDraft<CellData> &a, const std::size_t apex_a,
                   const SimplexCell<3, CellData> &b,
                   const std::size_t apex_b) {
  a.cell.neighbors[apex_a] = b.id;
  a.boundary_connections.emplace_back(BoundaryConnection{b.id, apex_b});
}

} // namespace qcmesh::mesh
