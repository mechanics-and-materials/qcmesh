// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/simplex_cell.hpp"
#include <Eigen/Core>
#include <array>
#include <vector>

namespace qcmesh::mesh {

/**
 * @brief Represents a connection of a not yet connected cell draft to
 * an existing cell of a mesh. The shared face is determined by
 * `neighbor_apex_idx`.
 */
struct BoundaryConnection {
  SimplexCellId neighbor_handle;
  std::size_t neighbor_apex_idx;
};

/**
 * @brief Intermediate struct holding a QCCell and additional cell data needed
 * when inserting the cell.
 */
template <class CellData> struct CellDraft {
  SimplexCell<3, CellData> cell{};
  Eigen::Matrix<double, 3, 3> cell_basis;
  int n_volume{};
  std::vector<BoundaryConnection> boundary_connections{};
};

/**
 * @brief Edge vector in euclidean and lattice coordinates.
 */
struct EdgeWithLatticeCoordinates {
  Eigen::Matrix<double, 3, 1> position;
  Eigen::Matrix<double, 3, 1> lattice;
};

/**
 * @brief For 3 given edges (euclidean positions e_i and lattice positions n_i),
 * Compute lattice vectors B=(b_1, b_2, b_3) and lattice volume, such that
 * (e_1, e_2, e_3) = B (n_1, n_2, n_3).
 */
std::tuple<int, Eigen::Matrix<double, 3, 3>>
lattice_vectors_with_vol(const EdgeWithLatticeCoordinates &a,
                         const EdgeWithLatticeCoordinates &b,
                         const EdgeWithLatticeCoordinates &c);

/**
 * @brief Create a new cell draft with id, for mpi process rank, simplex and
 * lettice edges.
 */
template <class CellData>
CellDraft<CellData> create_cell_draft(
    const SimplexCellId id, const int rank,
    const std::array<NodeId, 4> &simplex,
    const std::array<EdgeWithLatticeCoordinates, 3> &lattice_edges) {
  const auto cell = SimplexCell<3, CellData>{
      .id = id, .process_rank = rank, .nodes = simplex};
  const auto [n_vol, lattice_vectors] = lattice_vectors_with_vol(
      lattice_edges[0], lattice_edges[1], lattice_edges[2]);
  return CellDraft<CellData>{cell, lattice_vectors, n_vol, {}};
}

} // namespace qcmesh::mesh
