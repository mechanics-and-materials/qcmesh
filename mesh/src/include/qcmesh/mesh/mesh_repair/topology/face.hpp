// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <tuple>

namespace qcmesh::mesh {

/**
 * @brief A face (facet or n-1 dimensional simplex)
 * defined by a simplex (n dimensional) and the
 * vertex opposite of the face (apex).
 */
template <class CellHandle, class VertexHandle> struct Face {
  CellHandle cell;
  VertexHandle apex;
};

template <class CellHandle, class VertexHandle>
constexpr bool operator==(const Face<CellHandle, VertexHandle> &lhs,
                          const Face<CellHandle, VertexHandle> &rhs) noexcept {
  return std::tie(lhs.cell, lhs.apex) == std::tie(rhs.cell, rhs.apex);
}

template <class CellHandle, class VertexHandle>
constexpr bool operator!=(const Face<CellHandle, VertexHandle> &lhs,
                          const Face<CellHandle, VertexHandle> &rhs) noexcept {
  return !(lhs == rhs);
}

} // namespace qcmesh::mesh
