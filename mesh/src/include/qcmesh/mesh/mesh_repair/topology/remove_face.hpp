// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

//  Copyright 2006-2008 Bryan Klingner, all rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are
//  met:
//
//  - Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
//  - Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//  - Neither the name of Bryan Klingner nor the name of the University
//    of California nor the names of its contributors may be used to endorse
//    or promote products derived from this software without specific prior
//    written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
//  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
//  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
//  PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
//  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

/** @file
 * @brief Modified from Stellar's implementation of the topological
 * improvement algorithms by Jonathan Richard Shewchuk.
 *
 * See https://people.eecs.berkeley.edu/~jrs/papers/edge.ps.
 * See https://people.eecs.berkeley.edu/~jrs/stellar/ for more information about
 * Bryan Matthew Klingner and Jonathan Richard Shewchuk's Stellar software.
 * @author Manuel Weberndorfer
 */

#pragma once

#include "qcmesh/mesh/mesh_repair/topology/face_vertices_query.hpp"
#include "qcmesh/mesh/mesh_repair/topology/flip23.hpp"
#include "qcmesh/mesh/mesh_repair/topology/flip32.hpp"
#include "qcmesh/mesh/mesh_repair/topology/multimin.hpp"
#include "qcmesh/mesh/mesh_repair/topology/quality_measure.hpp"
#include "qcmesh/mesh/mesh_repair/topology/removal_info.hpp"
#include "qcmesh/mesh/mesh_repair/topology/shared_face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/shared_face_predicate.hpp"
#include "qcmesh/mesh/mesh_repair/topology/tetrahedron.hpp"
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <optional>

namespace qcmesh::mesh {

/**
 * @brief A function to select the two other vertices u, v in a triangle such
 * that v directly follows in the cyclic sequence of the triangle vertices.
 */
template <class VertexHandle>
std::tuple<VertexHandle, VertexHandle>
opposite_edge(const std::array<VertexHandle, 3> &triangle,
              const VertexHandle apex) {
  return triangle[0] == apex
             ? std::make_tuple(triangle[1], triangle[2])
             : (triangle[1] == apex
                    ? std::make_tuple(triangle[2], triangle[0])
                    : std::make_tuple(triangle[0], triangle[1]));
};

/**
 * @brief A function that for a given face, returns the same face but defined as
 * a member of the face neighbor cell, if it exists.
 */
template <class CellHandle, class VertexHandle>
using FaceMirrorQuery =
    std::function<std::optional<Face<CellHandle, VertexHandle>>(
        const Face<CellHandle, VertexHandle> &)>;

/**
 * @brief The return type of test_neighbor.
 */
template <class RemovalInfoType> struct TestNeighborResult {
  /**
   * @brief Quality if neighbor face is not removed. Considers the two
   * tetrahedra spanning the neighboring face and keeping all neighbors of that
   * face.
   */
  double quality_if_kept;

  /**
   * @brief Quality if neighbor face(s) is/are removed.
   */
  double quality_if_removed;

  /**
   * @brief Neighbor faces to remove.
   */
  std::vector<RemovalInfoType> faces_to_remove;
};

/**
 * @brief Given a face f shared by two tetrahedra and an edge uw of that face,
 * this function determines whether removing g (f's neighbor across uw) with a
 * 3-2 flip would be beneficial when removing f.
 *
 * @remark Performing a 3-2 flip (removing the edge uw) on the neighbor face
 * would remove the tetrahedra:
 *
 * {shared_f.face.apex, shared_f.bottom, uw.a, uw.b}: created by removing f
 * {shared_f.face.apex, uw.a, x, uw.b}: top tetrahedron sandwiching g
 * {uw.a, x, uw.b, shared_f.bottom}: bottom tetrahedron sandwiching g
 *
 * Here, x is the vertex of g not on uw.
 *
 * It would create the tetrahedra:
 *
 * {shared_f.face.apex, shared_f.bottom, uw.a, x},
 * {shared_f.face.apex, shared_f.bottom, x, uw.b}.
 *
 * @param is_flippable Returns true if and only if adding an additional edge
 * from top to bottom does not create inverted tetrahedra.
 *
 * @param get_vertices Returns the array of vertices that define a given face.
 *
 * @param face_mirror The function returns the congruent face of the cell
 * neighboring the given face or nullopt if it does not exist.
 */
template <class CellHandle, class VertexHandle>
TestNeighborResult<RemovalInfo<Face<CellHandle, VertexHandle>, VertexHandle>>
test_neighbor(
    const SharedFace<CellHandle, VertexHandle> &shared_f,
    const VertexHandle neighbor_apex,
    const QualityMeasure<VertexHandle> &compute_quality,
    const SharedFacePredicate<SharedFace<CellHandle, VertexHandle>>
        &is_flippable,
    const FaceVerticesQuery<CellHandle, VertexHandle> &get_vertices,
    const FaceMirrorQuery<CellHandle, VertexHandle> &face_mirror) noexcept {
  using Result = TestNeighborResult<
      RemovalInfo<Face<CellHandle, VertexHandle>, VertexHandle>>;
  using FaceType = Face<CellHandle, VertexHandle>;
  using TetrahedronType = Tetrahedron<VertexHandle>;
  /**
   * @brief Given a face of a cell, check if it connects to another cell
   * containing lower_apex.
   */
  const auto is_sandwich = [face_mirror](const auto &face,
                                         const auto &lower_apex) {
    const auto maybe_neighbor_cell_face = face_mirror(face);
    return (maybe_neighbor_cell_face &&
            maybe_neighbor_cell_face->apex == lower_apex);
  };

  const auto a = shared_f.face.apex;
  const auto b = shared_f.bottom;
  // Select u, w:
  const auto [u, w] = opposite_edge(get_vertices(shared_f.face), neighbor_apex);

  // the quality of this individual tetrahedron in the new ring
  const auto quality_abuw = compute_quality(TetrahedronType{a, b, u, w});

  // a function to create a return value if we do not want to remove the
  // neighbor face
  const auto create_do_not_remove_result = [quality_abuw]() {
    return Result{std::numeric_limits<double>::infinity(), quality_abuw, {}};
  };

  // Contains a and two other vertices u, w:
  const auto face_auw = FaceType{shared_f.face.cell, neighbor_apex};
  // check if f has a neighbor face across uw
  const auto maybe_neighbor_auw = face_mirror(face_auw);

  // if not, we certainly can't remove this neighbor
  if (!maybe_neighbor_auw)
    return create_do_not_remove_result();
  const auto shared_g = SharedFace<CellHandle, VertexHandle>{
      FaceType{maybe_neighbor_auw->cell, a}, b};
  // or if shared_g is not sandwiched between a and b,
  // removal is not possible too
  if (!is_sandwich(shared_g.face, b)) {
    return create_do_not_remove_result();
  }

  // use orientation tests to confirm that none of the vertices
  // u, v, or w lie inside conv({a,b,u,v,w})
  if (!is_flippable(shared_g)) {
    return create_do_not_remove_result();
  }

  // now test the two potential children of this face
  const auto uv_neighbor = test_neighbor(
      shared_g, w, compute_quality, is_flippable, get_vertices, face_mirror);
  const auto vw_neighbor = test_neighbor(
      shared_g, u, compute_quality, is_flippable, get_vertices, face_mirror);

  // find 3rd vertex of shared_g:
  const auto v = maybe_neighbor_auw->apex;

  // compute potential qualities
  const auto quality_auvw = compute_quality(TetrahedronType{a, u, v, w});
  const auto quality_uvwb = compute_quality(TetrahedronType{u, v, w, b});

  const auto quality_if_g_kept =
      multimin(quality_auvw, quality_uvwb, uv_neighbor.quality_if_kept,
               vw_neighbor.quality_if_kept);
  const auto quality_if_f_removed_and_g_kept =
      multimin(quality_if_g_kept, quality_abuw);
  const auto quality_if_g_removed =
      multimin(uv_neighbor.quality_if_removed, vw_neighbor.quality_if_removed);

  // should g be removed if f is?
  const auto removing_g_is_beneficial =
      bool{quality_if_g_removed > quality_if_f_removed_and_g_kept};
  if (!removing_g_is_beneficial) {
    return create_do_not_remove_result();
  }

  auto result = Result{quality_if_g_kept,
                       quality_if_g_removed,
                       {RemovalInfo<FaceType, VertexHandle>{
                           shared_g.face, maybe_neighbor_auw->apex}}};

  auto &faces = result.faces_to_remove;

  // insert neighbors provided by neighbors
  faces.reserve(faces.size() + uv_neighbor.faces_to_remove.size() +
                vw_neighbor.faces_to_remove.size());
  faces.insert(faces.end(), uv_neighbor.faces_to_remove.begin(),
               uv_neighbor.faces_to_remove.end());
  faces.insert(faces.end(), vw_neighbor.faces_to_remove.begin(),
               vw_neighbor.faces_to_remove.end());

  return result;
}

template <class VertexHandle> struct MultifaceRemovalResult {
  std::size_t number_of_removed_faces;
  double quality;
};

/**
 * @brief Attempts to remove a face (shared_f) guided by quality measurements.
 * All faces sandwiched between the same two vertices (a and b) as shared_f are
 * considered for removal.
 *
 * @param is_flippable Returns true if and only if adding an additional edge
 * from top to bottom does not create inverted tetrahedra.
 *
 * @param segment_intersects_face Returns true if and only if the segment
 * defined by the face.apex and bottom vertices intersects the face.
 *
 * @param get_vertices Returns the array of vertices that define a given face.
 *
 * @param face_mirror The function returns the congruent face of the cell
 * neighboring the given face or nullopt if it does not exist.
 *
 * @param flip32 The function performs a 3-2 flip on the mesh that removes the
 * provided edge (the second parameter). This edge needs to be on the boundary
 * of the shared face (the first parameter).
 */
template <class CellHandle, class VertexHandle>
MultifaceRemovalResult<VertexHandle>
remove_face(const SharedFace<CellHandle, VertexHandle> &shared_f,
            const QualityMeasure<VertexHandle> &compute_quality,
            const SharedFacePredicate<SharedFace<CellHandle, VertexHandle>>
                &is_flippable,
            const SharedFacePredicate<SharedFace<CellHandle, VertexHandle>>
                &segment_intersects_face,
            const FaceVerticesQuery<CellHandle, VertexHandle> &get_vertices,
            const FaceMirrorQuery<CellHandle, VertexHandle> &face_mirror,
            const Flip23<SharedFace<CellHandle, VertexHandle>> &flip23,
            const Flip32<SharedFace<CellHandle, VertexHandle>, VertexHandle>
                &flip32) noexcept {
  using FaceType = Face<CellHandle, VertexHandle>;
  using Result = MultifaceRemovalResult<VertexHandle>;
  using SharedFaceType = SharedFace<CellHandle, VertexHandle>;

  if (!is_flippable(shared_f)) {
    return Result{0, 0.};
  }

  const auto a = shared_f.face.apex;
  const auto b = shared_f.bottom;
  const auto &vertices = get_vertices(shared_f.face);
  const auto u = vertices[0];
  const auto v = vertices[1];
  const auto w = vertices[2];

  // figure out what neighbors might also be removed
  const auto neighbor_uv = test_neighbor(
      shared_f, w, compute_quality, is_flippable, get_vertices, face_mirror);
  const auto neighbor_vw = test_neighbor(
      shared_f, u, compute_quality, is_flippable, get_vertices, face_mirror);
  const auto neighbor_wu = test_neighbor(
      shared_f, v, compute_quality, is_flippable, get_vertices, face_mirror);

  // calculate potential qualities
  const auto qauvw = compute_quality({a, u, v, w});
  const auto quvwb = compute_quality({u, v, w, b});

  const auto qold =
      multimin(qauvw, quvwb, neighbor_uv.quality_if_kept,
               neighbor_vw.quality_if_kept, neighbor_wu.quality_if_kept);
  const auto qnew =
      multimin(neighbor_uv.quality_if_removed, neighbor_vw.quality_if_removed,
               neighbor_wu.quality_if_removed);

  const auto intersects_face_predicate =
      [&](const RemovalInfo<FaceType, VertexHandle> &info) {
        return segment_intersects_face(SharedFaceType{info.face, b});
      };

  const auto intersects_a_face =
      bool{segment_intersects_face(shared_f) ||
           std::any_of(neighbor_uv.faces_to_remove.begin(),
                       neighbor_uv.faces_to_remove.end(),
                       intersects_face_predicate) ||
           std::any_of(neighbor_vw.faces_to_remove.begin(),
                       neighbor_vw.faces_to_remove.end(),
                       intersects_face_predicate) ||
           std::any_of(neighbor_wu.faces_to_remove.begin(),
                       neighbor_wu.faces_to_remove.end(),
                       intersects_face_predicate)};

  if (!intersects_a_face) {
    return Result{0, qold};
  }

  // start by removing f with a single 2-3 flip
  flip23(shared_f);

  // remove children (and their children)
  const auto handle_children =
      [&flip32, b](const std::vector<RemovalInfo<FaceType, VertexHandle>>
                       &faces_to_remove) {
        for (const auto &info : faces_to_remove) {
          flip32(SharedFaceType{info.face, b}, info.edge_apex);
        }
      };

  handle_children(neighbor_uv.faces_to_remove);
  handle_children(neighbor_vw.faces_to_remove);
  handle_children(neighbor_wu.faces_to_remove);

  return Result{1 + neighbor_uv.faces_to_remove.size() +
                    neighbor_vw.faces_to_remove.size() +
                    neighbor_wu.faces_to_remove.size(),
                qnew};
}

/**
 * @brief Encapsulates information on how to build new cells based on a
 * ring of vertices which should be connected to two apex vertices.
 */
template <class VertexHandle> struct FaceRemovalDraft {
  std::vector<VertexHandle> vertex_ring;
  VertexHandle apex_a;
  VertexHandle apex_b;
};

template <class CellHandle, class VertexHandle> struct FaceRemovalPreview {
  std::vector<CellHandle> out;
  FaceRemovalDraft<VertexHandle> replacement_draft;
  double new_quality;
};

/**
 * @brief Delegates to the other overload of remove_face, passing on its
 * arguments. However, instead of modifying the mesh directly using flips, this
 * overload returns a diff (in- and out- elements).
 */
template <class CellHandle, class VertexHandle>
FaceRemovalPreview<CellHandle, VertexHandle> remove_face(
    const Face<CellHandle, VertexHandle> &face,
    const QualityMeasure<VertexHandle> &compute_quality,
    const SharedFacePredicate<SharedFace<CellHandle, VertexHandle>>
        &is_flippable,
    const SharedFacePredicate<SharedFace<CellHandle, VertexHandle>>
        &segment_intersects_face,
    const FaceVerticesQuery<CellHandle, VertexHandle> &get_vertices,
    const FaceMirrorQuery<CellHandle, VertexHandle> &face_mirror) noexcept {
  using SharedFaceType = SharedFace<CellHandle, VertexHandle>;

  auto result = FaceRemovalPreview<CellHandle, VertexHandle>{};

  const auto maybe_neighbor_face = face_mirror(face);
  if (!maybe_neighbor_face)
    return result;
  const auto apex_bottom = maybe_neighbor_face->apex;

  const auto flip23 =
      Flip23<SharedFaceType>{[&](const SharedFaceType &shared_f) {
        const auto a = shared_f.face.apex;
        const auto b = shared_f.bottom;
        const auto &vertices = get_vertices(shared_f.face);
        const auto u = vertices[0];
        const auto v = vertices[1];
        const auto w = vertices[2];

        result.out.emplace_back(shared_f.face.cell);
        result.out.emplace_back(face_mirror(shared_f.face).value().cell);

        result.replacement_draft.apex_a = a;
        result.replacement_draft.apex_b = b;

        result.replacement_draft.vertex_ring.emplace_back(u);
        result.replacement_draft.vertex_ring.emplace_back(v);
        result.replacement_draft.vertex_ring.emplace_back(w);
      }};

  const auto flip32 = Flip32<SharedFaceType, VertexHandle>{
      [&](const SharedFaceType &shared_g, const VertexHandle x) {
        /*
         * x---u
         *  \ / \
         *   w---v
         */
        const auto [u, w] = opposite_edge(get_vertices(shared_g.face), x);

        result.out.emplace_back(shared_g.face.cell);
        result.out.emplace_back(face_mirror(shared_g.face).value().cell);

        const auto pos =
            std::find(result.replacement_draft.vertex_ring.cbegin(),
                      result.replacement_draft.vertex_ring.cend(), u);
        assert(pos != result.replacement_draft.vertex_ring.cend() &&
               "This tetrahedron must have been created in a previous step.");
        result.replacement_draft.vertex_ring.emplace(pos, x);
      }};

  const auto stats =
      remove_face(SharedFace<CellHandle, VertexHandle>{face, apex_bottom},
                  compute_quality, is_flippable, segment_intersects_face,
                  get_vertices, face_mirror, flip23, flip32);
  result.new_quality = stats.quality;
  return result;
}

} // namespace qcmesh::mesh
