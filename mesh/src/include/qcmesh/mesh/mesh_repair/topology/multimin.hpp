// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <initializer_list>
#include <limits>
#include <type_traits>

#pragma once

namespace qcmesh::mesh {

namespace detail {
/**
 * @brief Returns the minimum of a range of values. If an empty range is
 * provided, then the maximum of the value range is returned.
 */
template <class Range> typename Range::value_type rangemin(const Range &range) {
  using ValueType = typename Range::value_type;
  if (std::begin(range) == std::end(range)) {
    return std::numeric_limits<ValueType>::max();
  }
  return *std::min_element(std::begin(range), std::end(range));
}
} // namespace detail

/**
 * @brief Returns the minimum of the input values.
 */
template <class S, class... T>
typename std::decay<S>::type multimin(S &&s, T &&...t) noexcept {
  const auto list = std::initializer_list<typename std::decay<S>::type>{
      std::forward<S>(s), std::forward<T>(t)...};
  return detail::rangemin(list);
}

} // namespace qcmesh::mesh
