// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/topology/face.hpp"

namespace qcmesh::mesh {

/**
 * @brief A description of a face that is to be removed by a 3-2 flip.
 */
template <class FaceType, class VertexHandle> struct RemovalInfo {
  /**
   * @brief The face to remove.
   */
  FaceType face;

  /**
   * @brief The face is removed by removing the edge opposite to edge_apex.
   */
  VertexHandle edge_apex;
};

template <class FaceType, class VertexHandle>
constexpr bool
operator==(const RemovalInfo<FaceType, VertexHandle> &lhs,
           const RemovalInfo<FaceType, VertexHandle> &rhs) noexcept {
  return lhs.face == rhs.face && lhs.edge_apex == rhs.edge_apex;
}
template <class FaceType, class VertexHandle>
constexpr bool
operator!=(const RemovalInfo<FaceType, VertexHandle> &lhs,
           const RemovalInfo<FaceType, VertexHandle> &rhs) noexcept {
  return !(lhs == rhs);
}

} // namespace qcmesh::mesh
