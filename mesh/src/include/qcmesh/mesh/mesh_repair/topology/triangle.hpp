// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <algorithm>
#include <array>

namespace qcmesh::mesh {

/**
 * @brief Triangle of vertex handles.
 */
template <class VertexHandle> using Triangle = std::array<VertexHandle, 3>;

/**
 * @brief Construct a triangle with ordered vertex handles.
 */
template <class VertexHandle>
Triangle<VertexHandle> ordered_triangle(const VertexHandle h1,
                                        const VertexHandle h2,
                                        const VertexHandle h3) {
  auto t = Triangle<VertexHandle>{h1, h2, h3};
  std::sort(t.begin(), t.end());
  return t;
}

} // namespace qcmesh::mesh
