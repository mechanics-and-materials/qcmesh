// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/topology/face.hpp"
#include <tuple>

namespace qcmesh::mesh {

/**
 * @brief Defines a face that is shared between two tetrahedra.
 */
template <class CellHandle, class VertexHandle> struct SharedFace {
  Face<CellHandle, VertexHandle> face;
  VertexHandle bottom;
};

template <class CellHandle, class VertexHandle>
constexpr bool
operator==(const SharedFace<CellHandle, VertexHandle> &lhs,
           const SharedFace<CellHandle, VertexHandle> &rhs) noexcept {
  return std::tie(lhs.face, lhs.bottom) == std::tie(rhs.face, rhs.bottom);
}

template <class CellHandle, class VertexHandle>
constexpr bool
operator!=(const SharedFace<CellHandle, VertexHandle> &lhs,
           const SharedFace<CellHandle, VertexHandle> &rhs) noexcept {
  return !(lhs == rhs);
}

} // namespace qcmesh::mesh
