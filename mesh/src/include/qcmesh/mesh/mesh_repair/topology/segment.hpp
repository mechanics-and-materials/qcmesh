// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace qcmesh::mesh {

/**
 * @brief A segment connecting two vertices a and b.
 */
template <class VertexHandle> struct Segment {
  VertexHandle a, b;
};

template <class VertexHandle>
constexpr bool operator==(const Segment<VertexHandle> &lhs,
                          const Segment<VertexHandle> &rhs) noexcept {
  return lhs.a == rhs.a && lhs.b == rhs.b;
}

template <class VertexHandle>
constexpr bool operator!=(const Segment<VertexHandle> &lhs,
                          const Segment<VertexHandle> &rhs) noexcept {
  return !(lhs == rhs);
}

} // namespace qcmesh::mesh
