// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Core edge removal algorithm.
 *
 * @author Prateek Gupta
 */

#pragma once

#include "qcmesh/mesh/mesh_repair/topology/face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/quality_measure.hpp"
#include "qcmesh/mesh/mesh_repair/topology/tetrahedron.hpp"
#include "qcmesh/mesh/mesh_repair/topology/triangle.hpp"
#include <array>
#include <limits>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh {

/**
 * @brief For a given ring of cells around an edge,
 * determine the corresponding ring of vertices
 *
 * @returns A std::vector containing the union of all vertices of the cells
 * with the 2 apex vertices removed,
 * ordered according to the ring of vertices.
 */
template <class VertexHandle>
std::vector<VertexHandle>
ring_vertices(const std::vector<Tetrahedron<VertexHandle>> &cell_ring,
              const VertexHandle a, const VertexHandle b) {
  auto vertex_ring = std::vector<VertexHandle>{};
  if (cell_ring.empty())
    return vertex_ring;
  vertex_ring.reserve(cell_ring.size() + 1);
  for (const auto &vertex_handle : cell_ring.front()) {
    if (vertex_handle != a && vertex_handle != b)
      vertex_ring.emplace_back(vertex_handle);
    if (vertex_ring.size() == 2)
      break;
  }
  if (cell_ring.size() == 1)
    return vertex_ring;
  for (const auto &vertex_handle : cell_ring[1])
    if (vertex_handle == vertex_ring[0])
      vertex_ring = {{vertex_ring[1], vertex_ring[0]}};
  for (std::size_t i = 1; i < cell_ring.size(); i++)
    for (const auto &vertex_handle : cell_ring[i])
      if (vertex_handle != a && vertex_handle != b &&
          vertex_handle != vertex_ring.back() &&
          vertex_handle != vertex_ring.front()) {
        vertex_ring.emplace_back(vertex_handle);
        break;
      }
  return vertex_ring;
}

namespace detail {

template <class VertexHandle>
std::vector<Triangle<VertexHandle>>
deref_triangles(const std::vector<Triangle<std::size_t>> &triangle_indices,
                const std::vector<VertexHandle> &vertices) {
  auto triangles = std::vector<std::array<VertexHandle, 3>>{};
  triangles.reserve(triangle_indices.size());
  for (const auto &t : triangle_indices)
    triangles.emplace_back(
        std::array{vertices[t[0]], vertices[t[1]], vertices[t[2]]});
  return triangles;
}

template <class Object, class Handle, class Deref>
auto deref_vector(const std::vector<Handle> &objects, const Deref deref) {
  auto out = std::vector<Object>{};
  out.reserve(objects.size());
  for (const auto &obj : objects)
    out.emplace_back(deref(obj));
  return out;
}

/**
 * @brief Consists of two (ring_size - 2) x ring_size tables for quality and
 * indices. quality[i, j] is the maximal quality of all possible triangulations
 * of the vertices i,...,j, where the quality of a triangulation is the lowest
 * quality of all the triangles in the triangulation. index[i, j] holds the
 * index k (i < k < j), for which the triangle (i,j,k) is contained in the
 * triangulation of maximal quality (for given i,j such a k is unique).
 *
 * As a consequence of this definition, quality[0, ring_size-1] will hold
 * the maximal quality over all triangulations of all vertices.
 */
struct KlincsekTable {
  std::vector<double> quality;
  std::vector<std::size_t> index;
};

template <class VertexHandle>
KlincsekTable
create_klincsek_table(const std::vector<VertexHandle> &ring,
                      const VertexHandle a, const VertexHandle b,
                      const QualityMeasure<VertexHandle> &quality_measure) {
  const auto ring_size = ring.size();
  auto table = KlincsekTable{
      .quality = std::vector<double>((ring_size - 2) * ring_size),
      .index = std::vector<std::size_t>((ring_size - 2) * ring_size)};
  const auto triangle_quality_residual =
      [&](const std::size_t i, const std::size_t j, const std::size_t k) {
        const auto residual_b = quality_measure(
            Tetrahedron<VertexHandle>{ring[i], ring[j], ring[k], b});
        const auto residual_a = quality_measure(
            Tetrahedron<VertexHandle>{a, ring[i], ring[j], ring[k]});
        return std::min(residual_b, residual_a);
      };
  // i < k < j
  for (std::size_t i = ring_size - 3; i + 1 > 0; --i) {
    for (std::size_t j = i + 2; j < ring_size; ++j) {
      for (std::size_t k = i + 1; k < j; ++k) {
        auto q = triangle_quality_residual(i, k, j);
        if (k + 1 < j) {
          q = std::min(q, table.quality[ring_size * k + j]);
        }
        if (i + 1 < k) {
          q = std::min(q, table.quality[ring_size * i + k]);
        }
        if (k == i + 1 || q >= table.quality[ring_size * i + j]) {
          table.quality[ring_size * i + j] = q;
          table.index[ring_size * i + j] = k;
        }
      }
    }
  }
  return table;
}

} // namespace detail

/**
 * @brief Node of a Klincsek tree.
 * Each node consists of a triangle {a, b, c} and the information of its
 * neighbor (parent) at edge a-c, aswell as the edge of the parent it is
 * connected to (either a-b / parent_apex=2 or b-c / parent_apex=0).
 */
struct KlincsekTreeNode {
  Triangle<std::size_t> triangle;
  std::size_t parent;
  std::size_t parent_apex;
};

/**
 * @brief Create a Klincsek tree from a Klincsek table.
 */
std::vector<KlincsekTreeNode>
create_klincsek_tree(const std::vector<std::size_t> &klincsek_index,
                     const std::size_t ring_size);

/**
 * @brief Encapsulates information on how to build new cells based on a
 * Klincsek tree of triangles.
 *
 * The root of the tree is a triangle {a,b,c}, having its edge a-c on the
 * boundary of the ring to be triangulated. Each following node is another
 * triangle {a,b,c} whose edge a-c will be attached to a previous node (parent),
 * either at edge a-b (parent_apex=2) or b-c (parent_apex=0).
 */
template <class VertexHandle> struct EdgeRemovalDraft {
  VertexHandle apex_a{};
  VertexHandle apex_b{};
  std::vector<VertexHandle> vertex_ring{};
  std::vector<KlincsekTreeNode> klincsek_tree = {};
};

/**
 * @brief Return resulting quality and EdgeRemovalDraft for a
 * given ring of cells around an edge.
 */
template <class CellHandle, class VertexHandle, class DerefCellHandle>
std::tuple<double, EdgeRemovalDraft<VertexHandle>> create_edge_removal_draft(
    const std::vector<CellHandle> &ring_handles, const VertexHandle a_handle,
    const VertexHandle b_handle, const DerefCellHandle deref_cell_handle,
    const QualityMeasure<VertexHandle> &quality_measure) {
  if (ring_handles.size() <= 1)
    return std::make_tuple(0., EdgeRemovalDraft<VertexHandle>{});
  const auto ring = detail::deref_vector<Tetrahedron<VertexHandle>>(
      ring_handles, deref_cell_handle);
  auto draft = EdgeRemovalDraft<VertexHandle>{
      .apex_a = a_handle,
      .apex_b = b_handle,
      .vertex_ring = ring_vertices(ring, a_handle, b_handle),
  };
  const auto table = detail::create_klincsek_table(draft.vertex_ring, a_handle,
                                                   b_handle, quality_measure);
  draft.klincsek_tree =
      create_klincsek_tree(table.index, draft.vertex_ring.size());
  return std::make_tuple(table.quality[draft.vertex_ring.size() - 1], draft);
}

/**
 * @brief Struct encapsulating the mesh difference resulting from an edge
 * removal.
 */
template <class VertexHandle, class CellHandle> struct EdgeRemovalPreview {
  std::vector<CellHandle> out;
  EdgeRemovalDraft<VertexHandle> replacement_draft;
  double replacement_quality = 0.;
};

template <class VertexHandle, class CellHandle, class DerefCellHandle>
double
min_quality_of_cells(const std::vector<CellHandle> &cells,
                     const DerefCellHandle deref_cell_handle,
                     const QualityMeasure<VertexHandle> &quality_measure) {
  auto quality = std::numeric_limits<double>::max();
  for (const auto &cell : cells) {
    const auto cell_quality = quality_measure(deref_cell_handle(cell));
    if (cell_quality < quality)
      quality = cell_quality;
  }
  return quality;
}

/**
 * @brief Attempts to remove an edge guided by quality measurements.
 * All cells containing the edge (forming a ring around the edge)
 * are considered for removal.
 *
 * @param edge The edge to remove.
 *
 * @param deref_cell_handle Returns the vertex handles cointained in a given
 * cell.
 *
 * @param deref_edge_handle Returns the vertex handles, defining a given edge.
 *
 * @param simplex_ring The function collects all cells containing a given edge
 * such that consecutive cells are face neighbors.
 *
 * @param quality_measure The function computes for given a cell a value
 * measuring the quality of the cell (lowest value means degenerate cell).
 *
 * @param overlap_check For given points t1, t2, t3, apex_a, apex_b, check
 * if the tetrahedra (t1, t2, t3, apex_a) and (t1, t2, t3, apex_b) do not
 * intersect (in this case return true).
 *
 * @return An EdgeRemovalPreview instance containing cells to be removed (cell
 * handles) and cells to be created (cells defined by a Klincsek tree).
 */
template <class CellHandle, class VertexHandle, class EdgeHandle,
          class DerefCellHandle, class DerefEdgeHandle,
          class CollectRingAroundEdge, class OverlapCheck>
EdgeRemovalPreview<VertexHandle, CellHandle>
remove_edge(const EdgeHandle &edge, const DerefCellHandle deref_cell_handle,
            const DerefEdgeHandle deref_edge_handle,
            const CollectRingAroundEdge simplex_ring,
            const QualityMeasure<VertexHandle> &quality_measure,
            const OverlapCheck overlap_check) {
  const auto ring = simplex_ring(edge);
  const auto [p1, p2] = deref_edge_handle(edge);
  if (ring.size() == 1)
    // just remove this edge and its incident element.
    // only two ring vertices mean loner bad element.
    return {.out = ring,
            .replacement_draft = {p1, p2},
            .replacement_quality = std::numeric_limits<double>::max()};
  const auto [quality, draft] = create_edge_removal_draft(
      ring, p1, p2, deref_cell_handle, quality_measure);
  // Check for inverted tetrahedra:
  for (const auto &node : draft.klincsek_tree)
    if (!overlap_check(draft.vertex_ring[node.triangle[0]],
                       draft.vertex_ring[node.triangle[1]],
                       draft.vertex_ring[node.triangle[2]], draft.apex_a,
                       draft.apex_b))
      return {};
  return {
      .out = ring, .replacement_draft = draft, .replacement_quality = quality};
}
} // namespace qcmesh::mesh
