// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/topology/tetrahedron.hpp"
#include <functional>

namespace qcmesh::mesh {

/**
 * @brief Type of a function that measures the quality of a tetrahedron.
 */
template <class VertexHandle>
using QualityMeasure = std::function<double(const Tetrahedron<VertexHandle> &)>;

} // namespace qcmesh::mesh
