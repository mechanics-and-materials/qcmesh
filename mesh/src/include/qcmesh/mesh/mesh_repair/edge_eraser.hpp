// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

/** @file
 * @brief Edge removal.
 *
 * @author Prateek Gupta
 */

#pragma once

#include "qcmesh/mesh/face.hpp"
#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/mesh/mesh_repair/compute_quality.hpp"
#include "qcmesh/mesh/mesh_repair/connect_cells.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_preview.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_edge.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <boost/mpi.hpp>

namespace qcmesh::mesh {

namespace detail {

std::tuple<std::size_t, std::size_t>
opposite_edge_indices(const std::size_t a_idx, const std::size_t b_idx);

template <class NodalData, class CellData> struct CellRingIterator {
  const SimplexMesh<3, 3, NodalData, CellData> &mesh;
  const SimplexCell<3, CellData> *current_cell;
  NodeId edge_start;
  NodeId edge_end;
  std::size_t forward_face_apex{};
  NodeId shared_ring_vertex{};

  CellRingIterator(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                   const SimplexCell<3, CellData> &cell,
                   std::size_t edge_start_idx, std::size_t edge_end_idx)
      : mesh{mesh}, current_cell{&cell}, edge_start{cell.nodes[edge_start_idx]},
        edge_end{cell.nodes[edge_end_idx]} {
    auto apex_back = std::size_t{};
    std::tie(forward_face_apex, apex_back) =
        opposite_edge_indices(edge_start_idx, edge_end_idx);
    shared_ring_vertex = cell.nodes[apex_back];
  }
};

template <class NodalData, class CellData>
CellRingIterator<NodalData, CellData> &
begin(CellRingIterator<NodalData, CellData> &ring_iter) {
  return ring_iter;
}

struct CellRingTerminator {};

template <class NodalData, class CellData>
CellRingTerminator end(CellRingIterator<NodalData, CellData> &) {
  return CellRingTerminator{};
}

template <class NodalData, class CellData>
bool operator!=(const CellRingIterator<NodalData, CellData> &ring_iter,
                const CellRingTerminator &) {
  return ring_iter.current_cell != nullptr;
}
template <class NodalData, class CellData>
CellRingIterator<NodalData, CellData> &
operator*(CellRingIterator<NodalData, CellData> &ring_iter) {
  return ring_iter;
}

template <class NodalData, class CellData>
CellRingIterator<NodalData, CellData> &
operator++(CellRingIterator<NodalData, CellData> &iter) {
  const auto next_cell_handle =
      iter.current_cell->neighbors[iter.forward_face_apex];
  if (!next_cell_handle.has_value()) {
    iter.current_cell = nullptr;
    return iter;
  }
  const auto &next_cell = deref_cell_id(iter.mesh, next_cell_handle.value());
  const auto &next_nodes = next_cell.nodes;
  auto next_apex_forward = std::size_t{0};
  while (iter.shared_ring_vertex != next_nodes[next_apex_forward])
    ++next_apex_forward;
  auto next_shared_ring_vertex = std::size_t{0};
  while (next_nodes[next_shared_ring_vertex] == iter.edge_start ||
         next_nodes[next_shared_ring_vertex] == iter.edge_end ||
         next_shared_ring_vertex == next_apex_forward)
    ++next_shared_ring_vertex;
  iter.current_cell = &next_cell;
  iter.forward_face_apex = next_apex_forward;
  iter.shared_ring_vertex = next_nodes[next_shared_ring_vertex];
  return iter;
}
template <class NodalData, class CellData>
CellRingIterator<NodalData, CellData>
next(CellRingIterator<NodalData, CellData> &&iter) {
  ++iter;
  return iter;
}

} // namespace detail

/**
 * @brief Collect all cells around an edge specified by a cell
 * handle and 2 indices of vertices inside the 4-array of vertices of the cell.
 */
template <class NodalData, class CellData>
std::vector<SimplexCellId> collect_cell_ring_around_edge(
    const SimplexMesh<3, 3, NodalData, CellData> &mesh,
    const SimplexCellId cell_id, const std::size_t edge_start_idx,
    const std::size_t edge_end_idx) {
  auto forward_ring = std::vector<SimplexCellId>{cell_id};
  const auto &cell = deref_cell_id(mesh, cell_id);
  for (const auto &iter : detail::next(detail::CellRingIterator(
           mesh, cell, edge_start_idx, edge_end_idx))) {
    if (iter.current_cell->id == cell_id)
      return forward_ring;
    forward_ring.push_back(iter.current_cell->id);
  }
  auto backward_ring = std::vector<SimplexCellId>{};
  for (const auto &iter : detail::next(detail::CellRingIterator(
           mesh, cell, edge_end_idx, edge_start_idx))) {
    if (iter.current_cell->id == cell_id)
      return forward_ring;
    backward_ring.push_back(iter.current_cell->id);
  }
  forward_ring.insert(forward_ring.begin(), backward_ring.rbegin(),
                      backward_ring.rend());
  return forward_ring;
}

/**
 * @brief A pair of faces (upper, lower) belonging to the boundary of a ring of
 * cells around an edge.
 */
template <class CellData> struct RingBoundaryFacePair {
  std::optional<std::tuple<const SimplexCell<3, CellData> &, std::size_t>>
      upper;
  std::optional<std::tuple<const SimplexCell<3, CellData> &, std::size_t>>
      lower;
};

namespace detail {
/**
 * @brief Returns either a tuple containing the cell handle
 * neighboring another cell and the apex vertex to that face,
 * or nullopt if there is no neighboring cell.
 */
template <class NodalData, class CellData>
std::optional<std::tuple<SimplexCellId, std::size_t>>
get_face_neighbor(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                  const SimplexCellId cell_handle, const std::size_t apex_idx) {
  const auto cell = deref_cell_id(mesh, cell_handle);
  const auto neighbor_handle = cell.neighbors[apex_idx];
  if (!neighbor_handle.has_value())
    return std::nullopt;
  const auto neighbor = deref_cell_id(mesh, neighbor_handle.value());
  const auto face_id = index_of_neighbor(neighbor, cell_handle);
  if (!face_id.has_value())
    return std::nullopt;
  return std::make_tuple(neighbor_handle.value(), face_id.value());
}
template <class CellData>
std::array<std::size_t, 2>
vertex_indices_of(const SimplexCell<3, CellData> &cell, const NodeId a,
                  const NodeId b) {
  auto indices = std::array<std::size_t, 2>{};
  auto n_found = (std::size_t){0};
  for (std::size_t i = 0; i < cell.nodes.size() && n_found < 2; i++)
    if (cell.nodes[i] == a)
      indices[0] = i, n_found++;
    else if (cell.nodes[i] == b)
      indices[1] = i, n_found++;
  return indices;
}
} // namespace detail

/**
 * @brief Returns pairs of faces (upper: contains a, lower: contains b)
 * belonging to the boundary of a ring of cells around an edge.
 */
template <class NodalData, class CellData>
std::vector<RingBoundaryFacePair<CellData>>
collect_boundary_faces_around_edge_ring(
    const SimplexMesh<3, 3, NodalData, CellData> &mesh,
    const std::vector<SimplexCellId> &cell_ring, const NodeId a,
    const NodeId b) {
  auto boundary_face_pairs = std::vector<RingBoundaryFacePair<CellData>>{};
  boundary_face_pairs.reserve(cell_ring.size());
  const auto deref_face_handle = [&](const auto &face_handle) {
    if (!face_handle.has_value())
      return std::optional<
          std::tuple<const SimplexCell<3, CellData> &, std::size_t>>{};
    const auto &[cell_id, apex] = face_handle.value();
    auto &cell = deref_cell_id(mesh, cell_id);
    return std::optional{
        std::tuple<const SimplexCell<3, CellData> &, std::size_t>{cell, apex}};
  };
  for (const auto &cell_handle : cell_ring) {
    const auto [a_idx, b_idx] =
        detail::vertex_indices_of(deref_cell_id(mesh, cell_handle), a, b);
    boundary_face_pairs.emplace_back(RingBoundaryFacePair<CellData>{
        .upper = deref_face_handle(
            detail::get_face_neighbor(mesh, cell_handle, b_idx)),
        .lower = deref_face_handle(
            detail::get_face_neighbor(mesh, cell_handle, a_idx))});
  }
  return boundary_face_pairs;
}

/**
 * Exception thrown when attempting to invert a singular matrix
 */
struct InvertError : std::runtime_error {
  using std::runtime_error::runtime_error;
};

/**
 * @brief Return the first lu decomposition within a vector which is invertible.
 * Start searching at start_at. If search reaches end of vector, search in
 * reverse direction starting at start_at-1.
 */
template <class LU>
auto find_first_neighboring_invertible_basis(
    const std::vector<LU> &lu_decompositions, const std::size_t start_at = 0) {
  for (std::size_t pos = start_at; pos < lu_decompositions.size(); pos++)
    if (lu_decompositions[pos].isInvertible())
      return std::optional{lu_decompositions[pos]};
  for (std::size_t pos = start_at; pos != 0; pos--)
    if (lu_decompositions[pos].isInvertible())
      return std::optional{lu_decompositions[pos]};
  return std::optional<LU>{};
}

/**
 * @brief For a given ring of cells in a mesh, retrieve their cell bases and
 * compute their lu decompositions.
 */
template <class NodalData, class CellData>
auto cell_basis_lu_decompositions(
    const std::vector<SimplexCellId> &cell_ring,
    const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  using CellType = SimplexCell<3, CellData>;
  static_assert(traits::IS_IMPLEMENTED<traits::LatticeBasis<CellType>>);
  using LU = Eigen::FullPivLU<Eigen::Matrix<double, 3, 3>>;
  auto lu_decompositions = std::vector<LU>{};
  lu_decompositions.reserve(cell_ring.size());
  for (const auto cell_id : cell_ring)
    lu_decompositions.emplace_back(LU(array_ops::as_eigen_matrix(
        traits::lattice_basis(deref_cell_id(mesh, cell_id)))));
  return lu_decompositions;
}

/**
 * @brief For a given ring of cells around a central edge,
 * collect the lateral edges of the two polygonal pyramids (top, bottom), which
 * connected at their bases combine to the boundary of the ring of cells. The
 * edges are with respect to the apexes in euclidean coordinates and lattice
 * coordinates.
 */
template <class NodalData, class CellData>
std::tuple<std::vector<EdgeWithLatticeCoordinates>,
           std::vector<EdgeWithLatticeCoordinates>>
collect_bipyramid(const std::vector<SimplexCellId> &cell_ring,
                  const std::vector<NodeId> &vertex_ring, const NodeId apex_a,
                  const NodeId apex_b,
                  const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  if constexpr (traits::IS_IMPLEMENTED<
                    traits::LatticeBasis<SimplexCell<3, CellData>>>) {
    const auto lu_decompositions =
        cell_basis_lu_decompositions(cell_ring, mesh);
    const auto basis_lu =
        find_first_neighboring_invertible_basis(lu_decompositions);
    if (!basis_lu.has_value())
      return {{}, {}};

    const auto a = array_ops::as_eigen(deref_node_id(mesh, apex_a).position);
    const auto b = array_ops::as_eigen(deref_node_id(mesh, apex_b).position);
    // Lattice coordinates for b - a (need to feed the result of lu into a
    // vector, otherwise the expression will be consumed usable only 1 time):
    auto upper_pyramid = std::vector<EdgeWithLatticeCoordinates>{};
    auto lower_pyramid = std::vector<EdgeWithLatticeCoordinates>{};
    auto dn_ab = Eigen::Vector3d{};
    dn_ab = Eigen::Vector3d{basis_lu.value().solve(b - a)};
    upper_pyramid.reserve(vertex_ring.size());
    lower_pyramid.reserve(vertex_ring.size());

    for (std::size_t i = 0; i < vertex_ring.size(); i++) {
      const auto r =
          array_ops::as_eigen(deref_node_id(mesh, vertex_ring[i]).position);
      // If the basis of the current cell is not invertible, assume that the
      // cell is flat and that we can use the neighbors basis to determine the
      // lattice position hops of the current cell:
      const auto basis_lu = find_first_neighboring_invertible_basis(
          lu_decompositions, i > 0 ? i - 1 : i);
      if (!basis_lu.has_value())
        return {{}, {}};
      const auto d_br = r - b;
      // Lattice coordinates are integers. Reduce the numerical error by
      // rounding towards integers.
      const auto dn_br = Eigen::Vector3d{
          basis_lu.value().solve(d_br).array().round().matrix()};
      upper_pyramid.emplace_back(
          EdgeWithLatticeCoordinates{r - a, dn_br + dn_ab});
      lower_pyramid.emplace_back(EdgeWithLatticeCoordinates{d_br, dn_br});
    }
    return std::make_tuple(upper_pyramid, lower_pyramid);
  } else {
    const auto a = array_ops::as_eigen(deref_node_id(mesh, apex_a).position);
    const auto b = array_ops::as_eigen(deref_node_id(mesh, apex_b).position);
    // Lattice coordinates for b - a (need to feed the result of lu into a
    // vector, otherwise the expression will be consumed usable only 1 time):
    auto upper_pyramid = std::vector<EdgeWithLatticeCoordinates>{};
    upper_pyramid.reserve(vertex_ring.size());
    auto lower_pyramid = std::vector<EdgeWithLatticeCoordinates>{};
    lower_pyramid.reserve(vertex_ring.size());
    for (const auto &vertex : vertex_ring) {
      const auto r = array_ops::as_eigen(deref_node_id(mesh, vertex).position);
      const auto d_br = r - b;
      upper_pyramid.emplace_back(
          EdgeWithLatticeCoordinates{r - a, {0., 0., 0.}});
      lower_pyramid.emplace_back(
          EdgeWithLatticeCoordinates{d_br, {0., 0., 0.}});
    }
    return std::make_tuple(upper_pyramid, lower_pyramid);
  }
}

/**
 * @brief Returns true if the two tetrahedra {t1, t2, t3, apex_b} (upper
 * pyramid) and {apex_a, t1, t2, t3} (lower pyramid) both have positive
 * orientation.
 */
bool double_pyramid_has_positive_orientation(
    const std::array<double, 3> &t1, const std::array<double, 3> &t2,
    const std::array<double, 3> &t3, const std::array<double, 3> &apex_a,
    const std::array<double, 3> &apex_b);

template <class CellData>
void connect_cell_pair_to_boundary(
    CellDraft<CellData> &cell_upper, CellDraft<CellData> &cell_lower,
    const std::size_t triangle_apex,
    const RingBoundaryFacePair<CellData> &boundary_face_pair) {
  // triangle indices inside upper cell: 1, 2, 3
  const auto apex_upper = triangle_apex + 1;
  if (boundary_face_pair.upper.has_value())
    connect_cells(cell_upper, apex_upper,
                  std::get<0>(boundary_face_pair.upper.value()),
                  std::get<1>(boundary_face_pair.upper.value()));
  // triangle indices inside lower cell: 3, 2, 1
  if (boundary_face_pair.lower.has_value())
    connect_cells(cell_lower, 3 - triangle_apex,
                  std::get<0>(boundary_face_pair.lower.value()),
                  std::get<1>(boundary_face_pair.lower.value()));
}

/**
 * @brief A Modifier class for edge removal. To be used with
 * optimize_*_mesh.
 *
 * It wraps @ref remove_edge
 */
template <class NodalData, class CellData> class EdgeEraser {
  using Mesh = SimplexMesh<3, 3, NodalData, CellData>;
  using Cell = SimplexCell<3, CellData>;

public:
  using Preview = ChangePreview<SimplexCellId, EdgeRemovalDraft<NodeId>>;
  using CellHandle = SimplexCellId;
  using PrimitiveHandle =
      std::tuple<SimplexCellId, int, int>; // Our primitives are edges
  using ChangeSet = qcmesh::mesh::ChangeSet<CellDraft<CellData>, SimplexCellId>;
  /**
   * @brief Returns an iterable of PrimitiveHandles (edges) belonging to
   * cell_handle
   */
  [[nodiscard]] std::vector<PrimitiveHandle>
  primitives_for_element(const Mesh &mesh, const CellHandle cell_handle) const {
    const auto cell = mesh.cells.find(cell_handle);
    auto edges = std::vector<PrimitiveHandle>{};
    if (cell == mesh.cells.end())
      return edges;
    edges.reserve(6);
    for (auto start_node = 0; start_node < 3; ++start_node)
      for (auto end_node = start_node + 1; end_node < 4; ++end_node)
        edges.emplace_back(cell_handle, start_node, end_node);
    return edges;
  }

  /**
   * @brief Computes a preview of the operation.
   */
  [[nodiscard]] Preview preview(const Mesh &mesh,
                                const PrimitiveHandle &edge) const {
    const auto cell_node_ids = [](const auto &cell) {
      auto vertex_ids = Tetrahedron<NodeId>{};
      for (std::size_t i = 0; i < cell.nodes.size(); i++)
        vertex_ids[i] = cell.nodes[i];
      return vertex_ids;
    };
    const auto deref_cell_handle = [&](const auto cell_handle) {
      return cell_node_ids(deref_cell_id(mesh, cell_handle));
    };
    const auto deref_edge_handle = [&](const auto &edge_handle) {
      const auto nodes = deref_cell_id(mesh, std::get<0>(edge_handle)).nodes;
      return std::make_tuple(nodes[std::get<1>(edge_handle)],
                             nodes[std::get<2>(edge_handle)]);
    };
    const auto simplex_ring = [&](const auto &edge_handle) {
      auto cells = collect_cell_ring_around_edge(mesh, std::get<0>(edge_handle),
                                                 std::get<1>(edge_handle),
                                                 std::get<2>(edge_handle));
      if (cells.size() == 1) { // Check if the lone edge has a lone vertex.
        // This is the case if the simplex only has one neighbor.
        const auto cell = deref_cell_id(mesh, std::get<0>(edge_handle));
        if (!cell.neighbors[std::get<1>(edge_handle)].has_value() ||
            !cell.neighbors[std::get<2>(edge_handle)].has_value())
          return std::vector<SimplexCellId>{};
      }
      return cells;
    };
    const auto quality_measure = [&](const Tetrahedron<NodeId> &cell) {
      return compute_quality(mesh, cell);
    };
    const auto overlap_check = [&](const NodeId t1, const NodeId t2,
                                   const NodeId t3, const NodeId apex_a,
                                   const NodeId apex_b) {
      return double_pyramid_has_positive_orientation(
          deref_node_id(mesh, t1).position, deref_node_id(mesh, t2).position,
          deref_node_id(mesh, t3).position,
          deref_node_id(mesh, apex_a).position,
          deref_node_id(mesh, apex_b).position);
    };

    const auto preview = remove_edge<SimplexCellId, NodeId>(
        edge, deref_cell_handle, deref_edge_handle, simplex_ring,
        quality_measure, overlap_check);

    const auto [upper_edge_pyramid, lower_edge_pyramid] =
        collect_bipyramid(preview.out, preview.replacement_draft.vertex_ring,
                          preview.replacement_draft.apex_a,
                          preview.replacement_draft.apex_b, mesh);

    return {
        .region = (upper_edge_pyramid.size() > 0 ||
                   preview.replacement_draft.vertex_ring.empty())
                      ? preview.out
                      : std::vector<SimplexCellId>{},
        .replacement_draft = preview.replacement_draft,
        .replacement_residual = -preview.replacement_quality,
        .upper_edge_pyramid = upper_edge_pyramid,
        .lower_edge_pyramid = lower_edge_pyramid,
    };
  }

  /**
   * @brief Turns a preview to cell drafts.
   */
  [[nodiscard]] ChangeSet preview_to_changeset(const Preview &&preview,
                                               const Mesh &mesh) const {
    auto cell_drafts = std::vector<CellDraft<CellData>>{};
    if (preview.replacement_draft.klincsek_tree.empty())
      return {cell_drafts, preview.region};

    cell_drafts.reserve(2 * preview.replacement_draft.klincsek_tree.size());
    const auto mpi_rank = boost::mpi::communicator{}.rank();
    const auto apex_a = preview.replacement_draft.apex_a;
    const auto apex_b = preview.replacement_draft.apex_b;
    const auto &vertex_ring = preview.replacement_draft.vertex_ring;
    const auto boundary_face_pairs = collect_boundary_faces_around_edge_ring(
        mesh, preview.region, apex_a, apex_b);
    auto cell_id_counter = IdCounter(mesh.cell_id_counter, mpi_rank,
                                     boost::mpi::communicator{}.size());
    // Create cell together with lattice vectors and lattice volume.
    const auto add_cell_draft =
        [&](const auto apex, const auto i, const auto j, const auto k,
            const auto &pyramid) -> CellDraft<CellData> & {
      cell_drafts.emplace_back(create_cell_draft<CellData>(
          SimplexCellId{cell_id_counter.next()}, mpi_rank,
          std::array{apex, vertex_ring[i], vertex_ring[j], vertex_ring[k]},
          std::array{pyramid[i], pyramid[j], pyramid[k]}));
      return cell_drafts.back();
    };
    auto previous_cells =
        std::optional<std::tuple<Cell &, Cell &>>{std::nullopt};
    for (const auto &node : preview.replacement_draft.klincsek_tree) {
      const auto [i, j, k] = node.triangle;
      auto &cell_upper =
          add_cell_draft(apex_a, i, j, k, preview.upper_edge_pyramid);
      auto &cell_lower =
          add_cell_draft(apex_b, k, j, i, preview.lower_edge_pyramid);
      // Connect upper and lower:
      connect_cells(cell_upper.cell, 0, cell_lower.cell, 0);
      if (previous_cells.has_value()) {
        auto &[prev_upper, prev_lower] = previous_cells.value();
        // Connect cells to previously created cells:
        connect_cells(cell_upper.cell, 2, prev_upper, 1 + node.parent_apex);
        connect_cells(cell_lower.cell, 2, prev_lower,
                      1 + (2 - node.parent_apex));
      } else if (preview.region.size() ==
                 preview.replacement_draft.vertex_ring.size()) {
        // This is the first cell, which has to be connected to the boundary.
        // But only if the ring is not an open ring (this is the case when we
        // have less ring cells than ring vertices)
        connect_cell_pair_to_boundary(cell_upper, cell_lower,
                                      1, // edge 2-0
                                      boundary_face_pairs.back());
      }
      // Can not use `=` in the next line, as this would overwrite cells instead
      // of references:
      previous_cells.emplace(
          std::tuple<Cell &, Cell &>{cell_upper.cell, cell_lower.cell});
      // Connect cells to rest of mesh:
      for (const auto i : {std::size_t{0}, std::size_t{1}})
        // If 2 adjacent vertices of a triangle have difference 1 in the index,
        // this means that the corresponding edge is part of the boundary of the
        // ring.
        if (node.triangle[i] + 1 == node.triangle[i + 1])
          // edge 0-1 (i=0) has apex 2, edge 1-2 (i=1) has apex 0:
          connect_cell_pair_to_boundary(cell_upper, cell_lower,
                                        (i + 2) % 3, // triangle_apex
                                        boundary_face_pairs[node.triangle[i]]);
    }

    return {cell_drafts, preview.region};
  }
};

} // namespace qcmesh::mesh
