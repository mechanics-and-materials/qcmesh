// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include <boost/serialization/array.hpp>
#include <boost/serialization/vector.hpp>

namespace qcmesh::mesh::detail {

template <class Archive, int Rows, int Cols>
void serialize_eigen_matrix(Archive &ar, Eigen::Matrix<double, Rows, Cols> &obj,
                            const unsigned int) {
  for (auto val : obj.reshaped())
    ar &val;
}

} // namespace qcmesh::mesh::detail

namespace boost::serialization {

template <class Archive, class CellData>
void serialize(Archive &ar, qcmesh::mesh::CellDraft<CellData> &obj,
               const unsigned int version) {
  ar &obj.cell;
  ::qcmesh::mesh::detail::serialize_eigen_matrix(ar, obj.cell_basis, version);
  ar &obj.n_volume;
  ar &obj.boundary_connections;
}

template <class Archive>
void serialize(Archive &ar, qcmesh::mesh::BoundaryConnection &obj,
               const unsigned int) {
  ar &obj.neighbor_handle;
  ar &obj.neighbor_apex_idx;
}

} // namespace boost::serialization
