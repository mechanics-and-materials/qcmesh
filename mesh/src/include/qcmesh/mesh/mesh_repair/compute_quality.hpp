// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include "qcmesh/mesh/simplex_mesh.hpp"

namespace qcmesh::mesh {
/**
 * @brief Quality of a cell specified by 4 handles - corresponds to negative
 * residual.
 */
template <class NodalData, class CellData>
double compute_quality(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                       const std::array<NodeId, 4> &cell) {
  return -geometry::simplex_metric_residual(simplex_cell_points(mesh, cell));
};

} // namespace qcmesh::mesh
