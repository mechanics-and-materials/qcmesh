// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/mesh/face.hpp"
#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/mesh/mesh_repair/compute_quality.hpp"
#include "qcmesh/mesh/mesh_repair/connect_cells.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_preview.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/change_set.hpp"
#include "qcmesh/mesh/mesh_repair/topology/face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_face.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <Eigen/Core>
#include <boost/mpi.hpp>
#include <stdexcept>
#include <tuple>
#include <vector>

namespace qcmesh::mesh {

/**
 * @brief For given face and a segment defined by the apex vertex of the face
 * and apex_bottom determine wether the segment intersects the face.
 */
template <class NodalData, class CellData>
bool segment_intersects_face(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                             const Face<SimplexCellId, NodeId> &face,
                             const NodeId apex_bottom) {
  /**
   *  n = (v-u) x (w-u) = uv x uw
   *  <uw, n x uv> = <n, uv x uw> = |n|^2 >= 0
   *  ---
   *  p(r) = a + r(b-a)
   *  <n, p(r) - u> != 0 => r = -<n, ua> / <n, ab>
   *  ---
   *  s = <p-u, n x uv> >= 0
   *  t = <p-u, n x -uw> >= 0
   *    <p-v, n x vw> >= 0
   *    0 = uv + vw + wu = uv + vw - uw
   *    <p-v, n x vw> = <p-v, n x uw> - <p-v, n x uv>
   *                  = <p-u + u-v, n x uw> - <p-u + u-v, n x uv>
   *                  = -t -s - <uv, n x uw> + <uv, n x uv>
   *    =>
   *  t+s <= - <uv, n x uw> = |n|^2
   */

  const auto vertices = get_face_vertices(mesh, face);
  const auto u =
      deref_node_id(mesh, vertices[0]).position; // coordinates of vertex u
  const auto v =
      deref_node_id(mesh, vertices[1]).position; // coordinates of vertex v
  const auto w =
      deref_node_id(mesh, vertices[2]).position; // coordinates of vertex w
  const auto a =
      deref_node_id(mesh, face.apex).position; // coordinates of vertex a
  const auto b =
      deref_node_id(mesh, apex_bottom).position; // coordinates of vertex b
  const auto uv = array_ops::as_eigen(v) - array_ops::as_eigen(u);
  const auto uw = array_ops::as_eigen(w) - array_ops::as_eigen(u);
  const auto ab = array_ops::as_eigen(b) - array_ops::as_eigen(a);
  const auto ua = array_ops::as_eigen(a) - array_ops::as_eigen(u);
  const auto n = uv.cross(uw);
  const auto r = -n.dot(ua) / n.dot(ab);
  const auto up_perp = (ua + r * ab).cross(n);
  const auto s = up_perp.dot(uv);
  const auto t = -up_perp.dot(uw);

  return s >= 0. && t >= 0. && s + t <= n.dot(n);
}

/**
 * @brief For a given set of cells with shared faces sandwiched between apex_a
 * and apex_b, collect the lateral edges of the top polygonal pyramid (all edges
 * from apex_a to the ring vertices) and return together with the edge apex_a -
 * apex_b. The edges are with respect to apex_a in euclidean coordinates and
 * lattice coordinates.
 */
template <class NodalData, class CellData>
std::tuple<EdgeWithLatticeCoordinates, std::vector<EdgeWithLatticeCoordinates>>
collect_pyramid_with_central_edge(
    const std::vector<SimplexCellId> &cells_to_replace,
    const std::vector<NodeId> &vertex_ring, const NodeId apex_a,
    const NodeId apex_b, const SimplexMesh<3, 3, NodalData, CellData> &mesh) {
  if constexpr (traits::IS_IMPLEMENTED<
                    traits::LatticeBasis<SimplexCell<3, CellData>>>) {
    const auto a = array_ops::as_eigen(deref_node_id(mesh, apex_a).position);
    const auto b = array_ops::as_eigen(deref_node_id(mesh, apex_b).position);
    auto out = std::vector<EdgeWithLatticeCoordinates>(vertex_ring.size());
    auto value_is_computed = std::vector<bool>(vertex_ring.size());
    auto n_computed = std::size_t{0};
    auto ab = EdgeWithLatticeCoordinates{b - a, {}};
    auto have_ab = false;
    // Assume, even cells are upper cells:
    for (std::size_t i = 0;
         i < cells_to_replace.size() && n_computed < out.size(); i += 2) {
      const auto &cell = deref_cell_id(mesh, cells_to_replace[i]);
      const auto basis =
          array_ops::as_eigen_matrix(traits::lattice_basis(cell));
      const auto lu = basis.lu();
      for (std::size_t i_vx = 0; i_vx < out.size() && n_computed < out.size();
           i_vx++) {
        if (value_is_computed[i_vx])
          continue;
        const auto vertex = vertex_ring[i_vx];
        if (!cell_contains_vertex(cell.nodes, vertex))
          continue;
        const auto r =
            array_ops::as_eigen(deref_node_id(mesh, vertex).position);
        const auto da = Eigen::Vector3d{r - a};
        const auto lattice_vertex = EdgeWithLatticeCoordinates{
            da, lu.solve(da).array().round().matrix()};
        if (!have_ab) {
          have_ab = true;
          const auto basis_lower =
              array_ops::as_eigen_matrix(traits::lattice_basis(
                  deref_cell_id(mesh, cells_to_replace[i + 1])));
          // (b - r) + (r - a) = b - a
          ab.lattice = basis_lower.lu().solve(b - r).array().round().matrix() +
                       lattice_vertex.lattice;
        }
        out[i_vx] = lattice_vertex;
        value_is_computed[i_vx] = true;
        n_computed++;
      }
    }
    return std::make_tuple(ab, out);
  } else {
    const auto a = array_ops::as_eigen(deref_node_id(mesh, apex_a).position);
    const auto b = array_ops::as_eigen(deref_node_id(mesh, apex_b).position);
    auto out = std::vector<EdgeWithLatticeCoordinates>{};
    out.reserve(vertex_ring.size());
    for (const auto &vertex : vertex_ring) {
      const auto r = array_ops::as_eigen(deref_node_id(mesh, vertex).position);
      out.emplace_back(
          EdgeWithLatticeCoordinates{Eigen::Vector3d{r - a}, {0., 0., 0.}});
    }
    return std::make_tuple(EdgeWithLatticeCoordinates{b - a, {0., 0., 0.}},
                           out);
  }
}

namespace detail {

/**
 * Exception thrown when some mesh corruption occurs
 */
struct MeshCorruptionError : std::runtime_error {
  using std::runtime_error::runtime_error;
};

template <std::size_t N>
std::size_t
other_vertex_index(const std::array<NodeId, N> &vertices,
                   const std::array<NodeId, N> &complement_vertices) {
  const auto contains = [](const auto &container, const auto &element) {
    for (const auto &candidate : container)
      if (element == candidate)
        return true;
    return false;
  };
  // Search for the apex vertex:
  for (std::size_t i = 0; i < vertices.size(); i++) {
    if (contains(complement_vertices, vertices[i]))
      continue;
    return i;
  }
  throw MeshCorruptionError{"other_vertex_index: Vertices of cell and "
                            "complement_vertices are identical!"};
}

template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, std::size_t>>
face_mirror_index(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                  const SimplexCell<3, CellData> &cell,
                  const std::size_t apex_index) {
  const auto &vertices = cell.nodes;
  const auto neighbor = cell.neighbors[apex_index];
  if (!neighbor.has_value())
    return std::nullopt;
  const auto neighbor_cell = deref_cell_id(mesh, neighbor.value());
  const auto neighbor_apex_index =
      other_vertex_index(neighbor_cell.nodes, vertices);
  return Face<SimplexCellId, std::size_t>{neighbor.value(),
                                          neighbor_apex_index};
}

template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, std::size_t>>
face_mirror_index(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                  const Face<SimplexCellId, NodeId> &face) {
  const auto cell = deref_cell_id(mesh, face.cell);
  const auto &vertices = cell.nodes;
  for (std::size_t i = 0; i < vertices.size(); i++)
    if (face.apex == vertices[i])
      return face_mirror_index(mesh, cell, i);
  return std::nullopt;
}

template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, std::size_t>>
face_mirror_index(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                  const Face<SimplexCellId, std::size_t> &face) {
  const auto cell = deref_cell_id(mesh, face.cell);
  return face_mirror_index(mesh, cell, face.apex);
}

} // namespace detail

/**
 * @brief For given face within the a mesh,
 * return the other side of the mesh
 * (the same face but represented by a face of the neighbor cell) if it exists.
 */
template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, NodeId>>
face_mirror(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
            const Face<SimplexCellId, NodeId> &face) {
  if (const auto mirror_index = detail::face_mirror_index(mesh, face)) {
    const auto neighbor_cell = deref_cell_id(mesh, mirror_index->cell);
    return Face<SimplexCellId, NodeId>{mirror_index->cell,
                                       neighbor_cell.nodes[mirror_index->apex]};
  }
  return std::nullopt;
}

namespace detail {
/**
 * @brief Return corresponding face if a cell contains the 3 specified vertices,
 * std::nullopt otherwise.
 */
template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, std::size_t>>
cell_face_by_vertices(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                      const SimplexCellId cell, const NodeId a, const NodeId b,
                      const NodeId c) {
  const auto &vertices = deref_cell_id(mesh, cell).nodes;
  auto apex = std::size_t{};
  auto n_matches = std::size_t{0};
  for (std::size_t i = 0; i < vertices.size(); i++) {
    if (vertices[i] == a || vertices[i] == b || vertices[i] == c)
      n_matches++;
    else
      apex = i;
  }
  if (n_matches == 3)
    return Face<SimplexCellId, std::size_t>{cell, apex};
  return std::nullopt;
}
} // namespace detail

/**
 * @brief Same as face_mirror, but face is specified by 3 vertices and
 * returns the position of the apex vertex in the
 * array of vertices of the cell containing the face and apex.
 */
template <class NodalData, class CellData>
std::optional<Face<SimplexCellId, std::size_t>>
face_mirror_by_vertices(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                        const SimplexCellId cell, const NodeId a,
                        const NodeId b, const NodeId c) {
  if (auto face = detail::cell_face_by_vertices(mesh, cell, a, b, c))
    return detail::face_mirror_index(mesh, *face);
  return std::nullopt;
}

/**
 * @brief For given face within the a mesh,
 * return the vertex handles of the face.
 * If the original vertices are v1,v2,v3,v4, the returned face vertices u, v, w
 * and the apex a, then <(u-a) x (v-a), w-a> should have the same sign as
 * <(v2-v1) x (v3-v1), v4-v1>.
 */
template <class NodalData, class CellData>
std::array<NodeId, 3>
get_face_vertices(const SimplexMesh<3, 3, NodalData, CellData> &mesh,
                  const Face<SimplexCellId, NodeId> &face) {
  const auto [cell_handle, face_apex] = face;
  return face_vertex_ids(deref_cell_id(mesh, cell_handle).nodes, face_apex);
}

/**
 * @brief For given face and a segment defined by the apex vertex of the face
 * and apex_bottom determine whether a 2-3 flip would create ill-orientated
 * simplices.
 *
 * This is equivalent to the face having 2 "reflect edges".
 */
template <class NodalData, class CellData>
bool sandwiched_face_is_flippable(
    const SimplexMesh<3, 3, NodalData, CellData> &mesh,
    const Face<SimplexCellId, NodeId> &face, const NodeId apex_bottom) {
  const auto vertices = get_face_vertices(mesh, face);
  const auto u =
      deref_node_id(mesh, vertices[0]).position; // coordinates of vertex u
  const auto v =
      deref_node_id(mesh, vertices[1]).position; // coordinates of vertex v
  const auto w =
      deref_node_id(mesh, vertices[2]).position; // coordinates of vertex w
  const auto a =
      deref_node_id(mesh, face.apex).position; // coordinates of vertex a
  const auto b =
      deref_node_id(mesh, apex_bottom).position; // coordinates of vertex b
  const auto original_orientation = geometry::simplex_volume({{a, u, v, w}});
  auto juv = geometry::simplex_volume({{a, b, u, v}}); // orient3d(a,b,u,v);
  auto jvw = geometry::simplex_volume({{a, b, v, w}}); // orient3d(a,b,v,w);
  auto jwu = geometry::simplex_volume({{a, b, w, u}}); // orient3d(a,b,w,u);
  if (original_orientation < 0.) {
    juv = -juv;
    jvw = -jvw;
    jwu = -jwu;
  }
  return (((juv > 0) && (jvw > 0)) || ((jvw > 0) && (jwu > 0)) ||
          ((jwu > 0) && (juv > 0)));
}

/**
 * @brief A Modifier class for face removal. To be used with
 * optimize_*_mesh
 *
 * It wraps remove_face
 */
template <class NodalData, class CellData> class FaceEraser {
  constexpr static std::size_t D = 3; // only works in 3D
  using Mesh = SimplexMesh<3, 3, NodalData, CellData>;

public:
  using VertexHandle = NodeId;
  using FaceHandle = Face<SimplexCellId, VertexHandle>;
  using PrimitiveHandle = FaceHandle;
  using ChangeSet = qcmesh::mesh::ChangeSet<CellDraft<CellData>, SimplexCellId>;

  /**
   * @brief Returns an iterable of PrimitiveHandles (faces) belonging to
   * cell_handle
   */
  [[nodiscard]] std::vector<PrimitiveHandle>
  primitives_for_element(const Mesh &mesh,
                         const SimplexCellId cell_handle) const {
    const auto &vertices = deref_cell_id(mesh, cell_handle).nodes;
    return std::vector<PrimitiveHandle>{FaceHandle{cell_handle, vertices[0]},
                                        FaceHandle{cell_handle, vertices[1]},
                                        FaceHandle{cell_handle, vertices[2]},
                                        FaceHandle{cell_handle, vertices[3]}};
  }

  /**
   * @brief Computes a preview of the operation.
   */
  [[nodiscard]] ChangePreview<SimplexCellId, FaceRemovalDraft<VertexHandle>>
  preview(const Mesh &mesh, const PrimitiveHandle face) const {
    const auto get_vertices = [&](const FaceHandle &face) {
      return get_face_vertices(mesh, face);
    };
    const auto quality_measure = [&](const Tetrahedron<VertexHandle> &cell) {
      return compute_quality(mesh, cell);
    };
    const auto is_flippable =
        [&](const SharedFace<SimplexCellId, VertexHandle> &shared_f) {
          return sandwiched_face_is_flippable(mesh, shared_f.face,
                                              shared_f.bottom);
        };
    const auto intersects_face =
        [&](const SharedFace<SimplexCellId, VertexHandle> &shared_f) {
          return segment_intersects_face(mesh, shared_f.face, shared_f.bottom);
        };

    const auto mirrored_face = [&](const FaceHandle &face) {
      return face_mirror(mesh, face);
    };

    // check if is_flippable should be in those two inputs
    const auto face_removal_change_set =
        remove_face<SimplexCellId, VertexHandle>(face, quality_measure,
                                                 is_flippable, intersects_face,
                                                 get_vertices, mirrored_face);

    return ChangePreview<SimplexCellId, FaceRemovalDraft<VertexHandle>>{
        .region = face_removal_change_set.out,
        .replacement_draft = face_removal_change_set.replacement_draft,
        .replacement_residual = -face_removal_change_set.new_quality,
    };
  }

  using Preview = ChangePreview<SimplexCellId, FaceRemovalDraft<VertexHandle>>;
  /**
   * @brief Turns a preview to cell drafts.
   */
  [[nodiscard]] ChangeSet preview_to_changeset(const Preview &&preview,
                                               const Mesh &mesh) const {
    if (preview.region.empty() && preview.replacement_draft.vertex_ring.empty())
      return {{}, {}};
    auto cell_drafts = std::vector<CellDraft<CellData>>{};
    cell_drafts.reserve(preview.replacement_draft.vertex_ring.size());
    const auto mpi_rank = boost::mpi::communicator{}.rank();
    const auto apex_a = preview.replacement_draft.apex_a;
    const auto apex_b = preview.replacement_draft.apex_b;
    const auto [central_edge, pyramid] = collect_pyramid_with_central_edge(
        preview.region, preview.replacement_draft.vertex_ring, apex_a, apex_b,
        mesh);

    auto i_prev = preview.replacement_draft.vertex_ring.size() - 1;
    auto cell_id_counter = IdCounter(mesh.cell_id_counter, mpi_rank,
                                     boost::mpi::communicator{}.size());
    for (std::size_t i = 0; i < preview.replacement_draft.vertex_ring.size();
         i++) {
      const auto &ring_vertex = preview.replacement_draft.vertex_ring[i];
      auto cell_draft = create_cell_draft<CellData>(
          SimplexCellId{cell_id_counter.next()}, mpi_rank,
          std::array{apex_a, apex_b,
                     preview.replacement_draft.vertex_ring[i_prev],
                     ring_vertex},
          std::array{central_edge, pyramid[i_prev], pyramid[i]});
      if (!cell_drafts.empty())
        connect_cells(cell_drafts.back().cell, 2, cell_draft.cell, 3);
      cell_drafts.emplace_back(cell_draft);
      i_prev = i;
    }
    connect_cells(cell_drafts.back().cell, 2, cell_drafts.front().cell, 3);
    // Connect new cells to rest of mesh:
    // For a ring of cells where each cell shares its vertices at position 0 and
    // 1, we only have to consider the following 2 faces:
    constexpr std::array FACES_WITH_APEX = std::array{
        std::tuple<std::array<std::size_t, 3>, std::size_t>{{0, 2, 3}, 1},
        std::tuple<std::array<std::size_t, 3>, std::size_t>{{1, 2, 3}, 0}};
    for (auto &cell_draft : cell_drafts) {
      const auto &vertices = cell_draft.cell.nodes;
      for (const auto &out_cell : preview.region)
        for (const auto &[face, apex] : FACES_WITH_APEX) {
          if (auto mirror_face = face_mirror_by_vertices(
                  mesh, out_cell, vertices[face[0]], vertices[face[1]],
                  vertices[face[2]])) {
            connect_cells(cell_draft, apex,
                          deref_cell_id(mesh, mirror_face->cell),
                          mirror_face->apex);
            break;
          }
        }
    }
    return {cell_drafts, preview.region};
  }
};

} // namespace qcmesh::mesh
