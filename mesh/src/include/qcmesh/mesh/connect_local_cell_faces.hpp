// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "qcmesh/geometry/triple_hash.hpp"
#include "qcmesh/mesh/simplex_cell.hpp"
#include <algorithm>
#include <unordered_map>
#include <vector>

namespace qcmesh::mesh {
namespace detail {

/**
 * @brief This is needed so that @ref connect_local_cell_faces can be passed
 * a `std::unordered_map<SimplexCellId, SimplexCell<...>>` as the cells
 * argument.
 */
template <class CellData>
const std::array<NodeId, 4> &
extract_nodes(const SimplexCell<3, CellData> &cell) {
  return cell.nodes;
}

/**
 * @brief This is needed so that @ref connect_local_cell_faces can be passed
 * a `std::unordered_map<SimplexCellId, std::array<NodeId, 4>>` as the cells
 * argument.
 */
inline const std::array<NodeId, 4> &
extract_nodes(const std::array<NodeId, 4> &nodes) {
  return nodes;
}

/**
 * @brief Return an array containing all faces with corresponding apex of a
 * given cell.
 */
inline std::array<std::tuple<std::array<NodeId, 3>, std::size_t>, 4>
faces_with_apex(const std::array<NodeId, 4> &cell) {
  const auto ordered_face = [](const std::array<NodeId, 4> &cell,
                               const std::size_t apex) {
    auto face = std::array<NodeId, 3>{};
    auto counter = std::size_t{0};
    for (std::size_t i = 0; i < cell.size(); i++)
      if (i != apex)
        face[counter++] = cell[i];
    std::sort(face.begin(), face.end());
    return face;
  };
  return std::array<std::tuple<std::array<NodeId, 3>, std::size_t>, 4>{
      std::make_tuple(ordered_face(cell, 3), 3),
      std::make_tuple(ordered_face(cell, 2), 2),
      std::make_tuple(ordered_face(cell, 1), 1),
      std::make_tuple(ordered_face(cell, 0), 0)};
}

/**
 * @brief Return the apex opposite to the given face if cell contains the face,
 * std::nullopt otherwise.
 */
inline std::optional<std::size_t> face_apex(const std::array<NodeId, 4> &cell,
                                            const std::array<NodeId, 3> &face) {

  for (const auto &[face_candidate, apex] : faces_with_apex(cell))
    if (face_candidate == face)
      return std::optional{apex};
  return std::optional<std::size_t>{};
}

/**
 * @brief Used in this file below as well as
 * ./mesh_repair/testing/collect_face_connections.h
 */
template <class MapType, class ConnectCells>
std::unordered_map<std::size_t, std::vector<SimplexCellId>>
connect_local_cell_faces(MapType cells, const ConnectCells connect_cells) {
  auto cell_by_face_hash =
      std::unordered_map<std::size_t, std::vector<SimplexCellId>>{};
  for (const auto &[cell_id, cell] : cells) {
    for (const auto &[face, apex] :
         faces_with_apex(detail::extract_nodes(cell))) {
      const auto hash = geometry::TripleHash{}(face);
      const auto [item, hash_not_taken] =
          cell_by_face_hash.emplace(hash, std::vector<SimplexCellId>{});
      auto &face_neighbor_candidates = item->second;
      for (const auto other_cell_id : face_neighbor_candidates) {
        const auto other_cell = cells.at(other_cell_id);
        const auto other_apex =
            face_apex(detail::extract_nodes(other_cell), face);
        if (other_apex.has_value()) {
          connect_cells(cell_id, apex, other_cell_id, other_apex.value());
          break;
        }
      }
      face_neighbor_candidates.push_back(cell_id);
    }
  }
  return cell_by_face_hash;
}

} // namespace detail

/**
 * @brief Populates the @ref SimplexCell::neighbors field of all local cells
 * based on their extrinsic connections (shared nodes).
 * Returns the map of face hashes to hash bucket of cell ids.
 */
template <std::size_t K, class CellData>
std::unordered_map<std::size_t, std::vector<SimplexCellId>>
connect_local_cell_faces(
    std::unordered_map<SimplexCellId, SimplexCell<K, CellData>> &cells) {
  const auto connect_cells =
      [&](const SimplexCellId cell_a, const std::size_t apex_a,
          const SimplexCellId cell_b, const std::size_t apex_b) {
        cells.at(cell_a).neighbors[apex_a] = cell_b;
        cells.at(cell_b).neighbors[apex_b] = cell_a;
      };
  return detail::connect_local_cell_faces<
      std::unordered_map<SimplexCellId, SimplexCell<K, CellData>> &>(
      cells, connect_cells);
}
} // namespace qcmesh::mesh
