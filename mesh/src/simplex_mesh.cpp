// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_mesh.hpp"

namespace qcmesh::mesh {

std::size_t increment_counter(const std::size_t current_value,
                              const std::size_t process_rank,
                              const std::size_t n_processes) {
  const auto delta = (current_value - process_rank) % n_processes;
  return current_value + n_processes - delta;
}

IdCounter::IdCounter(std::size_t current_value, std::size_t process_rank,
                     std::size_t n_processes)
    : current_value{current_value}, process_rank{process_rank},
      n_processes{n_processes} {}
std::size_t IdCounter::next() {
  const auto current = this->current_value;
  this->current_value =
      increment_counter(current, this->process_rank, this->n_processes);
  return current;
}

} // namespace qcmesh::mesh
