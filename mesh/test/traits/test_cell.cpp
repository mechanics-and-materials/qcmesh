// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/testing/lattice_cell_data.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_cell, lattice_basis_can_read_lattice_cell_data) {
  const auto expected_basis = std::array{
      std::array{1., 0., 0.}, std::array{0., 1., 0.}, std::array{0., 0., 1.}};
  const auto cell =
      SimplexCell<3, testing::LatticeCellData>{.data = {expected_basis}};
  const auto basis = traits::lattice_basis(cell);
  EXPECT_THAT(basis, qcmesh::testing::DoubleMatrixEq(expected_basis));
}

TEST(test_cell, lattice_basis_can_write_lattice_cell_data) {
  const auto expected_basis = std::array{
      std::array{1., 0., 0.}, std::array{0., 1., 0.}, std::array{0., 0., 1.}};
  auto cell = SimplexCell<3, testing::LatticeCellData>{};
  traits::lattice_basis(cell) = expected_basis;
  EXPECT_THAT(cell.data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(expected_basis));
}

struct TestCellData {
  double volume{};
  double quality{};
  double jacobian{};
  bool is_atomistic{};
};

} // namespace

// Implement Volume
template <> struct traits::Volume<TestCellData> {
  static double &volume(TestCellData &data) { return data.volume; }
  static double volume(const TestCellData &data) { return data.volume; }
};
// Implement Quality
template <> struct traits::Quality<TestCellData> {
  static double &quality(TestCellData &data) { return data.quality; }
  static double quality(const TestCellData &data) { return data.quality; }
};
// Implement Jacobian
template <> struct traits::Jacobian<TestCellData> {
  static double &jacobian(TestCellData &data) { return data.jacobian; }
  static double jacobian(const TestCellData &data) { return data.jacobian; }
};
// Implement IsAtomistic
template <> struct traits::IsAtomistic<TestCellData> {
  static bool &is_atomistic(TestCellData &data) { return data.is_atomistic; }
  static bool is_atomistic(const TestCellData &data) {
    return data.is_atomistic;
  }
};

namespace {

constexpr TestCellData TEST_CELL_DATA{1., 2., 3., true};

TEST(test_cell, volume_can_read_test_cell_data) {
  const auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  EXPECT_DOUBLE_EQ(traits::volume(cell), cell.data.volume);
}
TEST(test_cell, volume_can_write_to_test_cell_data) {
  auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  traits::volume(cell) = -1.;
  EXPECT_DOUBLE_EQ(cell.data.volume, -1.);
}
TEST(test_cell, quality_can_read_test_cell_data) {
  const auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  EXPECT_DOUBLE_EQ(traits::quality(cell), cell.data.quality);
}
TEST(test_cell, quality_can_write_to_test_cell_data) {
  auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  traits::quality(cell) = -1.;
  EXPECT_DOUBLE_EQ(cell.data.quality, -1.);
}
TEST(test_cell, jacobian_can_read_test_cell_data) {
  const auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  EXPECT_DOUBLE_EQ(traits::jacobian(cell), cell.data.jacobian);
}
TEST(test_cell, jacobian_can_write_to_test_cell_data) {
  auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  traits::jacobian(cell) = -1.;
  EXPECT_DOUBLE_EQ(cell.data.jacobian, -1.);
}
TEST(test_cell, is_atomistic_can_read_test_cell_data) {
  const auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  EXPECT_EQ(traits::is_atomistic(cell), cell.data.is_atomistic);
}
TEST(test_cell, is_atomistic_can_write_to_test_cell_data) {
  auto cell = SimplexCell<3, TestCellData>{.data = TEST_CELL_DATA};
  traits::is_atomistic(cell) = false;
  EXPECT_EQ(cell.data.is_atomistic, false);
}

} // namespace
} // namespace qcmesh::mesh
