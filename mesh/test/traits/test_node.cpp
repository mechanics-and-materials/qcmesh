// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/testing/lattice_vertex.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

constexpr testing::LatticeVertex LATTICE_VERTEX{std::array{1., 2., 3.},
                                                std::array{-1, -2, -3}};

TEST(test_node, position_can_read_from_lattice_vertex) {
  const auto expected_position = std::array{1., 2., 3.};
  const auto position = traits::position(LATTICE_VERTEX);
  EXPECT_THAT(position, qcmesh::testing::DoubleArrayEq(expected_position));
}

TEST(test_node, lattice_coordinate_can_read_from_lattice_vertex) {
  const auto expected_coordinates = std::array{-1, -2, -3};
  const auto coordinates = traits::lattice_coordinate(LATTICE_VERTEX);
  EXPECT_THAT(coordinates, ::testing::ContainerEq(expected_coordinates));
}

} // namespace
} // namespace qcmesh::mesh
