// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_undeformed_lattice_basis,
     undeformed_lattice_basis_works_for_bare_mesh) {
  const auto mesh = testing::LatticeMesh<EmptyData>{};
  const auto basis =
      traits::undeformed_lattice_basis(mesh, SimplexCell<3, EmptyData>{});
  const auto expected_basis = std::array{
      std::array{1., 0., 0.}, std::array{0., 1., 0.}, std::array{0., 0., 1.}};
  EXPECT_THAT(basis, qcmesh::testing::DoubleMatrixEq(expected_basis));
}

} // namespace
} // namespace qcmesh::mesh
