// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/traits/transfer_data.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

struct X {
  int x{};
};
struct Y {
  int y{};
};

} // namespace

// Implement TransferData for X -> Y:
template <> struct traits::TransferData<X, Y> {
  static void transfer_data(const X &from, Y &to) { to.y = from.x; }
};

namespace {

TEST(test_transfer_data, transfer_data_works) {
  const auto x = X{42};
  auto y = Y{};
  traits::transfer_data(x, y);
  EXPECT_EQ(y.y, 42);
}

TEST(test_transfer_data, convert_works) {
  const auto x = X{42};
  auto y = traits::convert<Y>(x);
  EXPECT_EQ(y.y, 42);
}

} // namespace
} // namespace qcmesh::mesh
