// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/traits/is_implemented.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

template <class T> struct TestTrait;

struct X {};
struct Y {};

// Implement TestTrait for X:
template <> struct TestTrait<X> {};

TEST(test_is_implemented, is_implemented_detects_implemented_trait) {
  EXPECT_TRUE(traits::IS_IMPLEMENTED<TestTrait<X>>);
  EXPECT_FALSE(traits::IS_IMPLEMENTED<TestTrait<Y>>);
}

} // namespace
} // namespace qcmesh::mesh
