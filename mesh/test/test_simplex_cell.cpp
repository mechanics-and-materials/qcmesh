// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_cell.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

struct TestSimplexCell : testing::Test {
  SimplexCell<3, EmptyData> cell = SimplexCell<3, EmptyData>{
      .id = 51_cell,
      .process_rank = 3,
      .nodes = std::array{2_vx, 3_vx, 5_vx, 7_vx},
      .neighbors = std::array<std::optional<SimplexCellId>, 4>{
          10_cell, std::nullopt, std::nullopt, 11_cell}};
};

TEST_F(TestSimplexCell, index_of_node_returns_correct_index) {
  const auto result = index_of_node(cell, 5_vx);
  EXPECT_EQ(result, 2);
}
TEST_F(TestSimplexCell,
       index_of_node_returns_nullopt_if_node_does_not_contain_id) {
  const auto result = index_of_node(cell, 1_vx);
  EXPECT_EQ(result, std::nullopt);
}

TEST_F(TestSimplexCell,
       simplex_cell_has_node_returns_true_if_cell_contains_node) {
  const auto result = simplex_cell_has_node(cell, 5_vx);
  EXPECT_EQ(result, true);
}
TEST_F(TestSimplexCell,
       simplex_cell_has_node_returns_false_if_cell_does_not_contain_node) {
  const auto result = simplex_cell_has_node(cell, 1_vx);
  EXPECT_EQ(result, false);
}

TEST_F(TestSimplexCell, index_of_neighbor_returns_correct_index) {
  const auto result = index_of_neighbor(cell, 11_cell);
  EXPECT_EQ(result, 3);
}
TEST_F(TestSimplexCell,
       index_of_neighbor_returns_nullopt_if_node_does_not_contain_id) {
  const auto result = index_of_node(cell, 1_vx);
  EXPECT_EQ(result, std::nullopt);
}

TEST_F(TestSimplexCell, flip_orientation_works) {
  auto new_cell = cell;
  flip_orientation(new_cell);
  const auto expected_nodes = std::array{3_vx, 2_vx, 5_vx, 7_vx};
  EXPECT_EQ(new_cell.nodes, expected_nodes);
  const auto expected_neighbors = std::array<std::optional<SimplexCellId>, 4>{
      std::nullopt, 10_cell, std::nullopt, 11_cell};
  EXPECT_EQ(new_cell.neighbors, expected_neighbors);
}

TEST_F(TestSimplexCell, owner_rank_works) { EXPECT_EQ(cell.process_rank, 3); }

} // namespace
} // namespace qcmesh::mesh
