// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/face.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_face,
     cell_contains_vertex_returns_false_if_cell_does_not_contain_vertex) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  const auto vertex = 4_vx;

  const auto result = cell_contains_vertex(cell, vertex);
  EXPECT_EQ(result, false);
}
TEST(test_face, cell_contains_vertex_returns_true_if_cell_contains_vertex) {
  const auto cell = std::array{2_vx, 0_vx, 1_vx, 3_vx};
  const auto vertex = 1_vx;

  const auto result = cell_contains_vertex(cell, vertex);
  EXPECT_EQ(result, true);
}

TEST(test_face,
     cell_contains_face_returns_false_if_cell_does_not_contain_face) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  const auto face = std::array{0_vx, 1_vx, 4_vx};

  const auto result = cell_contains_face(cell, face);
  EXPECT_EQ(result, false);
}
TEST(test_face, cell_contains_face_returns_true_if_cell_contains_face) {
  const auto cell = std::array{2_vx, 0_vx, 1_vx, 3_vx};
  const auto face = std::array{0_vx, 1_vx, 3_vx};

  const auto result = cell_contains_face(cell, face);
  EXPECT_EQ(result, true);
}

TEST(test_face, face_apex_index_returns_nullopt_if_cell_does_not_contain_face) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  const auto face = std::array{0_vx, 1_vx, 4_vx};

  const auto index = face_apex_index(cell, face);
  EXPECT_EQ(index, std::nullopt);
}
TEST(test_face, face_apex_index_returns_correct_index_if_cell_contains_face) {
  const auto cell = std::array{3_vx, 2_vx, 0_vx, 1_vx};
  const auto face = std::array{0_vx, 1_vx, 3_vx};

  const auto index = face_apex_index(cell, face);
  EXPECT_EQ(index, 1);
}

TEST(test_face, face_vertex_ids_returns_correct_face) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  for (const auto &[apex_id, expected_face] :
       std::array{std::make_tuple(0_vx, std::array{1_vx, 2_vx, 3_vx}),
                  std::make_tuple(1_vx, std::array{2_vx, 0_vx, 3_vx}),
                  std::make_tuple(2_vx, std::array{0_vx, 1_vx, 3_vx}),
                  std::make_tuple(3_vx, std::array{1_vx, 0_vx, 2_vx})}) {
    const auto face = face_vertex_ids(cell, apex_id);
    EXPECT_EQ(face, expected_face);
  }
}
TEST(test_face,
     face_vertex_ids_raises_exception_if_cell_does_not_contain_apex) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  const auto apex_id = 4_vx;

  EXPECT_THROW((face_vertex_ids(cell, apex_id)), FaceException);
}

TEST(test_face, face_vertex_ids_by_idx_returns_correct_face) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  for (const auto &[apex_index, expected_face] :
       std::array{std::make_tuple(0, std::array{1_vx, 2_vx, 3_vx}),
                  std::make_tuple(1, std::array{2_vx, 0_vx, 3_vx}),
                  std::make_tuple(2, std::array{0_vx, 1_vx, 3_vx}),
                  std::make_tuple(3, std::array{1_vx, 0_vx, 2_vx})}) {
    const auto face = face_vertex_ids_by_idx(cell, apex_index);
    EXPECT_EQ(face, expected_face);
  }
}
TEST(test_face,
     face_vertex_ids_by_idx_raises_exception_if_index_is_out_of_bounds) {
  const auto cell = std::array{0_vx, 1_vx, 2_vx, 3_vx};

  EXPECT_THROW((face_vertex_ids_by_idx(cell, 4)), FaceException);
}

} // namespace
} // namespace qcmesh::mesh
