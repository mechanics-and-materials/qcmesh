// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_cell_data.hpp"
#include "qcmesh/mesh/testing/lattice_vertex.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

using LatticeCellData = testing::LatticeCellData;

struct SimplexMeshTest : ::testing::Test {
  using SimpleNode = Node<3, EmptyData>;
  std::array<double, 3> o = std::array{0., 0., 0.};
  std::array<double, 3> e_x = std::array{1., 0., 0.};
  std::array<double, 3> e_y = std::array{0., 1., 0.};
  std::array<double, 3> e_z = std::array{0., 0., 1.};
  std::array<double, 3> d = std::array{1., 1., 1.};
  std::array<double, 3> p = std::array{-1., 0., 0.};
  std::array<NodeId, 4> cell_a = std::array{0_vx, 1_vx, 2_vx, 3_vx};
  std::array<NodeId, 4> cell_b = std::array{2_vx, 1_vx, 3_vx, 4_vx};
  std::array<NodeId, 4> cell_c = std::array{0_vx, 1_vx, 2_vx, 4_vx};
  std::array<NodeId, 4> cell_d = std::array{0_vx, 2_vx, 3_vx, 5_vx};

  SimplexMesh<3, 3, EmptyData, EmptyData> mesh =
      SimplexMesh<3, 3, EmptyData, EmptyData>{
          .nodes =
              {
                  {0_vx, SimpleNode{.id = 0_vx, .position = o}},
                  {1_vx, SimpleNode{.id = 1_vx, .position = e_x}},
                  {2_vx, SimpleNode{.id = 2_vx, .position = e_y}},
                  {5_vx, SimpleNode{.id = 5_vx, .position = p}},
              },
          .cells = {{0_cell,
                     SimplexCell<3, EmptyData>{
                         .id = 0_cell,
                         .process_rank = 0,
                         .nodes = cell_a,
                         .neighbors =
                             std::array<std::optional<SimplexCellId>, 4>{
                                 1_cell, 2_cell, std::nullopt, std::nullopt}}},
                    {2_cell,
                     SimplexCell<3, EmptyData>{
                         .id = 2_cell,
                         .process_rank = 0,
                         .nodes = cell_d,
                         .neighbors =
                             std::array<std::optional<SimplexCellId>, 4>{
                                 std::nullopt, std::nullopt, std::nullopt,
                                 0_cell}}}},
          .halo_nodes =
              {{3_vx,
                SimpleNode{.id = 3_vx, .process_rank = 1, .position = e_z}},
               {4_vx,
                SimpleNode{.id = 4_vx, .process_rank = 1, .position = d}}},
          .halo_cells = {{1_cell,
                          SimplexCell<3, EmptyData>{
                              .id = 1_cell,
                              .process_rank = 0,
                              .nodes = cell_b,
                              .neighbors =
                                  std::array<std::optional<SimplexCellId>, 4>{
                                      std::nullopt, std::nullopt, std::nullopt,
                                      0_cell}}}},
      };
};

struct NodeMatcher {
  int rank{};
  NodeId id{};
  std::array<double, 3> position{};
  int type{};
};

bool operator==(const Node<3, EmptyData> &node, const NodeMatcher &matcher) {
  return node.id == matcher.id && node.process_rank == matcher.rank &&
         node.position == matcher.position;
}
bool operator==(const std::pair<NodeId, Node<3, EmptyData>> &node_item,
                const NodeMatcher &matcher) {
  const auto &[id, node] = node_item;
  return node == matcher && id == matcher.id;
}

struct CellMatcher {
  int rank{};
  SimplexCellId id{};
  std::array<NodeId, 4> nodes{};
  std::array<std::optional<SimplexCellId>, 4> neighbors{};
};

template <class CellData>
bool operator==(const SimplexCell<3, CellData> &cell,
                const CellMatcher &matcher) {
  return cell.id == matcher.id && cell.process_rank == matcher.rank &&
         cell.nodes == matcher.nodes && cell.neighbors == matcher.neighbors;
}
template <class CellData>
bool eq(const std::pair<SimplexCellId, SimplexCell<3, CellData>> &cell_item,
        const CellMatcher &matcher) {
  const auto &[id, cell] = cell_item;
  return cell == matcher && id == matcher.id;
}
inline bool
operator==(const std::pair<SimplexCellId, SimplexCell<3, EmptyData>> cell_item,
           const CellMatcher matcher) {
  return eq(cell_item, matcher);
}

TEST_F(SimplexMeshTest, create_local_unconnected_simplex_mesh_works) {
  const auto nodal_data = std::vector{
      PrimitiveNode<3>{.position = o},
      PrimitiveNode<3>{.position = e_x},
      PrimitiveNode<3>{.position = e_y},
      PrimitiveNode<3>{.position = e_z},
  };
  const auto cells = std::vector{std::array<std::size_t, 4>{0, 1, 2, 3}};
  const auto mesh = create_local_unconnected_simplex_mesh<EmptyData, EmptyData>(
      nodal_data, cells, 8);

  const auto expected_nodes = {
      NodeMatcher{.rank = 8, .id = 0_vx, .position = o},
      NodeMatcher{.rank = 8, .id = 1_vx, .position = e_x},
      NodeMatcher{.rank = 8, .id = 2_vx, .position = e_y},
      NodeMatcher{.rank = 8, .id = 3_vx, .position = e_z},
  };
  EXPECT_THAT(mesh.nodes, ::testing::UnorderedElementsAreArray(expected_nodes));
  const auto expected_cells = {
      CellMatcher{.rank = 8,
                  .id = 0_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 2_vx, 3_vx}}};
  EXPECT_THAT(mesh.cells, ::testing::UnorderedElementsAreArray(expected_cells));
}

inline bool operator==(
    const std::pair<SimplexCellId, SimplexCell<3, LatticeCellData>> cell_item,
    const CellMatcher matcher) {
  return eq(cell_item, matcher);
}

TEST_F(SimplexMeshTest,
       create_local_unconnected_simplex_mesh_works_with_lattice_vectors) {
  using Vx = testing::LatticeVertex;
  const auto vertices = std::vector{Vx{o, {0, 0, 0}}, Vx{e_x, {1, 0, 0}},
                                    Vx{e_y, {0, 1, 0}}, Vx{e_z, {0, 0, 1}}};
  const auto cells = std::vector{std::array<std::size_t, 4>{0, 1, 2, 3}};
  const auto mesh =
      create_local_unconnected_simplex_mesh<EmptyData, LatticeCellData>(
          vertices, cells, 0);

  const auto expected_nodes = {
      NodeMatcher{.rank = 0, .id = 0_vx, .position = o},
      NodeMatcher{.rank = 0, .id = 1_vx, .position = e_x},
      NodeMatcher{.rank = 0, .id = 2_vx, .position = e_y},
      NodeMatcher{.rank = 0, .id = 3_vx, .position = e_z},
  };
  EXPECT_THAT(mesh.nodes, ::testing::UnorderedElementsAreArray(expected_nodes));
  const auto expected_cells = {
      CellMatcher{.rank = 0,
                  .id = 0_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 2_vx, 3_vx}}};
  EXPECT_THAT(mesh.cells, ::testing::UnorderedElementsAreArray(expected_cells));
  const auto expected_lattice_vectors = std::array{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{0., 1., 0.},
      std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(mesh.cells.at(0_cell).data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(expected_lattice_vectors));
}

TEST_F(
    SimplexMeshTest,
    create_local_unconnected_simplex_mesh_works_with_lattice_vectors_from_unstructured_nodes) {
  const auto vertices = std::vector{o, e_x, e_y, e_z, d};
  const auto cells = std::vector{std::array<std::size_t, 4>{0, 1, 2, 3},
                                 std::array<std::size_t, 4>{1, 2, 3, 4}};
  const auto mesh =
      create_local_unconnected_simplex_mesh<EmptyData, LatticeCellData>(
          vertices, cells);

  const auto expected_nodes = {
      NodeMatcher{.rank = 0, .id = 0_vx, .position = o},
      NodeMatcher{.rank = 0, .id = 1_vx, .position = e_x},
      NodeMatcher{.rank = 0, .id = 2_vx, .position = e_y},
      NodeMatcher{.rank = 0, .id = 3_vx, .position = e_z},
      NodeMatcher{.rank = 0, .id = 4_vx, .position = d},
  };
  EXPECT_THAT(mesh.nodes, ::testing::UnorderedElementsAreArray(expected_nodes));
  const auto expected_cells = {
      CellMatcher{.rank = 0,
                  .id = 0_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 2_vx, 3_vx}},
      CellMatcher{.rank = 0,
                  .id = 1_cell,
                  .nodes = std::array<NodeId, 4>{1_vx, 2_vx, 3_vx, 4_vx}}};
  EXPECT_THAT(mesh.cells, ::testing::UnorderedElementsAreArray(expected_cells));
  const auto jac_1 = std::array{std::array<double, 3>{1., 0., 0.},
                                std::array<double, 3>{0., 1., 0.},
                                std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(mesh.cells.at(0_cell).data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(jac_1));
  const auto jac_2 = std::array{std::array<double, 3>{-1., 1., 0.},
                                std::array<double, 3>{-1., 0., 1.},
                                std::array<double, 3>{0., 1., 1.}};
  EXPECT_THAT(mesh.cells.at(1_cell).data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(jac_2));
}

TEST_F(SimplexMeshTest, create_local_simplex_mesh_works) {
  const std::array<double, 3> e_mx = std::array{-1., 0., 0.};
  const std::array<double, 3> e_my = std::array{0., -1., 0.};
  const auto nodal_data = std::vector{
      PrimitiveNode<3>{.position = o},    PrimitiveNode<3>{.position = e_z},
      PrimitiveNode<3>{.position = e_x},  PrimitiveNode<3>{.position = e_y},
      PrimitiveNode<3>{.position = e_mx}, PrimitiveNode<3>{.position = e_my},
  };
  const auto cells = std::vector{std::array<std::size_t, 4>{0, 1, 2, 3},
                                 std::array<std::size_t, 4>{0, 1, 3, 4},
                                 std::array<std::size_t, 4>{0, 1, 4, 5},
                                 std::array<std::size_t, 4>{0, 1, 5, 2}};
  const auto mesh =
      create_local_simplex_mesh<EmptyData, EmptyData>(nodal_data, cells);

  const auto expected_nodes = {
      NodeMatcher{.id = 0_vx, .position = o},
      NodeMatcher{.id = 1_vx, .position = e_z},
      NodeMatcher{.id = 2_vx, .position = e_x},
      NodeMatcher{.id = 3_vx, .position = e_y},
      NodeMatcher{.id = 4_vx, .position = e_mx},
      NodeMatcher{.id = 5_vx, .position = e_my},
  };
  EXPECT_THAT(mesh.nodes, ::testing::UnorderedElementsAreArray(expected_nodes));
  using Neighbors = std::array<std::optional<SimplexCellId>, 4>;
  const auto expected_cells = {
      CellMatcher{.id = 0_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 2_vx, 3_vx},
                  .neighbors =
                      Neighbors{std::nullopt, std::nullopt, 1_cell, 3_cell}},
      CellMatcher{.id = 1_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 3_vx, 4_vx},
                  .neighbors =
                      Neighbors{std::nullopt, std::nullopt, 2_cell, 0_cell}},
      CellMatcher{.id = 2_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 4_vx, 5_vx},
                  .neighbors =
                      Neighbors{std::nullopt, std::nullopt, 3_cell, 1_cell}},
      CellMatcher{.id = 3_cell,
                  .nodes = std::array<NodeId, 4>{0_vx, 1_vx, 5_vx, 2_vx},
                  .neighbors =
                      Neighbors{std::nullopt, std::nullopt, 0_cell, 2_cell}}};
  EXPECT_THAT(mesh.cells, ::testing::UnorderedElementsAreArray(expected_cells));
}

TEST_F(SimplexMeshTest, deref_node_id_returns_correct_local_node) {
  const auto node = deref_node_id(mesh, 1_vx);
  const auto expected_position = e_x;
  EXPECT_EQ(node.position, expected_position);
}

TEST_F(SimplexMeshTest, deref_node_id_returns_correct_halo_node) {
  const auto node = deref_node_id(mesh, 3_vx);
  const auto expected_position = e_z;
  EXPECT_EQ(node.position, expected_position);
}

TEST_F(SimplexMeshTest, deref_cell_id_returns_correct_local_cell) {
  const auto cell = deref_cell_id(mesh, 0_cell);
  const auto expected_cell = cell_a;
  EXPECT_EQ(cell.nodes, expected_cell);
}

TEST_F(SimplexMeshTest, deref_cell_id_returns_correct_halo_cell) {
  const auto cell = deref_cell_id(mesh, 1_cell);
  const auto expected_cell = cell_b;
  EXPECT_EQ(cell.nodes, expected_cell);
}

TEST_F(SimplexMeshTest, simplex_cell_points_by_id_returns_correct_simplex) {
  const auto cell = simplex_cell_points(mesh, 0_cell);
  const auto expected_cell = std::array{o, e_x, e_y, e_z};
  EXPECT_EQ(cell, expected_cell);
}

TEST_F(SimplexMeshTest, simplex_cell_points_returns_correct_simplex) {
  const auto cell = simplex_cell_points(mesh, cell_c);
  const auto expected_cell = std::array{o, e_x, e_y, d};
  EXPECT_EQ(cell, expected_cell);
}

TEST_F(SimplexMeshTest, mesh_has_node_returns_false_if_node_does_not_exist) {
  EXPECT_EQ(mesh_has_node(mesh, 4_vx, 0), false);
}
TEST_F(SimplexMeshTest,
       mesh_has_node_returns_true_if_node_exists_with_correct_rank) {
  EXPECT_EQ(mesh_has_node(mesh, 1_vx, 0), true);
}
TEST_F(SimplexMeshTest,
       mesh_has_node_returns_false_if_node_exists_with_wrong_rank) {
  EXPECT_EQ(mesh_has_node(mesh, 1_vx, 1), false);
}

TEST_F(SimplexMeshTest, mesh_has_cell_returns_false_if_cell_does_not_exist) {
  EXPECT_EQ(mesh_has_cell(mesh, 4_cell, 0), false);
}
TEST_F(SimplexMeshTest,
       mesh_has_cell_returns_true_if_cell_exists_with_correct_rank) {
  EXPECT_EQ(mesh_has_cell(mesh, 0_cell, 0), true);
}
TEST_F(SimplexMeshTest,
       mesh_has_cell_returns_false_if_cell_exists_with_wrong_rank) {
  EXPECT_EQ(mesh_has_cell(mesh, 0_cell, 1), false);
}
TEST_F(SimplexMeshTest, disconnect_cell_from_mesh_works) {
  disconnect_cell_from_mesh(mesh, 2_cell);
  const auto expected_neighbors_of_0 =
      std::array<std::optional<SimplexCellId>, 4>{1_cell, std::nullopt,
                                                  std::nullopt, std::nullopt};
  const auto expected_neighbors_of_1 =
      std::array<std::optional<SimplexCellId>, 4>{std::nullopt, std::nullopt,
                                                  std::nullopt, 0_cell};
  EXPECT_EQ(mesh.cells.at(0_cell).neighbors, expected_neighbors_of_0);
  EXPECT_EQ(mesh.halo_cells.at(1_cell).neighbors, expected_neighbors_of_1);
}

struct TestLatticeCoordinates {
  std::array<int, 3> crd;
};

} // namespace

namespace traits {
template <> struct LatticeCoordinate<TestLatticeCoordinates> {
  constexpr static std::size_t DIMENSION = 3;
  const static std::array<int, 3> &
  lattice_coordinate(const TestLatticeCoordinates &coordinates) {
    return coordinates.crd;
  }
};
} // namespace traits

namespace {

TEST_F(SimplexMeshTest, cell_lattice_coordinates_returns_correct_simplex) {
  const auto lattice_coordinates =
      std::vector{TestLatticeCoordinates{std::array{0, 1, 2}},
                  TestLatticeCoordinates{std::array{2, 3, 4}},
                  TestLatticeCoordinates{std::array{4, 5, 6}},
                  TestLatticeCoordinates{std::array{8, 9, 10}}};
  const auto cell = std::array<std::size_t, 4>{2, 1, 0, 3};
  const auto expected_coordinates = std::array<std::array<int, 3>, 4>{
      std::array{4, 5, 6}, std::array{2, 3, 4}, std::array{0, 1, 2},
      std::array{8, 9, 10}};
  const auto coordinates = cell_lattice_coordinates(lattice_coordinates, cell);
  EXPECT_EQ(coordinates, expected_coordinates);
}

TEST_F(SimplexMeshTest, build_cell_works_for_regular_undeformed_example) {
  using Vx = testing::LatticeVertex;
  const auto vertices =
      std::vector{Vx{{1., 0., 0.}, {1, 0, 0}}, Vx{{2., 1., 0.}, {2, 1, 0}},
                  Vx{{2., 0., 1.}, {2, 0, 1}}, Vx{{1., 1., 1.}, {1, 1, 1}}};
  const auto cell_draft = std::array<std::size_t, 4>{0, 1, 2, 3};
  const auto cell =
      build_cell<LatticeCellData>(vertices, cell_draft, 0_cell, 0);
  const auto expected_lattice_vectors = std::array{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{0., 1., 0.},
      std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(cell.data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(expected_lattice_vectors));
}

TEST_F(SimplexMeshTest, cell_basis_works_for_regular_deformed_example) {
  using Vx = testing::LatticeVertex;
  const auto vertices =
      std::vector{Vx{{1., 0., 0.}, {1, 0, 0}}, Vx{{2., -1., 0.}, {2, 1, 0}},
                  Vx{{1., 1., -1.}, {1, 1, 1}}, Vx{{2., 0., 1.}, {2, 0, 1}}};
  const auto cell_draft = std::array<std::size_t, 4>{0, 1, 2, 3};
  const auto cell =
      build_cell<LatticeCellData>(vertices, cell_draft, 0_cell, 0);
  /* Inverse of  [[1, 0,  0],
   *              [1, 0, -1],
   *              [1, 1,  0]]:
   */
  const auto expected_lattice_vectors = std::array{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{-1., 0., 1.},
      std::array<double, 3>{1., -1., 0.}};
  EXPECT_THAT(cell.data.lattice_vectors,
              qcmesh::testing::DoubleMatrixEq(expected_lattice_vectors));
}

TEST_F(SimplexMeshTest, cell_basis_throws_exception_for_degenerated_example) {
  using Vx = testing::LatticeVertex;
  const auto vertices =
      std::vector{Vx{{1., 0., 0.}, {1, 0, 0}}, Vx{{2., -1., 0.}, {2, 1, 0}},
                  Vx{{1., 1., -1.}, {1, 1, 1}}};
  const auto cell_draft = std::array<std::size_t, 4>{0, 1, 2, 2};
  EXPECT_THROW(build_cell<LatticeCellData>(vertices, cell_draft, 0_cell, 0),
               Exception);
}

TEST_F(
    SimplexMeshTest,
    cell_basis_does_not_throw_exception_for_degenerated_example_when_suppressed) {
  using Vx = testing::LatticeVertex;
  const auto vertices =
      std::vector{Vx{{1., 0., 0.}, {1, 0, 0}}, Vx{{2., -1., 0.}, {2, 1, 0}},
                  Vx{{1., 1., -1.}, {1, 1, 1}}};
  const auto cell_draft = std::array<std::size_t, 4>{0, 1, 2, 2};
  build_cell<LatticeCellData>(vertices, cell_draft, 0_cell, 0, false);
}

std::array<std::size_t, 2> increment_twice(const std::size_t initial_value,
                                           const std::size_t process_rank,
                                           const std::size_t n_processes) {
  const auto value_1 =
      increment_counter(initial_value, process_rank, n_processes);
  const auto value_2 = increment_counter(value_1, process_rank, n_processes);
  return std::array{value_1, value_2};
}

TEST(test_increment_counter, increment_counter_works_for_1_process) {
  const auto initial_value = std::size_t{0};
  EXPECT_THAT(increment_twice(initial_value, 0, 1),
              ::testing::ElementsAre(1, 2));
}

TEST(test_increment_counter, increment_counter_works_for_4_processes) {
  EXPECT_THAT(increment_twice(std::size_t{0}, 0, 4),
              ::testing::ElementsAre(4, 8));
  EXPECT_THAT(increment_twice(std::size_t{1}, 0, 4),
              ::testing::ElementsAre(4, 8));
  EXPECT_THAT(increment_twice(std::size_t{3}, 0, 4),
              ::testing::ElementsAre(4, 8));
  EXPECT_THAT(increment_twice(std::size_t{0}, 1, 4),
              ::testing::ElementsAre(1, 5));
  EXPECT_THAT(increment_twice(std::size_t{1}, 1, 4),
              ::testing::ElementsAre(5, 9));
  EXPECT_THAT(increment_twice(std::size_t{1}, 3, 4),
              ::testing::ElementsAre(3, 7));
  EXPECT_THAT(increment_twice(std::size_t{3}, 3, 4),
              ::testing::ElementsAre(7, 11));
}

} // namespace
} // namespace qcmesh::mesh
