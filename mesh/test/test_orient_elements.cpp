// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/orient_elements.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

using TopologicalMesh = std::set<testing::TopologicalCell<4>>;

TEST(test_orient_elements, orient_elements_reorients_inverted_cells) {
  const auto nodes = std::vector<std::array<double, 3>>{
      {0., 0., 0.}, {1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}, {1., 1., 1.},
  };
  const auto cells = std::vector<std::array<std::size_t, 4>>{
      {0, 1, 2, 3}, {4, 1, 2, 3}, // <- incorrect orientation
  };
  auto mesh =
      create_local_unconnected_simplex_mesh<EmptyData, EmptyData>(nodes, cells);
  const auto zero_vol_cells = orient_elements(mesh);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);

  const auto expected_cells =
      TopologicalMesh{{0_vx, 1_vx, 2_vx, 3_vx}, {1_vx, 4_vx, 2_vx, 3_vx}};

  EXPECT_THAT(topological_mesh, ::testing::ContainerEq(expected_cells));
  EXPECT_THAT(zero_vol_cells, ::testing::IsEmpty());
}

TEST(test_orient_elements, orient_elements_finds_0_vol_cells) {
  const auto nodes = std::vector<std::array<double, 3>>{
      {0., 0., 0.}, {1., 0., 0.}, {0., 1., 0.}, {0., 0., 1.}, {1., 1., 0.},
  };
  const auto cells = std::vector<std::array<std::size_t, 4>>{
      {0, 1, 2, 3}, {4, 0, 1, 2}, // <- 0-vol
  };
  auto mesh =
      create_local_unconnected_simplex_mesh<EmptyData, EmptyData>(nodes, cells);
  const auto zero_vol_cells = orient_elements(mesh);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);

  const auto expected_cells =
      TopologicalMesh{{0_vx, 1_vx, 2_vx, 3_vx}, {4_vx, 0_vx, 1_vx, 2_vx}};

  EXPECT_THAT(topological_mesh, ::testing::ContainerEq(expected_cells));
  EXPECT_THAT(zero_vol_cells, ::testing::ElementsAre(1_cell));
}

struct Orient0VolCellsTestCase {
  std::string_view name;
  std::vector<std::array<double, 3>> nodes{};
  std::vector<std::array<std::size_t, 4>> cells{};
  std::unordered_set<SimplexCellId> initial_0_vol_cells{};
  TopologicalMesh expected_cells{};
  std::unordered_set<SimplexCellId> expected_0_vol_cells{};
};

std::ostream &operator<<(std::ostream &stream,
                         const Orient0VolCellsTestCase &test_case) {
  return stream << test_case.name;
}

struct Orient0VolCellsTest : ::testing::TestWithParam<Orient0VolCellsTestCase> {
};

INSTANTIATE_TEST_SUITE_P(
    orient_0_vol_cells_test_cases, Orient0VolCellsTest,
    ::testing::Values(
        Orient0VolCellsTestCase{
            .name = "Reorients inconsistent orientation",
            .nodes =
                {
                    {0., 0., 0.},
                    {1., 0., 0.},
                    {0., 1., 0.},
                    {0., 0., 1.},
                    {1., 1., 0.},
                    {1., 0., 1.},
                },
            .cells =
                {
                    {0, 1, 2, 3},
                    {0, 1, 2, 4}, // 0-vol, incorrect orientation
                    {1, 0, 3, 5}, // 0-vol, incorrect orientation
                },
            .initial_0_vol_cells = {1_cell, 2_cell},
            .expected_cells =
                {
                    {0_vx, 1_vx, 2_vx, 3_vx},
                    {0_vx, 1_vx, 4_vx, 2_vx},
                    {0_vx, 1_vx, 3_vx, 5_vx},
                },
            .expected_0_vol_cells = {}},
        Orient0VolCellsTestCase{
            .name = "Leaves consistent orientation",
            .nodes =
                {
                    {0., 0., 0.},
                    {1., 0., 0.},
                    {0., 1., 0.},
                    {0., 0., 1.},
                    {1., 1., 1.},
                    {1., 1., 0.},
                },
            .cells =
                {
                    {0, 1, 2, 3}, {0, 1, 5, 2}, // <- 0-vol, correct orientation
                },
            .initial_0_vol_cells = {1_cell},
            .expected_cells =
                {
                    {0_vx, 1_vx, 2_vx, 3_vx},
                    {0_vx, 1_vx, 5_vx, 2_vx},
                },
            .expected_0_vol_cells = {}},
        Orient0VolCellsTestCase{
            .name = "Reorients 0 vol cell indirectly",
            .nodes =
                {
                    {0., 0., 0.},
                    {1., 0., 0.},
                    {0., 1., 0.},
                    {0., 0., 1.},
                    {1., 1., 0.},
                    {-1., 0., 0.},
                },
            .cells =
                {
                    {0, 1, 2, 3},
                    {0, 1, 2, 4}, // 0-vol, incorrect orientation
                    {1, 2, 4, 5}, // 0-vol, incorrect orientation
                },
            .initial_0_vol_cells = {1_cell, 2_cell},
            .expected_cells =
                {
                    {0_vx, 1_vx, 2_vx, 3_vx},
                    {0_vx, 1_vx, 4_vx, 2_vx},
                    {1_vx, 2_vx, 5_vx, 4_vx},
                },
            .expected_0_vol_cells = {}}));

TEST_P(Orient0VolCellsTest, orient_local_0_vol_cells_works) {
  const auto test_case = GetParam();
  auto mesh = create_local_unconnected_simplex_mesh<EmptyData, EmptyData>(
      test_case.nodes, test_case.cells);
  auto zero_vol_cells = test_case.initial_0_vol_cells;
  orient_local_0_vol_cells(mesh, zero_vol_cells);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);

  EXPECT_THAT(topological_mesh,
              ::testing::ContainerEq(test_case.expected_cells));
  EXPECT_THAT(zero_vol_cells,
              ::testing::ContainerEq(test_case.expected_0_vol_cells));
}

TEST_P(Orient0VolCellsTest, orient_local_0_vol_cells_using_neighbors_works) {
  const auto test_case = GetParam();
  auto mesh = create_local_simplex_mesh<EmptyData, EmptyData>(test_case.nodes,
                                                              test_case.cells);
  auto zero_vol_cells = test_case.initial_0_vol_cells;
  orient_local_0_vol_cells_using_neighbors(mesh, zero_vol_cells);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);

  EXPECT_THAT(topological_mesh,
              ::testing::ContainerEq(test_case.expected_cells));
  EXPECT_THAT(zero_vol_cells,
              ::testing::ContainerEq(test_case.expected_0_vol_cells));
}

} // namespace
} // namespace qcmesh::mesh
