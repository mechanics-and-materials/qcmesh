// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/create_distributed_simplex_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/topological_cell.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/testing/collect_keys.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_create_distributed_simplex_mesh,
     create_distributed_simplex_mesh_generates_correct_local_mesh) {
  const auto world = boost::mpi::communicator{};
  const auto global_mesh = testing::mesh_cell_ring(world.size());
  const auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  const auto mpi_rank = static_cast<std::size_t>(world.rank());

  const auto local_cells = testing::collect_local_topological_cells(mesh.cells);
  const auto expected_local_cells =
      std::set{testing::to_topological_cell(global_mesh.cells[mpi_rank])};

  EXPECT_TRUE(mesh.cells.find(SimplexCellId{mpi_rank}) != mesh.cells.end());
  EXPECT_THAT(local_cells, ::testing::ContainerEq(expected_local_cells));
}

TEST(
    test_create_distributed_simplex_mesh,
    create_distributed_simplex_mesh_generates_correct_local_mesh_for_more_cells_than_procs) {
  const auto world = boost::mpi::communicator{};
  const auto mpi_size = static_cast<std::size_t>(world.size());
  const auto global_mesh = testing::mesh_cell_ring(mpi_size + 1);
  const auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  const auto mpi_rank = static_cast<std::size_t>(world.rank());

  const auto local_cells = testing::collect_local_topological_cells(mesh.cells);
  const auto expected_local_cells =
      mpi_rank + 1 == mpi_size
          ? std::set{testing::to_topological_cell(global_mesh.cells[mpi_rank]),
                     testing::to_topological_cell(global_mesh.cells.back())}
          : std::set{testing::to_topological_cell(global_mesh.cells[mpi_rank])};

  EXPECT_TRUE(mesh.cells.find(SimplexCellId{mpi_rank}) != mesh.cells.end());
  EXPECT_THAT(local_cells, ::testing::ContainerEq(expected_local_cells));
}

TEST(
    test_create_distributed_simplex_mesh,
    create_distributed_simplex_mesh_generates_no_nodes_not_belonging_to_local_cells) {
  const auto world = boost::mpi::communicator{};
  const auto global_mesh = testing::mesh_cell_ring(world.size());
  const auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);

  const auto local_nodes = testing::collect_keys(mesh.nodes);
  const auto expected_local_nodes = [&]() {
    auto out = std::set<NodeId>{};
    for (const auto &[_, cell] : mesh.cells)
      for (const auto node_id : cell.nodes)
        out.emplace(node_id);
    return out;
  }();

  EXPECT_THAT(local_nodes, ::testing::ContainerEq(expected_local_nodes));
}

TEST(test_create_distributed_simplex_mesh,
     create_distributed_simplex_mesh_generates_correct_global_mesh) {
  const auto global_mesh =
      testing::mesh_cell_ring(boost::mpi::communicator{}.size());
  const auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  const auto topological_mesh = qcmesh::mpi::all_union_set(
      testing::collect_local_topological_cells(mesh.cells));
  const auto expected_topological_mesh =
      testing::to_topological_cells(global_mesh.cells);
  EXPECT_THAT(topological_mesh,
              ::testing::ContainerEq(expected_topological_mesh));
  ASSERT_THAT(testing::collect_intrinsic_face_connections(mesh.cells),
              ::testing::ContainerEq(
                  testing::collect_extrinsic_face_connections(mesh.cells)));
}

} // namespace
} // namespace qcmesh::mesh
