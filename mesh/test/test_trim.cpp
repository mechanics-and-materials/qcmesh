// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/trim.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_trim, trim_works_for_one_flat_cell_on_the_boundary) {
  const auto nodal_data = std::vector{
      PrimitiveNode<3>{.position = std::array{0., 0., 0.}},
      PrimitiveNode<3>{.position = std::array{1., 0., 0.}},
      PrimitiveNode<3>{.position = std::array{0., 1., 0.}},
      PrimitiveNode<3>{.position = std::array{0., 0., 1.}},
      PrimitiveNode<3>{.position = std::array{1., 1., 0.}},
  };
  const auto cells = std::vector{std::array<std::size_t, 4>{0, 1, 2, 3},
                                 // Flat cell:
                                 std::array<std::size_t, 4>{0, 1, 4, 2}};
  auto mesh = create_local_unconnected_simplex_mesh<EmptyData, EmptyData>(
      nodal_data, cells);
  const auto domain = geometry::Box<3>{{0., 0., 0.}, {1., 1., 1.}};
  trim(mesh, domain, 1.0e-5);

  const auto expected_cell = std::array<NodeId, 4>{0_vx, 1_vx, 2_vx, 3_vx};
  for (const auto &[id, cell] : mesh.cells) {
    EXPECT_EQ(cell.nodes, expected_cell);
  }
  EXPECT_EQ(mesh.cells.size(), 1);
}

} // namespace
} // namespace qcmesh::mesh
