// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/testing/collect_keys.hpp"
#include <gmock/gmock.h>
#include <string>

namespace qcmesh::mesh::testing {

namespace {

TEST(test_topological_mesh, collect_keys_works) {
  const auto map =
      std::unordered_map<std::string, int>{{"A", 1}, {"B", 2}, {"C", 3}};
  const auto keys = collect_keys(map);
  const auto expected_keys = std::set<std::string>{"A", "B", "C"};
  EXPECT_THAT(keys, ::testing::ContainerEq(expected_keys));
}

} // namespace

} // namespace qcmesh::mesh::testing
