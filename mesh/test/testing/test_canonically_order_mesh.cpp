// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/empty_data.hpp"
#include "qcmesh/mesh/testing/canoncially_order_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace {
template <std::size_t D>
double distance(const std::array<double, D> &a,
                const std::array<double, D> &b) {
  auto err = 0.;
  for (std::size_t i = 0; i < D; i++)
    err += std::abs(a[i] - b[i]);
  return err;
}
} // namespace

namespace qcmesh::mesh {
template <std::size_t D, class NodalData>
bool operator==(const Node<D, NodalData> &a, const Node<D, NodalData> &b) {
  constexpr double EPS = 1.0e-10;
  return a.id == b.id && distance(a.position, b.position) < EPS;
}

template <std::size_t K, class CellData>
bool operator==(const SimplexCell<K, CellData> &a,
                const SimplexCell<K, CellData> &b) {
  return a.id == b.id && testing::TopologicalCell<K + 1>{a.nodes} ==
                             testing::TopologicalCell<K + 1>{b.nodes};
}
} // namespace qcmesh::mesh

namespace qcmesh::mesh::testing {

namespace {

TEST(test_canonically_order_mesh, canonically_order_mesh_orders_3_cell_mesh) {
  using Cell = SimplexCell<3, EmptyData>;
  using Node = Node<3, EmptyData>;
  const auto nodes = std::vector{
      Node{0_vx, 0, {1., 2., 3.}},  Node{1_vx, 0, {0., 1., 0.}},
      Node{2_vx, 0, {1., 0., 0.}},  Node{3_vx, 0, {0., 0., 1.}},
      Node{4_vx, 0, {0., 0., 0.}},  Node{7_vx, 0, {1., 1., 0.}},
      Node{10_vx, 0, {1., 0., 1.}}, Node{12_vx, 0, {0., 1., 1.}},
  };
  const auto cells = std::vector{
      Cell{0_cell, 0, {3_vx, 2_vx, 4_vx, 7_vx}},
      Cell{1_cell, 0, {0_vx, 3_vx, 10_vx, 12_vx}},
      Cell{4_cell, 0, {1_vx, 4_vx, 12_vx, 2_vx}},
  };
  const auto expected_ordered_nodes = std::vector{
      Node{0_vx, 0, {0., 0., 0.}}, Node{1_vx, 0, {0., 0., 1.}},
      Node{2_vx, 0, {0., 1., 0.}}, Node{3_vx, 0, {0., 1., 1.}},
      Node{4_vx, 0, {1., 0., 0.}}, Node{5_vx, 0, {1., 0., 1.}},
      Node{6_vx, 0, {1., 1., 0.}}, Node{7_vx, 0, {1., 2., 3.}},
  };
  const auto expected_ordered_cells = std::vector{
      Cell{0_cell, 0, {0_vx, 1_vx, 4_vx, 6_vx}},
      Cell{1_cell, 0, {0_vx, 2_vx, 4_vx, 3_vx}},
      Cell{2_cell, 0, {1_vx, 3_vx, 5_vx, 7_vx}},
  };
  const auto [ordered_nodes, ordered_cells] =
      canonically_order_mesh(nodes, cells);
  EXPECT_THAT(ordered_nodes, ::testing::ContainerEq(expected_ordered_nodes));
  EXPECT_THAT(ordered_cells, ::testing::ContainerEq(expected_ordered_cells));
}

} // namespace
} // namespace qcmesh::mesh::testing
