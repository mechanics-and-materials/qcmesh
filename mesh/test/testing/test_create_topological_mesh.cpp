// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/collect_keys.hpp"
#include "qcmesh/mesh/testing/create_topological_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {

namespace {

TEST(test_topological_mesh, create_topological_mesh_works) {
  using Cell = std::tuple<SimplexCellId, TopologicalCell<4>>;
  const auto original_topological_cells = std::vector{
      Cell{95_cell, {37_vx, 41_vx, 22_vx, 38_vx}},
      Cell{136_cell, {49_vx, 37_vx, 38_vx, 53_vx}},
      Cell{152_cell, {56_vx, 37_vx, 53_vx, 41_vx}},
      Cell{155_cell, {37_vx, 53_vx, 41_vx, 38_vx}},
      Cell{207_cell, {53_vx, 38_vx, 57_vx, 41_vx}},
  };
  auto mesh = create_topological_mesh(original_topological_cells);
  const auto topological_cells = [&]() {
    auto out = std::vector<Cell>{};
    for (const auto &[cell_id, cell] : mesh.cells)
      out.emplace_back(cell_id, TopologicalCell<4>{cell.nodes});
    return out;
  }();
  EXPECT_THAT(topological_cells,
              ::testing::UnorderedElementsAreArray(original_topological_cells));
  for (const auto &[cell_id, cell] : mesh.cells) {
    EXPECT_EQ(cell_id, cell.id);
  }

  const auto expected_node_ids =
      std::set<NodeId>{22_vx, 37_vx, 38_vx, 41_vx, 49_vx, 53_vx, 56_vx, 57_vx};
  EXPECT_THAT(collect_keys(mesh.nodes),
              ::testing::ContainerEq(expected_node_ids));
}

} // namespace

} // namespace qcmesh::mesh::testing
