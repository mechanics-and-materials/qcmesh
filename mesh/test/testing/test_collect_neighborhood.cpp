// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/create_lattice_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/testing/collect_neighborhood.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {
namespace {

/**
 * @brief Chain of 7 simplices.
 *
 * All vertices except 1,5,7 lie in the xy plane.
 * 1,5,7 are directly above 0,4,6 respectively.
 *
 *  (2)-(3)-(8)-(9)
 *   | / | \ | /
 *  (0)-(4)-(6)
 *   1   5   7
 */
testing::BareMesh<testing::LatticeVertex> mesh_simplex_chain() {
  const auto vx = [](int x, int y, int z) {
    return testing::LatticeVertex{{static_cast<double>(x),
                                   static_cast<double>(y),
                                   static_cast<double>(z)},
                                  {x, y, z}};
  };
  return testing::BareMesh<testing::LatticeVertex>{
      {vx(0, 0, 0), vx(0, 0, 1), vx(0, 1, 0), vx(1, 1, 0), vx(1, 0, 0),
       vx(1, 0, 1), vx(2, 0, 0), vx(2, 0, 1), vx(2, 1, 0), vx(3, 1, 0)},
      {{0, 1, 3, 2},
       {0, 1, 4, 3},
       {1, 3, 4, 5},
       {3, 4, 6, 5},
       {3, 5, 6, 7},
       {3, 6, 8, 7},
       {6, 7, 9, 8}}};
}

SimplexMesh<3, 3, EmptyData, testing::LatticeCellData> make_mesh() {
  return testing::create_distributed_lattice_mesh<testing::LatticeCellData>(
      mesh_simplex_chain());
}

TEST(test_collect_neighborhood, collect_neighborhood_works_for_level_0) {
  const auto mesh = make_mesh();
  const auto neighborhood = collect_neighborhood(mesh.cells, {2_cell}, 0);
  const auto expected_neighborhood = std::unordered_set{2_cell};
  EXPECT_EQ(neighborhood, expected_neighborhood);
}

TEST(test_collect_neighborhood, collect_neighborhood_works_for_level_1) {
  const auto mesh = make_mesh();
  const auto neighborhood = collect_neighborhood(mesh.cells, {2_cell}, 1);
  const auto expected_neighborhood =
      std::unordered_set{0_cell, 1_cell, 2_cell, 3_cell, 4_cell, 5_cell};
  EXPECT_EQ(neighborhood, expected_neighborhood);
}

TEST(test_collect_neighborhood, collect_neighborhood_works_for_level_2) {
  const auto mesh = make_mesh();
  const auto neighborhood = collect_neighborhood(mesh.cells, {2_cell}, 2);
  const auto expected_neighborhood = std::unordered_set{
      0_cell, 1_cell, 2_cell, 3_cell, 4_cell, 5_cell, 6_cell};
  EXPECT_EQ(neighborhood, expected_neighborhood);
}

TEST(test_collect_neighborhood,
     extend_neighborhood_works_for_empty_neighborhood) {
  const auto mesh = make_mesh();
  auto neighborhood = std::unordered_set<SimplexCellId>{};
  extend_neighborhood(mesh.cells, neighborhood);
  EXPECT_THAT(neighborhood, ::testing::IsEmpty());
}

TEST(test_collect_neighborhood, sub_mesh_extracts_correct_mesh) {
  const auto mesh = make_mesh();
  auto cell_ids = std::unordered_set<SimplexCellId>{0_cell, 1_cell, 2_cell};
  const auto sub_mesh = extract_sub_mesh(mesh, cell_ids);
  const auto expected_topological_mesh =
      std::set<testing::TopologicalCell<4>>{{0_vx, 1_vx, 3_vx, 2_vx},
                                            {0_vx, 1_vx, 4_vx, 3_vx},
                                            {1_vx, 3_vx, 4_vx, 5_vx}};
  const auto topological_sub_mesh =
      testing::collect_local_topological_cells(sub_mesh.cells);
  EXPECT_THAT(topological_sub_mesh,
              ::testing::ContainerEq(expected_topological_mesh));
}

TEST(test_collect_neighborhood, sub_mesh_extracts_correct_mesh_for_level_1) {
  const auto mesh = make_mesh();
  const auto sub_mesh = extract_sub_mesh(mesh, {2_cell}, 1);
  const auto expected_topological_mesh = std::set<testing::TopologicalCell<4>>{
      {0_vx, 1_vx, 3_vx, 2_vx}, {0_vx, 1_vx, 4_vx, 3_vx},
      {1_vx, 3_vx, 4_vx, 5_vx}, {3_vx, 4_vx, 6_vx, 5_vx},
      {3_vx, 5_vx, 6_vx, 7_vx}, {3_vx, 6_vx, 8_vx, 7_vx}};
  const auto topological_sub_mesh =
      testing::collect_local_topological_cells(sub_mesh.cells);
  EXPECT_THAT(topological_sub_mesh,
              ::testing::ContainerEq(expected_topological_mesh));
}

} // namespace
} // namespace qcmesh::mesh::testing
