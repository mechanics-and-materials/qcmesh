// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/face_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/repair_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/simplex_lattice_jacobian.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

using FaceHandle = Face<SimplexCellId, NodeId>;
using testing::create_local_lattice_mesh;
using testing::create_local_lattice_mesh_with_lattice_coordinates;

struct FaceMirrorTest : ::testing::Test {
  SimplexMesh<3, 3, EmptyData, EmptyData> mesh =
      create_local_simplex_mesh<EmptyData, EmptyData>(
          std::vector<std::array<double, 3>>{{0., 0., -1.},
                                             {0., 0., 1.},
                                             {1., 0., 0.},
                                             {-1., 1., 0.},
                                             {-1., -1., 0.}},
          std::vector<std::array<std::size_t, 4>>{{0, 1, 2, 3}, {0, 1, 4, 2}});
  SimplexCellId cell_handle = 0_cell;
  SimplexCellId cell_handle_neighbor = 1_cell;
  NodeId apex = 3_vx;
  NodeId neighbor_apex = 4_vx;
  NodeId apex_with_no_neighbor = 0_vx;
};

TEST_F(FaceMirrorTest, face_mirror_yields_correct_face_if_neighbor_exists) {
  const auto face = FaceHandle{cell_handle, apex};
  const auto mirror = face_mirror(mesh, face);
  const auto expected_mirror = FaceHandle{cell_handle_neighbor, neighbor_apex};
  EXPECT_EQ(mirror, expected_mirror);
}

TEST_F(FaceMirrorTest, face_mirror_yields_no_face_if_no_neighbor_exists) {
  const auto face = FaceHandle{cell_handle, apex_with_no_neighbor};
  const auto mirror = face_mirror(mesh, face);
  EXPECT_EQ(mirror, std::nullopt);
}

TEST_F(FaceMirrorTest,
       get_face_vertices_yields_correct_vertices_when_apex_is_last_vertex) {
  const auto face = FaceHandle{cell_handle, apex};
  const auto vertices = get_face_vertices(mesh, face);
  const auto expected_vertices =
      std::array<NodeId, 3>{NodeId{1}, NodeId{0}, NodeId{2}};
  EXPECT_EQ(vertices, expected_vertices);
}
TEST_F(FaceMirrorTest,
       get_face_vertices_yields_correct_vertices_when_apex_is_first_vertex) {
  const auto apex = 0_vx;
  const auto face = FaceHandle{cell_handle, apex};
  const auto vertices = get_face_vertices(mesh, face);
  const auto expected_vertices = std::array<NodeId, 3>{1_vx, 2_vx, 3_vx};
  EXPECT_EQ(vertices, expected_vertices);
}
TEST_F(FaceMirrorTest,
       get_face_vertices_yields_correct_vertices_when_apex_is_second_vertex) {
  const auto apex = 1_vx;
  const auto face = FaceHandle{cell_handle, apex};
  const auto vertices = get_face_vertices(mesh, face);
  const auto expected_vertices = std::array<NodeId, 3>{2_vx, 0_vx, 3_vx};
  EXPECT_EQ(vertices, expected_vertices);
}
TEST_F(FaceMirrorTest,
       get_face_vertices_yields_correct_vertices_when_apex_is_third_vertex) {
  const auto apex = 2_vx;
  const auto face = FaceHandle{cell_handle, apex};
  const auto vertices = get_face_vertices(mesh, face);
  const auto expected_vertices = std::array<NodeId, 3>{0_vx, 1_vx, 3_vx};
  EXPECT_EQ(vertices, expected_vertices);
}

TEST_F(FaceMirrorTest,
       ModifierFaceEraser_primitives_for_element_yields_correct_faces) {
  const auto modifier = FaceEraser<EmptyData, EmptyData>{};
  const auto faces = modifier.primitives_for_element(mesh, cell_handle);
  const auto expected_faces = std::vector<FaceHandle>{
      FaceHandle{cell_handle, 0_vx}, FaceHandle{cell_handle, 1_vx},
      FaceHandle{cell_handle, 2_vx}, FaceHandle{cell_handle, 3_vx}};
  EXPECT_THAT(faces, ::testing::UnorderedElementsAreArray(expected_faces));
}

TEST_F(FaceMirrorTest, face_mirror_by_vertices_finds_existing_face) {
  const auto expected_face = Face<SimplexCellId, std::size_t>{1_cell, 2};
  const auto face =
      face_mirror_by_vertices(mesh, cell_handle, 0_vx, 1_vx, 2_vx);
  EXPECT_EQ(face, expected_face);
}
TEST_F(FaceMirrorTest,
       face_mirror_by_vertices_yields_nullopt_for_non_existing_face) {
  const auto expected_face = std::nullopt;
  const auto face =
      face_mirror_by_vertices(mesh, cell_handle, 0_vx, 3_vx, 4_vx);
  EXPECT_EQ(face, expected_face);
}
TEST_F(FaceMirrorTest,
       face_mirror_by_vertices_yields_nullopt_for_face_with_no_mirror) {
  const auto expected_face = std::nullopt;
  const auto face =
      face_mirror_by_vertices(mesh, cell_handle, 0_vx, 1_vx, 3_vx);
  EXPECT_EQ(face, expected_face);
}

struct IsFlippableTest : ::testing::Test {
  std::vector<std::array<double, 3>> points = {
      {0., 0., 1.}, {0., 0., -1.}, {1., -1., 0.}, {1., 1., 0.}, {2., 0., 0.}};
  std::vector<std::array<std::size_t, 3 + 1>> cells = {{0, 2, 3, 4},
                                                       {1, 2, 4, 3}};
  FaceHandle face = FaceHandle{0_cell, 0_vx};
  NodeId apex_b = 1_vx;

  SimplexMesh<3, 3, EmptyData, EmptyData>
  prepare_mesh(const bool flip_orientation,
               const bool edge_intersects_face = false) {
    auto modified_points = points;
    if (flip_orientation) {
      modified_points[3] = points[2];
      modified_points[2] = points[3];
    }
    const auto shift = 2.;
    if (edge_intersects_face) {
      modified_points[2][0] -= shift;
      modified_points[3][0] -= shift;
    }
    return create_local_simplex_mesh<EmptyData, EmptyData>(modified_points,
                                                           cells);
  }
};

TEST_F(IsFlippableTest, is_flippable_detects_unflippable_case) {
  const auto points = std::vector<std::array<double, 3>>{
      {0., 0., 1.}, {0., 0., -1.}, {1., 0., 0.}, {2., 1., 0.}, {2., -1., 0.}};
  const auto cells =
      std::vector<std::array<std::size_t, 3 + 1>>{{0, 2, 3, 4}, {1, 2, 4, 3}};
  const auto mesh =
      create_local_simplex_mesh<EmptyData, EmptyData>(points, cells);
  const auto is_flippable = sandwiched_face_is_flippable(mesh, face, apex_b);
  EXPECT_EQ(is_flippable, false);
}
TEST_F(IsFlippableTest, is_flippable_detects_flippable_case) {
  const auto flip_orientation = false;
  const auto mesh = prepare_mesh(flip_orientation);
  const auto is_flippable = sandwiched_face_is_flippable(mesh, face, apex_b);
  EXPECT_EQ(is_flippable, true);
}
TEST_F(IsFlippableTest,
       is_flippable_detects_flippable_case_in_reversed_orientation) {
  const auto flip_orientation = true;
  const auto mesh = prepare_mesh(flip_orientation);
  const auto is_flippable = sandwiched_face_is_flippable(mesh, face, apex_b);
  EXPECT_EQ(is_flippable, true);
}
TEST_F(IsFlippableTest, segment_intersects_face_detects_intersection) {
  const auto flip_orientation = false;
  const auto expect_face_intersection = true;
  const auto mesh = prepare_mesh(flip_orientation, expect_face_intersection);
  const auto have_intersection = segment_intersects_face(mesh, face, apex_b);
  EXPECT_EQ(have_intersection, expect_face_intersection);
}
TEST_F(IsFlippableTest,
       segment_intersects_face_detects_intersection_with_inversed_orientation) {
  const auto flip_orientation = true;
  const auto expect_face_intersection = true;
  const auto mesh = prepare_mesh(flip_orientation, expect_face_intersection);
  const auto have_intersection = segment_intersects_face(mesh, face, apex_b);
  EXPECT_EQ(have_intersection, expect_face_intersection);
}
TEST_F(IsFlippableTest, segment_intersects_face_detects_non_intersection) {
  const auto flip_orientation = false;
  const auto expect_face_intersection = false;
  const auto mesh = prepare_mesh(flip_orientation, expect_face_intersection);
  const auto have_intersection = segment_intersects_face(mesh, face, apex_b);
  EXPECT_EQ(have_intersection, expect_face_intersection);
}
TEST_F(
    IsFlippableTest,
    segment_intersects_face_detects_non_intersection_with_inversed_orientation) {
  const auto flip_orientation = true;
  const auto expect_face_intersection = false;
  const auto mesh = prepare_mesh(flip_orientation, expect_face_intersection);
  const auto have_intersection = segment_intersects_face(mesh, face, apex_b);
  EXPECT_EQ(have_intersection, expect_face_intersection);
}

struct FaceRemovalTestCase {
  testing::BareMesh<testing::LatticeVertex> initial_mesh;
  std::set<testing::TopologicalCell<4>> expected_topological_mesh;

  template <class NodalData, class CellData>
  void apply_to(SimplexMesh<3, 3, NodalData, CellData> &mesh) const {
    using Modifier = FaceEraser<NodalData, CellData>;
    const auto cell = mesh.cells.at(0_cell);
    const auto face = typename Modifier::FaceHandle{cell.id, cell.nodes[0]};
    const auto modifier = Modifier{};
    const auto preview = modifier.preview(mesh, face);
    const auto change_set =
        modifier.preview_to_changeset(std::move(preview), mesh);
    apply_change_set(mesh, change_set);
  }
};
/**
 * @brief Test suite to check the lattice vectors and
 * atomistic_flag of newly created cells of an edge removal.
 */
struct FaceRemovalTest : ::testing::TestWithParam<FaceRemovalTestCase> {};
INSTANTIATE_TEST_CASE_P(
    edge_removal_test_cases, FaceRemovalTest,
    ::testing::Values(
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_3(),
            .expected_topological_mesh = {{0_vx, 1_vx, 2_vx, 3_vx},
                                          {0_vx, 1_vx, 3_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx}}},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_3b(),
            .expected_topological_mesh = {{0_vx, 1_vx, 2_vx, 3_vx},
                                          {0_vx, 1_vx, 3_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx},
                                          {0_vx, 4_vx, 3_vx, 5_vx},
                                          {1_vx, 3_vx, 4_vx, 6_vx},
                                          {0_vx, 3_vx, 2_vx, 5_vx},
                                          {1_vx, 2_vx, 3_vx, 6_vx},
                                          {0_vx, 2_vx, 4_vx, 5_vx},
                                          {1_vx, 4_vx, 2_vx, 6_vx}}},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_4(),
            .expected_topological_mesh = {{0_vx, 1_vx, 2_vx, 3_vx},
                                          {0_vx, 1_vx, 3_vx, 5_vx},
                                          {0_vx, 1_vx, 5_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx}}},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_4b(),
            .expected_topological_mesh = {{0_vx, 1_vx, 2_vx, 3_vx},
                                          {0_vx, 1_vx, 3_vx, 5_vx},
                                          {0_vx, 1_vx, 5_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx},
                                          {6_vx, 2_vx, 3_vx, 0_vx},
                                          {6_vx, 3_vx, 5_vx, 0_vx},
                                          {6_vx, 5_vx, 4_vx, 0_vx},
                                          {6_vx, 4_vx, 2_vx, 0_vx},
                                          {7_vx, 1_vx, 3_vx, 2_vx},
                                          {7_vx, 1_vx, 5_vx, 3_vx},
                                          {7_vx, 1_vx, 4_vx, 5_vx},
                                          {7_vx, 1_vx, 2_vx, 4_vx}}},
        FaceRemovalTestCase{.initial_mesh = testing::mesh_5(),
                            .expected_topological_mesh =
                                {
                                    {0_vx, 1_vx, 2_vx, 5_vx},
                                    {0_vx, 1_vx, 5_vx, 3_vx},
                                    {0_vx, 1_vx, 3_vx, 4_vx},
                                    {0_vx, 1_vx, 4_vx, 2_vx},
                                    {0_vx, 2_vx, 4_vx, 5_vx}, // unchanged
                                    {1_vx, 5_vx, 4_vx, 2_vx}, // unchanged
                                }},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_5b(),
            .expected_topological_mesh = {{0_vx, 1_vx, 2_vx, 5_vx},
                                          {0_vx, 1_vx, 5_vx, 3_vx},
                                          {0_vx, 1_vx, 3_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx},
                                          {0_vx, 2_vx, 4_vx, 5_vx}, // unchanged
                                          {1_vx, 5_vx, 4_vx, 2_vx}, // unchanged
                                          {6_vx, 0_vx, 5_vx, 3_vx},
                                          {6_vx, 0_vx, 3_vx, 4_vx},
                                          {6_vx, 0_vx, 4_vx, 5_vx},
                                          {7_vx, 1_vx, 3_vx, 5_vx},
                                          {7_vx, 1_vx, 4_vx, 3_vx},
                                          {7_vx, 1_vx, 5_vx, 4_vx}}},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_6(),
            .expected_topological_mesh = {{0_vx, 1_vx, 3_vx, 2_vx},
                                          {0_vx, 1_vx, 2_vx, 5_vx},
                                          {0_vx, 1_vx, 5_vx, 6_vx},
                                          {0_vx, 1_vx, 6_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 3_vx}}},
        FaceRemovalTestCase{
            .initial_mesh = testing::mesh_6b(),
            .expected_topological_mesh = {{0_vx, 1_vx, 3_vx, 2_vx},
                                          {0_vx, 1_vx, 2_vx, 5_vx},
                                          {0_vx, 1_vx, 5_vx, 6_vx},
                                          {0_vx, 1_vx, 6_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 3_vx},
                                          {7_vx, 0_vx, 3_vx, 2_vx},
                                          {7_vx, 0_vx, 2_vx, 5_vx},
                                          {7_vx, 0_vx, 5_vx, 6_vx},
                                          {7_vx, 0_vx, 6_vx, 4_vx},
                                          {7_vx, 0_vx, 4_vx, 3_vx},
                                          {8_vx, 1_vx, 2_vx, 3_vx},
                                          {8_vx, 1_vx, 5_vx, 2_vx},
                                          {8_vx, 1_vx, 6_vx, 5_vx},
                                          {8_vx, 1_vx, 4_vx, 6_vx},
                                          {8_vx, 1_vx, 3_vx, 4_vx}}}));

TEST_P(FaceRemovalTest, face_removal_yields_correct_mesh) {
  const auto test_case = GetParam();
  auto mesh = create_local_lattice_mesh(test_case.initial_mesh);
  test_case.apply_to(mesh);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);
  EXPECT_THAT(topological_mesh,
              ::testing::ContainerEq(test_case.expected_topological_mesh));
}
TEST_P(FaceRemovalTest, face_removal_does_not_break_connectivity) {
  const auto test_case = GetParam();
  auto mesh = create_local_lattice_mesh(test_case.initial_mesh);
  ASSERT_THAT(
      testing::collect_local_intrinsic_face_connections(mesh.cells),
      ::testing::ContainerEq(
          testing::collect_local_extrinsic_face_connections(mesh.cells)));

  test_case.apply_to(mesh);
  ASSERT_THAT(
      testing::collect_local_intrinsic_face_connections(mesh.cells),
      ::testing::ContainerEq(
          testing::collect_local_extrinsic_face_connections(mesh.cells)));
}
TEST_P(FaceRemovalTest, face_removal_does_not_change_lattice_coordinates) {
  const auto test_case = GetParam();
  auto [mesh, vertices] = create_local_lattice_mesh_with_lattice_coordinates(
      test_case.initial_mesh);
  ASSERT_THAT(
      testing::intrinsic_simplex_lattice_jacobians(mesh),
      ::testing::ContainerEq(
          testing::extrinsic_simplex_lattice_jacobians(mesh.cells, vertices)));
  test_case.apply_to(mesh);
  ASSERT_THAT(
      testing::intrinsic_simplex_lattice_jacobians(mesh),
      ::testing::ContainerEq(
          testing::extrinsic_simplex_lattice_jacobians(mesh.cells, vertices)));
}
TEST_P(FaceRemovalTest, face_removal_sets_correct_atomistic_flag) {
  const auto test_case = GetParam();
  auto [mesh, vertices] = create_local_lattice_mesh_with_lattice_coordinates(
      test_case.initial_mesh);
  ASSERT_THAT(testing::intrinsic_simplex_atomistic_flags(mesh.cells),
              ::testing::ContainerEq(testing::extrinsic_simplex_atomistic_flags(
                  mesh.cells, vertices)));
  test_case.apply_to(mesh);
  ASSERT_THAT(testing::intrinsic_simplex_atomistic_flags(mesh.cells),
              ::testing::ContainerEq(testing::extrinsic_simplex_atomistic_flags(
                  mesh.cells, vertices)));
}

} // namespace
} // namespace qcmesh::mesh
