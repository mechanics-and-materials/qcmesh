// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/connect_local_cell_faces.hpp"
#include "qcmesh/mesh/create_distributed_simplex_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/exchange_halos.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/topological_cell.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/serialization/empty_data.hpp"
#include "qcmesh/mesh/testing/collect_keys.hpp"
#include "qcmesh/mesh/testing/create_topological_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_union_set.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

template <class T>
std::set<T> set_diff(const std::set<T> &a, const std::set<T> &b) {
  auto out = std::set<T>{};
  for (const auto &element : a)
    if (b.find(element) == b.end())
      out.insert(element);
  return out;
}

template <class T>
std::set<T> id_range_set(const std::size_t start, const std::size_t n) {
  auto out = std::set<T>{};
  for (std::size_t i = start; i < n; i++)
    out.emplace(T{i});
  return out;
}
template <class T>
std::vector<T> id_range_vec(const std::size_t start, const std::size_t n) {
  auto out = std::vector<T>{};
  for (std::size_t i = start; i < n; i++)
    out.emplace_back(T{i});
  return out;
}

TEST(test_exchange_halos, exchange_halos_sets_correct_halo_for_whole_mesh) {
  const auto world = boost::mpi::communicator{};
  const auto global_mesh = testing::mesh_cell_ring(world.size());
  auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  const auto cell_ids = id_range_vec<SimplexCellId>(0, world.size());
  exchange_halos(mesh, cell_ids);

  const auto expected_local_halo_cells =
      set_diff(testing::to_topological_cells(global_mesh.cells),
               testing::collect_local_topological_cells(mesh.cells));
  EXPECT_THAT(testing::collect_local_topological_cells(mesh.halo_cells),
              ::testing::ContainerEq(expected_local_halo_cells));

  const auto halo_node_ids = testing::collect_keys(mesh.halo_nodes);
  const auto all_nodes = id_range_set<NodeId>(0, global_mesh.vertices.size());
  const auto expected_halo_node_ids =
      set_diff(all_nodes, testing::collect_keys(mesh.nodes));
  EXPECT_THAT(halo_node_ids, ::testing::ContainerEq(expected_halo_node_ids));
}

TEST(test_exchange_halos, exchange_halos_sets_correct_halo_around_one_cell) {
  const auto world = boost::mpi::communicator{};
  const auto global_mesh = testing::mesh_cell_ring(world.size());
  auto mesh = create_distributed_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  const auto cell_ids = std::vector{0_cell};
  exchange_halos(mesh, cell_ids);

  const auto expected_local_halo_cells =
      (world.rank() != 0) ? std::vector<SimplexCellId>{}
                          : id_range_vec<SimplexCellId>(1, world.size());
  const auto local_halo_cells = [&]() {
    auto out = std::vector<SimplexCellId>{};
    for (const auto &[cell_id, _] : mesh.halo_cells)
      out.push_back(cell_id);
    return out;
  }();
  EXPECT_THAT(local_halo_cells,
              ::testing::UnorderedElementsAreArray(expected_local_halo_cells));

  const auto halo_node_ids = testing::collect_keys(mesh.halo_nodes);
  const auto expected_local_halo_node_ids =
      (world.rank() != 0)
          ? std::set<NodeId>{}
          : id_range_set<NodeId>(4, global_mesh.vertices.size());
  EXPECT_THAT(halo_node_ids,
              ::testing::ContainerEq(expected_local_halo_node_ids));
}

TEST(test_exchange_halos, exchange_halos_yields_correct_halo_for_2_processes) {
  const auto world = boost::mpi::communicator{};
  using Cell = std::tuple<SimplexCellId, testing::TopologicalCell<4>>;
  auto partial_meshes =
      std::array{testing::create_topological_mesh(std::vector{
                     Cell{91_cell, {37_vx, 41_vx, 26_vx, 22_vx}},
                 }),
                 testing::create_topological_mesh(std::vector{
                     Cell{95_cell, {37_vx, 41_vx, 22_vx, 38_vx}},
                     Cell{136_cell, {49_vx, 37_vx, 38_vx, 53_vx}},
                     Cell{152_cell, {56_vx, 37_vx, 53_vx, 41_vx}},
                     Cell{155_cell, {37_vx, 53_vx, 41_vx, 38_vx}},
                     Cell{207_cell, {53_vx, 38_vx, 57_vx, 41_vx}},
                 })};
  // Manually connect foreign cells:
  partial_meshes[0].cells.at(91_cell).neighbors[2] = 95_cell;
  partial_meshes[1].cells.at(95_cell).neighbors[3] = 91_cell;
  auto mesh = world.rank() < 2 ? partial_meshes[world.rank()]
                               : SimplexMesh<0, 3, EmptyData, EmptyData>{};
  const auto cell_ids = std::vector{91_cell, 84_cell, 77_cell};
  exchange_halos(mesh, cell_ids);
  const auto halo_cell_ids = testing::collect_keys(mesh.halo_cells);
  const auto expected_halo_cell_ids_by_rank = std::array{
      std::set<SimplexCellId>{95_cell, 136_cell, 152_cell, 155_cell, 207_cell},
      std::set<SimplexCellId>{}};
  if (world.size() > 1 && world.rank() < 2) {
    EXPECT_THAT(
        halo_cell_ids,
        ::testing::ContainerEq(expected_halo_cell_ids_by_rank[world.rank()]));
  } else {
    EXPECT_THAT(halo_cell_ids, ::testing::IsEmpty());
  }
}

} // namespace
} // namespace qcmesh::mesh
