// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "face_mirror_mock.hpp"
#include "face_vertices_query_mock.hpp"
#include "is_flippable_mock.hpp"
#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include "quality_measure_mock.hpp"
#include <gmock/gmock.h>
#include <limits>

// NOLINTBEGIN(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::_;
// NOLINTEND(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::AtMost;
using testing::DoubleEq;
using testing::ElementsAre;
using testing::IsEmpty;
using testing::Return;
using testing::StrictMock;
using testing::Test;

namespace qcmesh::mesh {
namespace {

using Face = Face<CellHandle, VertexHandle>;
using SharedFace = SharedFace<CellHandle, VertexHandle>;
using FaceVerticesQuery = FaceVerticesQuery<CellHandle, VertexHandle>;
using FaceMirrorQuery = FaceMirrorQuery<CellHandle, VertexHandle>;
using QualityMeasure = QualityMeasure<VertexHandle>;
using RemovalInfo = RemovalInfo<Face, VertexHandle>;
using SharedFacePredicate = SharedFacePredicate<SharedFace>;
using Tetrahedron = Tetrahedron<VertexHandle>;

using OptionalFace = std::optional<Face>;
using Result = TestNeighborResult<RemovalInfo>;

struct TestTestNeighbor : Test {
  const VertexHandle u = 1_vertex, v = 2_vertex, w = 3_vertex;
  const VertexHandle a = 4_vertex, b = 5_vertex;

  const VertexHandle x = 6_vertex;
  const VertexHandle y = 7_vertex;
  const VertexHandle z = 8_vertex;

  const CellHandle cell = 11_cell, cell_l = 12_cell, cell_ll = 13_cell,
                   cell_lr = 14_cell, cell_lb = 15_cell, cell_llb = 16_cell,
                   cell_lrb = 17_cell;

  const SharedFace shared_f = {{cell, a}, b};

  StrictMock<QualityMeasureMock> compute_quality;
  StrictMock<IsFlippableMock> is_flippable;
  StrictMock<FaceVerticesQueryMock> get_vertices;
  StrictMock<FaceMirrorQueryMock> face_mirror;

  constexpr static auto INF = std::numeric_limits<double>::infinity();

  /**
   * @brief Calls test_neighbor for a face f.
   * If have_interface_cell=true, f has a neighbor g but the neighbor has no
   * face sandwiched between a, b. If have_interface_cell=false, f has no
   * neighbor.
   */
  Result call_for_no_neighbor(const bool have_interface_cell,
                              const double quality) const;

  /**
   * @brief Calls test_neighbor for a single face g next to f. The parameters
   * specify the quality of the existing/created tetrahedra.
   */
  Result call_for_single_neighbor(const double quality_abuw,
                                  const double quality_auxw,
                                  const double quality_uxwb,
                                  const double quality_abux,
                                  const double quality_abxw,
                                  const bool is_g_flippable) const;

  /**
   * @brief Calls test_neighbor for a face g next to f, which in turn has
   * neighbors h and j. The qualities are used to incentivize the exploration of
   * all neighbor faces.
   */
  Result call_for_two_neighbors(const double bad_quality,
                                const double good_quality,
                                const double optimal_quality) const;
};

Result TestTestNeighbor::call_for_no_neighbor(const bool have_interface_cell,
                                              const double quality) const {
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, u}))
      .WillOnce(Return(quality));

  const auto no_face = OptionalFace();
  if (!have_interface_cell) {
    // assume that there is no neighboring face intersecting uw
    EXPECT_CALL(face_mirror, face_mirror(Face{cell, v}))
        .WillOnce(Return(no_face));
  } else {
    // There is a neighboring cell
    EXPECT_CALL(face_mirror, face_mirror(Face{cell, v}))
        .WillOnce(Return(Face{cell_l, x}));
    // But it is not sandwiched between a and b
    EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, a}))
        .WillOnce(Return(Face{cell_lb, y}));
  }
  // Vertices of face uaw
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, a}))
      .WillOnce(Return(std::array<VertexHandle, 3>({{u, v, w}})));
  return test_neighbor(shared_f, v, QualityMeasure(std::ref(compute_quality)),
                       SharedFacePredicate(std::ref(is_flippable)),
                       FaceVerticesQuery(std::ref(get_vertices)),
                       FaceMirrorQuery(std::ref(face_mirror)));
}

TEST_F(TestTestNeighbor, fails_if_no_neighboring_vertex_with_interface_cell) {
  // set the quality of the new tetrahedron created by flipping f
  const auto quality = 0.0;

  const auto result = call_for_no_neighbor(true, quality);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(INF));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(quality));
  EXPECT_THAT(result.faces_to_remove, IsEmpty());
}

TEST_F(TestTestNeighbor, fails_if_no_neighboring_vertex) {
  // set the quality of the new tetrahedron created by flipping f
  const auto quality = 0.0;

  const auto result = call_for_no_neighbor(false, quality);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(INF));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(quality));
  EXPECT_THAT(result.faces_to_remove, IsEmpty());
}

Result TestTestNeighbor::call_for_single_neighbor(
    const double quality_abuw, const double quality_auxw,
    const double quality_uxwb, const double quality_abux,
    const double quality_abxw, const bool is_g_flippable) const {
  // specify the quality of the tetrahedron created by flipping f
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, u}))
      .Times(AtMost(1))
      .WillOnce(Return(quality_abuw));

  // g is the neighboring face uwx intersecting f at uw
  const auto shared_g =
      SharedFace{{cell_l, shared_f.face.apex}, shared_f.bottom};
  // cell neighbor of cell is cell_l, with apex x:
  EXPECT_CALL(face_mirror, face_mirror(Face{cell, v}))
      .WillRepeatedly(Return(std::optional{Face{cell_l, x}}));
  // cell_l has a face sandwiched by a and b:
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, a}))
      .WillRepeatedly(Return(std::optional{Face{cell_lb, b}}));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, a}))
      .WillOnce(Return(std::array<VertexHandle, 3>({{u, v, w}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_l, a}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{x, u, w}})));

  // assume that there are no neighboring faces of g (except for f)
  const auto no_face = OptionalFace();
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, u}))
      .Times(AtMost(1))
      .WillOnce(Return(no_face));
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, w}))
      .Times(AtMost(1))
      .WillOnce(Return(no_face));

  // flipping g removes old tetrahedra, we specify their quality
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, w, x, u}))
      .Times(AtMost(1))
      .WillOnce(Return(quality_auxw));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{w, x, u, b}))
      .Times(AtMost(1))
      .WillOnce(Return(quality_uxwb));

  // flipping g creates new tetrahedra, we specify their quality
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, x, u}))
      .Times(AtMost(1))
      .WillOnce(Return(quality_abux));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, x}))
      .Times(AtMost(1))
      .WillOnce(Return(quality_abxw));

  // assume that flipping g creates no invalid tetrahedra
  EXPECT_CALL(is_flippable, is_flippable(shared_g))
      .Times(AtMost(1))
      .WillOnce(Return(is_g_flippable));

  return test_neighbor(shared_f, v, QualityMeasure(std::ref(compute_quality)),
                       SharedFacePredicate(std::ref(is_flippable)),
                       FaceVerticesQuery(std::ref(get_vertices)),
                       FaceMirrorQuery(std::ref(face_mirror)));
}

TEST_F(TestTestNeighbor, works_for_a_single_flippable_neighboring_face) {
  const auto quality_abuw = 1.;
  const auto quality_auxw = 2.;
  const auto quality_uxwb = 3.;
  const auto quality_abux = 4.;
  const auto quality_abxw = 5.;
  const auto is_g_flippable = true;

  const auto result =
      call_for_single_neighbor(quality_abuw, quality_auxw, quality_uxwb,
                               quality_abux, quality_abxw, is_g_flippable);

  // note that quality_if_kept does not consider the quality of the tetrahedron
  // generated by flipping f.
  EXPECT_THAT(result.quality_if_kept,
              DoubleEq(multimin(quality_auxw, quality_uxwb)));
  EXPECT_THAT(result.quality_if_removed,
              DoubleEq(multimin(quality_abux, quality_abxw)));
  EXPECT_THAT(result.faces_to_remove, ElementsAre(RemovalInfo{{cell_l, a}, x}));
}

TEST_F(TestTestNeighbor, fails_for_a_single_nonflippable_neighboring_face) {
  const auto quality_abuw = 1.;
  const auto quality_auxw = 2.;
  const auto quality_uxwb = 3.;
  const auto quality_abux = 4.;
  const auto quality_abxw = 5.;
  const auto is_g_flippable = false;

  const auto result =
      call_for_single_neighbor(quality_abuw, quality_auxw, quality_uxwb,
                               quality_abux, quality_abxw, is_g_flippable);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(INF));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(quality_abuw));
  EXPECT_THAT(result.faces_to_remove, IsEmpty());
}

TEST_F(TestTestNeighbor,
       fails_for_a_single_flippable_neighboring_face_if_abux_bad) {
  const auto quality_abuw = 1.;
  const auto quality_auxw = 2.;
  const auto quality_uxwb = 3.;
  const auto quality_abux = 0.;
  const auto quality_abxw = 5.;
  const auto is_g_flippable = true;

  const auto result =
      call_for_single_neighbor(quality_abuw, quality_auxw, quality_uxwb,
                               quality_abux, quality_abxw, is_g_flippable);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(INF));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(quality_abuw));
  EXPECT_THAT(result.faces_to_remove, IsEmpty());
}

TEST_F(TestTestNeighbor,
       fails_for_a_single_flippable_neighboring_face_if_abxw_bad) {
  const auto quality_abuw = 1.;
  const auto quality_auxw = 2.;
  const auto quality_uxwb = 3.;
  const auto quality_abux = 4.;
  const auto quality_abxw = 0.;
  const auto is_g_flippable = true;

  const auto result =
      call_for_single_neighbor(quality_abuw, quality_auxw, quality_uxwb,
                               quality_abux, quality_abxw, is_g_flippable);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(INF));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(quality_abuw));
  EXPECT_THAT(result.faces_to_remove, IsEmpty());
}

Result
TestTestNeighbor::call_for_two_neighbors(const double bad_quality,
                                         const double good_quality,
                                         const double optimal_quality) const {
  // specify bad quality for f
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, u}))
      .WillOnce(Return(bad_quality));

  // specify bad quality for existing state of g
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, w, x, u}))
      .WillOnce(Return(bad_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{w, x, u, b}))
      .WillOnce(Return(bad_quality));

  // specify good quality for replacement of g
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, x, u}))
      .WillOnce(Return(good_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, x}))
      .WillOnce(Return(good_quality));

  // specify good quality for existing state of h
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, x, y, u}))
      .WillOnce(Return(good_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{x, y, u, b}))
      .WillOnce(Return(good_quality));

  // specify good quality for existing state of j
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, w, z, x}))
      .WillOnce(Return(good_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{w, z, x, b}))
      .WillOnce(Return(good_quality));

  // specify optimal quality for replacement of h
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, y, u}))
      .WillOnce(Return(optimal_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, x, y}))
      .WillOnce(Return(optimal_quality));

  // specify optimal quality for replacement of j
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, z, x}))
      .WillOnce(Return(optimal_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, b, w, z}))
      .WillOnce(Return(optimal_quality));

  // assume that there are no other neighboring faces
  const auto no_face = OptionalFace();
  EXPECT_CALL(face_mirror, face_mirror(_)).WillRepeatedly(Return(no_face));

  // adjacent face uwx
  EXPECT_CALL(face_mirror, face_mirror(Face{cell, v}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_l, x}))
      .RetiresOnSaturation();
  // cell_l has a face sandwiched by a and b
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, a}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_lb, b}))
      .RetiresOnSaturation();
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, a}))
      .WillOnce(Return(std::array<VertexHandle, 3>({{u, v, w}})));

  // adjacent face uxy to uwx
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, w}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_ll, y}))
      .RetiresOnSaturation();
  // cell_ll has a face sandwiched by a and b
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_ll, a}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_llb, b}))
      .RetiresOnSaturation();
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_l, a}))
      .Times(AtMost(2))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, w, x}})))
      .RetiresOnSaturation();
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_ll, a}))
      .Times(AtMost(2))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{y, u, x}})))
      .RetiresOnSaturation();

  // adjacent face xwz to uwx
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, u}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_lr, z}))
      .RetiresOnSaturation();
  // cell_lr has a face sandwiched by a and b
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_lr, a}))
      .Times(AtMost(1))
      .WillOnce(Return(Face{cell_lrb, b}))
      .RetiresOnSaturation();
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_lr, a}))
      .Times(AtMost(2))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{x, w, z}})))
      .RetiresOnSaturation();

  // assume that flipping the faces creates no invalid tetrahedra
  EXPECT_CALL(is_flippable, is_flippable(_)).WillRepeatedly(Return(true));

  return test_neighbor(shared_f, v, QualityMeasure(std::ref(compute_quality)),
                       SharedFacePredicate(std::ref(is_flippable)),
                       FaceVerticesQuery(std::ref(get_vertices)),
                       FaceMirrorQuery(std::ref(face_mirror)));
}

TEST_F(TestTestNeighbor, combining_two_neighbor_faces_works) {
  const auto bad_quality = 1.;
  const auto good_quality = 2.;
  const auto optimal_quality = 3.;
  const auto result =
      call_for_two_neighbors(bad_quality, good_quality, optimal_quality);

  EXPECT_THAT(result.quality_if_kept, DoubleEq(bad_quality));
  EXPECT_THAT(result.quality_if_removed, DoubleEq(optimal_quality));
  EXPECT_THAT(result.faces_to_remove,
              ElementsAre(RemovalInfo{{cell_l, a}, x},    // uwx
                          RemovalInfo{{cell_lr, a}, z},   // xwz
                          RemovalInfo{{cell_ll, a}, y})); // yux
}

} // namespace
} // namespace qcmesh::mesh
