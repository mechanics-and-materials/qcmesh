// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/removal_info.hpp"
#include "qcmesh/mesh/mesh_repair/topology/segment.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

using Face = Face<CellHandle, VertexHandle>;
using Segment = Segment<VertexHandle>;
using RemovalInfo = RemovalInfo<Face, Segment>;

TEST(test_removal_info, same_infos_are_equal) {
  const auto x =
      RemovalInfo{Face{1_cell, 3_vertex}, Segment{1_vertex, 2_vertex}};
  const auto y =
      RemovalInfo{Face{1_cell, 3_vertex}, Segment{1_vertex, 2_vertex}};
  EXPECT_THAT(x == y, Eq(true));
  EXPECT_THAT(x != y, Eq(false));
}

TEST(test_removal_info, infos_with_different_face_are_different) {
  const auto x =
      RemovalInfo{Face{1_cell, 3_vertex}, Segment{1_vertex, 2_vertex}};
  const auto y =
      RemovalInfo{Face{7_cell, 3_vertex}, Segment{1_vertex, 2_vertex}};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST(test_removal_info, infos_with_different_edge_are_different) {
  const auto x =
      RemovalInfo{Face{1_cell, 3_vertex}, Segment{1_vertex, 2_vertex}};
  const auto y =
      RemovalInfo{Face{1_cell, 3_vertex}, Segment{7_vertex, 2_vertex}};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

} // namespace
} // namespace qcmesh::mesh
