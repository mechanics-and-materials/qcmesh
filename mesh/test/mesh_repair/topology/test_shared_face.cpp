// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/shared_face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

using SharedFace = SharedFace<CellHandle, VertexHandle>;

TEST(test_shared_face, same_shared_faces_are_equal) {
  const auto x = SharedFace{{1_cell, 2_vertex}, 10_vertex};
  const auto y = SharedFace{{1_cell, 2_vertex}, 10_vertex};
  EXPECT_THAT(x == y, Eq(true));
  EXPECT_THAT(x != y, Eq(false));
}

TEST(test_shared_face, shared_faces_with_different_vertex_are_different) {
  const auto x = SharedFace{{1_cell, 2_vertex}, 10_vertex};
  const auto y = SharedFace{{1_cell, 2_vertex}, 0_vertex};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST(test_shared_face, shared_faces_with_different_face_are_different) {
  const auto x = SharedFace{{1_cell, 2_vertex}, 10_vertex};
  const auto y = SharedFace{{0_cell, 2_vertex}, 10_vertex};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

} // namespace
} // namespace qcmesh::mesh
