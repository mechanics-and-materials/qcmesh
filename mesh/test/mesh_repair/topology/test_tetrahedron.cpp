// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/tetrahedron.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

struct TestTetrahedron : Test {
  using VertexHandle = int;
  using T = Tetrahedron<VertexHandle>;
};

TEST_F(TestTetrahedron, same_tetrahedra_are_equal) {
  const auto x = T{1, 2, 3, 4};
  const auto y = T{1, 2, 3, 4};
  EXPECT_THAT(x == y, Eq(true));
  EXPECT_THAT(x != y, Eq(false));
}

TEST_F(TestTetrahedron, tetrahedra_with_different_vertex_1_are_different) {
  const auto x = T{0, 2, 3, 4};
  const auto y = T{1, 2, 3, 4};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST_F(TestTetrahedron, tetrahedra_with_different_vertex_2_are_different) {
  const auto x = T{1, 0, 3, 4};
  const auto y = T{1, 2, 3, 4};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST_F(TestTetrahedron, tetrahedra_with_different_vertex_3_are_different) {
  const auto x = T{1, 2, 0, 4};
  const auto y = T{1, 2, 3, 4};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST_F(TestTetrahedron, tetrahedra_with_different_vertex_4_are_different) {
  const auto x = T{1, 2, 3, 0};
  const auto y = T{1, 2, 3, 4};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

} // namespace
} // namespace qcmesh::mesh
