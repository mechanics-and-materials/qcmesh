// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "face_mirror_mock.hpp"
#include "face_vertices_query_mock.hpp"
#include "is_flippable_mock.hpp"
#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/segment.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include "quality_measure_mock.hpp"
#include "segment_intersects_face_mock.hpp"
#include <gmock/gmock.h>

// NOLINTBEGIN(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::_;
// NOLINTEND(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::ElementsAre;
using testing::Eq;
using testing::IsEmpty;
using testing::Return;
using testing::StrictMock;
using testing::Test;

namespace qcmesh::mesh {
namespace {

struct Flip23Mock {
  MOCK_CONST_METHOD1(
      flip23, void(const SharedFace<CellHandle, VertexHandle> &shared_f));
  void operator()(const SharedFace<CellHandle, VertexHandle> &f) const {
    return flip23(f);
  }
};

struct Flip32Mock {
  MOCK_CONST_METHOD2(flip32,
                     void(const SharedFace<CellHandle, VertexHandle> &shared_f,
                          const VertexHandle x));
  void operator()(const SharedFace<CellHandle, VertexHandle> &f,
                  const VertexHandle x) const {
    return flip32(f, x);
  }
};

using Face = Face<CellHandle, VertexHandle>;
using SharedFace = SharedFace<CellHandle, VertexHandle>;
using Segment = Segment<VertexHandle>;
using FaceVerticesQuery = FaceVerticesQuery<CellHandle, VertexHandle>;
using Flip23 = Flip23<SharedFace>;
using Flip32 = Flip32<SharedFace, VertexHandle>;
using FaceMirrorQuery = FaceMirrorQuery<CellHandle, VertexHandle>;
using QualityMeasure = QualityMeasure<VertexHandle>;
using SharedFacePredicate = SharedFacePredicate<SharedFace>;
using Tetrahedron = Tetrahedron<VertexHandle>;

using Result = MultifaceRemovalResult<VertexHandle>;
using MeshDifference = FaceRemovalPreview<CellHandle, VertexHandle>;
using OptionalFace = std::optional<Face>;

struct TestRemoveFace : Test {
  const VertexHandle u = 1_vertex, v = 2_vertex, w = 3_vertex;
  const VertexHandle a = 4_vertex, b = 5_vertex;

  const VertexHandle x = 6_vertex;
  const VertexHandle y = 7_vertex;
  const VertexHandle z = 8_vertex;

  const double bad_quality = 0.;
  const double good_quality = 1.;
  const double best_quality = 2.;

  const CellHandle cell = 11_cell, cell_l = 12_cell, cell_b = 13_cell,
                   cell_lb = 14_cell;

  SharedFace shared_f = {{cell, a}, b};

  StrictMock<QualityMeasureMock> compute_quality;
  StrictMock<IsFlippableMock> is_flippable;
  StrictMock<SegmentIntersectsFaceMock> segment_intersects_face;
  StrictMock<FaceVerticesQueryMock> get_vertices;
  StrictMock<FaceMirrorQueryMock> face_mirror;
  StrictMock<Flip23Mock> flip23;
  StrictMock<Flip32Mock> flip32;

  /**
   * Prepares remove_face for a single face f. The current tetrahedra have
   * quality quality_old, the created tetrahedra have quality quality_new.
   */
  void prepare_call_for_no_neighbors(const double quality_old,
                                     const double quality_new,
                                     const bool f_flippable,
                                     const bool ab_intersects_f = true) const;

  /**
   * Prepares remove_face to remove two faces: f and g. The face g is the face
   * that is connected to f by the given edge.
   */
  void prepare_call_for_one_neighbor(const Segment &edge,
                                     const bool ab_intersects_g = true,
                                     const bool expect_flips = true) const;

  /**
   * @brief Calls remove face for the face f.
   */
  Result remove_face_f() const;

  /**
   * @brief Calls remove face for the face f.
   */
  MeshDifference remove_face_f_and_return_diff() const;
};

void TestRemoveFace::prepare_call_for_no_neighbors(
    const double quality_old, const double quality_new, const bool f_flippable,
    const bool ab_intersects_f) const {
  EXPECT_CALL(compute_quality, compute_quality(_))
      .WillRepeatedly(Return(quality_new));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, u, v, w}))
      .WillRepeatedly(Return(quality_old));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{u, v, w, b}))
      .WillRepeatedly(Return(quality_old));

  EXPECT_CALL(is_flippable, is_flippable(shared_f))
      .WillRepeatedly(Return(f_flippable));
  EXPECT_CALL(segment_intersects_face, segment_intersects_face(shared_f))
      .WillRepeatedly(Return(ab_intersects_f));

  EXPECT_CALL(get_vertices, get_vertices(shared_f.face))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, v, w}})));
  // Faces of cell:
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, u}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{v, w, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, v}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, w, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, w}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, v, a}})));

  const auto no_face = OptionalFace();
  EXPECT_CALL(face_mirror, face_mirror(_))
      .WillRepeatedly(Return(no_face)); // cell_b is below cell
  EXPECT_CALL(face_mirror, face_mirror(Face{cell, a}))
      .WillRepeatedly(Return(std::optional{Face{cell_b, b}}));
}

Result TestRemoveFace::remove_face_f() const {
  return remove_face(shared_f, QualityMeasure{std::ref(compute_quality)},
                     SharedFacePredicate{std::ref(is_flippable)},
                     SharedFacePredicate{std::ref(segment_intersects_face)},
                     FaceVerticesQuery{std::ref(get_vertices)},
                     FaceMirrorQuery{std::ref(face_mirror)},
                     Flip23{std::ref(flip23)}, Flip32{std::ref(flip32)});
}

TEST_F(TestRemoveFace, removes_a_single_face_if_it_improves_quality) {
  const auto f_flippable = true;
  EXPECT_CALL(flip23, flip23(shared_f));

  prepare_call_for_no_neighbors(bad_quality, good_quality, f_flippable);
  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{1}));
}

TEST_F(TestRemoveFace, removes_no_face_if_not_flippable) {
  const auto f_flippable = false;
  prepare_call_for_no_neighbors(bad_quality, good_quality, f_flippable);
  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{0}));
}

TEST_F(TestRemoveFace, removes_no_face_if_ab_does_not_intersect_the_face) {
  const auto f_flippable = true;
  const auto ab_intersects_f = false;
  prepare_call_for_no_neighbors(bad_quality, good_quality, f_flippable,
                                ab_intersects_f);
  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{0}));
}

/**
 * Example for edge = u-v:
 *    u---x
 *   /f\ /
 *  w---v
 */
void TestRemoveFace::prepare_call_for_one_neighbor(
    const Segment &edge, const bool ab_intersects_g,
    const bool expect_flips) const {
  // g is the face across the edge
  const auto shared_g = SharedFace{{cell_l, a}, b};

  // best quality for all other tetrahedra
  EXPECT_CALL(compute_quality, compute_quality(_))
      .WillRepeatedly(Return(best_quality));

  // good quality for current state of g
  EXPECT_CALL(compute_quality,
              compute_quality(Tetrahedron{a, edge.a, x, edge.b}))
      .WillOnce(Return(good_quality));
  EXPECT_CALL(compute_quality,
              compute_quality(Tetrahedron{edge.a, x, edge.b, b}))
      .WillOnce(Return(good_quality));

  // bad quality for current state of f
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{a, u, v, w}))
      .WillOnce(Return(bad_quality));
  EXPECT_CALL(compute_quality, compute_quality(Tetrahedron{u, v, w, b}))
      .WillOnce(Return(bad_quality));

  // all faces are flippable
  EXPECT_CALL(is_flippable, is_flippable(_)).WillRepeatedly(Return(true));

  // only g is (maybe) intersected by ab
  EXPECT_CALL(segment_intersects_face, segment_intersects_face(shared_f))
      .WillRepeatedly(Return(false));
  EXPECT_CALL(segment_intersects_face, segment_intersects_face(shared_g))
      .WillRepeatedly(Return(ab_intersects_g));

  // there are no other neighbors
  const auto no_face = OptionalFace();
  EXPECT_CALL(face_mirror, face_mirror(_)).WillRepeatedly(Return(no_face));

  // u, v, w are the vertices of f
  EXPECT_CALL(get_vertices, get_vertices(shared_f.face))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, v, w}})));
  // Faces of cell:
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, u}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{v, w, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, v}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, w, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, w}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{u, v, a}})));

  // {u, v, w} \ edge:
  const auto neighbor_apex =
      (u != edge.a && u != edge.b) ? u : ((v != edge.a && v != edge.b) ? v : w);

  // f has a neighbor connected via the edge
  EXPECT_CALL(face_mirror, face_mirror(Face{cell, neighbor_apex}))
      .WillOnce(Return(Face{cell_l, x}));
  // the neighbor is sandwiched between a and b
  EXPECT_CALL(face_mirror, face_mirror(Face{cell_l, a}))
      .WillRepeatedly(Return(Face{cell_lb, b}));

  // cell_b is below cell
  EXPECT_CALL(face_mirror, face_mirror(Face{cell, a}))
      .WillRepeatedly(Return(std::optional{Face{cell_b, b}}));
  // edge.a, edge.b, a are the vertices of the face opposite of neighbor_apex
  EXPECT_CALL(get_vertices, get_vertices(Face{cell, neighbor_apex}))
      .WillRepeatedly(
          Return(std::array<VertexHandle, 3>({{edge.a, edge.b, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_l, a}))
      .WillRepeatedly(
          Return(std::array<VertexHandle, 3>({{edge.a, x, edge.b}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_l, edge.a}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{edge.b, x, a}})));
  EXPECT_CALL(get_vertices, get_vertices(Face{cell_l, edge.b}))
      .WillRepeatedly(Return(std::array<VertexHandle, 3>({{edge.a, x, a}})));

  if (expect_flips) {
    // both f and g are removed via a flip
    EXPECT_CALL(flip23, flip23(shared_f));
    EXPECT_CALL(flip32, flip32(shared_g, x));
  }
}

TEST_F(TestRemoveFace, removes_two_faces_across_uv) {
  const auto edge = Segment{u, v};

  prepare_call_for_one_neighbor(edge);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{2}));
}

TEST_F(TestRemoveFace, removes_two_faces_across_vw) {
  const auto edge = Segment{v, w};

  prepare_call_for_one_neighbor(edge);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{2}));
}

TEST_F(TestRemoveFace, removes_two_faces_across_wu) {
  const auto edge = Segment{w, u};

  prepare_call_for_one_neighbor(edge);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{2}));
}

TEST_F(TestRemoveFace,
       does_not_remove_a_face_across_uv_if_none_is_intersected_by_ab) {
  const auto edge = Segment{u, v};

  const auto ab_intersects_g = bool{false};
  const auto expect_flips = bool{false};
  prepare_call_for_one_neighbor(edge, ab_intersects_g, expect_flips);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{0}));
}

TEST_F(TestRemoveFace,
       does_not_remove_a_face_across_vw_if_none_is_intersected_by_ab) {
  const auto edge = Segment{v, w};

  const auto ab_intersects_g = bool{false};
  const auto expect_flips = bool{false};
  prepare_call_for_one_neighbor(edge, ab_intersects_g, expect_flips);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{0}));
}

TEST_F(TestRemoveFace,
       does_not_remove_a_face_across_wu_if_none_is_intersected_by_ab) {
  const auto edge = Segment{w, u};

  const auto ab_intersects_g = bool{false};
  const auto expect_flips = bool{false};
  prepare_call_for_one_neighbor(edge, ab_intersects_g, expect_flips);

  EXPECT_THAT(remove_face_f().number_of_removed_faces, Eq(std::size_t{0}));
}

MeshDifference TestRemoveFace::remove_face_f_and_return_diff() const {
  return remove_face(shared_f.face, QualityMeasure{std::ref(compute_quality)},
                     SharedFacePredicate{std::ref(is_flippable)},
                     SharedFacePredicate{std::ref(segment_intersects_face)},
                     FaceVerticesQuery{std::ref(get_vertices)},
                     FaceMirrorQuery{std::ref(face_mirror)});
}

TEST_F(TestRemoveFace, returns_diff_produced_by_23flip_if_f_flipped) {
  const auto flip_f = bool{true};

  prepare_call_for_no_neighbors(bad_quality, good_quality, flip_f);
  const auto result = remove_face_f_and_return_diff();

  EXPECT_THAT(result.out, ElementsAre(cell, cell_b));
  EXPECT_THAT(result.replacement_draft.vertex_ring, ElementsAre(u, v, w));
  EXPECT_THAT(result.replacement_draft.apex_a, Eq(a));
  EXPECT_THAT(result.replacement_draft.apex_b, Eq(b));
}

TEST_F(TestRemoveFace, returns_empty_diff_if_f_not_flipped) {
  const auto flip_f = bool{false};

  prepare_call_for_no_neighbors(bad_quality, good_quality, flip_f);
  const auto result = remove_face_f_and_return_diff();

  EXPECT_THAT(result.out, IsEmpty());
  EXPECT_THAT(result.replacement_draft.vertex_ring, IsEmpty());
}

TEST_F(TestRemoveFace, returns_diff_produced_by_two_flips) {
  const auto edge = Segment{u, v};

  const auto ab_intersects_g = bool{true};
  const auto expect_flips = bool{false};
  prepare_call_for_one_neighbor(edge, ab_intersects_g, expect_flips);
  const auto result = remove_face_f_and_return_diff();

  EXPECT_THAT(result.out, ElementsAre(cell, cell_b, cell_l, cell_lb));
  EXPECT_THAT(result.replacement_draft.vertex_ring, ElementsAre(u, x, v, w));
  EXPECT_THAT(result.replacement_draft.apex_a, Eq(a));
  EXPECT_THAT(result.replacement_draft.apex_b, Eq(b));
}

} // namespace
} // namespace qcmesh::mesh
