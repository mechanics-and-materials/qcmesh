// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_edge.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include "quality_measure_mock.hpp"
#include <algorithm>
#include <gmock/gmock.h>
#include <unordered_set>

namespace qcmesh::mesh {

void PrintTo( // NOLINT(readability-identifier-naming)
    const KlincsekTreeNode &node, std::ostream *os) {
  *os << "{{" << node.triangle[0] << ',' << node.triangle[1] << ','
      << node.triangle[2] << "}, parent=" << node.parent
      << ",apex=" << node.parent_apex
      << '}'; // whatever needed to print bar to os
}

inline bool operator==(const KlincsekTreeNode &a, const KlincsekTreeNode &b) {
  return a.triangle == b.triangle && a.parent == b.parent &&
         a.parent_apex == b.parent_apex;
}

namespace {

TEST(test_ring_vertices,
     ring_vertices_extracts_correct_vertices_for_closed_5_ring) {
  const auto cell_ring = std::vector<Tetrahedron<VertexHandle>>{
      Tetrahedron<VertexHandle>{2_vertex, 6_vertex, 1_vertex, 7_vertex},
      Tetrahedron<VertexHandle>{6_vertex, 2_vertex, 3_vertex, 7_vertex},
      Tetrahedron<VertexHandle>{4_vertex, 7_vertex, 3_vertex, 6_vertex},
      Tetrahedron<VertexHandle>{7_vertex, 6_vertex, 5_vertex, 4_vertex},
      Tetrahedron<VertexHandle>{1_vertex, 5_vertex, 6_vertex, 7_vertex}};
  const auto a = 6_vertex;
  const auto b = 7_vertex;
  const auto ring = ring_vertices(cell_ring, a, b);

  const auto expected_ring = std::vector<VertexHandle>{
      1_vertex, 2_vertex, 3_vertex, 4_vertex, 5_vertex};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_ring_vertices,
     ring_vertices_extracts_correct_vertices_for_closed_3_ring) {
  const auto cell_ring = std::vector<Tetrahedron<VertexHandle>>{
      Tetrahedron<VertexHandle>{1_vertex, 2_vertex, 4_vertex, 5_vertex},
      Tetrahedron<VertexHandle>{2_vertex, 3_vertex, 4_vertex, 5_vertex},
      Tetrahedron<VertexHandle>{3_vertex, 1_vertex, 4_vertex, 5_vertex}};
  const auto a = 4_vertex;
  const auto b = 5_vertex;
  const auto ring = ring_vertices(cell_ring, a, b);

  const auto expected_ring =
      std::vector<VertexHandle>{1_vertex, 2_vertex, 3_vertex};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_ring_vertices,
     ring_vertices_extracts_correct_vertices_for_open_ring) {
  const auto cell_ring = std::vector<Tetrahedron<VertexHandle>>{
      Tetrahedron<VertexHandle>{1_vertex, 2_vertex, 4_vertex, 5_vertex},
      Tetrahedron<VertexHandle>{2_vertex, 3_vertex, 4_vertex, 5_vertex}};
  const auto a = 4_vertex;
  const auto b = 5_vertex;
  const auto ring = ring_vertices(cell_ring, a, b);

  const auto expected_ring =
      std::vector<VertexHandle>{1_vertex, 2_vertex, 3_vertex};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_ring_vertices,
     ring_vertices_extracts_correct_vertices_for_single_cell) {
  const auto cell_ring = std::vector<Tetrahedron<VertexHandle>>{
      Tetrahedron<VertexHandle>{1_vertex, 2_vertex, 4_vertex, 5_vertex}};
  const auto a = 4_vertex;
  const auto b = 5_vertex;
  const auto ring = ring_vertices(cell_ring, a, b);

  const auto expected_ring = std::vector<VertexHandle>{1_vertex, 2_vertex};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_ring_vertices, ring_vertices_extracts_0_vertices_for_empty_ring) {
  const auto cell_ring = std::vector<Tetrahedron<VertexHandle>>{};
  const auto a = 4_vertex;
  const auto b = 5_vertex;
  const auto ring = ring_vertices(cell_ring, a, b);

  const auto expected_ring = std::vector<VertexHandle>{};
  EXPECT_EQ(ring, expected_ring);
}

struct DerefCellHandleMock {
  MOCK_CONST_METHOD1(deref_cell_handle,
                     Tetrahedron<VertexHandle>(const CellHandle &cell_handle));
  Tetrahedron<VertexHandle> operator()(const CellHandle &cell_handle) const {
    return deref_cell_handle(cell_handle);
  }
};
/**
 * @brief An "ID" handle for a vertex.
 */
enum class EdgeHandle : unsigned int {};

constexpr EdgeHandle operator"" _edge(const unsigned long long int x) {
  return static_cast<EdgeHandle>(x);
}

struct DerefEdgeHandleMock {
  MOCK_CONST_METHOD1(deref_edge_handle, std::tuple<VertexHandle, VertexHandle>(
                                            const EdgeHandle &edge_handle));
  std::tuple<VertexHandle, VertexHandle>
  operator()(const EdgeHandle &edge_handle) const {
    return deref_edge_handle(edge_handle);
  }
};
struct CollectRingAroundEdgeMock {
  MOCK_CONST_METHOD1(collect_ring_around_edge,
                     std::vector<CellHandle>(const EdgeHandle &edge_handle));
  std::vector<CellHandle> operator()(const EdgeHandle &edge_handle) const {
    return collect_ring_around_edge(edge_handle);
  }
};
struct OverlapCheckMock {
  MOCK_CONST_METHOD5(overlap_check,
                     bool(const VertexHandle t1, const VertexHandle t2,
                          const VertexHandle t3, const VertexHandle apex_a,
                          const VertexHandle apex_b));
  bool operator()(const VertexHandle t1, const VertexHandle t2,
                  const VertexHandle t3, const VertexHandle apex_a,
                  const VertexHandle apex_b) const {
    return overlap_check(t1, t2, t3, apex_a, apex_b);
  }
};

struct EdgeRemovalScenario {
  std::vector<std::tuple<CellHandle, Tetrahedron<VertexHandle>>> original_cells;
  std::vector<std::tuple<double, Tetrahedron<VertexHandle>>> replacement_cells;
  bool creates_overlapping_tetrahedra;
};

struct TestRemoveEdge : testing::Test {
  const VertexHandle a = 0_vertex, b = 1_vertex;
  const VertexHandle r1 = 2_vertex, r2 = 3_vertex, r3 = 4_vertex, r4 = 5_vertex,
                     r5 = 6_vertex, r6 = 7_vertex, r7 = 8_vertex;
  const CellHandle c1 = 1_cell, c2 = 2_cell, c3 = 3_cell, c4 = 4_cell,
                   c5 = 5_cell, c6 = 6_cell, c7 = 7_cell;
  const EdgeHandle e = 0_edge;

  const double bad_quality = 0.;
  const double good_quality = 1.;
  const double best_quality = 2.;

  testing::StrictMock<DerefCellHandleMock> deref_cell_handle;
  testing::StrictMock<DerefEdgeHandleMock> deref_edge_handle;
  testing::StrictMock<CollectRingAroundEdgeMock> collect_ring_around_edge;
  testing::StrictMock<QualityMeasureMock> compute_quality;
  testing::StrictMock<OverlapCheckMock> overlap_check;

  /**
   * @brief Prepares remove_edge for a single cell.
   */
  void prepare_call_for_1_cell() const;
  /**
   * @brief Prepares remove_edge for a scenario consisting of input cells and
   * qualities for potential replacements.
   */
  void prepare_calls_for_scenario(const EdgeRemovalScenario &scenario) const;

  /**
   * @brief Call remove_edge(), with all our mocks as arguments.
   */
  EdgeRemovalPreview<VertexHandle, CellHandle> call_remove_edge() const;
};

EdgeRemovalPreview<VertexHandle, CellHandle>
TestRemoveEdge::call_remove_edge() const {
  return remove_edge<CellHandle, VertexHandle>(
      e, std::ref(deref_cell_handle), std::ref(deref_edge_handle),
      std::ref(collect_ring_around_edge), std::ref(compute_quality),
      std::ref(overlap_check));
}

void TestRemoveEdge::prepare_calls_for_scenario(
    const EdgeRemovalScenario &scenario) const {
  // Edge is a-b:
  EXPECT_CALL(deref_edge_handle, deref_edge_handle(e))
      .WillRepeatedly(testing::Return(std::make_tuple(a, b)));
  // Define ring and the vertices of its cells:
  auto cells = std::vector<CellHandle>{};
  cells.reserve(scenario.original_cells.size());
  for (const auto &[cell_id, cell] : scenario.original_cells) {
    cells.push_back(cell_id);
    EXPECT_CALL(deref_cell_handle, deref_cell_handle(cell_id))
        .WillRepeatedly(testing::Return(cell));
  }
  EXPECT_CALL(collect_ring_around_edge, collect_ring_around_edge(e))
      .WillRepeatedly(testing::Return(cells));

  // Fix qualities for potential replacements
  // All not explicitly specified candidates have good qualities:
  EXPECT_CALL(compute_quality, compute_quality(testing::_))
      .WillRepeatedly(testing::Return(good_quality));
  // All explicitly specified candidates will return specific qualities:
  for (const auto &[cell_quality, cell] : scenario.replacement_cells) {
    EXPECT_CALL(compute_quality, compute_quality(cell))
        .Times(testing::AtLeast(1))
        .WillRepeatedly(testing::Return(cell_quality));
  }
  if (!scenario.replacement_cells.empty()) {
    EXPECT_CALL(overlap_check, overlap_check(testing::_, testing::_, testing::_,
                                             testing::_, testing::_))
        .WillOnce(testing::Return(!scenario.creates_overlapping_tetrahedra))
        .WillRepeatedly(testing::Return(true));
  }
}

TEST_F(TestRemoveEdge, remove_edge_suggest_no_change_for_0_cells) {
  prepare_calls_for_scenario(
      EdgeRemovalScenario{.original_cells = {},
                          .replacement_cells = {},
                          .creates_overlapping_tetrahedra = false});
  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.replacement_draft.vertex_ring, testing::IsEmpty());
  EXPECT_THAT(preview.out, testing::IsEmpty());
}

void TestRemoveEdge::prepare_call_for_1_cell() const {
  // Edge is a-b:
  EXPECT_CALL(deref_edge_handle, deref_edge_handle(e))
      .WillRepeatedly(testing::Return(std::make_tuple(a, b)));
  // Ring has one cell:
  EXPECT_CALL(collect_ring_around_edge, collect_ring_around_edge(e))
      .WillRepeatedly(testing::Return(std::vector{c1}));
}
TEST_F(TestRemoveEdge, remove_edge_removes_cell_for_ring_of_size_1) {
  prepare_call_for_1_cell();
  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.replacement_draft.vertex_ring, testing::IsEmpty());
  EXPECT_THAT(preview.out, testing::ElementsAre(c1));
  EXPECT_DOUBLE_EQ(preview.replacement_quality,
                   std::numeric_limits<double>::max());
}

TEST_F(TestRemoveEdge, remove_edge_replaces_cells_for_ring_of_size_3) {
  prepare_calls_for_scenario(EdgeRemovalScenario{
      .original_cells = {{c1, Tetrahedron<VertexHandle>{a, b, r1, r2}},
                         {c2, Tetrahedron<VertexHandle>{a, b, r2, r3}},
                         {c3, Tetrahedron<VertexHandle>{a, b, r3, r1}}},
      .replacement_cells = {{best_quality,
                             Tetrahedron<VertexHandle>{r1, r2, r3, b}}},
      .creates_overlapping_tetrahedra = false});

  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.out, testing::ElementsAre(c1, c2, c3));
  EXPECT_DOUBLE_EQ(preview.replacement_quality, good_quality);
  EXPECT_EQ(preview.replacement_draft.apex_a, a);
  EXPECT_EQ(preview.replacement_draft.apex_b, b);
  EXPECT_THAT(preview.replacement_draft.vertex_ring,
              testing::ElementsAre(r1, r2, r3));
  EXPECT_THAT(
      preview.replacement_draft.klincsek_tree,
      testing::ElementsAre(testing::Field(&KlincsekTreeNode::triangle,
                                          Triangle<std::size_t>{0, 1, 2})));
}

TEST_F(TestRemoveEdge, remove_edge_rejects_overlapping_tetrahedra) {
  prepare_calls_for_scenario(EdgeRemovalScenario{
      .original_cells = {{c1, Tetrahedron<VertexHandle>{a, b, r1, r2}},
                         {c2, Tetrahedron<VertexHandle>{a, b, r2, r3}},
                         {c3, Tetrahedron<VertexHandle>{a, b, r3, r1}}},
      .replacement_cells = {{best_quality,
                             Tetrahedron<VertexHandle>{r1, r2, r3, b}}},
      .creates_overlapping_tetrahedra = true});

  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.out, testing::IsEmpty());
}

TEST_F(TestRemoveEdge, remove_edge_replaces_cells_for_open_ring_of_size_3) {
  prepare_calls_for_scenario(EdgeRemovalScenario{
      .original_cells = {{c1, Tetrahedron<VertexHandle>{a, b, r1, r2}},
                         {c2, Tetrahedron<VertexHandle>{a, b, r2, r3}}},
      .replacement_cells = {{best_quality,
                             Tetrahedron<VertexHandle>{r1, r2, r3, b}}},
      .creates_overlapping_tetrahedra = false});

  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.out, testing::ElementsAre(c1, c2));
  EXPECT_DOUBLE_EQ(preview.replacement_quality, good_quality);
  EXPECT_EQ(preview.replacement_draft.apex_a, a);
  EXPECT_EQ(preview.replacement_draft.apex_b, b);
  EXPECT_THAT(preview.replacement_draft.vertex_ring,
              testing::ElementsAre(r1, r2, r3));
  EXPECT_THAT(
      preview.replacement_draft.klincsek_tree,
      testing::ElementsAre(testing::Field(&KlincsekTreeNode::triangle,
                                          Triangle<std::size_t>{0, 1, 2})));
}

TEST_F(TestRemoveEdge, remove_edge_replaces_cells_for_ring_of_size_5) {
  /**
   * Here we prepare the quality measure such that
   * the following triangulation should be selected, by penalizing
   * the triangles (r1,r2,r5) and (r1,r2,r3).
   *
   *    __ r4__
   *  r5  / \  r3
   *    \/   \/
   *    r1---r2
   */
  prepare_calls_for_scenario(EdgeRemovalScenario{
      .original_cells = {{c1, Tetrahedron<VertexHandle>{a, b, r1, r2}},
                         {c2, Tetrahedron<VertexHandle>{a, b, r2, r3}},
                         {c3, Tetrahedron<VertexHandle>{a, b, r3, r4}},
                         {c4, Tetrahedron<VertexHandle>{a, b, r4, r5}},
                         {c5, Tetrahedron<VertexHandle>{a, b, r5, r1}}},
      .replacement_cells =
          {{best_quality, Tetrahedron<VertexHandle>{r1, r2, r4, b}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r5}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r3}}},
      .creates_overlapping_tetrahedra = false});

  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.out, testing::ElementsAre(c1, c2, c3, c4, c5));
  EXPECT_DOUBLE_EQ(preview.replacement_quality, good_quality);
  EXPECT_EQ(preview.replacement_draft.apex_a, a);
  EXPECT_EQ(preview.replacement_draft.apex_b, b);
  EXPECT_THAT(preview.replacement_draft.vertex_ring,
              testing::ElementsAre(r1, r2, r3, r4, r5));
  EXPECT_THAT(preview.replacement_draft.klincsek_tree,
              testing::ElementsAre(
                  testing::Field(&KlincsekTreeNode::triangle,
                                 Triangle<std::size_t>{0, 3, 4}),
                  KlincsekTreeNode{Triangle<std::size_t>{0, 1, 3}, 0, 2},
                  KlincsekTreeNode{Triangle<std::size_t>{1, 2, 3}, 1, 0}));
}

TEST_F(TestRemoveEdge, remove_edge_replaces_cells_for_ring_of_size_7) {
  /**
   * Here we prepare the quality measure such that
   * the following triangulation should be selected, by penalizing
   * the triangles (r1,r2,r3), (r1,r2,r5), (r1,r2,r6), (r1,r2,r7), (r1,r4,r5).
   *
   *      r5
   *     / \
   *   r6---r4
   *   /|  /|\
   * r7 | / | r3
   *   \|/  |/
   *   r1---r2
   */
  prepare_calls_for_scenario(EdgeRemovalScenario{
      .original_cells = {{c1, Tetrahedron<VertexHandle>{a, b, r1, r2}},
                         {c2, Tetrahedron<VertexHandle>{a, b, r2, r3}},
                         {c3, Tetrahedron<VertexHandle>{a, b, r3, r4}},
                         {c4, Tetrahedron<VertexHandle>{a, b, r4, r5}},
                         {c5, Tetrahedron<VertexHandle>{a, b, r5, r6}},
                         {c6, Tetrahedron<VertexHandle>{a, b, r6, r7}},
                         {c7, Tetrahedron<VertexHandle>{a, b, r7, r1}}},
      .replacement_cells =
          {{best_quality, Tetrahedron<VertexHandle>{r1, r2, r4, b}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r3}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r5}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r6}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r2, r7}},
           {bad_quality, Tetrahedron<VertexHandle>{a, r1, r4, r5}}},
      .creates_overlapping_tetrahedra = false});

  const auto preview = call_remove_edge();

  EXPECT_THAT(preview.out, testing::ElementsAre(c1, c2, c3, c4, c5, c6, c7));
  EXPECT_DOUBLE_EQ(preview.replacement_quality, good_quality);
  EXPECT_EQ(preview.replacement_draft.apex_a, a);
  EXPECT_EQ(preview.replacement_draft.apex_b, b);
  EXPECT_THAT(preview.replacement_draft.vertex_ring,
              testing::ElementsAre(r1, r2, r3, r4, r5, r6, r7));
  EXPECT_THAT(preview.replacement_draft.klincsek_tree,
              testing::ElementsAre(
                  testing::Field(&KlincsekTreeNode::triangle,
                                 Triangle<std::size_t>{0, 5, 6}),
                  KlincsekTreeNode{Triangle<std::size_t>{0, 3, 5}, 0, 2},
                  KlincsekTreeNode{Triangle<std::size_t>{0, 1, 3}, 1, 2},
                  KlincsekTreeNode{Triangle<std::size_t>{3, 4, 5}, 1, 0},
                  KlincsekTreeNode{Triangle<std::size_t>{1, 2, 3}, 2, 0}));
}

} // namespace
} // namespace qcmesh::mesh
