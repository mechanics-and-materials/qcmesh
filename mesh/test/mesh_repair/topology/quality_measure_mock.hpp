// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/tetrahedron.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

struct QualityMeasureMock {
  MOCK_CONST_METHOD1(compute_quality,
                     double(const Tetrahedron<VertexHandle> &));
  double operator()(const Tetrahedron<VertexHandle> &x) const {
    return compute_quality(x);
  }
};

} // namespace qcmesh::mesh
