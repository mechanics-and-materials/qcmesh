// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/multimin.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

struct TestMultimin : Test {};

TEST_F(TestMultimin, minimum_of_7_is_7) { EXPECT_THAT(multimin(7), Eq(7)); }

TEST_F(TestMultimin, minimum_of_7_8_is_7) {
  EXPECT_THAT(multimin(7, 8), Eq(7));
}

TEST_F(TestMultimin, minimum_of_7_8_3_is_3) {
  EXPECT_THAT(multimin(7, 8, 3), Eq(3));
}

} // namespace
} // namespace qcmesh::mesh
