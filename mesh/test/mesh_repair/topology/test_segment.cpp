// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/segment.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

using VertexHandle = int;
using Segment = Segment<VertexHandle>;

TEST(test_segment, same_segments_are_equal) {
  const auto x = Segment{1, 2};
  const auto y = Segment{1, 2};
  EXPECT_THAT(x == y, Eq(true));
  EXPECT_THAT(x != y, Eq(false));
}

TEST(test_segment, segments_with_different_vertex_1_are_different) {
  const auto x = Segment{0, 2};
  const auto y = Segment{1, 2};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST(test_segment, segments_with_different_vertex_2_are_different) {
  const auto x = Segment{1, 0};
  const auto y = Segment{1, 2};
  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

} // namespace
} // namespace qcmesh::mesh
