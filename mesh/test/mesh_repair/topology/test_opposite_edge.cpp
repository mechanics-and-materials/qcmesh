// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/topology/cell_handle.hpp"
#include "qcmesh/mesh/mesh_repair/topology/remove_face.hpp"
#include "qcmesh/mesh/mesh_repair/topology/vertex_handle.hpp"
#include <gmock/gmock.h>

using testing::Test;

namespace qcmesh::mesh {
namespace {

struct TestOppositeEdge : Test {};

TEST_F(TestOppositeEdge, returns_correct_edge_when_apex_is_first_vertex) {
  const auto result = opposite_edge({{1_vertex, 2_vertex, 3_vertex}}, 1_vertex);
  EXPECT_EQ(result, std::make_tuple(2_vertex, 3_vertex));
}

TEST_F(TestOppositeEdge, returns_correct_edge_when_apex_is_second_vertex) {
  const auto result = opposite_edge({{1_vertex, 2_vertex, 3_vertex}}, 2_vertex);
  EXPECT_EQ(result, std::make_tuple(3_vertex, 1_vertex));
}

TEST_F(TestOppositeEdge, returns_correct_edge_when_apex_is_third_vertex) {
  const auto result = opposite_edge({{1_vertex, 2_vertex, 3_vertex}}, 3_vertex);
  EXPECT_EQ(result, std::make_tuple(1_vertex, 2_vertex));
}

} // namespace
} // namespace qcmesh::mesh
