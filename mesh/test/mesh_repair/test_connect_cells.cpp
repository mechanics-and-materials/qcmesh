// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/connect_cells.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

struct ConnectCellsTest : ::testing::Test {
  SimplexCell<3, EmptyData> cell_a = SimplexCell<3, EmptyData>{
      .id = 51_cell,
      .process_rank = 3,
      .nodes = std::array{2_vx, 3_vx, 5_vx, 7_vx},
      .neighbors = std::array<std::optional<SimplexCellId>, 4>{
          10_cell, std::nullopt, std::nullopt, 11_cell}};
  SimplexCell<3, EmptyData> cell_b = SimplexCell<3, EmptyData>{
      .id = 42_cell,
      .process_rank = 3,
      .nodes = std::array{0_vx, 3_vx, 5_vx, 7_vx},
      .neighbors = std::array<std::optional<SimplexCellId>, 4>{
          0_cell, std::nullopt, std::nullopt, 1_cell}};
};

TEST_F(ConnectCellsTest, connect_cells_works) {
  connect_cells(cell_a, 1, cell_b, 3);
  const auto expected_neighbors_a = std::array<std::optional<SimplexCellId>, 4>{
      10_cell, 42_cell, std::nullopt, 11_cell};
  const auto expected_neighbors_b = std::array<std::optional<SimplexCellId>, 4>{
      0_cell, std::nullopt, std::nullopt, 51_cell};
  EXPECT_EQ(cell_a.neighbors, expected_neighbors_a);
  EXPECT_EQ(cell_b.neighbors, expected_neighbors_b);
}

} // namespace
} // namespace qcmesh::mesh
