// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/create_lattice_mesh.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {
namespace {

TEST(test_create_lattice_mesh, lattice_vertices_from_lattice_points_works) {
  const auto g = 1.;
  const auto basis = std::array{std::array<double, 3>{g, g, 0.},
                                std::array<double, 3>{0., g, g},
                                std::array<double, 3>{g, 0., g}};
  const auto lattice_points =
      std::vector{std::array<int, 3>{1, 4, -6}, std::array<int, 3>{1, 3, -6},
                  std::array<int, 3>{1, 3, -5}, std::array<int, 3>{0, 4, -5},
                  std::array<int, 3>{2, 4, -6}};
  const auto lattice_vertices =
      lattice_vertices_from_lattice_points(lattice_points, basis);

  const auto expected_lattice_vertices = std::vector{
      ::testing::Field(&LatticeVertex::position, qcmesh::testing::DoubleArrayEq(
                                                     std::array{-5., 5., -2.})),
      ::testing::Field(&LatticeVertex::position, qcmesh::testing::DoubleArrayEq(
                                                     std::array{-5., 4., -3.})),
      ::testing::Field(&LatticeVertex::position, qcmesh::testing::DoubleArrayEq(
                                                     std::array{-4., 4., -2.})),
      ::testing::Field(&LatticeVertex::position, qcmesh::testing::DoubleArrayEq(
                                                     std::array{-5., 4., -1.})),
      ::testing::Field(
          &LatticeVertex::position,
          qcmesh::testing::DoubleArrayEq(std::array{-4., 6., -2.}))};
  EXPECT_THAT(lattice_vertices,
              ::testing::ElementsAreArray(expected_lattice_vertices));
}

TEST(test_create_lattice_mesh, create_lattice_mesh_works) {
  const auto g = 1.;
  const auto basis = std::array{std::array<double, 3>{g, 0., 0.},
                                std::array<double, 3>{0., g, 0.},
                                std::array<double, 3>{0., 0., g}};
  const auto unit_connectivity = std::vector<std::array<std::size_t, 4>>{
      {0b000, 0b100, 0b010, 0b001}, {0b111, 0b011, 0b101, 0b110}};
  const auto bare_mesh = create_lattice_mesh(2, 3, 4, basis, unit_connectivity);

  const auto lc = [](const auto i, const auto j, const auto k) {
    return std::array{i, j, k};
  };
  const auto lat_vec = [](const auto i, const auto j, const auto k) {
    return ::testing::Field(&LatticeVertex::lattice_coordinates,
                            ::testing::ContainerEq(std::array{i, j, k}));
  };
  const auto expected_lattice_vertices = std::vector{
      lat_vec(0, 0, 0), lat_vec(0, 0, 1), lat_vec(0, 0, 2), lat_vec(0, 0, 3),
      lat_vec(0, 1, 0), lat_vec(0, 1, 1), lat_vec(0, 1, 2), lat_vec(0, 1, 3),
      lat_vec(0, 2, 0), lat_vec(0, 2, 1), lat_vec(0, 2, 2), lat_vec(0, 2, 3),
      lat_vec(1, 0, 0), lat_vec(1, 0, 1), lat_vec(1, 0, 2), lat_vec(1, 0, 3),
      lat_vec(1, 1, 0), lat_vec(1, 1, 1), lat_vec(1, 1, 2), lat_vec(1, 1, 3),
      lat_vec(1, 2, 0), lat_vec(1, 2, 1), lat_vec(1, 2, 2), lat_vec(1, 2, 3)};
  EXPECT_THAT(bare_mesh.vertices,
              ::testing::ElementsAreArray(expected_lattice_vertices));
  const auto expected_cells = std::vector<std::array<std::array<int, 3>, 4>>{
      // offset: (0,0,0)
      {lc(0, 0, 0), lc(1, 0, 0), lc(0, 1, 0), lc(0, 0, 1)},
      {lc(1, 1, 1), lc(0, 1, 1), lc(1, 0, 1), lc(1, 1, 0)},
      // offset: (0,0,1)
      {lc(0, 0, 1), lc(1, 0, 1), lc(0, 1, 1), lc(0, 0, 2)},
      {lc(1, 1, 2), lc(0, 1, 2), lc(1, 0, 2), lc(1, 1, 1)},
      // offset: (0,0,2)
      {lc(0, 0, 2), lc(1, 0, 2), lc(0, 1, 2), lc(0, 0, 3)},
      {lc(1, 1, 3), lc(0, 1, 3), lc(1, 0, 3), lc(1, 1, 2)},
      // offset: (0,1,0)
      {lc(0, 1, 0), lc(1, 1, 0), lc(0, 2, 0), lc(0, 1, 1)},
      {lc(1, 2, 1), lc(0, 2, 1), lc(1, 1, 1), lc(1, 2, 0)},
      // offset: (0,1,1)
      {lc(0, 1, 1), lc(1, 1, 1), lc(0, 2, 1), lc(0, 1, 2)},
      {lc(1, 2, 2), lc(0, 2, 2), lc(1, 1, 2), lc(1, 2, 1)},
      // offset: (0,1,2)
      {lc(0, 1, 2), lc(1, 1, 2), lc(0, 2, 2), lc(0, 1, 3)},
      {lc(1, 2, 3), lc(0, 2, 3), lc(1, 1, 3), lc(1, 2, 2)}};
  const auto actual_cells = [&]() {
    auto out = std::vector<std::array<std::array<int, 3>, 4>>{};
    out.reserve(bare_mesh.cells.size());
    for (const auto &[a, b, c, d] : bare_mesh.cells)
      out.emplace_back(std::array{bare_mesh.vertices[a].lattice_coordinates,
                                  bare_mesh.vertices[b].lattice_coordinates,
                                  bare_mesh.vertices[c].lattice_coordinates,
                                  bare_mesh.vertices[d].lattice_coordinates});
    return out;
  }();
  EXPECT_THAT(actual_cells,
              ::testing::UnorderedElementsAreArray(expected_cells));
}
} // namespace
} // namespace qcmesh::mesh::testing
