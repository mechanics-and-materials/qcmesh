// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {
namespace {

TEST(test_topological_cell,
     topological_cells_are_equivalent_if_they_are_equal) {
  const auto t1 = TopologicalCell<4>{0_vx, 1_vx, 2_vx, 3_vx};
  const auto t2 = TopologicalCell<4>{0_vx, 1_vx, 2_vx, 3_vx};
  EXPECT_EQ(t1, t2);
}
TEST(
    test_topological_cell,
    topological_cells_are_equivalent_if_vertices_are_permuted_with_even_permutation) {
  const auto t1 = TopologicalCell<4>{0_vx, 1_vx, 2_vx, 3_vx};
  const auto t2 = TopologicalCell<4>{0_vx, 3_vx, 1_vx, 2_vx};
  EXPECT_EQ(t1, t2);
}
TEST(test_topological_cell,
     topological_cells_are_not_equivalent_if_two_vertices_are_swapped) {
  const auto t1 = TopologicalCell<4>{0_vx, 1_vx, 2_vx, 3_vx};
  const auto t2 = TopologicalCell<4>{1_vx, 0_vx, 2_vx, 3_vx};
  EXPECT_NE(t1, t2);
}

TEST(test_topological_cell, topological_cell_is_not_smaller_than_self) {
  const auto t = TopologicalCell<4>{0_vx, 1_vx, 2_vx, 3_vx};
  EXPECT_FALSE(t < t);
}
TEST(test_topological_cell, topological_cell_is_smaller_for_flipped_vertices) {
  const auto t1 = TopologicalCell<4>{0_vx, 3_vx, 2_vx, 1_vx}; // -> {0, 1, 3, 2}
  const auto t2 = TopologicalCell<4>{3_vx, 0_vx, 2_vx, 1_vx}; // -> {0, 1, 2, 3}
  EXPECT_TRUE(t2 < t1);
  EXPECT_FALSE(t1 < t2);
}

TEST(test_topological_cell, topological_cells_returns_correct_cells) {
  const auto t1 = TopologicalCell<4>{0_vx, 3_vx, 2_vx, 1_vx}; // -> {0, 1, 3, 2}
  const auto t2 = TopologicalCell<4>{3_vx, 0_vx, 2_vx, 1_vx}; // -> {0, 1, 2, 3}
  const auto cells =
      std::unordered_map<SimplexCellId, SimplexCell<3, EmptyData>>{
          {0_cell,
           SimplexCell<3, EmptyData>{.id = 0_cell, .nodes = t1.node_ids}},
          {1_cell,
           SimplexCell<3, EmptyData>{.id = 1_cell, .nodes = t2.node_ids}}};
  const auto topological_cells = collect_local_topological_cells(cells);
  const auto expected_topological_cells = std::set{t1, t2};
  EXPECT_THAT(topological_cells,
              ::testing::ContainerEq(expected_topological_cells));
}

} // namespace
} // namespace qcmesh::mesh::testing
