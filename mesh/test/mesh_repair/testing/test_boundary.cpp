// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/boundary.hpp"
#include <array>
#include <gmock/gmock.h>
#include <tuple>

namespace qcmesh::mesh::testing {
namespace {

using Vertex = NodeId;
using Tetrahedron = std::array<NodeId, 4>;
using Face = std::array<NodeId, 3>;
using Mesh = std::set<Tetrahedron>;
using Boundary = std::set<Face>;

std::unordered_map<SimplexCellId, SimplexCell<3, EmptyData>>
to_mesh(const std::set<Tetrahedron> &cells) {
  using Cell = SimplexCell<3, EmptyData>;
  auto mesh = std::unordered_map<SimplexCellId, Cell>{};
  auto id = std::size_t{0};
  for (const auto &cell : cells) {
    const auto cell_id = SimplexCellId{id++};
    mesh.emplace(cell_id, Cell{.id = cell_id, .nodes = cell});
  }
  return mesh;
}

/**
 * @brief Test suite for testing the function boundary_faces
 */
struct TestBoundary : ::testing::TestWithParam<std::tuple<Mesh, Boundary>> {};
TEST_P(TestBoundary, boundary_works) {
  const auto [cells, expected_boundary] = GetParam();
  const auto boundary = boundary_faces<Face>(cells, detail::cell_faces);
  EXPECT_EQ(boundary, expected_boundary);
}

std::tuple<Mesh, Boundary> ex_3_ring() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 1_vx, 2_vx, 3_vx}, // {0,1,2}, {0,1,3}, {0,2,3}, {1,2,3}
          {0_vx, 1_vx, 2_vx, 4_vx}, // {0,1,2}, {0,1,4}, {0,2,4}, {1,2,4}
          {0_vx, 1_vx, 3_vx, 4_vx}, // {0,1,3}, {0,1,4}, {0,3,4}, {1,3,4}
      },
      std::set<Face>{{0_vx, 2_vx, 3_vx},
                     {1_vx, 2_vx, 3_vx},
                     {0_vx, 2_vx, 4_vx},
                     {1_vx, 2_vx, 4_vx},
                     {0_vx, 3_vx, 4_vx},
                     {1_vx, 3_vx, 4_vx}});
}
std::tuple<Mesh, Boundary> ex_3_chain() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 1_vx, 2_vx, 3_vx}, // {0,1,2}, {0,1,3}, {0,2,3}, {1,2,3}
          {0_vx, 1_vx, 2_vx, 4_vx}, // {0,1,2}, {0,1,4}, {0,2,4}, {1,2,4}
          {0_vx, 1_vx, 3_vx, 5_vx}, // {0,1,3}, {0,1,5}, {0,3,5}, {1,3,5}
      },
      std::set<Face>{{0_vx, 2_vx, 3_vx},
                     {1_vx, 2_vx, 3_vx},
                     {0_vx, 1_vx, 4_vx},
                     {0_vx, 2_vx, 4_vx},
                     {1_vx, 2_vx, 4_vx},
                     {0_vx, 1_vx, 5_vx},
                     {0_vx, 3_vx, 5_vx},
                     {1_vx, 3_vx, 5_vx}});
}
/**
 * @brief Results from removing edge {0,1} from ex_3_ring
 */
std::tuple<Mesh, Boundary> ex_4_ring() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 2_vx, 3_vx, 5_vx}, // {0,2,3}, {0,2,5}, {0,3,5}, {2,3,5}
          {1_vx, 2_vx, 3_vx, 5_vx}, // {1,2,3}, {1,2,5}, {1,3,5}, {2,3,5}
          {0_vx, 2_vx, 4_vx, 5_vx}, // {0,2,4}, {0,2,5}, {0,4,5}, {2,4,5}
          {1_vx, 2_vx, 4_vx, 5_vx}, // {1,2,4}, {1,2,5}, {1,4,5}, {2,4,5}
      },
      std::set<Face>{{0_vx, 2_vx, 3_vx},
                     {0_vx, 3_vx, 5_vx},
                     {1_vx, 2_vx, 3_vx},
                     {1_vx, 3_vx, 5_vx},
                     {0_vx, 2_vx, 4_vx},
                     {0_vx, 4_vx, 5_vx},
                     {1_vx, 2_vx, 4_vx},
                     {1_vx, 4_vx, 5_vx}});
}
std::tuple<Mesh, Boundary> ex_tetrahedron_in_tetrahedron() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 1_vx, 2_vx, 3_vx}, // Center
          {0_vx, 1_vx, 2_vx, 4_vx}, // Face neighbors
          {0_vx, 1_vx, 3_vx, 5_vx},
          {0_vx, 2_vx, 3_vx, 6_vx},
          {1_vx, 2_vx, 3_vx, 7_vx},
          {0_vx, 1_vx, 4_vx, 5_vx}, // Edge neighbors
          {0_vx, 2_vx, 4_vx, 6_vx},
          {1_vx, 2_vx, 4_vx, 7_vx},
          {0_vx, 3_vx, 5_vx, 6_vx},
          {1_vx, 3_vx, 5_vx, 7_vx},
          {2_vx, 3_vx, 6_vx, 7_vx},
          {0_vx, 4_vx, 5_vx, 6_vx}, // Vertex neighbors
          {1_vx, 4_vx, 5_vx, 7_vx},
          {2_vx, 4_vx, 6_vx, 7_vx},
          {3_vx, 5_vx, 6_vx, 7_vx},
      },
      std::set<Face>{{4_vx, 5_vx, 6_vx},
                     {4_vx, 5_vx, 7_vx},
                     {4_vx, 6_vx, 7_vx},
                     {5_vx, 6_vx, 7_vx}});
}
INSTANTIATE_TEST_CASE_P(Boundary, TestBoundary,
                        ::testing::Values(ex_3_chain(), ex_3_ring(),
                                          ex_4_ring(),
                                          ex_tetrahedron_in_tetrahedron()));

TEST(test_boundary, boundary_faces_finds_no_boundary_for_2_stacked_flat_cells) {
  const auto mesh = std::set<Tetrahedron>{
      {0_vx, 1_vx, 2_vx, 3_vx},
      {0_vx, 1_vx, 3_vx, 2_vx},
  };
  const auto boundary = boundary_faces<Face>(mesh, detail::cell_faces);
  EXPECT_THAT(boundary, ::testing::IsEmpty());
}

/**
 * @brief Test suite for testing the function boundary_vertices.
 */
struct TestBoundaryVertices
    : ::testing::TestWithParam<std::tuple<Mesh, std::set<Vertex>>> {};
TEST_P(TestBoundaryVertices, boundary_vertices_works) {
  const auto [cells, expected_vertices] = GetParam();
  const auto vertices = boundary_vertices(to_mesh(cells));
  EXPECT_EQ(vertices, expected_vertices);
}

/**
 * @brief ex_3_ring
 */
std::tuple<Mesh, std::set<Vertex>> ex_boundary_vertices_3_ring() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 1_vx, 2_vx, 3_vx},
          {0_vx, 1_vx, 2_vx, 4_vx},
          {0_vx, 1_vx, 3_vx, 5_vx},
      },
      std::set<Vertex>{0_vx, 1_vx, 2_vx, 3_vx, 4_vx, 5_vx});
}
/**
 * @brief ex_4_ring
 */
std::tuple<Mesh, std::set<Vertex>> ex_boundary_vertices_4_ring() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 2_vx, 3_vx, 5_vx},
          {1_vx, 2_vx, 3_vx, 5_vx},
          {0_vx, 2_vx, 4_vx, 5_vx},
          {1_vx, 2_vx, 4_vx, 5_vx},
      },
      std::set<Vertex>{0_vx, 1_vx, 2_vx, 3_vx, 4_vx, 5_vx});
}

/**
 * @brief Central tetrahedron, surrounded by 14 other tetrahedra, forming a big
 * tetrahedron
 */
std::tuple<Mesh, std::set<Vertex>> ex_boundary_vertex_composed_tetrahedron() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          {0_vx, 1_vx, 2_vx, 3_vx}, // Center
          {0_vx, 1_vx, 2_vx, 4_vx}, // Face neighbors
          {0_vx, 1_vx, 3_vx, 5_vx},
          {0_vx, 2_vx, 3_vx, 6_vx},
          {1_vx, 2_vx, 3_vx, 7_vx},
          {0_vx, 1_vx, 4_vx, 5_vx}, // Edge neighbors
          {0_vx, 2_vx, 4_vx, 6_vx},
          {1_vx, 2_vx, 4_vx, 7_vx},
          {0_vx, 3_vx, 5_vx, 6_vx},
          {1_vx, 3_vx, 5_vx, 7_vx},
          {2_vx, 3_vx, 6_vx, 7_vx},
          {0_vx, 4_vx, 5_vx, 6_vx}, // Vertex neighbors
          {1_vx, 4_vx, 5_vx, 7_vx},
          {2_vx, 4_vx, 6_vx, 7_vx},
          {3_vx, 5_vx, 6_vx, 7_vx},
      },
      std::set<Vertex>{4_vx, 5_vx, 6_vx, 7_vx});
}
std::tuple<Mesh, std::set<Vertex>>
ex_boundary_vertex_composed_tetrahedron_without_center() {
  return std::make_tuple(
      std::set<Tetrahedron>{
          // Same without center
          {0_vx, 1_vx, 2_vx, 4_vx},
          {0_vx, 1_vx, 3_vx, 5_vx},
          {0_vx, 2_vx, 3_vx, 6_vx},
          {1_vx, 2_vx, 3_vx, 7_vx},
          {0_vx, 1_vx, 4_vx, 5_vx},
          {0_vx, 2_vx, 4_vx, 6_vx},
          {1_vx, 2_vx, 4_vx, 7_vx},
          {0_vx, 3_vx, 5_vx, 6_vx},
          {1_vx, 3_vx, 5_vx, 7_vx},
          {2_vx, 3_vx, 6_vx, 7_vx},
          {0_vx, 4_vx, 5_vx, 6_vx},
          {1_vx, 4_vx, 5_vx, 7_vx},
          {2_vx, 4_vx, 6_vx, 7_vx},
          {3_vx, 5_vx, 6_vx, 7_vx},
      },
      std::set<Vertex>{0_vx, 1_vx, 2_vx, 3_vx, 4_vx, 5_vx, 6_vx, 7_vx});
}

INSTANTIATE_TEST_CASE_P(
    BoundaryVertices, TestBoundaryVertices,
    ::testing::Values(
        ex_boundary_vertices_3_ring(), ex_boundary_vertices_4_ring(),
        ex_boundary_vertex_composed_tetrahedron(),
        ex_boundary_vertex_composed_tetrahedron_without_center()));

TEST(test_boundary, unoriented_mesh_orders_cell_nodes) {
  using Cell = SimplexCell<3, EmptyData>;
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
      {1_cell, Cell{.id = 1_cell, .nodes = {0_vx, 1_vx, 3_vx, 4_vx}}},
      {2_cell, Cell{.id = 2_cell, .nodes = {0_vx, 1_vx, 4_vx, 2_vx}}}};
  const auto expected_cells =
      std::set<std::array<NodeId, 4>>{{0_vx, 1_vx, 2_vx, 3_vx},
                                      {0_vx, 1_vx, 3_vx, 4_vx},
                                      {0_vx, 1_vx, 2_vx, 4_vx}};
  EXPECT_THAT(unoriented_mesh(cells), ::testing::ContainerEq(expected_cells));
}

TEST(test_boundary, unoriented_mesh_collects_distributed_mesh) {
  using Cell = SimplexCell<3, EmptyData>;
  const auto mpi_rank =
      static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto mpi_size =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());
  const auto node_id = static_cast<NodeId>(4 + mpi_rank);
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
      {1_cell, Cell{.id = 1_cell, .nodes = {node_id, 1_vx, 2_vx, 3_vx}}}};
  auto expected_cells =
      std::set<std::array<NodeId, 4>>{{0_vx, 1_vx, 2_vx, 3_vx}};
  for (std::size_t i = 0; i < mpi_size; i++)
    expected_cells.emplace(
        std::array<NodeId, 4>{1_vx, 2_vx, 3_vx, static_cast<NodeId>(4 + i)});
  EXPECT_THAT(unoriented_mesh(cells), ::testing::ContainerEq(expected_cells));
}

} // namespace
} // namespace qcmesh::mesh::testing
