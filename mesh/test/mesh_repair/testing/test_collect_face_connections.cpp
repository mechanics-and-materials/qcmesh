// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/geometry/triple_hash.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {
namespace {

TEST(test_face_connection, face_connection_is_not_smaller_than_self) {
  const auto connection =
      FaceConnection{.cell_id = 0_cell, .apex_idx = 0, .neighbor_id = 1_cell};
  EXPECT_FALSE(connection < connection);
}
// NOLINTBEGIN(readability-function-cognitive-complexity)
TEST(test_face_connection, face_connections_are_orderable) {
  const auto connections = std::array{
      FaceConnection{.cell_id = 0_cell, .apex_idx = 0, .neighbor_id = 3_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 0, .neighbor_id = 3_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 1, .neighbor_id = 3_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 1, .neighbor_id = 4_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 1, .neighbor_id = 5_cell}};
  for (std::size_t i = 0; i < connections.size(); i++)
    for (std::size_t j = 0; j < connections.size(); j++) {
      EXPECT_EQ(connections[i] < connections[j], i < j);
      EXPECT_EQ(connections[j] < connections[i], j < i);
      EXPECT_EQ(connections[i] == connections[j], j == i);
    }
}
// NOLINTEND(readability-function-cognitive-complexity)

using TestCell = SimplexCell<3, EmptyData>;
using MeshCells = std::unordered_map<SimplexCellId, TestCell>;

TEST(
    test_face_connection,
    collect_local_extrinsic_face_connections_finds_no_connections_for_single_cell) {
  const auto cells =
      MeshCells{{0_cell, TestCell{.nodes = {0_vx, 1_vx, 2_vx, 3_vx}}}};
  EXPECT_THAT(collect_local_extrinsic_face_connections(cells),
              ::testing::IsEmpty());
}
TEST(
    test_face_connection,
    collect_local_extrinsic_face_connections_finds_correct_connections_for_3_cells) {
  const auto cells =
      MeshCells{{0_cell, TestCell{.nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
                {1_cell, TestCell{.nodes = {0_vx, 1_vx, 3_vx, 4_vx}}},
                {2_cell, TestCell{.nodes = {0_vx, 1_vx, 4_vx, 2_vx}}}};
  const auto expected_connections = std::set<FaceConnection>{
      FaceConnection{.cell_id = 0_cell, .apex_idx = 2, .neighbor_id = 1_cell},
      FaceConnection{.cell_id = 0_cell, .apex_idx = 3, .neighbor_id = 2_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 2, .neighbor_id = 2_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 3, .neighbor_id = 0_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 2, .neighbor_id = 0_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 3, .neighbor_id = 1_cell},
  };
  EXPECT_THAT(collect_local_extrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}
TEST(
    test_face_connection,
    collect_local_extrinsic_face_connections_finds_correct_connections_for_2_cells) {
  const auto cells =
      MeshCells{{10_cell, TestCell{.nodes = {0_vx, 3_vx, 4_vx, 5_vx}}},
                {7_cell, TestCell{.nodes = {6_vx, 0_vx, 4_vx, 5_vx}}}};
  const auto expected_connections = std::set<FaceConnection>{
      FaceConnection{.cell_id = 10_cell, .apex_idx = 1, .neighbor_id = 7_cell},
      FaceConnection{.cell_id = 7_cell, .apex_idx = 0, .neighbor_id = 10_cell},
  };
  EXPECT_THAT(collect_local_extrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}

TEST(
    test_face_connection,
    collect_local_extrinsic_face_connections_finds_correct_connections_for_hash_collision) {
  constexpr std::size_t N_ROT =
      std::size_t{std::numeric_limits<std::size_t>::digits / 3} &
      std::size_t{std::size_t{0} - 2};
  const auto o = 0_vx;
  const auto a = 1_vx;
  const auto b = 2_vx;
  const auto d = NodeId{1U | qcmesh::geometry::circ_shift_l(1, N_ROT)};
  const auto e = 3_vx; // e == b | 1

  //     original   circ_shift   bit flip   back shift   result
  // a   ....01     ..01..       .101..     ...101       a | circ_shift(1)
  // b   ....10     10....       11....     ....11       b | 1

  EXPECT_EQ(geometry::TripleHash{}(std::array{o, a, b}),
            geometry::TripleHash{}(std::array{o, d, e}));
  const auto cells = MeshCells{{0_cell, TestCell{.nodes = {a, b, o, 5_vx}}},
                               {1_cell, TestCell{.nodes = {a, b, o, d}}},
                               {2_cell, TestCell{.nodes = {b, o, d, e}}},
                               {3_cell, TestCell{.nodes = {o, d, e, 6_vx}}}};
  const auto expected_connections = std::set<FaceConnection>{
      FaceConnection{.cell_id = 0_cell, .apex_idx = 3, .neighbor_id = 1_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 3, .neighbor_id = 0_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 0, .neighbor_id = 2_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 3, .neighbor_id = 1_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 0, .neighbor_id = 3_cell},
      FaceConnection{.cell_id = 3_cell, .apex_idx = 3, .neighbor_id = 2_cell},
  };
  EXPECT_THAT(collect_local_extrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}

TEST(
    test_face_connection,
    collect_extrinsic_face_connections_finds_no_connections_for_single_cells_on_each_process) {
  const auto rank = static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto cells = MeshCells{
      {SimplexCellId{rank},
       TestCell{.nodes = {NodeId{4 * rank}, NodeId{4 * rank + 1},
                          NodeId{4 * rank + 2}, NodeId{4 * rank + 3}}}}};
  EXPECT_THAT(collect_extrinsic_face_connections(cells), ::testing::IsEmpty());
}
TEST(
    test_face_connection,
    collect_extrinsic_face_connections_finds_correct_connections_for_a_ring_of_distributed_cells) {
  const auto rank = static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto n_procs =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());
  const auto a = 0_vx;
  const auto b = 1_vx;
  // One cell per process:
  const auto id = SimplexCellId{rank};
  const auto cells = MeshCells{
      {id, TestCell{.id = id,
                    .process_rank = static_cast<int>(rank),
                    .nodes = {a, b, NodeId{2 + rank}, NodeId{3 + rank}}}}};
  auto expected_connections = std::set<FaceConnection>{};
  for (std::size_t r = 1; r < n_procs; r++) {
    expected_connections.emplace(
        FaceConnection{.cell_id = SimplexCellId{r - 1},
                       .apex_idx = 2,
                       .neighbor_id = SimplexCellId{r}});
    expected_connections.emplace(
        FaceConnection{.cell_id = SimplexCellId{r},
                       .apex_idx = 3,
                       .neighbor_id = SimplexCellId{r - 1}});
  }
  EXPECT_THAT(collect_extrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}

// -------

TEST(
    test_face_connection,
    collect_local_intrinsic_face_connections_finds_no_connections_for_single_cell) {
  const auto cells =
      MeshCells{{0_cell, TestCell{.nodes = {0_vx, 1_vx, 2_vx, 3_vx}}}};
  EXPECT_THAT(collect_local_intrinsic_face_connections(cells),
              ::testing::IsEmpty());
}
TEST(
    test_face_connection,
    collect_local_intrinsic_face_connections_finds_correct_connections_for_3_cells) {
  const auto cells = MeshCells{
      {0_cell,
       TestCell{.neighbors =
                    std::array<std::optional<SimplexCellId>, 4>{
                        std::nullopt, std::nullopt, std::nullopt, 5_cell}}},
      {1_cell,
       TestCell{// broken connection to cell 0:
                .neighbors =
                    std::array<std::optional<SimplexCellId>, 4>{
                        std::nullopt, std::nullopt, 0_cell, std::nullopt}}},
      {2_cell,
       TestCell{// broken self connection:
                .neighbors = std::array<std::optional<SimplexCellId>, 4>{
                    10_cell, 42_cell, 2_cell, std::nullopt}}}};
  const auto expected_connections = std::set<FaceConnection>{
      FaceConnection{.cell_id = 0_cell, .apex_idx = 3, .neighbor_id = 5_cell},
      FaceConnection{.cell_id = 1_cell, .apex_idx = 2, .neighbor_id = 0_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 0, .neighbor_id = 10_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 1, .neighbor_id = 42_cell},
      FaceConnection{.cell_id = 2_cell, .apex_idx = 2, .neighbor_id = 2_cell},
  };
  EXPECT_THAT(collect_local_intrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}

TEST(
    test_face_connection,
    collect_intrinsic_face_connections_finds_no_connections_for_single_cells_on_each_process) {
  const auto rank = static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto cells = MeshCells{
      {SimplexCellId{rank},
       TestCell{.nodes = {NodeId{4 * rank}, NodeId{4 * rank + 1},
                          NodeId{4 * rank + 2}, NodeId{4 * rank + 3}}}}};
  EXPECT_THAT(collect_intrinsic_face_connections(cells), ::testing::IsEmpty());
}
TEST(
    test_face_connection,
    collect_intrinsic_face_connections_finds_correct_connections_for_a_ring_of_distributed_cells) {
  const auto rank = static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto n_procs =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());
  // One cell per process:
  const auto id = SimplexCellId{rank + 1};
  auto neighbors = std::array<std::optional<SimplexCellId>, 4>{
      std::nullopt, std::nullopt, SimplexCellId{rank}, SimplexCellId{rank + 2}};
  const auto cells =
      MeshCells{{id, TestCell{.id = id,
                              .process_rank = static_cast<int>(rank),
                              .neighbors = neighbors}}};
  auto expected_connections = std::set<FaceConnection>{};
  for (std::size_t r = 1; r <= n_procs; r++) {
    expected_connections.emplace(
        FaceConnection{.cell_id = SimplexCellId{r},
                       .apex_idx = 2,
                       .neighbor_id = SimplexCellId{r - 1}});
    expected_connections.emplace(
        FaceConnection{.cell_id = SimplexCellId{r},
                       .apex_idx = 3,
                       .neighbor_id = SimplexCellId{r + 1}});
  }
  EXPECT_THAT(collect_intrinsic_face_connections(cells),
              ::testing::Eq(expected_connections));
}

} // namespace
} // namespace qcmesh::mesh::testing
