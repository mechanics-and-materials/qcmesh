// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/cell_id_collisions.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <array>
#include <gmock/gmock.h>
#include <tuple>

namespace qcmesh::mesh::testing {
namespace {

using Cell = SimplexCell<3, EmptyData>;

std::pair<SimplexCellId, Cell> make_cell(const SimplexCellId id) {
  return {id, Cell{.id = id}};
}

TEST(test_cell_id_collisions,
     cell_id_collisions_detects_no_collisions_for_unique_ids) {
  const auto mpi_rank =
      static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      make_cell(SimplexCellId{mpi_rank})};
  EXPECT_THAT(cell_id_collisions(cells), ::testing::IsEmpty());
}

TEST(test_cell_id_collisions,
     cell_id_collisions_detects_collisions_in_distributed_mesh) {
  const auto mpi_size =
      static_cast<std::size_t>(boost::mpi::communicator{}.size());
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  const auto cell_id = static_cast<SimplexCellId>(mpi_rank);
  const auto next_cell_id = static_cast<SimplexCellId>(mpi_rank + 1);
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      make_cell(cell_id), make_cell(next_cell_id)};
  auto expected_collisions = std::set<SimplexCellId>{};
  for (std::size_t id = 1; id < mpi_size; id++)
    expected_collisions.emplace(SimplexCellId{id});

  EXPECT_THAT(cell_id_collisions(cells),
              ::testing::ContainerEq(expected_collisions));
}

TEST(test_cell_id_collisions,
     topological_cell_collisions_detects_no_collisions_for_unique_node_ids) {
  const auto mpi_rank =
      static_cast<std::size_t>(boost::mpi::communicator{}.rank());
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {SimplexCellId{mpi_rank},
       Cell{.nodes = {NodeId{mpi_rank}, NodeId{mpi_rank + 1},
                      NodeId{mpi_rank + 2}, NodeId{mpi_rank + 3}}}}};
  EXPECT_THAT(topological_cell_collisions(cells), ::testing::IsEmpty());
}

TEST(test_cell_id_collisions,
     topological_cell_collisions_detects_collisions_in_local_mesh) {
  const auto mpi_rank = boost::mpi::communicator{}.rank();
  const auto cells =
      mpi_rank == 0
          ? std::unordered_map<SimplexCellId,
                               Cell>{{0_cell,
                                      Cell{.nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
                                     {1_cell,
                                      Cell{.nodes = {1_vx, 0_vx, 2_vx, 3_vx}}}}
          : std::unordered_map<SimplexCellId, Cell>{};
  const auto expected_collisions =
      std::set<std::array<NodeId, 4>>{{0_vx, 1_vx, 2_vx, 3_vx}};

  EXPECT_THAT(topological_cell_collisions(cells),
              ::testing::ContainerEq(expected_collisions));
}

} // namespace
} // namespace qcmesh::mesh::testing
