// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/simplex_lattice_jacobian.hpp"
#include "qcmesh/mesh/simplex_mesh.hpp"
#include "qcmesh/mesh/testing/lattice_cell_data.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {
namespace testing {
namespace {

TEST(test_simplex_lattice_jacobian, extrinsic_simplex_lattice_jacobians_works) {
  const auto lattice_vertices = std::unordered_map<NodeId, LatticeVertex>{
      {0_vx, LatticeVertex{{0., 0., -1.}, {0, 0, -1}}},
      {1_vx, LatticeVertex{{0., 0., 1.}, {0, 0, 1}}},
      {2_vx, LatticeVertex{{1., 0., 0.}, {1, 0, 0}}},
      {3_vx, LatticeVertex{{-1., 1., 0.}, {-1, 1, 0}}},
      {4_vx, LatticeVertex{{-1., -1., 0.}, {-1, -1, 0}}}};
  using Cell = SimplexCell<3, EmptyData>;
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
      {1_cell, Cell{.id = 1_cell, .nodes = {0_vx, 1_vx, 3_vx, 4_vx}}},
      {2_cell, Cell{.id = 2_cell, .nodes = {0_vx, 1_vx, 4_vx, 2_vx}}}};
  const auto lattice_jacs =
      extrinsic_simplex_lattice_jacobians(cells, lattice_vertices);
  const auto expected_lattice_jacs =
      std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>{
          {0_cell,
           std::array{std::array<int, 3>{0, 0, 2}, std::array<int, 3>{1, 0, 1},
                      std::array<int, 3>{-1, 1, 1}}},
          {1_cell,
           std::array{std::array<int, 3>{0, 0, 2}, std::array<int, 3>{-1, 1, 1},
                      std::array<int, 3>{-1, -1, 1}}},
          {2_cell, std::array{std::array<int, 3>{0, 0, 2},
                              std::array<int, 3>{-1, -1, 1},
                              std::array<int, 3>{1, 0, 1}}}};
  EXPECT_THAT(lattice_jacs, ::testing::ContainerEq(expected_lattice_jacs));
}

TEST(test_simplex_lattice_jacobian,
     intrinsic_simplex_lattice_jacobians_works_works) {
  const auto d = double{0.1};
  const auto vertices = std::vector{LatticeVertex{{0., 0., -1.}, {0, 0, -1}},
                                    LatticeVertex{{0., 0., 1.}, {0, 0, 1}},
                                    LatticeVertex{{1., 0., 0.}, {1, 0, 0}},
                                    LatticeVertex{{-1., d, 0.}, {-1, 1, 0}},
                                    LatticeVertex{{-1., -d, 0.}, {-1, -1, 0}}};
  const auto cells = std::vector<std::array<std::size_t, 4>>{
      {0, 1, 2, 3}, {0, 1, 3, 4}, {0, 1, 4, 2}};
  const auto mesh =
      create_local_simplex_mesh<EmptyData, LatticeCellData>(vertices, cells);
  const auto lattice_jacs = intrinsic_simplex_lattice_jacobians(mesh);
  const auto expected_lattice_jacs =
      std::unordered_map<SimplexCellId, std::array<std::array<int, 3>, 3>>{
          {0_cell,
           std::array{std::array<int, 3>{0, 0, 2}, std::array<int, 3>{1, 0, 1},
                      std::array<int, 3>{-1, 1, 1}}},
          {1_cell,
           std::array{std::array<int, 3>{0, 0, 2}, std::array<int, 3>{-1, 1, 1},
                      std::array<int, 3>{-1, -1, 1}}},
          {2_cell, std::array{std::array<int, 3>{0, 0, 2},
                              std::array<int, 3>{-1, -1, 1},
                              std::array<int, 3>{1, 0, 1}}}};
  EXPECT_THAT(lattice_jacs, ::testing::ContainerEq(expected_lattice_jacs));
}

TEST(test_simplex_lattice_jacobian, extrinsic_simplex_lattice_volumes_works) {
  const auto lattice_vertices = std::unordered_map<NodeId, LatticeVertex>{
      {0_vx, LatticeVertex{{0., 0., -1.}, {0, 0, 0}}},
      {1_vx, LatticeVertex{{0., 0., 1.}, {0, 0, 1}}},
      {2_vx, LatticeVertex{{1., 0., 0.}, {1, 0, 0}}},
      {3_vx, LatticeVertex{{-1., 1., 0.}, {-1, 1, 0}}},
      {4_vx, LatticeVertex{{-1., -1., 0.}, {-1, -1, 0}}}};
  using Cell = SimplexCell<3, EmptyData>;
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
      {1_cell, Cell{.id = 1_cell, .nodes = {0_vx, 1_vx, 3_vx, 4_vx}}},
      {2_cell, Cell{.id = 2_cell, .nodes = {0_vx, 1_vx, 4_vx, 2_vx}}}};
  const auto lattice_volumes =
      extrinsic_simplex_lattice_volumes(cells, lattice_vertices);
  const auto expected_lattice_volumes = std::unordered_map<SimplexCellId, int>{
      {0_cell, 1}, {1_cell, 2}, {2_cell, 1}};
  EXPECT_THAT(lattice_volumes,
              ::testing::ContainerEq(expected_lattice_volumes));
}

TEST(test_simplex_lattice_jacobian, extrinsic_simplex_atomistic_flags_works) {
  const auto lattice_vertices = std::unordered_map<NodeId, LatticeVertex>{
      {0_vx, LatticeVertex{{0., 0., -1.}, {0, 0, 0}}},
      {1_vx, LatticeVertex{{0., 0., 1.}, {0, 0, 1}}},
      {2_vx, LatticeVertex{{1., 0., 0.}, {1, 0, 0}}},
      {3_vx, LatticeVertex{{-1., 1., 0.}, {-1, 1, 0}}},
      {4_vx, LatticeVertex{{-1., -1., 0.}, {-1, -1, 0}}}};
  using Cell = SimplexCell<3, EmptyData>;
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .nodes = {0_vx, 1_vx, 2_vx, 3_vx}}},
      {1_cell, Cell{.id = 1_cell, .nodes = {0_vx, 1_vx, 3_vx, 4_vx}}},
      {2_cell, Cell{.id = 2_cell, .nodes = {0_vx, 1_vx, 4_vx, 2_vx}}}};
  const auto atomistic_flags =
      extrinsic_simplex_atomistic_flags(cells, lattice_vertices);
  const auto expected_atomistic_flags = std::unordered_map<SimplexCellId, bool>{
      {0_cell, true}, {1_cell, false}, {2_cell, true}};
  EXPECT_THAT(atomistic_flags,
              ::testing::ContainerEq(expected_atomistic_flags));
}

struct AtomisticCellData {
  bool is_atomistic{};
};

} // namespace
} // namespace testing

template <> struct traits::IsAtomistic<testing::AtomisticCellData> {
  inline static bool is_atomistic(const testing::AtomisticCellData &data) {
    return data.is_atomistic;
  }
};

namespace testing {
namespace {

TEST(test_simplex_lattice_jacobian, intrinsic_simplex_atomistic_flags_works) {
  using Cell = SimplexCell<3, AtomisticCellData>;
  const auto cells = std::unordered_map<SimplexCellId, Cell>{
      {0_cell, Cell{.id = 0_cell, .data = AtomisticCellData{true}}},
      {1_cell, Cell{.id = 1_cell, .data = AtomisticCellData{false}}},
      {2_cell, Cell{.id = 2_cell, .data = AtomisticCellData{true}}}};
  const auto atomistic_flags = intrinsic_simplex_atomistic_flags(cells);
  const auto expected_atomistic_flags = std::unordered_map<SimplexCellId, bool>{
      {0_cell, true}, {1_cell, false}, {2_cell, true}};
  EXPECT_THAT(atomistic_flags,
              ::testing::ContainerEq(expected_atomistic_flags));
}

} // namespace
} // namespace testing
} // namespace qcmesh::mesh
