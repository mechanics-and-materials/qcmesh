// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/testing/intersection_detection.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <algorithm>
#include <gmock/gmock.h>

namespace qcmesh::mesh::testing {

namespace {

::testing::AssertionResult &operator<<(::testing::AssertionResult &os,
                                       const std::array<double, 3> &p) {
  return os << '(' << p[0] << ',' << p[1] << ',' << p[2] << ')';
}

::testing::AssertionResult &operator<<(::testing::AssertionResult &os,
                                       const Simplex<3> &t) {
  return os << "{ " << t[0] << ", " << t[1] << ", " << t[2] << ", " << t[3]
            << " }";
}
::testing::AssertionResult &operator<<(::testing::AssertionResult &os,
                                       const std::array<double, 2> &p) {
  return os << '(' << p[0] << ',' << p[1] << ')';
}

::testing::AssertionResult &operator<<(::testing::AssertionResult &os,
                                       const Simplex<2> &t) {
  return os << "{ " << t[0] << ", " << t[1] << ", " << t[2] << " }";
}
template <std::size_t Dimension>
::testing::AssertionResult check_errors(const Simplex<Dimension> &t1,
                                        const Simplex<Dimension> &t2,
                                        unsigned int n_errors) {
  if (n_errors == 0)
    return ::testing::AssertionSuccess();
  auto os = ::testing::AssertionFailure();
  os << t1 << ", " << t2;
  return os;
}

constexpr double EPSILON = 2.E-10;

/**
 * @brief Test suite for the function simplices_dont_intersect.
 */
struct SeparationTest3d
    : ::testing::TestWithParam<std::tuple<Simplex<3>, Simplex<3>, bool>> {};

TEST_P(SeparationTest3d, separation_detection_works) {
  const auto [t1, t2, should_separate] = GetParam();
  auto n_errors = std::size_t{0};
  auto p1 = std::array<std::size_t, 4>{0, 1, 2, 3};
  auto p2 = p1;
  do {
    do {
      const auto t1_p = Simplex<3>{t1[p1[0]], t1[p1[1]], t1[p1[2]], t1[p1[3]]};
      const auto t2_p = Simplex<3>{t2[p2[0]], t2[p2[1]], t2[p2[2]], t2[p2[3]]};
      if (simplices_dont_intersect(t1_p, t2_p, EPSILON) != should_separate)
        n_errors++;
      if (simplices_dont_intersect(t2_p, t1_p, EPSILON) != should_separate)
        n_errors++;
    } while (std::next_permutation(p2.begin(), p2.end()));
  } while (std::next_permutation(p1.begin(), p1.end()));
  EXPECT_TRUE(check_errors(t1, t2, n_errors));
}

std::tuple<Simplex<3>, Simplex<3>, bool> ex_parallel_faces(const double d) {
  using Point = std::array<double, 3>;
  const auto p1 = Point{0., 0., 0.};
  const auto p2 = Point{1., 0., 0.};
  const auto p3 = Point{0., 1., 0.};
  const auto p4 = Point{0., 0., 1.};
  const auto q1 = Point{0., 0., -d};
  const auto q2 = Point{1., 0., -d};
  const auto q3 = Point{0., 1., -d};
  const auto q4 = Point{0., 0., -d - 1.};
  return std::make_tuple(Simplex<3>{p1, p2, p3, p4}, Simplex<3>{q1, q2, q3, q4},
                         d >= 0.);
}
std::tuple<Simplex<3>, Simplex<3>, bool> ex_skew_edges(const double d) {
  using Point = std::array<double, 3>;
  const auto p1 = Point{-1., 0., 0.};
  const auto p2 = Point{1., 0., 0.};
  const auto p3 = Point{0., -1., 1.};
  const auto p4 = Point{0., 1., 1.};
  const auto q1 = Point{0., -1., -d};
  const auto q2 = Point{0., 1., -d};
  const auto q3 = Point{-1., 0., -d - 1.};
  const auto q4 = Point{1., 0., -d - 1.};
  return std::make_tuple(Simplex<3>{p1, p2, p3, p4}, Simplex<3>{q1, q2, q3, q4},
                         d >= 0.);
}
/**
 * @brief Example where t1 has no face which separates t1 from t2 but t2 has
 * such a face.
 */
std::tuple<Simplex<3>, Simplex<3>, bool> ex_asymmetric() {
  using Point = std::array<double, 3>;
  const auto t1 =
      Simplex<3>{Point{64.98, -50.54, -144.4}, Point{72.2, -57.76, -144.4},
                 Point{64.98, -64.98, -144.4}, Point{57.76, -57.76, -173.28}};
  // Clearly, z = -200 is separating t1 and t2
  const auto t2 = Simplex<3>{
      Point{57.76, -57.76, -231.04},
      Point{28.88, -57.76, -202.16},
      Point{57.76, -86.64, -202.16},
      Point{57.76, -28.88, -202.16},
  };
  return std::make_tuple(t1, t2, true);
}
std::tuple<Simplex<3>, Simplex<3>, bool> ex_num_roundoff_1() {
  using Point = std::array<double, 3>;
  const auto p1 = Point{0., 0., -1.};
  const auto p2 = Point{0., 0.1, 0.};
  const auto p3 = Point{-1., 0.3, 0.};
  const auto p4 = Point{-1., -0.3, 0.};
  const auto p5 = Point{0., 0., 1.};
  return std::make_tuple(Simplex<3>{p1, p2, p3, p4}, Simplex<3>{p2, p3, p4, p5},
                         true);
}
std::tuple<Simplex<3>, Simplex<3>, bool> ex_num_roundoff_2() {
  using Point = std::array<double, 3>;
  const auto points =
      std::array{Point{18.05, -12.635, -41.515}, Point{16.245, -10.83, -41.515},
                 Point{18.05, -10.83, -43.32}};
  return std::make_tuple(
      Simplex<3>{points[0], Point{16.245, -12.635, -43.32}, points[1],
                 points[2]},
      Simplex<3>{Point{18.05, -10.83, -39.71}, points[0], points[1], points[2]},
      true);
}
std::tuple<Simplex<3>, Simplex<3>, bool> ex_num_roundoff_3() {
  using Point = std::array<double, 3>;
  const auto err = 2.84217e-14;
  const auto a = Point{173.28, -202.16, -490.96};
  const auto b = Point{202.16 + err, -173.28 + err, -490.96};
  const auto c = Point{202.16, -202.16, -462.08};
  const auto d = Point{202.16, -231.04, -490.96};
  const auto e = Point{173.28, -173.28, -462.08};
  return std::make_tuple(Simplex<3>{a, b, c, d}, Simplex<3>{e, b, c, a}, true);
}

INSTANTIATE_TEST_CASE_P(
    Intersection, SeparationTest3d,
    ::testing::Values(ex_parallel_faces(0.1), ex_parallel_faces(0.0),
                      ex_parallel_faces(-0.1), ex_skew_edges(0.1),
                      ex_skew_edges(0.0), ex_skew_edges(-0.1), ex_asymmetric(),
                      ex_num_roundoff_1(), ex_num_roundoff_2(),
                      ex_num_roundoff_3()));

/**
 * @brief Test suite for the function simplices_dont_intersect.
 */
struct SeparationTest2d
    : ::testing::TestWithParam<std::tuple<Simplex<2>, Simplex<2>, bool>> {};

TEST_P(SeparationTest2d, separation_detection_works) {
  const auto [t1, t2, should_separate] = GetParam();
  auto n_errors = std::size_t{0};
  auto p1 = std::array<std::size_t, 3>{0, 1, 2};
  auto p2 = p1;
  do {
    do {
      const auto t1_p = Simplex<2>{t1[p1[0]], t1[p1[1]], t1[p1[2]]};
      const auto t2_p = Simplex<2>{t2[p2[0]], t2[p2[1]], t2[p2[2]]};
      if (simplices_dont_intersect(t1_p, t2_p, EPSILON) != should_separate)
        n_errors++;
      if (simplices_dont_intersect(t2_p, t1_p, EPSILON) != should_separate)
        n_errors++;
    } while (std::next_permutation(p2.begin(), p2.end()));
  } while (std::next_permutation(p1.begin(), p1.end()));
  EXPECT_TRUE(check_errors(t1, t2, n_errors));
}
std::tuple<Simplex<2>, Simplex<2>, bool> ex_mirrored_triangles(const double d) {
  using Point = std::array<double, 2>;
  const auto p1 = Point{0., 0.};
  const auto p2 = Point{1., 0.};
  const auto p3 = Point{0., 1.};
  const auto q1 = Point{0., -d};
  const auto q2 = Point{1., -d};
  const auto q3 = Point{0., -1. - d};
  return std::make_tuple(Simplex<2>{p1, p2, p3}, Simplex<2>{q1, q2, q3},
                         d >= 0.);
}
std::tuple<Simplex<2>, Simplex<2>, bool>
ex_translated_triangles(const double d) {
  using Point = std::array<double, 2>;
  const auto p1 = Point{0., 0.};
  const auto p2 = Point{1., 1.};
  const auto p3 = Point{-1., 1.};
  const auto q1 = Point{1., -d};
  const auto q2 = Point{-1., -d};
  const auto q3 = Point{0., -1. - d};
  return std::make_tuple(Simplex<2>{p1, p2, p3}, Simplex<2>{q1, q2, q3},
                         d >= 0.);
}
INSTANTIATE_TEST_CASE_P(Intersection2d, SeparationTest2d,
                        ::testing::Values(ex_mirrored_triangles(0.1),
                                          ex_mirrored_triangles(0.0),
                                          ex_mirrored_triangles(-0.1),
                                          ex_translated_triangles(0.1),
                                          ex_translated_triangles(0.0),
                                          ex_translated_triangles(-0.1)));

SimplexMesh<3, 3, EmptyData, EmptyData> to_mesh(const Simplex<3> &cell_a,
                                                const Simplex<3> &cell_b) {
  using Cell = SimplexCell<3, EmptyData>;
  using EmptyNode = Node<3, EmptyData>;
  const auto make_node = [](const auto id, const auto &position) {
    return std::make_pair(id, EmptyNode{.id = id, .position = position});
  };
  const auto make_cell = [](const auto id, const auto &nodes) {
    return std::make_pair(id, Cell{.id = id, .nodes = nodes});
  };
  return SimplexMesh<3, 3, EmptyData, EmptyData>{
      .nodes = {make_node(0_vx, cell_a[0]), make_node(1_vx, cell_a[1]),
                make_node(2_vx, cell_a[2]), make_node(3_vx, cell_a[3]),
                make_node(4_vx, cell_b[0]), make_node(5_vx, cell_b[1]),
                make_node(6_vx, cell_b[2]), make_node(7_vx, cell_b[3])},
      .cells = {make_cell(0_cell, std::array{0_vx, 1_vx, 2_vx, 3_vx}),
                make_cell(1_cell, std::array{4_vx, 5_vx, 6_vx, 7_vx})}};
}

TEST(test_intersection_detection, cell_overlaps_detects_overlap) {
  const auto [cell_a, cell_b, _] = ex_parallel_faces(-0.1);
  const auto mesh = to_mesh(cell_a, cell_b);
  const auto overlaps = cell_overlaps(mesh);
  const auto expected_overlaps_ordered = std::vector{std::make_tuple(
      SimplexGeometry{0_cell, cell_a}, SimplexGeometry{1_cell, cell_b})};
  const auto expected_overlaps_unordered = std::vector{std::make_tuple(
      SimplexGeometry{1_cell, cell_b}, SimplexGeometry{0_cell, cell_a})};

  EXPECT_THAT(
      overlaps,
      ::testing::AnyOf(::testing::ContainerEq(expected_overlaps_ordered),
                       ::testing::ContainerEq(expected_overlaps_unordered)));
}

TEST(test_intersection_detection,
     cell_overlaps_detects_no_overlap_for_separated_cells) {
  const auto [cell_a, cell_b, _] = ex_parallel_faces(0.1);
  const auto mesh = to_mesh(cell_a, cell_b);
  const auto overlaps = cell_overlaps(mesh);
  EXPECT_THAT(overlaps, ::testing::IsEmpty());
}

TEST(test_intersection_detection,
     cell_overlaps_detects_no_overlap_for_separated_cells_2) {
  const auto [cell_a, cell_b, _] = ex_num_roundoff_3();
  const auto mesh = to_mesh(cell_a, cell_b);
  const auto overlaps = cell_overlaps(mesh);
  EXPECT_THAT(overlaps, ::testing::IsEmpty());
}

} // namespace
} // namespace qcmesh::mesh::testing
