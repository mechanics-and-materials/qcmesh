// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/distopt/greedy_driver.hpp"
#include "qcmesh/mesh/mesh_repair/repair_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/boundary.hpp"
#include "qcmesh/mesh/mesh_repair/testing/cell_id_collisions.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/intersection_detection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/serialization/face_connection.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/serialization/empty_data.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/serialization/lattice_cell_data.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include "qcmesh/mpi/all_reduce_sum.hpp"
#include "qcmesh/mpi/all_union_unordered_map.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

constexpr double REMESH_TOLERANCE = 1.0e1;

template <class VertexType>
testing::LatticeMesh<testing::LatticeCellData>
create_mesh(const testing::BareMesh<VertexType> &bare_mesh,
            const bool throw_on_degeneracy = true) {
  return testing::create_distributed_lattice_mesh<testing::LatticeCellData>(
      bare_mesh, throw_on_degeneracy);
}

struct RepairMeshTest
    : ::testing::TestWithParam<testing::LatticeMesh<testing::LatticeCellData>> {
};
INSTANTIATE_TEST_CASE_P(
    mesh_repair_test_cases, RepairMeshTest,
    ::testing::Values(create_mesh(testing::mesh_1()),
                      create_mesh(testing::mesh_2()),
                      create_mesh(testing::mesh_1b()),
                      create_mesh(testing::mesh_lattice(), false),
                      create_mesh(testing::mesh_lattice_4x4x4(), false),
                      create_mesh(testing::mesh_lattice_4x4x4_boundary(),
                                  false)));

TEST_P(RepairMeshTest, gather_bad_cells_works) {
  const auto mesh = GetParam();

  const auto bad_cells = gather_bad_elements(mesh, REMESH_TOLERANCE);
  const auto bad_cells_ids = [&]() {
    auto out = std::unordered_set<SimplexCellId>{};
    for (const auto &address : bad_cells)
      out.insert(address.cell_handle);
    return out;
  }();

  for (const auto cell_id : bad_cells_ids) {
    if (mesh.cells.find(cell_id) == mesh.cells.end())
      continue;
    const auto points = simplex_cell_points(mesh, cell_id);
    EXPECT_GE(geometry::simplex_metric_residual(points), REMESH_TOLERANCE);
  }
  // All other simplices should be below tolerance:
  for (const auto &[cell_id, cell] : mesh.cells) {
    if (bad_cells_ids.find(cell_id) != bad_cells_ids.end())
      continue;
    const auto points = simplex_cell_points(mesh, cell_id);
    EXPECT_LT(geometry::simplex_metric_residual(points), REMESH_TOLERANCE);
  }
}

TEST_P(RepairMeshTest, repair_mesh_does_not_change_boundary) {
  auto mesh = GetParam();
  const auto boundary_vertices_before = testing::boundary_vertices(mesh.cells);
  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);
  const auto boundary_vertices_after = testing::boundary_vertices(mesh.cells);
  EXPECT_THAT(boundary_vertices_before,
              ::testing::ContainerEq(boundary_vertices_after));
}

TEST_P(RepairMeshTest, repair_mesh_removes_all_bad_cells) {
  const auto num_bad_cells = [](const auto &mesh) {
    auto n_bad_cells = (std::size_t){0};
    for (const auto &[_, cell] : mesh.cells)
      if (cell.data.quality >= REMESH_TOLERANCE)
        n_bad_cells++;
    return n_bad_cells;
  };

  auto mesh = GetParam();
  const auto n_bad_cells_before =
      qcmesh::mpi::all_reduce_sum(num_bad_cells(mesh));
  EXPECT_GT(n_bad_cells_before, (std::size_t){0});
  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);
  const auto n_bad_cells = qcmesh::mpi::all_reduce_sum(num_bad_cells(mesh));
  EXPECT_EQ(n_bad_cells, (std::size_t){0});
}

TEST_P(RepairMeshTest, repair_mesh_does_not_create_overlappings) {
  auto mesh = GetParam();
  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);
  const auto intersections = testing::cell_overlaps(mesh);
  EXPECT_THAT(intersections, ::testing::IsEmpty());
}

TEST_P(RepairMeshTest, repair_mesh_does_not_change_vertices) {
  const auto node_positions = [](const auto &mesh) {
    auto out = std::unordered_map<NodeId, std::array<double, 3>>{};
    for (const auto &[node_id, node] : mesh.nodes)
      out.insert({node_id, node.position});
    return out;
  };
  auto mesh = GetParam();
  const auto vertices_before =
      qcmesh::mpi::all_union_unordered_map(node_positions(mesh));
  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);
  const auto vertices_after =
      qcmesh::mpi::all_union_unordered_map(node_positions(mesh));
  EXPECT_EQ(vertices_before, vertices_after);
}

TEST_P(RepairMeshTest, repair_mesh_does_not_break_connectivity) {
  auto mesh = GetParam();
  ASSERT_THAT(testing::collect_intrinsic_face_connections(mesh.cells),
              ::testing::ContainerEq(
                  testing::collect_extrinsic_face_connections(mesh.cells)));

  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);
  ASSERT_THAT(testing::collect_intrinsic_face_connections(mesh.cells),
              ::testing::ContainerEq(
                  testing::collect_extrinsic_face_connections(mesh.cells)));
}

TEST_P(RepairMeshTest, test_repair_mesh_does_not_create_cell_id_collisions) {
  auto mesh = GetParam();
  auto driver = GreedyDriver{};
  repair_mesh(mesh, driver);

  const auto collisions = testing::cell_id_collisions(mesh.cells);
  ASSERT_THAT(collisions, ::testing::IsEmpty());
}

} // namespace
} // namespace qcmesh::mesh
