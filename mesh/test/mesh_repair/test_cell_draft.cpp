// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/array_ops/array_ops.hpp"
#include "qcmesh/mesh/mesh_repair/cell_draft.hpp"
#include "qcmesh/testing/tensor_matchers.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

auto to_std_matrix(const Eigen::Matrix3d &eigen_matrix) {
  auto matrix = std::array<std::array<double, 3>, 3>{};
  array_ops::as_eigen_matrix(matrix) = eigen_matrix;
  return matrix;
}

TEST(test_cell_draft,
     lattice_vectors_with_vol_yields_correct_value_for_undeformed_mesh) {
  const auto edges = std::array{
      EdgeWithLatticeCoordinates{Eigen::Vector3d(1., 1., 0.).matrix(),
                                 Eigen::Vector3d(1., 1., 0.).matrix()},
      EdgeWithLatticeCoordinates{Eigen::Vector3d(1., 0., 1.).matrix(),
                                 Eigen::Vector3d(1., 0., 1.).matrix()},
      EdgeWithLatticeCoordinates{Eigen::Vector3d(0., 1., 1.).matrix(),
                                 Eigen::Vector3d(0., 1., 1.).matrix()}};
  const auto [vol, lattice_vectors] =
      lattice_vectors_with_vol(edges[0], edges[1], edges[2]);
  const auto expected_vol = int{-2};
  EXPECT_EQ(vol, expected_vol);
  const auto expected_lattice_vectors = std::array{
      std::array<double, 3>{1., 0., 0.}, std::array<double, 3>{0., 1., 0.},
      std::array<double, 3>{0., 0., 1.}};
  EXPECT_THAT(to_std_matrix(lattice_vectors),
              qcmesh::testing::DoubleMatrixEq(expected_lattice_vectors));
}
TEST(test_cell_draft,
     lattice_vectors_with_vol_yields_yields_correct_value_for_scaled_mesh) {
  const auto edges = std::array{
      EdgeWithLatticeCoordinates{Eigen::Vector3d(1., -1., 0.).matrix(),
                                 Eigen::Vector3d(1., 1., 0.).matrix()},
      EdgeWithLatticeCoordinates{Eigen::Vector3d(0., 1., -1.).matrix(),
                                 Eigen::Vector3d(0., 1., 1.).matrix()},
      EdgeWithLatticeCoordinates{Eigen::Vector3d(1., 0., 1.).matrix(),
                                 Eigen::Vector3d(1., 0., 1.).matrix()}};
  const auto [vol, lattice_vectors] =
      lattice_vectors_with_vol(edges[0], edges[1], edges[2]);
  const auto expected_vol = int{2};
  EXPECT_EQ(vol, expected_vol);
  // Inverse of  [[1, 0,  0],
  //              [1, 0, -1],
  //              [1, 1,  0]]:
  const auto expected_lattice_vectors =
      std::array{std::array<double, 3>{1.0, 0.0, 0.0},
                 std::array<double, 3>{-1.0, 0.0, 1.0},
                 std::array<double, 3>{1.0, -1.0, 0.0}};
  EXPECT_THAT(to_std_matrix(lattice_vectors),
              qcmesh::testing::DoubleMatrixEq(expected_lattice_vectors));
}

} // namespace
} // namespace qcmesh::mesh
