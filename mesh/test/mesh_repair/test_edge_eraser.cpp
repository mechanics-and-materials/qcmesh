// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/distopt/max_in_range.hpp"
#include "qcmesh/mesh/mesh_repair/edge_eraser.hpp"
#include "qcmesh/mesh/mesh_repair/repair_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/simplex_lattice_jacobian.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include "qcmesh/mesh/mesh_repair/testing/topological_cell.hpp"
#include "qcmesh/mesh/testing/node_id.hpp"
#include "qcmesh/mesh/testing/simplex_cell_id.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

using testing::create_local_lattice_mesh;

TEST(test_edge_eraser, collect_cell_ring_around_edge_works_for_1_cell_ring) {
  const auto mesh = create_local_lattice_mesh(testing::mesh_1());
  const auto cell_handle = 0_cell;
  const auto ring = collect_cell_ring_around_edge(mesh, cell_handle, 2, 3);
  const auto expected_ring = std::vector{cell_handle};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_edge_eraser, collect_cell_ring_around_edge_works_for_2_cell_ring) {
  const auto mesh = create_local_lattice_mesh(testing::mesh_1());
  const auto cell_handle = 0_cell;
  for (const auto &[a, b, cell_1, cell_2] : std::array<
           std::tuple<std::size_t, std::size_t, SimplexCellId, SimplexCellId>,
           8>{std::make_tuple(0, 2, 0_cell, 2_cell),
              std::make_tuple(2, 0, 2_cell, 0_cell),
              std::make_tuple(0, 3, 1_cell, 0_cell),
              std::make_tuple(3, 0, 0_cell, 1_cell),
              std::make_tuple(1, 2, 2_cell, 0_cell),
              std::make_tuple(2, 1, 0_cell, 2_cell),
              std::make_tuple(1, 3, 0_cell, 1_cell),
              std::make_tuple(3, 1, 1_cell, 0_cell)}) {
    const auto ring = collect_cell_ring_around_edge(mesh, cell_handle, a, b);
    const auto expected_ring =
        std::vector{mesh.cells.at(cell_1).id, mesh.cells.at(cell_2).id};
    EXPECT_EQ(ring, expected_ring);
  }
}
TEST(test_edge_eraser, collect_cell_ring_around_edge_works_for_3_cell_ring) {
  const auto mesh = create_local_lattice_mesh(testing::mesh_1());
  const auto cell_handle = 0_cell;
  const auto ring = collect_cell_ring_around_edge(mesh, cell_handle, 0, 1);
  const auto expected_ring = std::vector{cell_handle, 1_cell, 2_cell};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_edge_eraser,
     collect_cell_ring_around_edge_works_for_3_cell_ring_in_reversed_order) {
  const auto mesh = create_local_lattice_mesh(testing::mesh_1());
  const auto cell_handle = 0_cell;
  const auto ring = collect_cell_ring_around_edge(mesh, cell_handle, 1, 0);
  const auto expected_ring = std::vector{cell_handle, 2_cell, 1_cell};
  EXPECT_EQ(ring, expected_ring);
}
TEST(test_edge_eraser,
     collect_cell_ring_around_edge_works_for_3_cell_ring_with_halo_cell) {
  auto mesh = create_local_lattice_mesh(testing::mesh_1());
  auto cell = mesh.cells.extract(1_cell);
  mesh.halo_cells.insert(std::move(cell));
  const auto cell_handle = 0_cell;
  const auto ring = collect_cell_ring_around_edge(mesh, cell_handle, 0, 1);
  const auto expected_ring = std::vector{cell_handle, 1_cell, 2_cell};
  EXPECT_EQ(ring, expected_ring);
}

struct EdgeRemovalTestCase {
  testing::BareMesh<testing::LatticeVertex> initial_mesh;
  SimplexCellId bad_cell_id;
  std::array<std::size_t, 2> edge;
  std::set<testing::TopologicalCell<4>> expected_topological_mesh;

  template <class NodalData, class CellData>
  void apply_to(SimplexMesh<3, 3, NodalData, CellData> &mesh) const {
    using Modifier = EdgeEraser<NodalData, CellData>;
    const auto cell = mesh.cells.at(this->bad_cell_id);
    const auto edge = typename Modifier::PrimitiveHandle{cell.id, this->edge[0],
                                                         this->edge[1]};
    const auto modifier = Modifier{};
    const auto preview = modifier.preview(mesh, edge);
    const auto change_set =
        modifier.preview_to_changeset(std::move(preview), mesh);
    apply_change_set(mesh, change_set);
  }
};
/**
 * @brief Test suite to check the lattice vectors and
 * atomistic_flag of newly created cells of an edge removal.
 */
struct EdgeRemovalTest : ::testing::TestWithParam<EdgeRemovalTestCase> {};
INSTANTIATE_TEST_CASE_P(
    edge_removal_test_cases, EdgeRemovalTest,
    ::testing::Values(
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_1(),
            .bad_cell_id = 0_cell,
            .edge = {0, 1},
            // Note: another example would be edge={0, 2}. This would be
            // an example which would generate an inverted tetrahedron.
            // However, this would yield a negative mesh quality and would be
            // declined by optimize_mesh.
            .expected_topological_mesh = {{0_vx, 2_vx, 3_vx, 4_vx},
                                          {1_vx, 4_vx, 3_vx, 2_vx}}},
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_1b(),
            .bad_cell_id = 0_cell,
            .edge = {0, 1},
            .expected_topological_mesh = {{0_vx, 2_vx, 3_vx, 4_vx},
                                          {1_vx, 4_vx, 3_vx, 2_vx},
                                          {0_vx, 2_vx, 5_vx, 3_vx},
                                          {1_vx, 2_vx, 3_vx, 5_vx},
                                          {0_vx, 2_vx, 4_vx, 6_vx},
                                          {1_vx, 2_vx, 6_vx, 4_vx},
                                          {0_vx, 3_vx, 7_vx, 4_vx},
                                          {1_vx, 3_vx, 4_vx, 7_vx}}},
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_3(),
            .bad_cell_id = 0_cell,
            .edge = {0, 3}, // Cannot be removed
            .expected_topological_mesh = {{0_vx, 2_vx, 3_vx, 4_vx},
                                          {1_vx, 4_vx, 3_vx, 2_vx}}},
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_1(),
            .bad_cell_id = 0_cell,
            .edge = {2, 3}, // Should remove the bad cell
            .expected_topological_mesh = {{0_vx, 1_vx, 3_vx, 4_vx},
                                          {0_vx, 1_vx, 4_vx, 2_vx}}},
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_2(),
            .bad_cell_id = 0_cell,
            .edge = {0, 1},
            .expected_topological_mesh = {{0_vx, 2_vx, 3_vx, 4_vx},
                                          {1_vx, 4_vx, 3_vx, 2_vx},
                                          {0_vx, 2_vx, 4_vx, 5_vx},
                                          {1_vx, 5_vx, 4_vx, 2_vx}}},
        EdgeRemovalTestCase{
            .initial_mesh = testing::mesh_2b(),
            .bad_cell_id = 0_cell,
            .edge = {0, 1},
            .expected_topological_mesh = {{0_vx, 2_vx, 3_vx, 4_vx},
                                          {1_vx, 4_vx, 3_vx, 2_vx},
                                          {0_vx, 2_vx, 4_vx, 5_vx},
                                          {1_vx, 5_vx, 4_vx, 2_vx},
                                          {6_vx, 0_vx, 2_vx, 3_vx},
                                          {7_vx, 1_vx, 3_vx, 2_vx},
                                          {6_vx, 0_vx, 3_vx, 4_vx},
                                          {7_vx, 1_vx, 4_vx, 3_vx},
                                          {6_vx, 0_vx, 4_vx, 5_vx},
                                          {7_vx, 1_vx, 5_vx, 4_vx}}}));

TEST_P(EdgeRemovalTest, edge_removal_yields_correct_mesh) {
  const auto test_case = GetParam();
  auto mesh = create_local_lattice_mesh(test_case.initial_mesh);
  test_case.apply_to(mesh);
  const auto topological_mesh =
      testing::collect_local_topological_cells(mesh.cells);
  EXPECT_THAT(topological_mesh,
              ::testing::ContainerEq(test_case.expected_topological_mesh));
}

TEST_P(EdgeRemovalTest, edge_removal_does_not_break_connectivity) {
  const auto test_case = GetParam();
  auto mesh = create_local_lattice_mesh(test_case.initial_mesh);
  EXPECT_THAT(
      testing::collect_local_intrinsic_face_connections(mesh.cells),
      ::testing::ContainerEq(
          testing::collect_local_extrinsic_face_connections(mesh.cells)));
  test_case.apply_to(mesh);
  EXPECT_THAT(
      testing::collect_local_intrinsic_face_connections(mesh.cells),
      ::testing::ContainerEq(
          testing::collect_local_extrinsic_face_connections(mesh.cells)));
}

TEST_P(EdgeRemovalTest, edge_removal_does_not_change_lattice_coordinates) {
  const auto test_case = GetParam();
  auto [mesh, vertices] = create_local_lattice_mesh_with_lattice_coordinates(
      test_case.initial_mesh);
  EXPECT_THAT(
      testing::intrinsic_simplex_lattice_jacobians(mesh),
      ::testing::ContainerEq(
          testing::extrinsic_simplex_lattice_jacobians(mesh.cells, vertices)));
  test_case.apply_to(mesh);
  EXPECT_THAT(
      testing::intrinsic_simplex_lattice_jacobians(mesh),
      ::testing::ContainerEq(
          testing::extrinsic_simplex_lattice_jacobians(mesh.cells, vertices)));
}

TEST_P(EdgeRemovalTest, edge_removal_sets_correct_atomistic_flag) {
  const auto test_case = GetParam();
  auto [mesh, vertices] = create_local_lattice_mesh_with_lattice_coordinates(
      test_case.initial_mesh);
  EXPECT_THAT(testing::intrinsic_simplex_atomistic_flags(mesh.cells),
              ::testing::ContainerEq(testing::extrinsic_simplex_atomistic_flags(
                  mesh.cells, vertices)));
  test_case.apply_to(mesh);
  EXPECT_THAT(testing::intrinsic_simplex_atomistic_flags(mesh.cells),
              ::testing::ContainerEq(testing::extrinsic_simplex_atomistic_flags(
                  mesh.cells, vertices)));
}

/**
 * @brief Edge removal of edge 0-1 in example mesh_flat_cell
 * would lead to the following mesh (ring: 2-3-4, apices: 0,1):
 *
 *        z             z
 *
 *        2---          2---
 *       /|  /|        /|  /|
 *      3---  |       3---  |
 *      |  -|-4 y     | 0-|-4 y
 *      |/  |/        |/  |/
 *      1---           ---
 *     x             x
 *
 * Theese 2 cells are intersecting, one of them has negative volume.
 */
TEST(test_edge_eraser, edge_removal_rejects_inverted_tetrahedra) {
  auto mesh = create_local_lattice_mesh(testing::mesh_flat_cell(), false);
  using Modifier = EdgeEraser<EmptyData, testing::LatticeCellData>;
  const auto element = 0_cell;
  const auto edge = typename Modifier::PrimitiveHandle{element, 0, 1};
  auto preview = Modifier{}.preview(mesh, edge);
  EXPECT_THAT(preview.region, ::testing::IsEmpty());
  const auto change_set =
      Modifier{}.preview_to_changeset(std::move(preview), mesh);
  EXPECT_THAT(change_set.out, ::testing::IsEmpty());
  EXPECT_THAT(change_set.in, ::testing::IsEmpty());
}

/**
 * @brief Edge removal of edge 0-1 in a 3 cell mesh
 *
 * y  (4)_   _(2)      (1)       z
 * ^    \ (3) /       / | \      ^
 * |     \ | /     (4)-(3)-(2)   |
 *  ->x  (0,1)        \ | /       ->x
 *                     (0)
 *
 * will create a duplicate of {1,2,3,4}
 */
TEST(test_edge_eraser, edge_removal_rejects_overlapping_tetrahedra) {
  const auto bare_mesh = testing::BareMesh<testing::LatticeVertex>{
      .vertices = {{{0., 0., -1.}, {0, 0, -1}},
                   {{0., 0., 1.}, {0, 0, 1}},
                   {{1., 1., 0.}, {1, 1, 0}},
                   {{0., 0.5, 0.}, {0, 1, 0}},
                   {{-1., 1., 0.}, {-1, 1, 0}}},
      .cells = {{0, 1, 2, 3}, {0, 1, 3, 4}, {1, 2, 3, 4}}};
  auto mesh = create_local_lattice_mesh(bare_mesh, false);
  using Modifier = EdgeEraser<EmptyData, testing::LatticeCellData>;
  const auto element = 0_cell;
  const auto edge = typename Modifier::PrimitiveHandle{element, 0, 1};
  auto preview = Modifier{}.preview(mesh, edge);
  EXPECT_THAT(preview.region, ::testing::IsEmpty());
  const auto change_set =
      Modifier{}.preview_to_changeset(std::move(preview), mesh);
  EXPECT_THAT(change_set.out, ::testing::IsEmpty());
  EXPECT_THAT(change_set.in, ::testing::IsEmpty());
}

/**
 * @brief Example of a mesh which cannot be repaired by neither edge removal nor
 * face removal.
 *
 * Ideally, we would have an example which can not be repaired by edge removal,
 * but can be repaired by face removal.
 * Here both modifiers dont repair the example.
 */
TEST(edge_removal_example, edge_removal_cannot_improve_quality_of_mesh_3c) {
  auto mesh = create_local_lattice_mesh(testing::mesh_3c());
  const auto compute_residual_out = [&](const SimplexCellId cell_handle) {
    return geometry::simplex_metric_residual(
        simplex_cell_points(mesh, cell_handle));
  };
  using Modifier = EdgeEraser<EmptyData, testing::LatticeCellData>;
  const auto modifier = Modifier{};
  const auto element = 0_cell;
  for (const auto &edge : modifier.primitives_for_element(mesh, element)) {
    const auto preview = modifier.preview(mesh, edge);
    const auto in_residual = preview.replacement_residual;
    const auto out_residual = max_in_range(
        preview.region.begin(), preview.region.end(),
        std::numeric_limits<double>::lowest(), compute_residual_out);
    EXPECT_GE(in_residual, out_residual);
  }
  constexpr double REMESH_TOLERANCE = 1.0e1;
  EXPECT_GE(
      geometry::simplex_metric_residual(simplex_cell_points(mesh, element)),
      REMESH_TOLERANCE);
}

TEST(test_edge_eraser,
     edge_removal_rejects_edge_with_a_ring_without_invertible_cell_basis) {
  // 3 ring around (0,0,-1) - (0,0,1) with singular lattice bases
  const auto d = 0.1;
  const auto bare_mesh = testing::BareMesh<testing::LatticeVertex>{
      .vertices = {{{0., 0., -1.}, {0, 0, 0}},
                   {{0., 0., 1.}, {0, 0, 0}},
                   {{1., 0., 0.}, {1, 0, 0}},
                   {{-1., d, 0.}, {0, 0, 0}},
                   {{-1., -d, 0.}, {0, 0, 0}}},
      .cells = {{0, 1, 2, 3}, {0, 1, 3, 4}, {0, 1, 4, 2}}};
  auto mesh = create_local_lattice_mesh(bare_mesh, false);
  using Modifier = EdgeEraser<EmptyData, testing::LatticeCellData>;
  const auto element = 0_cell;
  const auto edge = typename Modifier::PrimitiveHandle{element, 0, 1};
  const auto preview = Modifier{}.preview(mesh, edge);
  EXPECT_THAT(preview.region, ::testing::IsEmpty());
}

} // namespace
} // namespace qcmesh::mesh
