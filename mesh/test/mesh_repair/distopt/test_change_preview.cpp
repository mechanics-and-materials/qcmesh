// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/distopt/change_preview.hpp"
#include <gmock/gmock.h>

using testing::Eq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

struct TestChangePreview : Test {};

TEST_F(TestChangePreview, two_empty_changesets_are_equal) {
  const ChangePreview<int, bool> x;
  const ChangePreview<int, bool> y;

  EXPECT_THAT(x == y, Eq(true));
  EXPECT_THAT(x != y, Eq(false));
}

TEST_F(TestChangePreview, different_region_means_different_changeset) {
  const ChangePreview<int, bool> x = {{7}, true, 0.};
  const ChangePreview<int, bool> y = {{}, true, 0.};

  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST_F(TestChangePreview, different_draft_means_different_changeset) {
  const ChangePreview<int, bool> x = {{}, true, 0.};
  const ChangePreview<int, bool> y = {{}, false, 0.};

  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

TEST_F(TestChangePreview,
       different_replacement_residual_means_different_changeset) {
  const ChangePreview<int, bool> x = {{}, true, 0.};
  const ChangePreview<int, bool> y = {{}, true, 1.};

  EXPECT_THAT(x == y, Eq(false));
  EXPECT_THAT(x != y, Eq(true));
}

} // namespace
} // namespace qcmesh::mesh
