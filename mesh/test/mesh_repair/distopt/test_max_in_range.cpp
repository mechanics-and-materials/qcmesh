// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/distopt/max_in_range.hpp"
#include <gmock/gmock.h>
#include <string>

using testing::DoubleEq;
using testing::Test;

namespace qcmesh::mesh {
namespace {

struct TestMaxInRange : Test {};

TEST_F(TestMaxInRange, returns_init_if_empty_range) {
  const std::vector<std::string> values;
  const auto init = .7;

  EXPECT_THAT(max_in_range(values.begin(), values.end(), init,
                           [](const std::string &) { return double{}; }),
              DoubleEq(init));
}

TEST_F(TestMaxInRange, returns_value_if_single_value_larger_than_init) {
  const std::vector<std::string> values = {"one"};
  const auto init = .3;
  const auto value = .7;

  EXPECT_THAT(max_in_range(values.begin(), values.end(), init,
                           [&](const std::string &) { return value; }),
              DoubleEq(value));
}

TEST_F(TestMaxInRange, returns_init_if_single_value_smaller_than_init) {
  const std::vector<std::string> values = {"one"};
  const auto init = .7;
  const auto value = .3;

  EXPECT_THAT(max_in_range(values.begin(), values.end(), init,
                           [&](const std::string &) { return value; }),
              DoubleEq(init));
}

TEST_F(TestMaxInRange, returns_max_of_two_values) {
  const std::vector<int> values = {3, 7};

  EXPECT_THAT(
      max_in_range(values.begin(), values.end(), double{},
                   [&](const int x) { return static_cast<double>(x) / 10.; }),
      DoubleEq(.7));
}

} // namespace
} // namespace qcmesh::mesh
