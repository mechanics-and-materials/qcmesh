// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/mesh_repair/distopt/change_preview.hpp"
#include "qcmesh/mesh/mesh_repair/distopt/optimize_mesh.hpp"
#include <gmock/gmock.h>
#include <unordered_map>
#include <vector>

// NOLINTBEGIN(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::_;
// NOLINTEND(bugprone-reserved-identifier,cert-dcl37-c,cert-dcl51-cpp)
using testing::Return;
using testing::StrictMock;
using testing::Test;

namespace qcmesh::mesh {
namespace {

enum class CellHandle : std::size_t {};
enum class CellDraft : std::size_t {};
enum class ProcessId : std::size_t {};

using TestCellAddress = CellAddress<ProcessId, CellHandle>;

struct TestMesh {};

bool operator==(const TestMesh &, const TestMesh &) noexcept { return true; }
bool operator==(const TestCellAddress &a, const TestCellAddress &b) noexcept {
  return a.process_id == b.process_id && a.cell_handle == b.cell_handle;
}

struct DriverMock {
  MOCK_METHOD0(run_queue, bool());
  MOCK_METHOD0(notify, void());
  MOCK_METHOD3(is_improvement, bool(const double, const double, const double));
};

struct ComputeResidualMock {
  MOCK_CONST_METHOD1(compute_residual, double(CellHandle));
  double operator()(const CellHandle x) const { return compute_residual(x); }
};

struct GatherBadElementsMock {
  MOCK_CONST_METHOD1(gather_bad_elements,
                     std::vector<CellHandle>(const TestMesh &));
  std::vector<CellHandle> operator()(const TestMesh &mesh) const {
    return gather_bad_elements(mesh);
  }
};

struct ApplyChangeSetMock {
  MOCK_CONST_METHOD2(apply_change_set,
                     void(TestMesh &,
                          const ChangeSet<CellDraft, CellHandle> &));
  void operator()(TestMesh &mesh,
                  const ChangeSet<CellDraft, CellHandle> &change_set) const {
    apply_change_set(mesh, change_set);
  }
};

struct ModifierAMock {
  enum class PrimitiveHandle : std::size_t {};
  enum class DraftType : std::size_t {};

  using Preview = ChangePreview<CellHandle, DraftType>;

  MOCK_CONST_METHOD2(primitives_for_element,
                     std::vector<PrimitiveHandle>(const TestMesh &,
                                                  CellHandle));
  MOCK_CONST_METHOD2(preview,
                     ChangePreview<CellHandle, DraftType>(const TestMesh &,
                                                          PrimitiveHandle));
  MOCK_CONST_METHOD2(preview_to_changeset,
                     ChangeSet<CellDraft, CellHandle>(
                         ChangePreview<CellHandle, DraftType>, TestMesh &));
};

struct ModifierBMock {
  enum class PrimitiveHandle : std::size_t {};
  enum class DraftType : std::size_t {};

  using Preview = ChangePreview<CellHandle, DraftType>;

  MOCK_CONST_METHOD2(primitives_for_element,
                     std::vector<PrimitiveHandle>(const TestMesh &,
                                                  CellHandle));
  MOCK_CONST_METHOD2(preview,
                     ChangePreview<CellHandle, DraftType>(const TestMesh &,
                                                          PrimitiveHandle));
  MOCK_CONST_METHOD2(preview_to_changeset,
                     ChangeSet<CellDraft, CellHandle>(
                         ChangePreview<CellHandle, DraftType>, TestMesh &));
};

struct TestOptimizeLocalMesh : Test {
  StrictMock<DriverMock> driver;
  StrictMock<ComputeResidualMock> compute_residual;
  StrictMock<GatherBadElementsMock> gather_bad_elements;
  StrictMock<ApplyChangeSetMock> apply_change_set;

  TestMesh mesh{};
  std::tuple<> no_modifiers{};
  std::tuple<ModifierAMock> one_modifier{};
  std::tuple<ModifierAMock, ModifierBMock> two_modifiers{};
  ModifierAMock &modifier = std::get<0>(one_modifier);
  ModifierAMock &modifier_a = std::get<0>(two_modifiers);
  ModifierBMock &modifier_b = std::get<1>(two_modifiers);
  std::vector<CellHandle> no_bad_elements{};

  template <class... Modifiers>
  void run_optimize_local_mesh(const std::tuple<Modifiers...> &modifiers) {
    optimize_local_mesh<CellDraft, CellHandle>(
        mesh, std::ref(gather_bad_elements), driver, std::ref(compute_residual),
        std::ref(apply_change_set), modifiers);
  }
};

template <class Modifier>
auto expect_modifier_previews_cell_with_residual(const Modifier &modifier,
                                                 const CellHandle bad_cell,
                                                 const double residual) {
  using PrimitiveHandleType = typename Modifier::PrimitiveHandle;
  const auto single_primitive = std::vector<PrimitiveHandleType>{
      PrimitiveHandleType((std::size_t)bad_cell)};
  EXPECT_CALL(modifier, primitives_for_element(_, bad_cell))
      .WillOnce(Return(single_primitive));
  using DraftType = typename Modifier::DraftType;
  const auto some_draft = DraftType((std::size_t)bad_cell);
  auto preview =
      ChangePreview<CellHandle, DraftType>{{bad_cell}, some_draft, residual};
  EXPECT_CALL(modifier, preview(_, single_primitive.back()))
      .WillOnce(Return(preview));
  return preview;
}

TEST_F(TestOptimizeLocalMesh, succeeds_for_no_bad_elements) {
  EXPECT_CALL(driver, run_queue()).WillOnce(Return(true));
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillRepeatedly(Return(no_bad_elements));

  run_optimize_local_mesh(one_modifier);
}

TEST_F(TestOptimizeLocalMesh,
       succeeds_for_single_local_element_and_no_modifiers) {
  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_elements = std::vector<CellHandle>{CellHandle(7)};
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(bad_elements));

  run_optimize_local_mesh(no_modifiers);
}

TEST_F(TestOptimizeLocalMesh,
       succeeds_for_single_local_element_single_modifier_and_no_primitives) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}));

  const auto no_primitives = std::vector<ModifierAMock::PrimitiveHandle>{};
  const auto &modifier_a = std::get<0>(one_modifier);
  EXPECT_CALL(modifier_a, primitives_for_element(_, bad_element))
      .WillOnce(Return(no_primitives));

  run_optimize_local_mesh(one_modifier);
}

TEST_F(TestOptimizeLocalMesh,
       succeeds_for_single_local_element_two_modifiers_and_no_primitives) {
  const auto &modifier_a = std::get<0>(two_modifiers);
  const auto &modifier_b = std::get<1>(two_modifiers);

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}));

  const auto no_a_primitives = std::vector<ModifierAMock::PrimitiveHandle>{};
  EXPECT_CALL(modifier_a, primitives_for_element(_, bad_element))
      .WillOnce(Return(no_a_primitives));

  const auto no_b_primitives = std::vector<ModifierBMock::PrimitiveHandle>{};
  EXPECT_CALL(modifier_b, primitives_for_element(_, bad_element))
      .WillOnce(Return(no_b_primitives));

  run_optimize_local_mesh(two_modifiers);
}

TEST_F(TestOptimizeLocalMesh,
       does_not_apply_preview_for_single_modifier_if_no_progress) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}));

  const auto worse_residual = .7;
  const auto bad_residual = .3;
  expect_modifier_previews_cell_with_residual(modifier, bad_element,
                                              worse_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(worse_residual, bad_residual, _))
      .WillOnce(Return(false));

  run_optimize_local_mesh(one_modifier);
}

TEST_F(TestOptimizeLocalMesh,
       does_not_apply_preview_for_two_modifiers_if_no_progress) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}));

  const auto worse_residual = .7;
  const auto bad_residual = .3;
  expect_modifier_previews_cell_with_residual(modifier_a, bad_element,
                                              worse_residual);
  expect_modifier_previews_cell_with_residual(modifier_b, bad_element,
                                              worse_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(worse_residual, bad_residual, _))
      .WillRepeatedly(Return(false));

  run_optimize_local_mesh(two_modifiers);
}

TEST_F(TestOptimizeLocalMesh, applies_change_if_progress_for_one_modifier) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}))
      .WillOnce(Return(no_bad_elements));

  const auto bad_residual = .7;
  const auto good_residual = .3;
  const auto preview = expect_modifier_previews_cell_with_residual(
      modifier, bad_element, good_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(good_residual, bad_residual, _))
      .WillOnce(Return(true));

  const auto change_set = ChangeSet<CellDraft, CellHandle>{
      .in = {CellDraft{42}}, .out = {bad_element}};

  EXPECT_CALL(modifier, preview_to_changeset(preview, _))
      .WillOnce(Return(change_set));

  EXPECT_CALL(apply_change_set, apply_change_set(mesh, change_set));

  EXPECT_CALL(driver, notify());

  run_optimize_local_mesh(one_modifier);
}

TEST_F(TestOptimizeLocalMesh,
       applies_change_if_progress_for_1st_of_two_modifiers) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}))
      .WillOnce(Return(no_bad_elements));

  const auto bad_residual = .7;
  const auto good_residual = .3;
  const auto preview = expect_modifier_previews_cell_with_residual(
      modifier_a, bad_element, good_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(good_residual, bad_residual, _))
      .WillOnce(Return(true));

  const auto change_set = ChangeSet<CellDraft, CellHandle>{
      .in = {CellDraft{42}}, .out = {bad_element}};

  EXPECT_CALL(modifier_a, preview_to_changeset(preview, _))
      .WillOnce(Return(change_set));

  EXPECT_CALL(apply_change_set, apply_change_set(mesh, change_set));

  EXPECT_CALL(driver, notify());

  run_optimize_local_mesh(two_modifiers);
}

TEST_F(TestOptimizeLocalMesh,
       applies_change_if_progress_for_2nd_of_two_modifiers) {

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  const auto bad_element = CellHandle(7);
  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element}))
      .WillOnce(Return(no_bad_elements));

  const auto bad_residual = .7;
  const auto good_residual = .3;
  const auto worse_residual = .8;
  expect_modifier_previews_cell_with_residual(modifier_a, bad_element,
                                              worse_residual);

  const auto preview = expect_modifier_previews_cell_with_residual(
      modifier_b, bad_element, good_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(_, bad_residual, _))
      .WillOnce(Return(false))
      .WillOnce(Return(true));

  const auto change_set = ChangeSet<CellDraft, CellHandle>{
      .in = {CellDraft{42}}, .out = {bad_element}};

  EXPECT_CALL(modifier_b, preview_to_changeset(preview, _))
      .WillOnce(Return(change_set));

  EXPECT_CALL(apply_change_set, apply_change_set(mesh, change_set));

  EXPECT_CALL(driver, notify());

  run_optimize_local_mesh(two_modifiers);
}

struct GatherGlobalBadElementsMock {
  MOCK_CONST_METHOD1(gather_bad_elements,
                     std::vector<TestCellAddress>(const TestMesh &));
  std::vector<TestCellAddress> operator()(const TestMesh &mesh) const {
    return gather_bad_elements(mesh);
  }
};

struct ExchangeHalosMock {
  MOCK_CONST_METHOD2(exchange_halos,
                     void(const TestMesh &,
                          const std::vector<TestCellAddress> &));
  void operator()(const TestMesh &mesh,
                  const std::vector<TestCellAddress> &bad_elements) const {
    exchange_halos(mesh, bad_elements);
  }
};

struct ApplyChangeSetDistributedMock {
  MOCK_CONST_METHOD3(apply_change_set,
                     bool(TestMesh &, const ChangeSet<CellDraft, CellHandle> &,
                          const TestCellAddress &));
  bool operator()(TestMesh &mesh,
                  const ChangeSet<CellDraft, CellHandle> &change_set,
                  const TestCellAddress &bad_cell) const {
    return apply_change_set(mesh, change_set, bad_cell);
  }
};

struct TestOptimizeGlobalMesh : Test {
  StrictMock<DriverMock> driver;
  StrictMock<ComputeResidualMock> compute_residual;
  StrictMock<GatherGlobalBadElementsMock> gather_bad_elements;
  StrictMock<ExchangeHalosMock> exchange_halos;
  StrictMock<ApplyChangeSetDistributedMock> apply_change_set;

  TestMesh mesh{};
  std::tuple<> no_modifiers{};
  std::tuple<ModifierAMock> one_modifier{};
  std::tuple<ModifierAMock, ModifierBMock> two_modifiers{};
  ModifierAMock &modifier = std::get<0>(one_modifier);
  ModifierAMock &modifier_a = std::get<0>(two_modifiers);
  ModifierBMock &modifier_b = std::get<1>(two_modifiers);
  std::vector<TestCellAddress> no_bad_elements{};
  ProcessId local_process = ProcessId{0};
  ProcessId remote_process = ProcessId{1};
  CellHandle bad_element = CellHandle{7};

  template <class... Modifiers>
  void run_optimize_global_mesh(const std::tuple<Modifiers...> &modifiers) {
    optimize_global_mesh<CellDraft, CellHandle>(
        mesh, local_process, std::ref(gather_bad_elements),
        std::ref(exchange_halos), driver, std::ref(compute_residual),
        std::ref(apply_change_set), modifiers);
  }
};
TEST_F(TestOptimizeGlobalMesh, succeeds_for_single_remote_element) {
  const auto bad_element_address = TestCellAddress{remote_process, bad_element};

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element_address}))
      .WillOnce(Return(no_bad_elements));
  EXPECT_CALL(apply_change_set, apply_change_set(mesh, _, bad_element_address))
      .WillOnce(Return(true));
  EXPECT_CALL(exchange_halos,
              exchange_halos(mesh, std::vector{bad_element_address}));

  EXPECT_CALL(driver, notify());

  run_optimize_global_mesh(one_modifier);
}

TEST_F(TestOptimizeGlobalMesh,
       applies_change_if_progress_for_2nd_of_two_modifiers) {
  const auto bad_element_address = TestCellAddress{local_process, bad_element};

  EXPECT_CALL(driver, run_queue()).WillRepeatedly(Return(true));

  EXPECT_CALL(gather_bad_elements, gather_bad_elements(mesh))
      .WillOnce(Return(std::vector{bad_element_address}))
      .WillOnce(Return(no_bad_elements));
  EXPECT_CALL(exchange_halos,
              exchange_halos(mesh, std::vector{bad_element_address}));

  const auto bad_residual = .7;
  const auto good_residual = .3;
  const auto worse_residual = .8;
  expect_modifier_previews_cell_with_residual(modifier_a, bad_element,
                                              worse_residual);

  const auto preview = expect_modifier_previews_cell_with_residual(
      modifier_b, bad_element, good_residual);

  EXPECT_CALL(compute_residual, compute_residual(bad_element))
      .WillOnce(Return(bad_residual))
      .WillOnce(Return(bad_residual));

  EXPECT_CALL(driver, is_improvement(_, bad_residual, _))
      .WillOnce(Return(false))
      .WillOnce(Return(true));

  const auto change_set = ChangeSet<CellDraft, CellHandle>{
      .in = {CellDraft{42}}, .out = {bad_element}};

  EXPECT_CALL(modifier_b, preview_to_changeset(preview, _))
      .WillOnce(Return(change_set));

  EXPECT_CALL(apply_change_set,
              apply_change_set(mesh, change_set, bad_element_address))
      .WillOnce(Return(true));

  EXPECT_CALL(driver, notify());

  run_optimize_global_mesh(two_modifiers);
}

} // namespace
} // namespace qcmesh::mesh
