// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/connect_global_cell_faces.hpp"
#include "qcmesh/mesh/connect_local_cell_faces.hpp"
#include "qcmesh/mesh/create_distributed_simplex_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/collect_face_connections.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include <gmock/gmock.h>

namespace qcmesh::mesh {

namespace {

TEST(test_connect_global_cell_faces,
     connect_global_cell_faces_recovers_global_connectivity) {
  const auto world = boost::mpi::communicator{};
  const auto global_mesh = testing::mesh_cell_ring(world.size());
  traits::position(PrimitiveNode<3>{});
  auto mesh = create_distributed_unconnected_simplex_mesh<EmptyData, EmptyData>(
      global_mesh.vertices, global_mesh.cells);
  connect_global_cell_faces(mesh.cells);

  ASSERT_THAT(testing::collect_intrinsic_face_connections(mesh.cells),
              ::testing::ContainerEq(
                  testing::collect_extrinsic_face_connections(mesh.cells)));
}

} // namespace
} // namespace qcmesh::mesh
