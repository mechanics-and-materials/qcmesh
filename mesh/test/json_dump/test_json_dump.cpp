// © 2024 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of qcmesh.
//
// qcmesh is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// qcmesh. If not, see <https://www.gnu.org/licenses/>.

#include "qcmesh/mesh/json_dump/simplex_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/bare_mesh.hpp"
#include "qcmesh/mesh/mesh_repair/testing/test_meshes.hpp"
#include <gmock/gmock.h>
#include <sstream>

namespace qcmesh::json_dump {

namespace {

TEST(test_json_dump, json_dump_works_for_a_mesh) {
  const auto throw_on_degeneracy = true;
  const auto mesh = mesh::testing::create_distributed_lattice_mesh<
      mesh::testing::LatticeCellData>(mesh::testing::mesh_1(),
                                      throw_on_degeneracy);
  auto stream = std::stringstream{};
  stream << as_json(mesh);
  EXPECT_EQ(
      stream.str(),
      "{\"nodes\":[{\"id\":4,\"position\":[-1.0,-0.1,0.0]},{\"id\":3,"
      "\"position\":[-1.0,0.1,0.0]},{\"id\":2,\"position\":[1.0,0.0,0.0]},{"
      "\"id\":1,\"position\":[0.0,0.0,1.0]},{\"id\":0,\"position\":[0.0,0.0,-1."
      "0]}],\"cells\":[{\"id\":2,\"nodes\":[0,1,4,2],\"neighbors\":[null,null,"
      "0,1]},{\"id\":1,\"nodes\":[0,1,3,4],\"neighbors\":[null,null,2,0]},{"
      "\"id\":0,\"nodes\":[0,1,2,3],\"neighbors\":[null,null,1,2]}]}");
}

} // namespace
} // namespace qcmesh::json_dump
