<div align="center">

Core library for the aqcnes Quasi-Continuum Non-Equilibrium Solver

[![pipeline status](
    https://gitlab.ethz.ch/mechanics-and-materials/qcmesh/badges/main/pipeline.svg?ignore_skipped=true)](
    https://gitlab.ethz.ch/mechanics-and-materials/qcmesh/-/commits/main)
[![License](
    https://img.shields.io/badge/License-GPLv3-blue.svg)](
    https://www.gnu.org/licenses/gpl-3.0)
[![code style: clang-format](
    https://img.shields.io/badge/code%20style-clang--format-000000.svg)](
    https://clang.llvm.org/docs/ClangFormat.html)
[![code linter: clang-tidy](
    https://img.shields.io/badge/clang--tidy-checked-blue)](
    https://clang.llvm.org/extra/clang-tidy/)
[![DOI](
    https://img.shields.io/badge/DOI-10.5905%2Fethz--1007--823-blue)](
    https://doi.org/10.5905/ethz-1007-823)


**[Install](#installation)** •
**[Usage](#usage)** •
**[Docs](https://qc.mm.ethz.ch/qcmesh/)**


# qcmesh

</div>

## Summary

The following libraries are provided:

- [qcmesh::array_ops](./array_ops): Arithmetic / linear algebra operations for `std::array`.

  Provides functionality from [Eigen](http://eigen.tuxfamily.org) for
  `std::array` objects.
- [qcmesh::geometry](./geometry): Compute geometric quantities of simplices.
- [qcmesh::mesh](./mesh): Flexible simplex mesh class.
  
  Support for mesh repair (edge removal / multi face removal).
- [qcmesh::mpi](./mpi): Convenience wrappers around [Boost.MPI](https://www.boost.org/doc/libs/release/libs/mpi/)
- [qcmesh::testing](./testing): Some [Gmock
  Matchers](https://google.github.io/googletest/reference/matchers.html)
  for testing.
- [qcmesh::triangulation](./triangulation): Triangulation of point
  data to 2d or 3d meshes.

## Installation

The project uses [CMake](https://cmake.org) as its build system.
The following third party libraries are required and located using CMake's `find_package`.

- [Boost](https://www.boost.org) (component mpi, serialization): version 1.67
- [Eigen](http://eigen.tuxfamily.org): version 3.4
- [Google Test](https://github.com/google/googletest): version 1.8.1
- [CGAL](https://www.cgal.org/): version >=5.4

Of course, these libraries are covered by their own license
terms.

Once you have installed these libraries, run CMake to build the
project, choosing a location to install the library to by specifying
`CMAKE_INSTALL_PREFIX`. Of course,
depending on your setup, you might need to add a
`-DCMAKE_PREFIX_PATH='...'` parameter to tell CMake the location
of the third party library installations.

Example:

```sh
cmake \
    -S . \
    -B build \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS="-Werror -Wall -Wextra" \
    -DCMAKE_INSTALL_PREFIX='install/to/path'
```

At this stage, it is also possible to choose a different
[build type](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html)
than `Release` (e.g. `Debug`, `RelWithDebugInfo`).


Now use `cmake` to build:

```sh
cmake --build build --target all
```

or

```sh
cmake --build build --target install
```

to build and install.

## Usage

To link and use the libraries point CMake to the installation using
`-DCMAKE_PREFIX_PATH='...'` and use CMake's `find_package` to
find the package `qcmesh`. Now link the libraries that you want to
link using CMake's `target_link_libraries`.
The available targets are listed in the [summary](#summary).

## Development container

The library is containerized using
[container/Dockerfile](./container/Dockerfile).
An image is automatically built in [ci](.gitlab-ci.yml) and published
to the [container registry](../container_registry).

### Using the build container

**Method 1:** Using rootless docker 🐋

Rootless docker only simulates root privileges for the user in the
container. Therefore the user in the container has correct permissions
by default.

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work registry.ethz.ch/mechanics-and-materials/qcmesh/qcmesh-dev:main
```

**Method 2:** Using docker 🐋 (specify uid at runtime to correctly set
the owner of files)

Run (and remove it after usage: `--rm`):

```sh
docker run -it --rm -v "$(pwd)":/work -w /work --user $(id --user) registry.ethz.ch/mechanics-and-materials/qcmesh/dev:main
```

**Method 3:** Use [podman](https://podman.io/) 🦭 (specify uid at runtime to correctly set
the owner of files)

Run (and remove it after usage: `--rm`):

```sh
podman run -it --rm -v .:/work -w /work --user $(id --user) --userns=keep-id registry.ethz.ch/mechanics-and-materials/qcmesh/dev:main
```

ℹ Here, podman also allows to mount `.` (you dont need the `"$(pwd)"`).

## Running tests

Run

```sh
CTEST_OUTPUT_ON_FAILURE=1 cmake --build build --target test
```

Specific tests can be skipped / selected by using the [`GTEST_FILTER`](https://google.github.io/googletest/advanced.html#running-a-subset-of-the-tests) environment
variable (see
[.gitlab-ci.yml](.gitlab-ci.yml) for an example).

or
```sh
cd build
ctest [--gtest_filter=...]
```

## Contributors

* Prateek Gupta
* Gerhard Bräunlich
* Manuel Weberndorfer
* Shashank Saxena
* Miguel Spinola
